import gulp from 'gulp';
import gulpif from 'gulp-if';
import awspublish from 'gulp-awspublish';
import awspublishRouter from 'gulp-awspublish-router';
import invalidateCloudfront from 'gulp-invalidate-cloudfront';
import config from './config';
import watch from './semantic/tasks/watch';
import build from './semantic/tasks/build';
import chalk from 'chalk';

const routerConfig = {
  routes: {
    'index.html': {
      cacheTime: 86400,
      gzip: true
    },
    '.*': {
      cacheTime: 630720000,
      gzip: true
    }
  }
};

const invalidationBatch = {
  CallerReference: new Date().toString(),
  Paths: {
    Quantity: 1,
    Items: ['/index.html']
  }
};

gulp.task('default', () => {
  console.log(chalk.cyan(`\nDeploying to "${config.stage}" stage\n`));
  const publisher = awspublish.create(config.S3);
  gulp
    .src('dist/**/*.*')
    .pipe(awspublishRouter(routerConfig))
    .pipe(publisher.publish())
    .pipe(publisher.sync())
    .pipe(publisher.cache())
    .pipe(awspublish.reporter())
    .pipe(
      gulpif(config.stage === 'release', invalidateCloudfront(invalidationBatch, config.cloudfront))
    );
});

gulp.task('watch:semantic', watch);
gulp.task('build:semantic', build);
