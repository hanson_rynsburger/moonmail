# MoonMail Dashboard

## Start the project

- `yarn install` to install all dependencies
- `yarn dev` to run local dev server with staging api
- `yarn dev:release` to run local dev server with production api
- `yarn build` to build static files for staging
- `yarn build:release` to build static files for production

## Code style

It's important to be consistent in code style.   
Use `eslint` and `prettier` to keep your code formated.  
We use our own set of eslint rules.
[Check them here](https://github.com/jimmyn/eslint-config-microapps-react).

Run `yarn lint:fix` to fix code style issues and check for linting errors

## Redux modules

To reduce boilerplate code for common redux logic like CRUD api operations we created a [src/modules/common](src/modules/common)

How to use it:

- add a plop generator globally: `yarn global add plop`
- run `plop module` in project dir
- select a name and required actions
- it will create a module in [src/modules](src/modules) directory
- you should import it in [src/modules/index.js](src/modules/index.js) so redux store can attach the reducer

You'll have all common actions out of the box. Please inspect [src/modules/common](src/modules/common) to see all the actions/selectors/reduces

Check out other modules to see how they could be extended

###IMPORTANT

- if you have a module with name `campaign`, action `fetchCampaigns` will call the api function `fetchCampaigns` in [src/lib/api.js](src/lib/api.js).   
  This function should return a promise resolved with `{items: [...]}`
- if you have an api function with nested resources like

    ```
    fetchAutomationActionReport = (actionId, automationId) =>
      apiClient.get(`automations/${automationId}/actions/${actionId}/report`);
    ```
    
    a module's resource id should always come as a first argument (`actionId` in this case)
    
## Routes

We use declarative routes for react-router, meaning we constract a big routs tree object and then pass it to router provider.

All routes go in [src/routes](src/routes) and should be included in [src/routes/index.js](src/routes/index.js)

You can use `plop route` to generate a basic route folder with files.

### Containers vs Components

- Components or Views are presentational ui parts so they are usually `PureComponents` of functional components in React. They should not contain react lifecycle methods or any state. Just a ui derived from props (there could be some exceptions when you deal with external libs or need refs)

- Containers connect Views with the redux state, call actions in react lifecycle methods, can have internal state and implement most of the business logic that is not in the redux actions

## Mocking the back-end api

To speed up development, if a back-end is not ready yet you can mock the api

We have a [Dinosaur-Mask](https://github.com/microapps/Dinosaur-Mask) for that.
Check the instructions in the repo.

You can use `mockApiClient` from [src/lib/api.js](src/lib/api.js), it will call `http://localhost:3001` in development.   
You need to run Dinosaur-Mask first.
