import config from '../config';
import server from '../server/main';
import chalk from 'chalk';
const port = config.server_port;
const host = config.server_host;

server.listen(port, host, err => {
  if (err) {
    return console.log(err);
  }
  console.log(chalk.cyan(`Starting the development server at http://${host}:${port}.\n`));
});

['SIGINT', 'SIGTERM'].forEach(function(sig) {
  process.on(sig, function() {
    server.close();
    process.exit();
  });
});
