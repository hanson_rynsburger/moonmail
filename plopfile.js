const defaultModuleAction = require('./src/modules/common/constants').DEFAULT_ACTIONS;
module.exports = function(plop) {
  // create your generators here
  plop.setGenerator('module', {
    description: 'redux module for a collection',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Resource name (singular)',
        validate(value) {
          if (/.+/.test(value)) return true;
          return 'name is required';
        }
      },
      {
        type: 'checkbox',
        name: 'actions',
        message: 'Select actions (press Enter to select all)',
        choices: defaultModuleAction.map(action => ({
          name: action,
          value: action
        }))
      }
    ],
    actions: [
      {
        type: 'add',
        path: 'src/modules/{{name}}s/module.js',
        templateFile: 'templates/module/module.hbs'
      },
      {
        type: 'add',
        path: 'src/modules/{{name}}s/actions.js',
        templateFile: 'templates/module/actions.hbs'
      },
      {
        type: 'add',
        path: 'src/modules/{{name}}s/reducer.js',
        templateFile: 'templates/module/reducer.hbs'
      },
      {
        type: 'add',
        path: 'src/modules/{{name}}s/selectors.js',
        templateFile: 'templates/module/selectors.hbs'
      },
      {
        type: 'add',
        path: 'src/modules/{{name}}s/types.js',
        templateFile: 'templates/module/types.hbs'
      }
    ]
  });
  plop.setGenerator('route', {
    description: 'route files',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Route name (capital case)',
        validate(value) {
          if (/.+/.test(value)) return true;
          return 'name is required';
        }
      },
      {
        type: 'input',
        name: 'path',
        message: 'Route path (relative to src/routes, ending with /)'
      }
    ],
    actions: [
      {
        type: 'add',
        path: 'src/routes/{{path}}{{name}}/components/{{name}}View.js'
      },
      {
        type: 'add',
        path: 'src/routes/{{path}}{{name}}/containers/{{name}}Container.js'
      },
      {
        type: 'add',
        path: 'src/routes/{{path}}{{name}}/index.js'
      }
    ]
  });
};
