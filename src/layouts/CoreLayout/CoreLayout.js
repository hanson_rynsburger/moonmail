/* eslint-disable max-len */
import cx from 'classnames';
import Header from 'components/Header';
import Loader from 'components/Loader';
import {NO_LOCALIZE} from 'lib/constants';
import MessagesStack from 'components/MessagesStack';
import StaticMessages, {StaticMessage} from 'components/StaticMessages';
import MarketingMessage from 'components/MarketingMessage';
import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router/lib/Link';
import 'styles/core.scss';
import classNames from './CoreLayout.scss';
import Footer from 'components/Footer';
import ErrorBoundary from 'containers/ErrorBoundary';
import {contactSupport} from 'lib/remoteUtils';
import WelcomeModal from 'components/WelcomeModal';

const CoreLayout = ({
  isLoading,
  isWaiting,
  children,
  isPlain,
  isSesUser,
  hideAlerts,
  hideFooter,
  isBlocked,
  loaderText,
  insecureSender,
  isContactInfoProvided
}) => {
  if (isLoading) return <Loader active inline={false} text={loaderText} />;
  const sectionClass = cx({
    'ui main container': !isPlain,
    [classNames.section]: !isPlain,
    [classNames.plainSection]: isPlain
  });
  return (
    <div className={classNames.container}>
      {!isPlain && <Header />}
      <section className={sectionClass}>
        <StaticMessages className={classNames.alerts} hideAlerts={hideAlerts || isPlain}>
          <MarketingMessage active />
          <StaticMessage
            type="info"
            active={isWaiting && isContactInfoProvided}
            header="Thank you for the upgrade, we have received your payment.">
            Your request is in validation process. We will inform you once your account is ready for
            use. This is a manual process and multiple checks are performed. We try to give you a
            seat as soon as we can, but we can't guarantee lightspeed. If risk department declines
            your request, we will inform you accordingly.
          </StaticMessage>
          <StaticMessage
            type="error"
            icon="warning sign"
            active={isWaiting && !isContactInfoProvided}
            header="Thank you for the upgrade, please provide your contact information to start sending emails.">
            <p>
              We have received your payment, but your account can not be validated without contact
              information. <br />It's crucial for the risk team to allocate you a server to start
              sending emails.
            </p>
            <p>
              <Link to="/profile/contact-info">
                <b>Click here to provide contact information.</b>
              </Link>
            </p>
          </StaticMessage>
          <StaticMessage
            type="warning"
            active={insecureSender.id}
            header={
              <span>
                Please {!insecureSender.domainVerified && 'verify the domain and'} enable DKIM
                signing for{' '}
                <Link className={NO_LOCALIZE} to={`/profile/senders/${insecureSender.id}`}>
                  {insecureSender.emailAddress}
                </Link>{' '}
                sender.{' '}
                <Link to={`/profile/senders/${insecureSender.id}`}>Click here to start</Link>.
              </span>
            }>
            <span>
              {!insecureSender.domainVerified &&
                `This will confirm that you are the owner of a particular
              email address hosted at a domain you can access. `}
              DKIM signing will keep your campaigns out of spam folders and protect your reputation.{' '}
              <a
                href="http://support.moonmail.io/spf-dkim-setup-guides/about-txt-spf-dkim-records/authenticating-email-with-dkim-in-moonmail"
                target="_blank"
                rel="noopener noreferrer">
                Learn more about DKIM
              </a>
            </span>
          </StaticMessage>
          <StaticMessage
            type="error"
            active={isBlocked}
            header="Your account has been suspended because your reputation is less than 15%.">
            <p>
              You are not allowed to send more emails using your actual plan. <br />
              Still interested in using MoonMail?{' '}
              <a href="#" onClick={contactSupport}>
                Contact support
              </a>{' '}
              {isSesUser ? (
                'to see if we can reinstate your account.'
              ) : (
                <span>
                  so you can{' '}
                  <a
                    href={
                      'http://support.moonmail.io/the-platform/aws-ses-connected-accounts' +
                      '/how-do-i-connect-my-amazon-ses-account-with-moonmail'
                    }
                    target="_blank"
                    rel="noopener noreferrer">
                    tie your Amazon SES account with MoonMail
                  </a>.
                </span>
              )}
              <br />
              <a
                href="http://support.moonmail.io/faq/validation/what-are-the-acceptable-complaint-and-bounce-rates"
                target="_blank"
                rel="noopener noreferrer">
                Click here
              </a>{' '}
              to understand the maximum bounce and complaint rates.
              <br />
              <a
                href="http://support.moonmail.io/faq/validation-and-reputation/how-is-my-moonmail-reputation-calculated"
                target="_blank"
                rel="noopener noreferrer">
                Click here
              </a>{' '}
              to better understand how your Account Reputation is calculated.
            </p>
          </StaticMessage>
        </StaticMessages>
        <ErrorBoundary>{children}</ErrorBoundary>
        <WelcomeModal />
      </section>
      <MessagesStack />
      {!hideFooter && !isPlain && <Footer />}
    </div>
  );
};

CoreLayout.propTypes = {
  children: PropTypes.element.isRequired,
  isLoading: PropTypes.bool,
  isWaiting: PropTypes.bool,
  isPlain: PropTypes.bool,
  hideAlerts: PropTypes.bool,
  hideFooter: PropTypes.bool,
  isBlocked: PropTypes.bool,
  isContactInfoProvided: PropTypes.bool,
  isSesUser: PropTypes.bool,
  insecureSender: PropTypes.object,
  loaderText: PropTypes.string
};

export default CoreLayout;
