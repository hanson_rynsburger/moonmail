import {checkAuth} from 'modules/app/actions';
import {getIsLoading, getLoaderText} from 'modules/app/selectors';
import * as selectors from 'modules/profile/selectors';
import sendersSelectors from 'modules/senders/selectors';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import CoreLayoutView from './CoreLayout';

class CoreLayout extends Component {
  static defaultProps = {
    insecureSender: {}
  };

  static propTypes = {
    checkAuth: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.props.checkAuth();
  }

  render() {
    return <CoreLayoutView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  profile: selectors.getProfile(state),
  isSesUser: selectors.getIsSesUser(state),
  isWaiting: selectors.getIsWaiting(state),
  isLoading: getIsLoading(state),
  loaderText: getLoaderText(state),
  isPlain: ownProps.routes && ownProps.routes.some(r => r.isPlain),
  hideAlerts: ownProps.routes && ownProps.routes.some(r => r.hideAlerts),
  hideFooter: ownProps.routes && ownProps.routes.some(r => r.hideFooter),
  isBlocked: selectors.getIsBlocked(state),
  isNewUser: selectors.getIsNewUser(state),
  isContactInfoProvided: selectors.getIsContactInfoProvided(state),
  insecureSender: sendersSelectors.getInsecureSenders(state)[0]
});

export default connect(mapStateToProps, {checkAuth})(CoreLayout);
