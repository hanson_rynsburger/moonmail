import {schema} from 'normalizr';

export const message = new schema.Entity('messages');

export const recipient = new schema.Entity('recipients');
export const arrayOfRecipients = [recipient];
