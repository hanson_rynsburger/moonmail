import 'test/polyfills';

import 'jest-enzyme';
import jsdom from 'jsdom';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter()});

const {JSDOM} = jsdom;

const dom = new JSDOM(`<!doctype html><html><body></body></html>`, {
  beforeParse(window) {
    const jquery = require('jquery')(window);
    global.jQuery = jquery;
    global.$ = jquery;
    //require('../semantic/dist/components/transition');
    //require('../semantic/dist/components/dropdown');
    //require('../semantic/dist/components/popup');
  }
});
const {window} = dom;

function copyProps(src, target) {
  const props = Object.getOwnPropertyNames(src)
    .filter(prop => typeof target[prop] === 'undefined')
    .map(prop => Object.getOwnPropertyDescriptor(src, prop));
  Object.defineProperties(target, props);
}

global.window = window;
global.document = window.document;
global.navigator = {
  userAgent: 'node.js'
};

copyProps(window, global);
