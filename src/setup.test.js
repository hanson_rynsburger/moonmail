import React, {Component} from 'react';
import {shallow, mount} from 'enzyme';

class Fixture extends Component {
  render() {
    return (
      <div>
        <input id="checked" defaultChecked />
      </div>
    );
  }
}

describe('test framework', () => {
  it('should render component without crashing', () => {
    let wrapper = shallow(<Fixture />);
    expect(wrapper.find('#checked').prop('defaultChecked')).toBeTruthy();

    wrapper = mount(<Fixture />);
    expect(wrapper.find('#checked').prop('defaultChecked')).toBeTruthy();
  });
});
