import load from 'loadjs';
import * as profileSelectors from 'modules/profile/selectors';

export const triggerEvent = (event, data = {}) => {
  window.dataLayer = window.dataLayer || [];
  window.dataLayer.push({event, ...data});
};

export const triggerEventAction = (event, data = {}) => {
  return (_, getState) => {
    const state = getState();
    const profile = profileSelectors.getProfile(state);
    const isFreeUser = profileSelectors.getIsFreeUser(state);
    const isSesUser = profileSelectors.getIsSesUser(state);
    const planId = profileSelectors.getPlanId(state);
    const subscriptionType = profileSelectors.getPlanSubscription(state);
    triggerEvent(event, {
      isFreeUser,
      isSesUser,
      planId,
      email: profile.email,
      userId: profile.user_id,
      subscriptionType,
      ...data
    });
  };
};

export const bootIntercom = () => {
  return (_, getState) => {
    const state = getState();
    const intercomProfile = profileSelectors.getIntercomProfile(state);
    const profile = profileSelectors.getProfile(state);
    const isDemoUser = profileSelectors.getIsDemoUser(state);
    if (profile.impersonated || isDemoUser || __DEV__ || __STAGE__ === 'development') return;
    window.Intercom('boot', {
      ...intercomProfile,
      app_id: APP_CONFIG.intercomID,
      app: 'MoonMail v2'
    });
  };
};

export const updateIntercom = (data = {}) => {
  return (_, getState) => {
    const profile = profileSelectors.getIntercomProfile(getState());
    window.Intercom('update', {...profile, ...data});
  };
};

export const contactSupport = (e, message) => {
  e && e.preventDefault();
  window.Intercom('showNewMessage', message);
};

const loadSessionStack = (a, b) => {
  return new Promise((resolve, reject) => {
    const c = window;
    c.SessionStack = a;
    c[a] =
      c[a] ||
      function() {
        c[a].q = c[a].q || [];
        c[a].q.push(arguments);
      };
    c[a].t = b;

    load('https://cdn.sessionstack.com/sessionstack.js', {
      success: resolve,
      fail: reject
    });
  });
};

const getSessionStackKey = () => {
  const {sessionStackKeys} = APP_CONFIG;
  const period = 32 / Object.keys(sessionStackKeys).length;
  const day = new Date().getDate();
  const index = Math.floor(day / period);
  return sessionStackKeys[index];
};

export const bootSessionStack = () => {
  return async (_, getState) => {
    const state = getState();
    const {user_id, data} = profileSelectors.getIntercomProfile(state);
    const profile = profileSelectors.getProfile(state);
    if (profile.impersonated || __DEV__ || __STAGE__ === 'development') return;
    await loadSessionStack('sessionstack', getSessionStackKey());
    window.sessionstack('identify', {
      ...data,
      userId: user_id,
      displayName: profile.name
    });
    window.sessionstack('startRecording', sessionId => {
      if (sessionId) {
        window.Intercom('update', {
          sessionstack_url: `https://app.sessionstack.com/player/#/sessions/${sessionId}`
        });
      }
    });
  };
};

const langTags = {
  en: 'en-US',
  es: 'es-ES'
};
const defaultLangTag = 'en-US';

export const getLangTag = () => {
  if (!window.Localize) return defaultLangTag;
  const lang = window.Localize.getLanguage();
  return langTags[lang] || defaultLangTag;
};
