import S3 from 'aws-sdk/clients/s3';
import storage from 'store';
import {slugify} from 'lib/utils';

export default class S3Bucket {
  constructor({userId, bucketName = APP_CONFIG.userBucket, region = APP_CONFIG.region}) {
    const {AccessKeyId, SecretAccessKey, SessionToken} = storage.get('credentials') || {};
    this.userId = userId;
    this.bucketName = bucketName;
    this.endpoint = `https://s3.${region}.amazonaws.com`;
    this.bucket = new S3({
      params: {Bucket: bucketName},
      accessKeyId: AccessKeyId,
      secretAccessKey: SecretAccessKey,
      sessionToken: SessionToken,
      endpoint: this.endpoint
    });
  }

  uploadFile(file, {customName, onProgress, ...options} = {}) {
    const name = file.name.split('.');
    const ext = name.pop();
    const fileName = `${slugify(customName || name.join('.'))}.${ext}`;
    return new Promise((resolve, reject) => {
      const Key = `${this.userId}/${fileName}`;
      const fileUrl = `${this.endpoint}/${this.bucketName}/${Key}`;
      const params = {
        Key,
        ContentType: file.type,
        Body: file,
        ServerSideEncryption: 'AES256',
        ...options
      };
      this.bucket
        .putObject(params, err => {
          return err ? reject(err) : resolve({fileName, fileUrl});
        })
        .on('httpUploadProgress', progress => {
          if (onProgress) onProgress(progress);
        });
    });
  }

  deleteFile(fileName) {
    return new Promise((resolve, reject) => {
      const Key = `${this.userId}/${fileName}`;
      const params = {Key};
      this.bucket.deleteObject(params, (err, data) => {
        return err ? reject(err) : resolve(data);
      });
    });
  }

  getFileUrl(fileName) {
    return new Promise((resolve, reject) => {
      const Key = `${this.userId}/${fileName}`;
      const params = {Key};
      this.bucket.getSignedUrl('getObject', params, (err, url) => {
        return err ? reject(err) : resolve(url);
      });
    });
  }
}
