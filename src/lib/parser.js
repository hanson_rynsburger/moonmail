import Liquid from 'liquid.js';

export const parse = (html, requireTag = true, tag = 'unsubscribe_url') => {
  return new Promise((resolve, reject) => {
    if (!html) return reject();
    try {
      const template = Liquid.parse(html);
      if (requireTag && !template.root.nodelist.some(n => n.name === tag)) {
        reject(`Please include {{${tag}}}`);
      }
      resolve(template);
    } catch (error) {
      reject(error);
    }
  });
};
