import Validator from 'validatorjs';
import dot from 'dot-object';

export const zipRegex = /^\d{5,6}(?:[-\s]\d{4})?$/;
export const slugRegex = /^[A-Za-z_]+[A-Za-z0-9_]*$/;

// eslint-disable-next-line max-len
export const emailRegEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// eslint-disable-next-line no-control-regex, max-len
export const emailRegEx2 = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)])/;

const messages = Validator.getMessages('en');
messages.required = 'This field is required.';
messages.url = 'The url format is invalid. Forgot http:// ?';
Validator.setMessages('en', messages);

Validator.register('phoneNumber', value => value.match(/^\+\d{10,14}$/), 'Incorrect format');
Validator.register(
  'fromName',
  value => !value.match(/[$%^&*+@~=`\\/";'<>?,]/),
  'Incorrect format, no special characters and commas are allowed'
);
Validator.register(
  'slug',
  value => value.match(slugRegex),
  'Incorrect format. Only letters, numbers and underscores are allowed'
);
Validator.register('email', value => value.match(emailRegEx), 'The email format is invalid');
Validator.register('zip', value => value.match(zipRegex), 'Incorrect Zip / Postal Code format');
Validator.register('timestamp', value => typeof value === 'number', 'Incorrect date format');

export const createValidator = rules => {
  return values => {
    const validator = new Validator(values, rules);
    validator.passes();
    return dot.object(validator.errors.all());
  };
};

export default Validator;
