import moment from 'moment/src/moment';
import humanizeString from 'humanize-string';
import {DATE_FORMAT_SIMPLE, UNSUPPORTED_FILE_EXTENSIONS} from './constants';
import numeral from 'numeral';
import debug from 'debug';

const info = debug('app:utils');

export const formatDate = (
  timestamp,
  {format = DATE_FORMAT_SIMPLE, showTime = false, fromNow = false, unix = true} = {}
) => {
  if (!timestamp) return '';
  const date = unix ? moment.unix(timestamp) : moment(timestamp);
  if (fromNow && moment().diff(date, 'days') < 6) {
    return date.fromNow();
  }
  return date.format(showTime ? format + ' H:mm' : format);
};

export const stringToArray = (string = '') => {
  return string.replace(/ /g, '').split(',');
};

export const mergeArrays = (...arrays) => {
  arrays = arrays.filter(a => a !== undefined);
  return Array.from(new Set([].concat(...arrays)));
};

export const last = (array = []) => (array.length ? array[array.length - 1] : null);

export const pickProps = (object, ...props) =>
  props.reduce((a, x) => {
    if (object.hasOwnProperty(x)) a[x] = object[x];
    return a;
  }, {});

export const omitProps = (object, ...props) => {
  const no = {...object};
  props.forEach(p => delete no[p]);
  return no;
};

export const renameProp = (object, oldProp, newProp) => {
  object[newProp] = object[oldProp];
  delete object[oldProp];
  return object;
};

export const omitInternalProps = object => {
  const no = {...object};
  Object.keys(object).forEach(p => p.startsWith('_') && delete no[p]);
  return no;
};

function* iterator(begin, end, interval = 1) {
  for (let i = begin; i <= end; i += interval) {
    yield i;
  }
}

export const range = (begin, end, interval = 1) => Array.from(iterator(begin, end, interval));

export const padDigits = (number, digits) => {
  return new Array(Math.max(digits - String(number).length + 1, 0)).join('0') + number;
};

export const compact = object => {
  Object.keys(object).forEach(key => {
    if (object[key] === undefined || object[key] === null || object[key] === '') {
      delete object[key];
    }
  });
  return object;
};

export const capitalize = (s = ' ') => s[0].toUpperCase() + s.slice(1);
export const deCapitalize = (s = ' ') => s[0].toLowerCase() + s.slice(1);

export const isEmpty = (object = {}) =>
  object.constructor === Object && Object.keys(object).length === 0;

export const formatPercents = (value, decimals = 2) => {
  if (value <= 0 || isNaN(value)) return 0;
  if (value >= 100) return 100;
  return +value.toFixed(decimals);
};

export const formatRate = (value, decimals) => formatPercents(value * 100, decimals);

export const formatStat = (value, cutoff = 99999) => {
  if (value <= 0 || isNaN(value)) return 0;
  if (value > cutoff) return numeral(value).format('0.0a');
  return numeral(value).format();
};

export const objectToArray = (obj = {}) => {
  return Object.keys(obj).map(k => obj[k]);
};

export const removeFromArray = (arr = [], el) => {
  const index = arr.indexOf(el);
  if (index === -1) return arr;
  return [...arr.slice(0, index), ...arr.slice(index + 1)];
};

export const isObject = val => {
  if (val === null) return false;
  return typeof val === 'object';
};

export const slugify = text => {
  return text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(/[^\w-]+/g, '') // Remove all non-word chars
    .replace(/--+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, ''); // Trim - from end of text
};

export const propsChanged = (obj1, obj2) => {
  return !Object.keys(obj1).every(key => obj1[key] === obj2[key]);
};

export const forEachProp = (obj, cb) => {
  Object.keys(obj).forEach(key => cb(obj[key], key));
};

export const filterProps = (obj, cb) => {
  const result = {};
  forEachProp(obj, (value, key) => {
    if (cb(value, key)) {
      result[key] = value;
    }
  });
  return result;
};

export const mapProps = (obj, cb) => {
  const result = {};
  forEachProp(obj, (value, key) => {
    result[key] = cb(value, key);
  });
  return result;
};

export const diff = (obj1, obj2) => filterProps(obj1, (value, key) => value !== obj2[key]);

export const includesAny = (arr, ...values) => arr.some(value => values.includes(value));

export const copyTextToClipboard = text => {
  const textArea = document.createElement('textarea');
  textArea.style.top = 0;
  textArea.style.left = 0;
  textArea.style.width = '2em';
  textArea.style.height = '2em';
  textArea.style.padding = 0;
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';
  textArea.style.background = 'transparent';
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.select();
  try {
    const successful = document.execCommand('copy');
    const msg = successful ? 'successful' : 'unsuccessful';
    info('Copying text command was ' + msg);
  } catch (err) {
    info('Oops, unable to copy');
  }
  document.body.removeChild(textArea);
};

export const trim = str => str.replace(/^\s+/, '').replace(/\s+$/, '');

export const popupWindow = (url, title, w, h) => {
  const left = screen.width / 2 - w / 2;
  const top = screen.height / 2 - h / 2;
  return window.open(
    url,
    title,
    `toolbar=no, 
    location=no, 
    directories=no, 
    status=no, 
    menubar=no, 
    scrollbars=no, 
    resizable=no, 
    copyhistory=no, 
    width=${w}, 
    height=${h},
    top=${top}, 
    left=${left}`
  );
};

export const getRandomInt = (min, max) => Math.floor(Math.random() * (max - min)) + min;

export const isUnsupportedAttachment = file => {
  const ext = file.name.split('.').pop();
  return UNSUPPORTED_FILE_EXTENSIONS.includes('.' + ext);
};

export const joinComponents = (items, cb, delimiter = ', ') =>
  items.length ? items.map(cb).reduce((a, b) => [a, delimiter, b]) : null;

export const humanize = (str = '', ...args) => humanizeString(str, ...args);

export const Aux = props => props.children;

export const quote = (str = '') => `"${str}"`;

export const delay = delay =>
  new Promise(resolve => {
    setTimeout(resolve, delay);
  });
