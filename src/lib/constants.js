/* eslint max-len: 0 */

export const EMPTY_OBJECT = {};
export const EMPTY_ARRAY = [];

export const SES_REGIONS = [
  {
    value: 'us-east-1',
    name: 'US East (N. Virginia)'
  },
  {
    value: 'us-west-2',
    name: 'US West (Oregon)'
  },
  {
    value: 'eu-west-1',
    name: 'EU (Ireland)'
  }
];

export const DATE_FORMAT_SIMPLE = 'MMM D, YYYY';
export const NO_LOCALIZE = 'no-localize';

export const EXPERT_TYPES = {
  marketing: 'Marketing Expert',
  development: 'Design & Development Expert',
  aws: 'AWS Expert'
};

export const UNSUPPORTED_FILE_EXTENSIONS = [
  '.ade',
  '.adp',
  '.app',
  '.asp',
  '.bas',
  '.bat',
  '.cer',
  '.chm',
  '.cmd',
  '.com',
  '.cpl',
  '.crt',
  '.csh',
  '.der',
  '.exe',
  '.fxp',
  '.gadget',
  '.hlp',
  '.hta',
  '.inf',
  '.ins',
  '.isp',
  '.its',
  '.js',
  '.jse',
  '.ksh',
  '.lib',
  '.lnk',
  '.mad',
  '.maf',
  '.mag',
  '.mam',
  '.maq',
  '.mar',
  '.mas',
  '.mat',
  '.mau',
  '.mav',
  '.maw',
  '.mda',
  '.mdb',
  '.mde',
  '.mdt',
  '.mdw',
  '.mdz',
  '.msc',
  '.msh',
  '.msh1',
  '.msh2',
  '.mshxml',
  '.msh1xml',
  '.msh2xml',
  '.msi',
  '.msp',
  '.mst',
  '.ops',
  '.pcd',
  '.pif',
  '.plg',
  '.prf',
  '.prg',
  '.reg',
  '.scf',
  '.scr',
  '.sct',
  '.shb',
  '.shs',
  '.sys',
  '.ps1',
  '.ps1xml',
  '.ps2',
  '.ps2xml',
  '.psc1',
  '.psc2',
  '.tmp',
  '.url',
  '.vb.',
  'vbe',
  '.vbs',
  '.vps',
  '.vsmacros',
  '.vss',
  '.vst',
  '.vsw',
  '.vxd',
  '.ws',
  '.wsc',
  '.wsf',
  '.wsh',
  '.xnk'
];

export const ATTACHMENTS_LIMIT = 9500000;

export const RECIPIENT_STATUSES = ['subscribed', 'unsubscribed', 'awaitingConfirmation'];

export const RATE_LIMITS = {
  bounceRate: 4.5,
  complaintRate: 0.085
};

export const DEMO_USER_ERROR =
  "You're logged in as a Demo User. You can not modify any data. This account is only for demonstration purposes. Please register to use MoonMail";
