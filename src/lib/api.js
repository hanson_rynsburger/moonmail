import axios from 'axios';
import storage from 'store';
import {omitInternalProps} from 'lib/utils';
import {getIsDemoUser} from 'modules/profile/selectors';
import {DEMO_USER_ERROR} from 'lib/constants';
import {renewAuth} from 'modules/app/auth';
import {isTokenExpired} from 'lib/jwt';

const apiClient = axios.create({
  baseURL: APP_CONFIG.apiBaseURL
});

const auth0Client = axios.create({
  baseURL: APP_CONFIG.auth0.apiBaseURL
});

const miniJsonDbClient = axios.create({
  baseURL: APP_CONFIG.miniJsonDbUrl
});

const rejectWithError = message => () =>
  new Promise((resolve, reject) =>
    reject({
      response: {data: {message}}
    })
  );

// JSON api server
const mockApiClient = axios.create({
  baseURL: APP_CONFIG.mockApiBaseUrl
});

export const addInterceptors = store => {
  const requestInterceptor = async config => {
    // encode url in case of unsupported symbols
    config.url = encodeURI(config.url);

    const isDemoUser = getIsDemoUser(store.getState());
    let token = storage.get('idToken');
    if (isTokenExpired(token)) {
      const authResult = await renewAuth();
      token = authResult.idToken;
    }
    if (isDemoUser && config.method !== 'get') {
      config.adapter = rejectWithError(DEMO_USER_ERROR);
    }
    config.headers['Authorization'] = `Bearer ${token}`;
    return config;
  };
  const responseInterceptor = response => response.data;
  const errorHandler = error => {
    if (error) {
      if (error.response) {
        const errorMessage = error.response.data && error.response.data.message;
        return Promise.reject(errorMessage);
      } else {
        return Promise.reject(error.message);
      }
    }
  };
  apiClient.interceptors.request.use(requestInterceptor, errorHandler);
  auth0Client.interceptors.request.use(requestInterceptor, errorHandler);
  apiClient.interceptors.response.use(responseInterceptor, errorHandler);
  auth0Client.interceptors.response.use(responseInterceptor, errorHandler);
  mockApiClient.interceptors.response.use(responseInterceptor, errorHandler);
  miniJsonDbClient.interceptors.request.use(config => {
    config.url = `${config.url}.json`;
    return config;
  });
  miniJsonDbClient.interceptors.response.use(responseInterceptor, errorHandler);
};

// Lists

export const fetchLists = (params = {}) =>
  apiClient.get('lists', {params: {limit: 250, ...params}});

export const fetchList = (listId, params = {}) => apiClient.get(`lists/${listId}`, {params});

export const updateList = (listId, list) => apiClient.patch(`lists/${listId}`, list);

export const deleteList = listId => apiClient.patch(`lists/${listId}`, {archived: true});

export const createList = list => apiClient.post('lists', list);

export const exportList = listId => apiClient.post(`lists/${listId}/export`, {});

// List Confirmation Email

export const sendTestConfirmationEmail = (listId, params = {}) =>
  apiClient.post(`lists/${listId}/confirmation-email/test`, params);

export const resetConfirmationEmail = listId =>
  apiClient.patch(`lists/${listId}/confirmation-email/reset`);

// Recipients

export const fetchRecipients = (listId, segmentId, params) =>
  apiClient.get(
    segmentId ? `/lists/${listId}/segments/${segmentId}/members` : `lists/${listId}/recipients`,
    {params}
  );

export const fetchRecipient = (listId, recipientId) =>
  apiClient.get(`lists/${listId}/recipients/${recipientId}`);

export const createRecipient = (listId, recipient) =>
  apiClient.post(`lists/${listId}/recipients`, recipient);

export const updateRecipient = (recipientId, listId, data) =>
  apiClient.patch(`lists/${listId}/recipients/${recipientId}`, data);

export const deleteRecipients = (listId, recipientIds = []) =>
  apiClient.delete(`lists/${listId}/recipients`, {
    params: {
      ids: recipientIds.join(',')
    }
  });

export const fetchRecipientsCount = () => apiClient.get(`lists/recipients/count`);

// List Segments and Fields

export const fetchSegments = listId => apiClient.get(`lists/${listId}/segments`);

export const createSegment = (listId, segment) =>
  apiClient.post(`lists/${listId}/segments`, segment);

export const updateSegment = (segmentId, listId, segment) =>
  apiClient.patch(`lists/${listId}/segments/${segmentId}`, segment);

export const deleteSegment = (segmentId, listId) =>
  updateSegment(segmentId, listId, {archived: true});

export const fetchSegmentMembers = (listId, segmentId, params = {}) =>
  apiClient.get(`lists/${listId}/segments/${segmentId}/members`, {params});

// Campaigns

export const fetchCampaigns = (params = {}) => apiClient.get('campaigns', {params});

export const fetchCampaign = (campaignId, params = {}) =>
  apiClient.get(`campaigns/${campaignId}`, {params});

export const fetchCampaignGraphData = (campaignId, {source, ...params}) =>
  apiClient.get(`campaigns/${campaignId}/report/${source}`, {params});

export const updateCampaign = (campaignId, campaign) =>
  apiClient.patch(`campaigns/${campaignId}`, omitInternalProps(campaign));

export const fetchCampaignReport = campaignId => apiClient.get(`campaigns/${campaignId}/report`);

export const fetchCampaignLinks = campaignId => apiClient.get(`campaigns/${campaignId}/links`);

export const deleteCampaign = campaignId => apiClient.delete(`campaigns/${campaignId}`);

export const createCampaign = campaign => apiClient.post('campaigns', campaign);

export const sendCampaign = (campaignId, campaign) =>
  apiClient.post(`campaigns/${campaignId}/send`, omitInternalProps(campaign));

export const testCampaign = (campaignId, data) =>
  apiClient.post(`campaigns/${campaignId}/test`, data);

export const scheduleCampaign = (campaignId, {scheduleAt}) =>
  apiClient.post(`campaigns/${campaignId}/schedule`, {scheduleAt});

export const cancelScheduledCampaign = campaignId =>
  apiClient.delete(`campaigns/${campaignId}/schedule`);

export const duplicateCampaign = campaignId =>
  apiClient.post(`campaigns/${campaignId}/duplicate`, {});

// Automations

export const fetchAutomations = (params = {}) => apiClient.get('automations', {params});

export const fetchAutomation = (automationId, params = {}) =>
  apiClient.get(`automations/${automationId}`, {params});

export const createAutomation = automation => apiClient.post('automations', automation);

export const updateAutomation = (automationId, automation) =>
  apiClient.patch(`automations/${automationId}`, omitInternalProps(automation));

export const deleteAutomation = automationId => apiClient.delete(`automations/${automationId}`);

export const activateAutomation = automationId =>
  apiClient.post(`automations/${automationId}/activate`);

export const pauseAutomation = automationId => apiClient.post(`automations/${automationId}/pause`);

// Automations Actions

export const fetchAutomationActions = (automationId, params = {}) =>
  apiClient.get(`automations/${automationId}/actions`, {params});

export const fetchAutomationAction = (actionId, automationId, params = {}) =>
  apiClient.get(`automations/${automationId}/actions/${actionId}`, {params});

export const updateAutomationAction = (actionId, automationId, action) =>
  apiClient.patch(`automations/${automationId}/actions/${actionId}`, action);

export const fetchAutomationActionReport = (actionId, automationId) =>
  apiClient.get(`automations/${automationId}/actions/${actionId}/report`);

export const fetchAutomationActionLinks = (actionId, automationId) =>
  apiClient.get(`automations/${automationId}/actions/${actionId}/links`);

export const deleteAutomationAction = (actionId, automationId) =>
  apiClient.delete(`automations/${automationId}/actions/${actionId}`);

export const createAutomationAction = (automationId, action) =>
  apiClient.post(`automations/${automationId}/actions`, action);

export const testAutomationAction = (actionId, data) =>
  apiClient.post(`campaigns/${actionId}/test`, data);

export const duplicateAutomationAction = (actionId, automationId) =>
  apiClient.post(`automations/${automationId}/actions/${actionId}/duplicate`, {});

// Senders

export const fetchSenders = (params = {}) =>
  apiClient.get('senders', {params: {limit: 250, ...params}});

export const fetchSender = (senderId, params = {}) =>
  apiClient.get(`senders/${senderId}`, {params});

export const createSender = sender => apiClient.post('senders', sender);

export const updateSender = (senderId, sender) => apiClient.patch(`senders/${senderId}`, sender);

export const deleteSender = senderId => apiClient.delete(`senders/${senderId}`);

export const verifySender = senderId => apiClient.post(`senders/${senderId}/verify`, {});

export const updateSenderStatus = senderId =>
  apiClient.patch(`senders/${senderId}/update_status`, {});

export const generateSenderDkim = senderId =>
  apiClient.post(`senders/${senderId}/generate-dkim`, {});

export const verifySenderDomain = senderId =>
  apiClient.post(`senders/${senderId}/verify-domain`, {});

export const activateSenderDkim = senderId =>
  apiClient.post(`senders/${senderId}/activate-dkim`, {});

// Account

export const fetchAccount = () => apiClient.get('account');

export const fetchCharges = (params = {}) => apiClient.get('account/charges', {params});

export const activateAmazonAccount = amazonCustomerId =>
  apiClient.post('account/activate-amazon-user', {amazonCustomerId});

export const fetchInvoice = (id, params = {}) =>
  apiClient.get(`account/charges`, {params}).then(response => {
    response.data = response.data.find(e => e.id === id);
    return response;
  });

export const updatePlan = ({plan, token, email, clickId, subscriptionType}) =>
  apiClient.post('account/subscription', {plan, token, email, clickId, subscriptionType});

export const generateApiKey = () => apiClient.post('account/api-key');
export const revokeApiKey = () => apiClient.delete('account/api-key');

export const installExtension = (extensionId, params = {}) =>
  apiClient.post(`extensions/${extensionId}/install`, params);

export const uninstallExtension = extensionId =>
  apiClient.post(`extensions/${extensionId}/uninstall`);

export const updateCard = ({token, email}) => apiClient.post('account/card', {token, email});

export const phoneVerificationStart = ({phoneNumber}) =>
  apiClient.post('account/phone-verification/start', {phoneNumber});

export const phoneVerificationCheck = ({phoneNumber, verificationCode}) =>
  apiClient.post('account/phone-verification/check', {phoneNumber, verificationCode});

export const updateSesCredentials = ({apiKey, apiSecret, region}) =>
  apiClient.post('account/ses-credentials', {apiKey, apiSecret, region});

export const updateAccount = data => apiClient.patch('account/', data);

export const updateExpertSettings = expertData => apiClient.patch('account/', {expertData});

export const createAuthorization = ({email}) => apiClient.post('account/authorizations', {email});

export const deleteAuthorization = userId => apiClient.delete(`account/authorizations/${userId}`);

export const impersonateUser = userId =>
  apiClient.get(`account/${userId}/impersonate`, {
    params: {origin: window.location.origin}
  });

export const updateUserMetaData = (userId, data) =>
  auth0Client.patch(`users/${userId}`, {user_metadata: data});

export const fetchTokenInfo = idToken =>
  axios
    .post('https://moonmail.auth0.com/tokeninfo', {id_token: idToken})
    .then(response => response.data);

// Affiliate

export const fetchConversions = (params = {}) =>
  apiClient.get('account/tapfilliate/conversions', {params});

export const fetchPayouts = (params = {}) => apiClient.get('account/tapfilliate/payouts', {params});

// Templates

export const fetchTemplates = (params = {}) => apiClient.get('templates', {params});

export const createTemplate = template => apiClient.post('templates', omitInternalProps(template));

export const fetchTemplate = (templateId, params = {}) =>
  apiClient.get(`templates/${templateId}`, {params});

export const deleteTemplate = templateId => apiClient.delete(`templates/${templateId}`);

export const updateTemplate = (templateId, template) =>
  apiClient.patch(`templates/${templateId}`, omitInternalProps(template));

export const duplicateTemplate = templateId =>
  apiClient.post(`templates/${templateId}/duplicate`, {});

export const fetchEditorToken = () => apiClient.get('templates/token');

export const fetchMarketTemplates = (params = {}) =>
  apiClient.get('marketplace/templates', {params});

export const fetchMarketTemplate = templateId =>
  apiClient.get(`marketplace/templates/${templateId}`);

export const createMarketTemplate = template =>
  apiClient.post('marketplace/templates', omitInternalProps(template));

export const deleteMarketTemplate = templateId =>
  apiClient.patch(`marketplace/templates/${templateId}`, {archived: true});

export const updateMarketTemplate = (templateId, template) =>
  apiClient.patch(`marketplace/templates/${templateId}`, omitInternalProps(template));

export const buyMarketTemplate = (templateId, params = {}) =>
  apiClient.post(`marketplace/templates/${templateId}/buy`, params).then(res => res.template);

export const fetchTemplateTags = () =>
  apiClient.get('marketplace/tags').then(res => ({items: res}));

// Mini JSON DB

export const fetchExtensions = () => miniJsonDbClient.get('extensions');

export const fetchPlans = () =>
  miniJsonDbClient.get('plans').then(({items, $common}) => ({
    items: items.map(item => ({
      ...$common,
      ...item
    }))
  }));

export const fetchMarketingMessages = () => miniJsonDbClient.get('marketing_messages');

export const fetchHints = () => miniJsonDbClient.get('hints');

export const fetchRecipientFields = () => miniJsonDbClient.get('recipient_fields');

// Zapier api
const zapierClient = axios.create({baseURL: 'https://api.zapier.com/v1/'});

export const fetchZapierTemplates = (params = {}) =>
  zapierClient
    .get('zap-templates', {params: {client_id: APP_CONFIG.zapierClientID, ...params}})
    .then(res => ({items: res.data}));

export default apiClient;
