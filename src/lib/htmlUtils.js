/* eslint-disable */
export const footer = `
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td align="center" valign="top" style="padding-top:20px;padding-bottom:20px">
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
          <tbody>
            <tr>
              <td align="center" valign="top" style="color:#666;font-family:'Helvetica Neue',Arial,sans-serif;font-size:13px;line-height:23px;padding-right:20px;padding-bottom:5px;padding-left:20px;text-align:center">
                This email was sent to <a href="mailto:{{recipient_email}}" style="color:rgb(64,64,64)!important" target="_blank" rel="noopener noreferrer">{{recipient_email}}</a><span>&nbsp;|&nbsp;</span>
                <a href="{{unsubscribe_url}}" style="color:rgb(64,64,64)!important" target="_blank" rel="noopener noreferrer">Unsubscribe from this list</a>
                <br />
                <a href="https://moonmail.io/?utm_source=newsletter&utm_medium=moonmail-user&utm_campaign=user-campaigns" target="_blank" rel="noopener noreferrer">
                  <img src="https://static.moonmail.io/moonmail-logo.png" border="0" alt="Email Marketing Powered by MoonMail" title="MoonMail Email Marketing" width="130" height="28" />
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>
`;

export const getAddress = (address = {}) => `
<p style="text-align: center;">
  <b>Our address is:</b></br>
  ${address.company}</br>
  ${address.address} ${address.address2}</br>
  ${address.zipCode} ${address.city}</br>
  ${address.state} ${address.country}</br>
<p>
`;

/* eslint-enable */
