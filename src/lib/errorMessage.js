export default class ErrorMessage extends Error {
  constructor(message) {
    super(message);
    this.name = 'ErrorMessage';
    this.message = message || 'Default Message';
    this.stack = null;
  }
}
