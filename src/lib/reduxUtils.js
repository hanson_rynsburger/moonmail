import snakeCase from 'snake-case';
import {createSelector as _createSelector} from 'reselect';
import {addMessage} from 'modules/messages/actions';
import {EMPTY_ARRAY, EMPTY_OBJECT} from 'lib/constants';
import * as api from 'lib/api';
import {schema, normalize} from 'normalizr';

export const resource = new schema.Entity('resources');
export const arrayOfResources = [resource];

export const getTypeName = actionName => snakeCase(actionName).toUpperCase();

export const createAsyncFlagReducer = (types, actionName, initialState = false) => {
  const typeName = getTypeName(actionName);
  return (state = initialState, action) => {
    switch (action.type) {
      case types[`${typeName}_REQUEST`]:
        return true;
      case types[`${typeName}_SUCCESS`]:
      case types[`${typeName}_FAIL`]:
        return false;
      default:
        return state;
    }
  };
};

export const createStartFlagReducer = (types, actionName) => {
  const typeName = getTypeName(actionName);
  return (state = false, action) => {
    switch (action.type) {
      case types[`${typeName}_START`]:
        return true;
      case types[`${typeName}_SUCCESS`]:
      case types[`${typeName}_CANCEL`]:
        return false;
      default:
        return state;
    }
  };
};

export const getEmptyValue = value => {
  if (JSON.stringify(value) === JSON.stringify({})) {
    return EMPTY_OBJECT;
  }
  if (JSON.stringify(value) === JSON.stringify([])) {
    return EMPTY_ARRAY;
  }
  return value;
};

export const createSelector = _createSelector;
export const createKeySelector = (stateSelector, key, defaultValue) =>
  createSelector(
    stateSelector,
    state => (state[key] !== undefined ? state[key] : getEmptyValue(defaultValue))
  );
export const createStateSelector = key => state => state[key];
export const keySelectorFactory = stateSelector => (...args) =>
  createKeySelector(stateSelector, ...args);

export const normalizeResources = (res, schema = arrayOfResources) => {
  const normalized = normalize(res.items, schema);
  return {
    byId: normalized.entities.resources,
    ids: normalized.result
  };
};

export const normalizeResource = (res, schema = resource) => {
  const normalized = normalize(res, schema);
  return {
    byId: normalized.entities.resources,
    resourceId: normalized.result
  };
};

export const createUpdateAction = (actions, types, actionName) => {
  const typeName = getTypeName(actionName);
  return (resourceId, ...args) => {
    return async dispatch => {
      dispatch({
        type: types[`${typeName}_REQUEST`]
      });
      try {
        const res = await api[actionName](resourceId, ...args);
        dispatch(actions.updateResourceLocally(resourceId, res));
        dispatch({
          type: types[`${typeName}_SUCCESS`]
        });
        return res;
      } catch (error) {
        dispatch({
          type: types[`${typeName}_FAIL`]
        });
        dispatch(addMessage({text: error}));
      }
    };
  };
};
