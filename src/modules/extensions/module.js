import getCollection from 'modules/common';

export default getCollection('extension', {
  include: ['fetchAll', 'update']
});
