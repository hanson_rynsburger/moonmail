import React from 'react';
import moment from 'moment';
import NoLocalize from 'components/NoLocalize';
import {ACTION_TYPES} from './constants';

export const getActionTrigger = (action, listName = 'a list') => {
  const type = ACTION_TYPES[action.type].replace('a list', listName);
  if (!action.delay) return type;
  const delayString = (
    <NoLocalize>{moment.duration(action.delay, 'seconds').humanize()}</NoLocalize>
  );
  return (
    <span>
      {delayString} after {type}
    </span>
  );
};
