import module from './module';
import * as api from 'lib/api';
import types from './types';
import selectors from './selectors';
import {isUnsupportedAttachment} from 'lib/utils';
import {ATTACHMENTS_LIMIT} from 'lib/constants';
import * as profileSelectors from 'modules/profile/selectors';
import {addMessage} from 'modules/messages/actions';
import numeral from 'numeral';
import S3 from 'lib/s3Bucket';

const {actions} = module;

actions.activateAllActions = () => ({
  type: types.ACTIVATE_ALL
});

actions.pauseAllActions = () => ({
  type: types.PAUSE_ALL
});

actions.fetchActionReport = (actionId, automationId) => {
  return async dispatch => {
    dispatch({
      type: types.FETCH_REPORT_REQUEST
    });
    try {
      const res = await api.fetchAutomationActionReport(actionId, automationId);
      dispatch(actions.updateResourceLocally(actionId, res));
      dispatch({
        type: types.FETCH_REPORT_SUCCESS
      });
      return res;
    } catch (error) {
      dispatch({
        type: types.FETCH_REPORT_FAIL
      });
      dispatch(
        addMessage({
          text: error,
          onRetry() {
            dispatch(actions.fetchActionReport(actionId, automationId));
          }
        })
      );
    }
  };
};

actions.fetchActionLinks = (actionId, automationId) => {
  return async dispatch => {
    dispatch({
      type: types.FETCH_LINKS_REQUEST
    });
    try {
      const res = await api.fetchAutomationActionLinks(actionId, automationId);
      dispatch(actions.updateResourceLocally(actionId, res));
      dispatch({
        type: types.FETCH_LINKS_SUCCESS
      });
      return res;
    } catch (error) {
      dispatch({
        type: types.FETCH_LINKS_FAIL
      });
      dispatch(
        addMessage({
          text: error,
          onRetry() {
            dispatch(actions.fetchActionLinks(actionId, automationId));
          }
        })
      );
    }
  };
};

actions.testActionStart = () => ({
  type: types.TEST_START
});

actions.testActionCancel = () => ({
  type: types.TEST_CANCEL
});

actions.testAction = (actionId, data) => {
  return async dispatch => {
    dispatch({
      type: types.TEST_REQUEST
    });
    try {
      await api.testAutomationAction(actionId, data);
      dispatch({
        type: types.TEST_SUCCESS
      });
      dispatch(
        addMessage({
          text: 'Test email was sent',
          style: 'success'
        })
      );
    } catch (error) {
      dispatch({
        type: types.TEST_FAIL
      });
      dispatch(
        addMessage({
          text: error,
          onRetry() {
            dispatch(actions.testAction(actionId, data));
          }
        })
      );
    }
  };
};

actions.attachFile = (actionId, automationId, file) => async (dispatch, getState) => {
  dispatch({type: types.ATTACH_FILE_REQUEST});
  try {
    if (isUnsupportedAttachment(file)) {
      throw 'Unsupported extension';
    }
    const state = getState();
    const userId = profileSelectors.getUserId(state);
    const bucket = new S3({userId});
    const attachments = selectors.getAttachments(state);
    const totalSize = attachments.reduce((a, b) => a + b.size, 0);
    if (totalSize + file.size >= ATTACHMENTS_LIMIT) {
      throw `Maximum email size including files could not exceed ${numeral(
        ATTACHMENTS_LIMIT
      ).format('0 b')}`;
    }
    const {fileName, fileUrl} = await bucket.uploadFile(file);
    await dispatch(
      actions.updateResource(
        actionId,
        automationId,
        {
          id: automationId,
          campaign: {
            attachments: [...attachments, {name: fileName, size: file.size, url: fileUrl}]
          }
        },
        true
      )
    );

    dispatch({type: types.ATTACH_FILE_SUCCESS});
  } catch (error) {
    dispatch({type: types.ATTACH_FILE_FAIL});
    dispatch(addMessage({text: error}));
  }
};

actions.detachFile = (actionId, automationId, fileName) => async (dispatch, getState) => {
  dispatch({
    type: types.DETACH_FILE_REQUEST,
    fileName
  });
  try {
    const state = getState();
    const userId = profileSelectors.getUserId(state);
    const bucket = new S3({userId});
    const attachments = selectors.getAttachments(state);

    await bucket.deleteFile(fileName);
    await dispatch(
      actions.updateResource(
        actionId,
        automationId,
        {
          id: automationId,
          campaign: {
            attachments: attachments.filter(a => a.name !== fileName)
          }
        },
        true
      )
    );

    dispatch({type: types.DETACH_FILE_SUCCESS});
  } catch (error) {
    dispatch({type: types.DETACH_FILE_FAIL});
    dispatch(addMessage({text: error}));
  }
};

export default actions;
