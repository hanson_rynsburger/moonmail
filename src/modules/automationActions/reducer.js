import module from './module';
import types from './types';
import {combineReducers} from 'redux';
import {createAsyncFlagReducer, createStartFlagReducer} from 'lib/reduxUtils';

const {reducers, key} = module;

reducers.isFetchingLinks = createAsyncFlagReducer(types, 'fetchLinks');
reducers.isTesting = createAsyncFlagReducer(types, 'test');
reducers.isTestStarted = createStartFlagReducer(types, 'test');
reducers.byId = (state = {}, action) => {
  let stateCopy;
  switch (action.type) {
    case types.FETCH_ALL_SUCCESS:
      return {...action.byId};
    case types.CREATE_SUCCESS:
      return {
        ...state,
        [action.resourceId]: {
          ...action.byId[action.resourceId],
          _isUpToDate: true
        }
      };
    case types.FETCH_SUCCESS:
      return {
        ...state,
        [action.resourceId]: {
          ...state[action.resourceId],
          ...action.byId[action.resourceId],
          _isUpToDate: true
        }
      };
    case types.UPDATE_SUCCESS:
      return {
        ...state,
        [action.resourceId]: {
          ...state[action.resourceId],
          ...action.byId[action.resourceId]
        }
      };
    case types.ACTIVATE_ALL:
      stateCopy = {...state};
      Object.keys(stateCopy).map(key => {
        stateCopy[key].status = 'active';
      });
      return stateCopy;
    case types.PAUSE_ALL:
      stateCopy = {...state};
      Object.keys(stateCopy).map(key => {
        stateCopy[key].status = 'paused';
      });
      return stateCopy;
    case types.DELETE_SUCCESS:
      stateCopy = {...state};
      delete stateCopy[action.resourceId];
      return stateCopy;
    case types.CLEAR_ALL:
      return {};
    default:
      return state;
  }
};

reducers.isAttachingFile = createAsyncFlagReducer(types, 'attachFile');
reducers.isDetachingFile = createAsyncFlagReducer(types, 'detachFile');
reducers.detachingFileName = (state = null, action) => {
  switch (action.type) {
    case types.DETACH_FILE_REQUEST:
      return action.fileName;
    case types.DETACH_FILE_SUCCESS:
    case types.DETACH_FILE_FAIL:
      return null;
    default:
      return state;
  }
};

export default {
  key,
  reducer: combineReducers(reducers)
};
