import module from './module';
import {keySelectorFactory, createSelector, createKeySelector} from 'lib/reduxUtils';

const {selectors} = module;
const keySelector = keySelectorFactory(selectors.stateSelector);
const getActiveActionCampaign = createSelector(
  selectors.getActiveResource,
  action => action.campaign || {}
);

selectors.getIsFetchingLinks = keySelector('isFetchingLinks');
selectors.getIsTesting = keySelector('isTesting');
selectors.getIsTestStarted = keySelector('isTestStarted');
selectors.getIsEditable = createSelector(
  selectors.getActiveResource,
  action => action.status !== 'active'
);

selectors.getIsActive = createSelector(
  selectors.getActiveResource,
  action => action.status === 'active'
);

selectors.getActionLinks = createSelector(selectors.getActiveResource, ({links = {}}) =>
  Object.keys(links).map(id => links[id])
);

selectors.getSortedActionLinks = createSelector(selectors.getActionLinks, links =>
  [...links].sort((l1, l2) => (l2.clicksCount || 0) - (l1.clicksCount || 0))
);

selectors.getAttachments = createKeySelector(getActiveActionCampaign, 'attachments', []);
selectors.getIsAttachingFile = keySelector('isAttachingFile', false);
selectors.getIsDetachingFile = keySelector('isDetachingFile', false);
selectors.getDetachingFileName = keySelector('detachingFileName', null);

export default selectors;
