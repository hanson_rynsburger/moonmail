import * as types from './types';
import {combineReducers} from 'redux';

export const key = 'route';

const query = (state = {}, action) => {
  switch (action.type) {
    case types.UPDATE_LOCATION_WITH_PARAMS:
      return action.query;
    default:
      return state;
  }
};

const params = (state = {}, action) => {
  switch (action.type) {
    case types.UPDATE_LOCATION_WITH_PARAMS:
      return action.params;
    default:
      return state;
  }
};

export const reducer = combineReducers({
  query,
  params
});

export default {key, reducer};
