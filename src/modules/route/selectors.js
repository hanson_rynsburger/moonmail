import {keySelectorFactory, createStateSelector} from 'lib/reduxUtils';
import {key} from './reducer';

const routeSelector = createStateSelector(key);
const keySelector = keySelectorFactory(routeSelector);

export const getRouteParams = keySelector('params');
export const getRouteQuery = keySelector('query');
