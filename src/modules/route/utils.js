import matchRoutes from 'react-router/lib/matchRoutes';
import {createRoutes} from 'react-router';
import * as actions from './actions';

export function syncRouteParams(store, routes, history) {
  const routesArray = createRoutes(routes);
  const syncRoute = location => {
    matchRoutes(routesArray, location, (error, state = {}) => {
      if (error) return;
      store.dispatch(
        actions.updateLocation({
          query: location.query,
          params: state.params
        })
      );
    });
  };
  const location = history.getCurrentLocation();
  syncRoute(location);
  return history.listen(syncRoute);
}
