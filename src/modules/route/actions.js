import * as types from './types';

export const updateLocation = ({query = {}, params = {}}) => ({
  type: types.UPDATE_LOCATION_WITH_PARAMS,
  query,
  params
});
