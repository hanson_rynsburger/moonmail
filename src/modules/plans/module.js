import getCollection from 'modules/common';

export default getCollection('plan', {
  include: ['fetchAll', 'select']
});
