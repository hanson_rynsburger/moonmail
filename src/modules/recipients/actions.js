import module from './module';
import selectors from './selectors';
import types from './types';
import segmentSelectors from 'modules/segments/selectors';
import {delay} from 'lib/utils';
import listsActions from 'modules/lists/actions';
import * as api from 'lib/api';
import {addMessage} from 'modules/messages/actions';

const {actions} = module;

actions.selectAllRecipients = isSelected => (dispatch, getState) => {
  dispatch({
    type: types.SELECT_ALL_RECIPIENTS,
    recipientIds: isSelected ? selectors.getIds(getState()) : []
  });
};

actions.selectRecipient = recipientId => ({
  type: types.SELECT_RECIPIENT,
  recipientId
});

actions.createRecipient = (listId, data) => {
  return async dispatch => {
    const recipient = await dispatch(actions.createResource(listId, data));
    setTimeout(() => {
      dispatch(listsActions.fetchList(listId, {}, true));
    }, 1000);
    return recipient;
  };
};

actions.deleteRecipientsStart = () => ({
  type: types.DELETE_RECIPIENTS_START
});

actions.deleteRecipientsCancel = () => ({
  type: types.DELETE_RECIPIENTS_CANCEL
});

actions.deleteRecipients = (listId, recipientIds) => {
  return async (dispatch, getState) => {
    dispatch({
      type: types.DELETE_RECIPIENTS_REQUEST
    });
    try {
      await api.deleteRecipients(listId, recipientIds);
      await delay(1000);
      const state = getState();
      const {status, from, size} = selectors.getParams(state);
      const segmentId = segmentSelectors.getSelectedId(state);
      dispatch(actions.fetchRecipients(listId, segmentId, {status, from, size}, true));
      dispatch(listsActions.fetchList(listId, {}, true));
      dispatch({
        type: types.DELETE_RECIPIENTS_SUCCESS
      });
      dispatch(
        addMessage({
          text: 'Subscribers have been deleted from a list',
          style: 'success'
        })
      );
    } catch (error) {
      dispatch({
        type: types.DELETE_RECIPIENTS_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export default actions;
