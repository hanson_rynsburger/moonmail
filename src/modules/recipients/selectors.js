import module from './module';
import {keySelectorFactory, createSelector} from 'lib/reduxUtils';

const {selectors} = module;
const keySelector = keySelectorFactory(selectors.stateSelector);

selectors.getSelectedIds = keySelector('selectedIds');

selectors.getRecipients = createSelector(
  selectors.getResourcesMap,
  selectors.getIds,
  selectors.getSelectedIds,
  (byId, ids, selectedIds) =>
    ids.map(id => {
      const r = byId[id];
      r.isSelected = selectedIds.indexOf(id) >= 0;
      return r;
    })
);

selectors.getIsDeleteStarted = keySelector('isDeleteStarted');
selectors.getIsDeleting = keySelector('isDeleting');

export default selectors;
