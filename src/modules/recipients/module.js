import getCollection from 'modules/common';

export default getCollection('recipient', {
  customName: 'Subscriber',
  include: ['fetchAll', 'fetch', 'create', 'update']
});
