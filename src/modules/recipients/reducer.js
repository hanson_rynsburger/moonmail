import module from './module';
import {combineReducers} from 'redux';
import types from './types';
import {EMPTY_ARRAY} from 'lib/constants';
import {createStartFlagReducer, createAsyncFlagReducer} from 'lib/reduxUtils';

const {reducers, key} = module;

const asyncFlagReducer = actionName => createAsyncFlagReducer(types, actionName);
const startFlagReducer = actionName => createStartFlagReducer(types, actionName);

reducers.selectedIds = (state = EMPTY_ARRAY, action) => {
  let index;
  switch (action.type) {
    case types.SELECT_ALL_RECIPIENTS:
      return action.recipientIds;
    case types.SELECT_RECIPIENT:
      index = state.indexOf(action.recipientId);
      if (index < 0) return [...state, action.recipientId];
      return [...state.slice(0, index), ...state.slice(index + 1)];
    case types.CLEAR_ALL:
    case types.DELETE_RECIPIENTS_SUCCESS:
      return EMPTY_ARRAY;
    default:
      return state;
  }
};

reducers.isDeleting = asyncFlagReducer('deleteRecipients');
reducers.isDeleteStarted = startFlagReducer('deleteRecipients');

export default {
  key,
  reducer: combineReducers(reducers)
};
