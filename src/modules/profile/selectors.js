import {createSelector, keySelectorFactory, createStateSelector} from 'lib/reduxUtils';
import {EMPTY_OBJECT, EMPTY_ARRAY} from 'lib/constants';
import {key} from './reducer';
import {isEmpty} from 'lib/utils';
import Validator from 'lib/validator';
import base64 from 'base64-url';
import plansSelectors from 'modules/plans/selectors';

const getPlansMap = plansSelectors.getResourcesMap;

const profileSelector = createStateSelector(key);
const keySelector = keySelectorFactory(profileSelector);

export const getIsFetching = createSelector(
  profileSelector,
  profile => profile.isFetching && !profile.data.id
);

export const getIsUpdating = keySelector('isUpdating', false);
export const getProfile = keySelector('data');
const profileKeySelector = keySelectorFactory(getProfile);

export const getPaymentMethod = createSelector(
  getProfile,
  data => (isEmpty(data.paymentMethod) ? undefined : data.paymentMethod)
);

export const getAddress = profileKeySelector('address', {});
export const getExpertData = profileKeySelector('expertData', {});
export const getUserId = profileKeySelector('user_id');
export const getIsPhoneVerified = profileKeySelector('phoneVerified', false);
export const getIsDemoUser = profileKeySelector('isDemoUser', false);
export const getIsTemplateManager = profileKeySelector('isTemplateManager', false);
export const getIsNewUser = profileKeySelector('isNew', false);
export const getLoginsCount = profileKeySelector('logins', 0);
export const getExtensions = profileKeySelector('installedExtensionIds', []);
export const getTemplates = profileKeySelector('installedTemplates', []);
export const getReputationData = profileKeySelector('reputationData', {});
export const getApiKey = profileKeySelector('apiKey');
export const getIsAmazonUser = createSelector(getProfile, data => !!data.amazonCustomerId);

export const getIsSesUser = createSelector(
  getProfile,
  data => data.isSesUser || !!data.amazonCustomerId
);

export const getIsExpert = createSelector(getExpertData, data => data.isExpert);

export const getPlanId = createSelector(getProfile, getIsSesUser, (data, isSesUser) => {
  if (!data.plan || data.plan === 'free') {
    return isSesUser ? 'free_ses' : 'free';
  }
  return data.plan;
});

export const getPlan = createSelector(
  getPlanId,
  getPlansMap,
  (planId, plans) => plans[planId] || EMPTY_OBJECT
);

export const getAvailablePlans = createSelector(
  plansSelectors.getResourcesMap,
  getIsSesUser,
  (plansMap, isSesUser) => {
    const keys = ['free', 'paid', 'pro', 'enterprise'];
    const availablePlans = {};
    keys.forEach(key => {
      availablePlans[key] = plansMap[isSesUser ? `${key}_ses` : key] || {};
    });
    return availablePlans;
  }
);

export const getIsFreeUser = createSelector(getPlanId, planId => planId.startsWith('free'));

export const getIsEnterpriseUser = createSelector(getPlanId, planId =>
  planId.includes('enterprise')
);

export const getIsStaffUser = createSelector(getPlanId, planId => planId === 'staff');

export const getIsFreeMessUser = createSelector(
  getIsFreeUser,
  getIsSesUser,
  (isFreeUser, isSesUser) => isFreeUser && !isSesUser
);

export const getIsPaidUser = createSelector(
  getIsFreeUser,
  getIsStaffUser,
  (isFreeUser, isStaffUser) => !isFreeUser || isStaffUser
);

export const getIsPaidMessUser = createSelector(
  getIsPaidUser,
  getIsSesUser,
  getIsStaffUser,
  (isPaidUser, isSesUser, isStaffUser) => isPaidUser && !isSesUser && !isStaffUser
);

export const getIsPaidSesUser = createSelector(
  getIsPaidUser,
  getIsSesUser,
  getIsStaffUser,
  (isPaidUser, isSesUser, isStaffUser) => isPaidUser && isSesUser && !isStaffUser
);

export const getIsFreeSesUser = createSelector(
  getIsFreeUser,
  getIsSesUser,
  (isFreeUser, isSesUser) => isFreeUser && isSesUser
);

export const getUserIdBase64 = createSelector(getUserId, id => id && base64.encode(id));

export const getIsWaiting = createSelector(
  getIsPaidMessUser,
  getProfile,
  (isPaidMessUser, data) => isPaidMessUser && !data.approved
);

export const getIsApproved = createSelector(
  getIsPaidMessUser,
  getProfile,
  (isPaidMessUser, data) => isPaidMessUser && data.approved
);

export const getIsContactInfoProvided = createSelector(getAddress, data => {
  const rules = {
    company: 'required',
    websiteUrl: 'required',
    address: 'required',
    city: 'required',
    state: 'required',
    country: 'required',
    zipCode: 'required'
  };

  return new Validator(data, rules).passes();
});

export const getIsSesVerified = createSelector(getProfile, data => !isEmpty(data.ses));

export const getIsBlocked = createSelector(
  getReputationData,
  ({minimumAllowedReputation, reputation}) => reputation < minimumAllowedReputation
);

export const getIsUnverifiedSesUser = createSelector(
  getIsSesUser,
  getIsSesVerified,
  (isSesUser, isSesVerified) => {
    return isSesUser && !isSesVerified;
  }
);

export const getIsLimited = createSelector(
  getIsWaiting,
  getIsUnverifiedSesUser,
  (isWaiting, isUnverifiedSesUser) => {
    return isWaiting || isUnverifiedSesUser;
  }
);

export const getIsVerified = createSelector(
  getIsPhoneVerified,
  getIsContactInfoProvided,
  (isPhoneVerified, isContactInfoProvided) => isPhoneVerified && isContactInfoProvided
);

export const getIsAllowedToSend = createSelector(
  getIsLimited,
  getIsVerified,
  (isLimited, isVerified) => !isLimited && isVerified
);

export const getMetaData = createSelector(getProfile, data => data.user_metadata || EMPTY_OBJECT);

export const getPlanSubscription = createSelector(
  getMetaData,
  metaData => metaData.planSubscription
);

export const getHasAnnualSubscription = createSelector(
  getPlanSubscription,
  subscription => subscription === 'annual'
);

export const getImpersonations = createSelector(
  getProfile,
  data => data.impersonations || EMPTY_ARRAY
);

export const getHasSenders = createSelector(
  getIsApproved,
  getIsSesUser,
  getIsStaffUser,
  (isApproved, isSesUser, isStaffUser) => {
    return isApproved || isSesUser || isStaffUser;
  }
);

export const getLimits = createSelector(getPlan, plan => plan.limits || {});

export const getPricingRate = createSelector(
  getProfile,
  ({pricingRate, reputationData: {reputation = 0} = {}}) => {
    if (pricingRate) return pricingRate / 100;
    if (reputation <= 35) return 1.49;
    if (reputation <= 50) return 1.29;
    if (reputation <= 65) return 0.99;
    if (reputation <= 80) return 0.79;
    if (reputation <= 95) return 0.5;
    return 0.2;
  }
);

export const getIsHintDisabled = createSelector(getMetaData, meta => meta.isHintsDisabled);

export const getIsCancelled = createSelector(getMetaData, meta => meta.isUserAccountCanceled);

export const getIsNewFreeUser = createSelector(
  getLoginsCount,
  getIsFreeUser,
  (loginsCount, isFreeUser) => isFreeUser && loginsCount < 2
);

export const getIsShopifyUser = createSelector(getMetaData, data => data.isShopifyUser === 'true');

export const getIntercomProfile = createSelector(
  getProfile,
  getPlanId,
  getPlanSubscription,
  getIsSesUser,
  getIsUnverifiedSesUser,
  getIsAmazonUser,
  getIsShopifyUser,
  getIsExpert,
  getIsCancelled,
  getAddress,
  getReputationData,
  (
    profile,
    plan,
    subscription,
    is_ses_user,
    is_unverified_ses_user,
    is_amazon_user,
    is_shopify_user,
    is_expert,
    is_canceled,
    address,
    rep
  ) => {
    return {
      user_id: `MMv2|${profile.user_id}`,
      email: profile.email,
      name: profile.name,
      avatar: profile.picture,
      gender: profile.gender,
      locale: profile.locale,
      nickname: profile.nickname,
      phone_number: profile.phoneNumber,
      phone_verified: profile.phoneVerified,
      country: address.country,
      zip_code: address.zipCode,
      address: address.address,
      address2: address.address2,
      city: address.city,
      website_url: address.websiteUrl,
      company: address.company,
      state: address.state,
      vat_number: profile.vat,
      affiliate_id: profile.affiliateId,
      reputation: rep.reputation,
      sent_campaigns: rep.sentCampaigns,
      sent_emails: rep.sentEmails,
      logins_count: profile.logins,
      isSesUser: is_ses_user,
      plan,
      subscription,
      is_unverified_ses_user,
      is_amazon_user,
      is_shopify_user,
      is_expert,
      is_canceled
    };
  }
);
