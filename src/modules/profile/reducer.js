import * as types from './types';
import {createAsyncFlagReducer} from 'lib/reduxUtils';
import {UPDATE_EXPERT_SETTINGS_SUCCESS} from 'routes/Profile/routes/Experts/modules/types';
import {combineReducers} from 'redux';

export const key = 'profile';

const isFetching = createAsyncFlagReducer(types, 'fetchProfile', true);
const isUpdating = createAsyncFlagReducer(types, 'updateProfile', true);

const data = (state = {}, action) => {
  switch (action.type) {
    case types.UPDATE_PROFILE_SUCCESS:
    case types.FETCH_PROFILE_SUCCESS:
      return {...state, ...action.data};
    case UPDATE_EXPERT_SETTINGS_SUCCESS:
      return {...state, expertData: {...state.expertData, ...action.data}};
    default:
      return state;
  }
};

export const reducer = combineReducers({
  isFetching,
  isUpdating,
  data
});

export default {key, reducer};
