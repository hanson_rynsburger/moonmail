import * as api from 'lib/api';
import * as types from './types';
import {getProfile, getPlanId} from 'modules/profile/selectors';
import {updateIntercom, triggerEvent} from 'lib/remoteUtils';
import {addMessage} from 'modules/messages/actions';
import plansSelectors from 'modules/plans/selectors';
import sendersActions from 'modules/senders/actions';
import {updateUserMetaData} from 'routes/Profile/routes/Settings/modules/actions';

export const fetchProfile = () => {
  return async dispatch => {
    dispatch({
      type: types.FETCH_PROFILE_REQUEST
    });
    try {
      const data = await api.fetchAccount();
      dispatch({
        type: types.FETCH_PROFILE_SUCCESS,
        data
      });
      return data;
    } catch (error) {
      dispatch({
        type: types.FETCH_PROFILE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const updateProfileLocally = (data = {}) => {
  if (data.app_metadata) Object.assign(data, data.app_metadata);
  return dispatch => {
    dispatch({
      type: types.UPDATE_PROFILE_SUCCESS,
      data
    });
  };
};

export const updateProfile = (data, successMsg) => {
  return async dispatch => {
    dispatch({
      type: types.UPDATE_PROFILE_REQUEST
    });
    try {
      await api.updateAccount(data);
      dispatch(updateProfileLocally(data));
      dispatch(
        addMessage({
          text: successMsg || 'Your profile was successfully updated',
          style: 'success'
        })
      );
      dispatch(updateIntercom());
    } catch (error) {
      dispatch({
        type: types.UPDATE_PROFILE_FAIL
      });
      error && dispatch(addMessage({text: error}));
    }
  };
};

export const updatePlan = (planId, {token, subscriptionType} = {}) => {
  return async (dispatch, getState) => {
    const state = getState();
    const {email, clickId, user_id} = getProfile(state);
    const oldPlanId = getPlanId(state);

    dispatch({
      type: types.UPDATE_PROFILE_REQUEST
    });

    try {
      const profile = await api.updatePlan({plan: planId, token, email, clickId, subscriptionType});
      const planResource = plansSelectors.getResourceById(getState(), planId);
      dispatch(updateProfileLocally(profile));
      dispatch(updateIntercom());
      dispatch(sendersActions.fetchSenders({}, true));
      dispatch(
        updateUserMetaData(
          {
            planId,
            planSubscription: subscriptionType
          },
          true
        )
      );
      dispatch(
        addMessage({
          text: `Your plan was successfully changed to ${planResource.title} plan`,
          style: 'success'
        })
      );
      triggerEvent('app.userChangedPlan', {
        userId: user_id,
        oldPlanId,
        planId: planResource.id,
        price: planResource.price,
        subscriptionType
      });
    } catch (error) {
      dispatch({
        type: types.UPDATE_PROFILE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const installExtension = (extension, token) => {
  return async (dispatch, getState) => {
    const profile = getProfile(getState());
    const {email, clickId} = profile;

    dispatch({
      type: types.UPDATE_PROFILE_REQUEST
    });

    try {
      const installedExtensionIds = await api.installExtension(extension.id, {
        extensionName: extension.name,
        token,
        email,
        clickId
      });
      dispatch(updateProfileLocally({installedExtensionIds}));
      dispatch(
        addMessage({
          text: `You have successfully installed "${extension.name}"`,
          style: 'success'
        })
      );
      triggerEvent('app.userInstalledExtension', {
        userId: profile.user_id,
        extensionId: extension.id,
        price: extension.price
      });
    } catch (error) {
      dispatch({
        type: types.UPDATE_PROFILE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const uninstallExtension = extension => {
  return async (dispatch, getState) => {
    const profile = getProfile(getState());

    dispatch({
      type: types.UPDATE_PROFILE_REQUEST
    });

    try {
      const installedExtensionIds = await api.uninstallExtension(extension.id);
      dispatch(updateProfileLocally({installedExtensionIds}));
      dispatch(
        addMessage({
          text: `You have successfully removed extension "${extension.name}"`,
          style: 'success'
        })
      );
      triggerEvent('app.userUninstalledExtension', {
        userId: profile.user_id,
        extensionId: extension.id
      });
    } catch (error) {
      dispatch({
        type: types.UPDATE_PROFILE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const activateAmazonAccount = amazonCustomerId => {
  return async dispatch => {
    dispatch({
      type: types.UPDATE_PROFILE_REQUEST
    });

    try {
      const profile = await api.activateAmazonAccount(amazonCustomerId);
      dispatch(updateProfileLocally(profile));
      dispatch(
        addMessage({
          text: `Your account had been successfully activated`,
          style: 'success'
        })
      );
      triggerEvent('app.userActivatedAmazonAccount', {
        userId: profile.user_id,
        amazonCustomerId
      });
      dispatch(updateIntercom());
    } catch (error) {
      dispatch({
        type: types.UPDATE_PROFILE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};
