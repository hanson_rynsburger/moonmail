import {combineReducers} from 'redux';
import {EMPTY_OBJECT, EMPTY_ARRAY} from 'lib/constants';
import module from './module';
import types from './types';

const {reducers, key} = module;

reducers.isExpanded = (state = false, action) => {
  switch (action.type) {
    case types.DELETE_SUCCESS:
      return action.resourceId !== 'new' ? false : state;
    case types.TOGGLE_EXPANDED:
      return typeof action.value === 'boolean' ? action.value : !state;
    default:
      return state;
  }
};

reducers.recipientFieldIds = (state = EMPTY_ARRAY, action) => {
  switch (action.type) {
    case types.FETCH_FIELDS_SUCCESS:
      return action.ids;
    default:
      return state;
  }
};

reducers.recipientFieldsById = (state = EMPTY_OBJECT, action) => {
  switch (action.type) {
    case types.FETCH_FIELDS_SUCCESS:
      return action.byId;
    default:
      return state;
  }
};

export default {
  key,
  reducer: combineReducers(reducers)
};
