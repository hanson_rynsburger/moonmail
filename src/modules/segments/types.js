import module from './module';

const {types} = module;

types.TOGGLE_EXPANDED = 'TOGGLE_EXPANDED';
types.FETCH_FIELDS_REQUEST = 'FETCH_FIELDS_REQUEST';
types.FETCH_FIELDS_SUCCESS = 'FETCH_FIELDS_SUCCESS';
types.FETCH_FIELDS_FAIL = 'FETCH_FIELDS_FAIL';

export default types;
