import {filterProps} from 'lib/utils';

export const isCorrectValue = value => !!value || value === false || value === 0;

const searchTermToFilters = (queryType, searchTerm) => {
  if (queryType === 'match') return {match: searchTerm};
  return searchTerm;
};

const filtersToSearchTerm = (queryType, {match, gte, lte}) => {
  if (queryType === 'range') return filterProps({gte, lte}, isCorrectValue);
  return match;
};

export const getQueryTypeByFieldType = fieldType => {
  switch (fieldType) {
    case 'date':
      return 'range';
    case 'text':
    case 'boolean':
    default:
      return 'match';
  }
};

export const conditionsToFormFields = (conditions = []) =>
  conditions.map(({condition: {fieldToQuery, queryType, searchTerm}}) => ({
    recipientField: fieldToQuery,
    filters: searchTermToFilters(queryType, searchTerm)
  }));

export const conditionsFromFormFields = (formFields, getRecipientField) =>
  formFields.map(({recipientField, filters}) => {
    const fieldType = getRecipientField(recipientField).type || 'string';
    const queryType = getQueryTypeByFieldType(fieldType);

    return {
      condition: {
        fieldToQuery: recipientField,
        queryType,
        searchTerm: filtersToSearchTerm(queryType, filters)
      },
      conditionType: 'filter'
    };
  });
