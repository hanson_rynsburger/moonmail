import {keySelectorFactory, createSelector} from 'lib/reduxUtils';
import {EMPTY_OBJECT} from 'lib/constants';
import module from './module';

const {selectors} = module;

const keySelector = keySelectorFactory(selectors.stateSelector);

selectors.getIsExpanded = keySelector('isExpanded');
selectors.getRecipientFieldsMap = keySelector('recipientFieldsById');
selectors.getRecipientFieldIds = keySelector('recipientFieldIds');

selectors.getRecipientFields = createSelector(
  selectors.getRecipientFieldIds,
  selectors.getRecipientFieldsMap,
  (ids, byId) => ids.map(id => byId[id])
);

selectors.recipientFieldSelector = createSelector(selectors.getRecipientFieldsMap, map => id =>
  map[id] || EMPTY_OBJECT
);

selectors.getSelectedConditions = createSelector(
  selectors.getSelectedResource,
  segment => segment.conditions
);

selectors.getIsSaving = createSelector(
  selectors.getIsCreating,
  selectors.getIsUpdating,
  (isCreating, isUpdating) => isCreating || isUpdating
);

selectors.getIsProcessing = createSelector(
  selectors.getIsSaving,
  selectors.getIsDeleting,
  (isSaving, isDeleting) => isSaving || isDeleting
);

export default selectors;
