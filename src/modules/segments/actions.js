import {normalizeResources} from 'lib/reduxUtils';
import {conditionsFromFormFields} from './utils';
import * as api from 'lib/api';
import module from './module';
import selectors from './selectors';
import types from './types';
import listSelectors from 'modules/lists/selectors';
import {humanize} from 'lib/utils';

const {actions} = module;

actions.addNew = () => {
  return dispatch => {
    dispatch(actions.clearSelectedResource());
    dispatch(actions.expand());
  };
};

actions.expand = () => ({type: types.TOGGLE_EXPANDED, value: true});
actions.collapse = () => ({type: types.TOGGLE_EXPANDED, value: false});

actions.fetchRecipientFields = listId => {
  return async (dispatch, getState) => {
    const state = getState();
    const fields = selectors.getRecipientFieldIds(state);
    if (fields.length > 0) return;
    dispatch({
      type: types.FETCH_FIELDS_REQUEST
    });
    try {
      const res = await api.fetchRecipientFields(listId);
      const metaData = listSelectors.getActiveListMetaFields(state);
      metaData.forEach(field => {
        if (['name', 'surname'].includes(field)) return;
        res.items.push({
          id: `metadata.${field}`,
          name: humanize(field),
          type: 'text'
        });
      });
      dispatch({
        type: types.FETCH_FIELDS_SUCCESS,
        ...normalizeResources(res)
      });
    } catch (error) {
      dispatch({
        type: types.FETCH_FIELDS_FAIL
      });
      return Promise.reject(error);
    }
  };
};

actions.saveSegment = (listId, {conditions, ...data}) => async (dispatch, getState) => {
  const state = getState();
  const id = selectors.getSelectedId(state);
  const getRecipientField = selectors.recipientFieldSelector(state);
  const segment = {
    ...data,
    conditions: conditionsFromFormFields(conditions, getRecipientField)
  };
  if (!id) {
    const newSegment = await dispatch(actions.createResource(listId, segment));
    dispatch(actions.collapse());
    return newSegment.id;
  } else {
    await dispatch(actions.updateResource(id, listId, segment));
    dispatch(actions.collapse());
    return id;
  }
};

export default actions;
