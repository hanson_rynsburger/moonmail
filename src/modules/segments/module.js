import getCollection from 'modules/common';

export default getCollection('segment', {
  include: ['fetchAll', 'create', 'update', 'delete', 'select']
});
