import {createSelector, createStateSelector} from 'lib/reduxUtils';
import {EMPTY_OBJECT} from 'lib/constants';
import {key} from './reducer';

export const getHints = createStateSelector(key);
export const getHintById = createSelector(getHints, data => id => data[id] || EMPTY_OBJECT);
