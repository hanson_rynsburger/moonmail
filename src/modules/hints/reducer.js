import * as types from './types';

export const key = 'hints';

export const reducer = (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_HINTS_SUCCESS:
      return action.data;
    default:
      return state;
  }
};

export default {reducer, key};
