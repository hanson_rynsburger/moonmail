import * as api from 'lib/api';
import * as types from './types';
import {isEmpty} from 'lib/utils';
import {getHints} from './selectors';
import {getIsHintDisabled} from 'modules/profile/selectors';

export const fetchHints = (forceRefresh = false) => {
  return async (dispatch, getState) => {
    const state = getState();
    const isHintsDisabled = getIsHintDisabled(state);
    const hints = getHints(state);
    const noNeedToRefresh = isHintsDisabled || !isEmpty(hints);
    if (!forceRefresh && noNeedToRefresh) return;
    dispatch({type: types.FETCH_HINTS_REQUEST});
    try {
      const data = await api.fetchHints();
      dispatch({type: types.FETCH_HINTS_SUCCESS, data});
    } catch (error) {
      //eslint-disable-next-line no-console
      console.error(error);
      dispatch({type: types.FETCH_HINTS_FAIL});
    }
  };
};
