import * as types from './types';
import {combineReducers} from 'redux';

export const key = 'app';

const isWelcomePopUpVisible = (state = false, action) => {
  switch (action.type) {
    case types.SHOW_WELCOME_POPUP:
      return true;
    case types.HIDE_WELCOME_POPUP:
      return false;
    default:
      return state;
  }
};

const isLoading = (state = true, action) => {
  switch (action.type) {
    case types.START_LOADER:
      return true;
    case types.STOP_LOADER:
      return false;
    default:
      return state;
  }
};

const loaderText = (state = null, action) => {
  switch (action.type) {
    case types.START_LOADER:
      return action.text;
    case types.STOP_LOADER:
      return null;
    default:
      return state;
  }
};

export const reducer = combineReducers({
  isWelcomePopUpVisible,
  isLoading,
  loaderText
});

export default {key, reducer};
