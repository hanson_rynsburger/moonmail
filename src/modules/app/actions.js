import storage from 'store';
import * as api from 'lib/api';
import {updateProfileLocally, fetchProfile, activateAmazonAccount} from 'modules/profile/actions';
import {getProfile, getIsNewFreeUser, getIsUnverifiedSesUser} from 'modules/profile/selectors';
import {replace} from 'react-router-redux';
import {isTokenExpired} from 'lib/jwt';
import {bootIntercom, bootSessionStack, triggerEvent} from 'lib/remoteUtils';
import * as types from 'modules/app/types';
import sendersActions from 'modules/senders/actions';
import {fetchHints} from 'modules/hints/actions';
import plansActions from 'modules/plans/actions';
import * as auth from './auth';
import * as notifications from 'modules/notifications/actions';
import {addMessage} from 'modules/messages/actions';

export const startLoader = (text = null) => ({
  type: types.START_LOADER,
  text
});

export const stopLoader = () => ({
  type: types.STOP_LOADER
});

export const checkAuth = () => {
  return async dispatch => {
    const {hash, pathname} = window.location;

    // finish the auth flow if callback url is valid
    if (pathname === auth.CALLBACK && /access_token|id_token|error/.test(hash)) {
      return dispatch(finishAuth(hash));
    }
    if (auth.isAuthenticated()) {
      return dispatch(fetchResources());
    }
    dispatch(startLoader('Welcome back, one moment...'));
    await auth.renewAuth();
    dispatch(fetchResources());
  };
};

export const signIn = params => dispatch => {
  setTimeout(() => dispatch(startLoader()), 300);
  auth.startAuth(params);
};
export const signOut = () => dispatch => {
  setTimeout(() => dispatch(startLoader()), 300);
  auth.logout();
};

export const finishAuth = hash => {
  return async dispatch => {
    await auth.finishAuth(hash);
    dispatch(fetchResources(true));
  };
};

const authEvent = profile => {
  if (profile.isDemoUser) {
    triggerEvent('app.demoUserLogin');
  }
  if (profile.impersonated) return;
  const data = {
    email: profile.email,
    isSesUser: profile.isSesUser,
    userId: profile.user_id,
    source: profile.source
  };
  if (profile.logins < 2) {
    triggerEvent('app.newUserLogin', data);
  } else {
    triggerEvent('app.userLogin', data);
  }
};

const fetchAuthProfile = idToken => {
  return async dispatch => {
    // fetch user profile from auth0, this endpoint will be deprecated
    // and webAuth.client.userInfo is not returning metadata
    // TODO: a workaround to fetch all user data
    let profile = await api.fetchTokenInfo(idToken);
    dispatch(updateProfileLocally(profile));
    auth
      .fetchAWSCredentials(idToken)
      .then(
        credentials => dispatch(notifications.connect(profile.user_id, credentials)),
        error => dispatch(addMessage({text: error}))
      );
    // trigger auth event
    authEvent(profile);
  };
};

// fetch app resources on load
export const fetchResources = (isLogin = false) => {
  return async (dispatch, getState) => {
    let idToken = storage.get('idToken');

    // renew all credentials if idToken is expired
    if (isTokenExpired(idToken)) {
      dispatch(startLoader('Welcome back, one moment...'));
      const authResult = await auth.renewAuth();
      idToken = authResult.idToken;
    }

    // fetch global resources
    dispatch(plansActions.fetchPlans());
    dispatch(fetchHints());

    // redirect user to the initially requested path
    const returnTo = storage.get('returnTo');
    if (returnTo) {
      dispatch(replace(returnTo));
      storage.remove('returnTo');
    }

    // fetch profile data from auth0 and MM api
    await Promise.all([dispatch(fetchAuthProfile(idToken)), dispatch(fetchProfile())]);
    const state = getState();

    // at this point the app could be rendered
    dispatch(stopLoader());

    // get full profile from redux state
    const profile = getProfile(state);

    dispatch(bootIntercom());
    dispatch(bootSessionStack());

    // fetch senders and check if use has unverified ones
    dispatch(sendersActions.fetchSenders());

    // activate amazon subscription if amazonCustomerId is present
    if (profile.amazonCustomerId && !profile.amazonSubscriptionActive) {
      dispatch(activateAmazonAccount(profile.amazonCustomerId));
    }

    // show welcome popup for new logged in users
    if (isLogin && getIsNewFreeUser(state)) {
      dispatch(showWelcomePopup());
    }

    // show getting started page for unverified ses users
    if (getIsUnverifiedSesUser(state)) {
      dispatch(replace('/getting-started-aws-ses'));
    }

    // automatically renew token when it expires
    auth.scheduleRenew(idToken);
  };
};

export const impersonateUser = userId => {
  return async dispatch => {
    dispatch(startLoader('Changing account'));
    try {
      const {url} = await api.impersonateUser(userId);
      auth.removeSession();
      window.location.href = url;
    } catch (error) {
      dispatch(stopLoader());
      auth.startAuth({error: 'Something went wrong'});
      return Promise.reject(error);
    }
  };
};

export const showWelcomePopup = () => ({
  type: types.SHOW_WELCOME_POPUP
});
