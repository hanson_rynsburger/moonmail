import {keySelectorFactory, createStateSelector} from 'lib/reduxUtils';
import {key} from './reducer';

const authSelector = createStateSelector(key);
const keySelector = keySelectorFactory(authSelector);
export const getIsWelcomePopUpVisible = keySelector('isWelcomePopUpVisible');
export const getIsLoading = keySelector('isLoading');
export const getLoaderText = keySelector('loaderText');
