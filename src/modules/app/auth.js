import queryString from 'query-string';
import _debug from 'debug';
import auth0 from 'auth0-js';
import storage from 'store';
const debugAuth = _debug('app:debug:auth');
import {getRenewDelay} from 'lib/jwt';

// set nonce and state values to cliendID to allow cross domain authentication
// if those values are not set for all methods, they will be generated automatically
// if they don't match auth fails.
const nonce = APP_CONFIG.auth0.clientID;
const state = APP_CONFIG.auth0.clientID;

export const CALLBACK = '/signin';

const webAuth = new auth0.WebAuth({
  domain: APP_CONFIG.auth0.clientDomain,
  clientID: APP_CONFIG.auth0.clientID,
  audience: `https://${APP_CONFIG.auth0.clientDomain}/userinfo`,
  redirectUri: window.location.origin + '/signin',
  responseType: 'token id_token',
  scope: 'openid'
});

// save user session to localStorage
export const setSession = authResult => {
  if (!authResult) return;
  let expiresAt = authResult.expiresIn * 1000 + new Date().getTime();
  storage.set('accessToken', authResult.accessToken);
  storage.set('idToken', authResult.idToken);
  storage.set('expiresAt', expiresAt);
};

// remove credentials from localStorage
export const removeSession = () => {
  storage.remove('accessToken');
  storage.remove('idToken');
  storage.remove('expiresAt');
  storage.remove('credentials');
  window.sessionstack && window.sessionstack('clearUserCookie', true);
};

export const isAuthenticated = () => {
  // Check whether the current time is past the
  // Access Token's expiry time
  let expiresAt = storage.get('expiresAt');
  return new Date().getTime() < expiresAt;
};

// initiate auth0 flow
export const startAuth = ({hint, error, silent, amazonCustomerId, source} = {}) => {
  const query = queryString.parse(window.location.search);
  // Pass query params to auth0 rule.
  // They will be converted to snake case (amazonCustomerId => amazon_customer_id)
  const params = {
    nonce,
    state,
    amazonCustomerId: amazonCustomerId || query.amazonCustomerId,
    source: source || query.source,
    loginHint: hint,
    customError: error
  };
  if (silent) params.prompt = 'none';
  webAuth.authorize(params);
};

// finish auth flow with query params from auth0
export const finishAuth = hash =>
  new Promise((resolve, reject) => {
    // remove old credentials if exist
    removeSession();

    const {state} = queryString.parse(window.location.hash);

    // in all apps nonce and state are set to clientId,
    // if state is not passed as a query param it means that this is an impersonation flow
    // this trick does not work in auth0.js v9.3.1
    webAuth.parseHash(
      {hash, state, nonce: state, __enableImpersonation: true},
      (error, authResult) => {
        if (error) {
          startAuth({error: error.errorDescription || 'Something went wrong, please try again.'});
          return reject(error);
        }
        setSession(authResult);
        return resolve(authResult);
      }
    );
  });

export const logout = () => {
  removeSession();
  const url = webAuth.client.buildAuthorizeUrl({
    responseType: 'token id_token',
    redirectUri: window.location.origin + CALLBACK,
    nonce,
    state
  });
  webAuth.logout({
    returnTo: url
  });
};

// fetch AWS temporary credentials with auth0 delegation
export const fetchAWSCredentials = idToken =>
  new Promise((resolve, reject) => {
    const credentials = storage.get('credentials');
    if (credentials && new Date(credentials.Expiration) - new Date() > 60000) {
      return resolve(credentials);
    }
    const params = {
      id_token: idToken,
      grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
      api: 'aws',
      stage: __STAGE__
    };
    webAuth.client.delegation(params, (error, data) => {
      if (error) return reject(error);
      storage.set('credentials', data.Credentials);
      return resolve(data.Credentials);
    });
  });

// renew auth credentials
export const renewAuth = () => {
  return new Promise((resolve, reject) => {
    debugAuth('refreshing Tokens...');
    const params = {
      responseType: 'token id_token',
      redirectUri: window.location.origin + CALLBACK,
      usePostMessage: true,
      nonce,
      state
    };
    webAuth.checkSession(params, (error, authResult) => {
      if (error) {
        const {pathname} = window.location;
        // save pathname to redirect user after login
        if (pathname !== CALLBACK) {
          storage.set('returnTo', pathname);
        }
        startAuth();
        return reject(error);
      }
      setSession(authResult);
      fetchAWSCredentials(authResult.idToken).then(() => {
        debugAuth('Tokens are refreshed');
        return resolve(authResult);
      });
    });
  });
};

let renewTimeout;
export const scheduleRenew = idToken => {
  clearTimeout(renewTimeout);
  const delay = getRenewDelay(idToken);
  if (delay <= 0) return renewAuth();
  renewTimeout = setTimeout(() => {
    renewAuth();
  }, delay);
};
