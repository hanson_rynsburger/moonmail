import app from './app/reducer';
import messages from './messages/reducer';
import plans from './plans/reducer';
import extensions from './extensions/reducer';
import profile from './profile/reducer';
import route from './route/reducer';
import campaigns from './campaigns/reducer';
import automations from './automations/reducer';
import automationActions from './automationActions/reducer';
import segments from './segments/reducer';
import senders from './senders/reducer';
import lists from './lists/reducer';
import recipients from './recipients/reducer';
import templates from './templates/reducer';
import marketTemplates from './marketTemplates/reducer';
import templateTags from './templateTags/reducer';
import billingInfo from './billingInfo/reducer';
import hints from './hints/reducer';
import zapierTemplates from './zapierTemplates/reducer';

export default [
  app,
  messages,
  plans,
  extensions,
  profile,
  route,
  automations,
  automationActions,
  segments,
  senders,
  lists,
  recipients,
  campaigns,
  templates,
  marketTemplates,
  templateTags,
  billingInfo,
  hints,
  zapierTemplates
];
