import * as api from 'lib/api';
import types from './types';
import {addMessage} from 'modules/messages/actions';
import {push} from 'react-router-redux';
import module from './module';
import {humanize} from 'lib/utils';

const {actions} = module;

actions.fetchTemplates = (params, forceRefresh) =>
  actions.fetchResources(
    {
      ...params,
      fields: 'body,html',
      include_fields: false
    },
    forceRefresh
  );

actions.createTemplate = data => {
  return async dispatch => {
    const template = await dispatch(actions.createResource(data, true));
    dispatch(push(`/templates/${template.id}`));
  };
};

actions.duplicateTemplate = templateId => {
  return async dispatch => {
    dispatch({
      type: types.DUPLICATE_TEMPLATE_REQUEST
    });
    try {
      dispatch(actions.selectResource(templateId));
      const template = await api.duplicateTemplate(templateId);
      dispatch(actions.createResourceLocally(template));
      dispatch(push(`/templates/${template.id}`));
      dispatch(actions.clearSelectedResource());
      dispatch({
        type: types.DUPLICATE_TEMPLATE_SUCCESS
      });
    } catch (error) {
      dispatch({
        type: types.DUPLICATE_TEMPLATE_FAIL
      });
      dispatch(actions.clearSelectedResource());
      dispatch(addMessage({text: error}));
    }
  };
};

actions.importTemplate = file => {
  return dispatch => {
    if (file.type !== 'application/json') {
      dispatch(
        addMessage({
          text:
            'Only templates exported from the editor are supported. ' +
            'To export a template go to actions / save as template in your template editor'
        })
      );
      return;
    }
    dispatch({
      type: types.IMPORT_TEMPLATE_REQUEST
    });
    const name = file.name.replace(/\.[^/.]+$/, '');
    const reader = new FileReader();
    reader.onload = async e => {
      await dispatch(
        actions.createTemplate({
          body: e.target.result,
          name: humanize(name)
        })
      );
      dispatch({
        type: types.IMPORT_TEMPLATE_SUCCESS
      });
    };
    reader.onerror = error => {
      dispatch({
        type: types.IMPORT_TEMPLATE_FAIL
      });
      dispatch(addMessage({text: error}));
    };
    reader.readAsText(file);
  };
};

export default actions;
