import module from './module';
import {keySelectorFactory} from 'lib/reduxUtils';

const {selectors} = module;
const keySelector = keySelectorFactory(selectors.stateSelector);

selectors.getIsDuplicating = keySelector('isDuplicating');
selectors.getIsImporting = keySelector('isImporting');

export default selectors;
