import module from './module';
import types from './types';
import {combineReducers} from 'redux';
import {createAsyncFlagReducer} from 'lib/reduxUtils';

const {reducers, key} = module;

reducers.isDuplicating = createAsyncFlagReducer(types, 'duplicateTemplate');
reducers.isImporting = createAsyncFlagReducer(types, 'importTemplate');

export default {
  key,
  reducer: combineReducers(reducers)
};
