import {signOut} from 'modules/app/actions';
import {addMessage} from 'modules/messages/actions';
import {updateProfileLocally} from '../profile/actions';
import campaignActions from 'modules/campaigns/actions';
import campaignSelectors from 'modules/campaigns/selectors';
import listsAction from 'modules/lists/actions';
import * as types from './types';
import _debug from 'debug';

const infoMqtt = _debug('app:info:mqtt');

export default (dispatch, getState) => action => {
  infoMqtt('Action from server => ', action);
  switch (action.type) {
    case types.BAN_USER:
      dispatch(signOut());
      dispatch(
        addMessage({
          text: 'You have been banned. Please contact support'
        })
      );
      return;
    case types.SHOW_MESSAGE:
      dispatch(addMessage(action.data));
      return;
    case types.PROFILE_UPDATED:
      dispatch(updateProfileLocally(action.data));
      return;
    //eslint-disable-next-line no-case-declarations
    case types.CAMPAIGN_UPDATED:
      dispatch(campaignActions.updateResourceDataLocally(action.data));
      const {status, sentCount = 0} = campaignSelectors.getResourceById(getState(), action.data.id);
      if (status === 'sent' && sentCount === 0) {
        dispatch(campaignActions.pollCampaignReport(action.data.id));
      }
      return;
    case types.CAMPAIGN_LINKS_UPDATED:
    case types.CAMPAIGN_REPORT_UPDATED:
      dispatch(campaignActions.updateResourceDataLocally(action.data));
      return;
    case types.LIST_UPDATED:
    case types.LIST_EXPORT_SUCCEEDED:
      dispatch(listsAction.updateResourceDataLocally(action.data));
      return;
    case types.LIST_EXPORT_FAILED:
      dispatch(listsAction.updateResourceDataLocally(action.data));
      dispatch(
        addMessage({
          text: 'Something went wrong with your list export'
        })
      );
      return;
    case types.LIST_IMPORT_PROCESSED:
    case types.LIST_IMPORT_SUCCEEDED:
      dispatch(listsAction.fetchList(action.data.listId, null, true));
      return;
    case types.LIST_IMPORT_FAILED:
      dispatch(listsAction.fetchList(action.data.listId, null, true));
      dispatch(
        addMessage({
          text: 'Something went wrong with your list import'
        })
      );
  }
};
