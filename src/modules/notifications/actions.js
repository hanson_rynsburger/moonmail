import AWSMqtt from 'aws-mqtt-client';
import storage from 'store';
import createDispatcher from './dispatcher';
import _debug from 'debug';

let websocket = {};

const infoMqtt = _debug('app:info:mqtt');
export const connect = (topic, awsCredentials) => {
  const credentials = awsCredentials || storage.get('credentials') || {};
  websocket = new AWSMqtt({
    accessKeyId: credentials.AccessKeyId,
    secretAccessKey: credentials.SecretAccessKey,
    sessionToken: credentials.SessionToken,
    endpointAddress: APP_CONFIG.iotEndpoint,
    region: APP_CONFIG.region,
    expires: 3600,
    reconnectPeriod: 60000,
    connectTimeout: 60000,
    keepalive: 3600
  });
  return (dispatch, getState) => {
    const dispatchMessage = createDispatcher(dispatch, getState);
    websocket.on('connect', () => {
      if (topic) {
        websocket.subscribe(topic);
        infoMqtt(`connected to ${topic}`);
      }
    });
    websocket.on('message', (topic, message) => {
      const action = JSON.parse(message.toString());
      dispatchMessage(action);
    });
  };
};

export const disconnect = () => {
  return () => {
    if (websocket.end) {
      websocket.end();
    }
  };
};
