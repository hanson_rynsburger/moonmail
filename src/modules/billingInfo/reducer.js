import * as types from './types';
import {combineReducers} from 'redux';
import {createAsyncFlagReducer} from 'lib/reduxUtils';

export const key = 'billingInfo';

const isStripeLoading = createAsyncFlagReducer(types, 'loadStripe');
const isTokenCreating = createAsyncFlagReducer(types, 'createToken');
const isUpdating = createAsyncFlagReducer(types, 'updateCard');

const reducer = combineReducers({
  isStripeLoading,
  isTokenCreating,
  isUpdating
});

export default {key, reducer};
