import {keySelectorFactory, createStateSelector} from 'lib/reduxUtils';
import {key} from './reducer';

const stateSelector = createStateSelector(key);
const keySelector = keySelectorFactory(stateSelector);

export const getIsStripeLoading = keySelector('isStripeLoading');
export const getIsTokenCreating = keySelector('isTokenCreating');
export const getIsUpdating = keySelector('isUpdating');
