import * as types from './types';
import * as api from 'lib/api';
import load from 'loadjs';
import {addMessage} from 'modules/messages/actions';
import {getProfile} from 'modules/profile/selectors';
import {updateProfileLocally} from 'modules/profile/actions';

export const loadStripe = () => {
  return dispatch => {
    if (!window.Stripe) {
      dispatch({
        type: types.LOAD_STRIPE_REQUEST
      });
      load('https://js.stripe.com/v2/', {
        success() {
          window.Stripe.setPublishableKey(APP_CONFIG.stripeKey);
          dispatch({
            type: types.LOAD_STRIPE_SUCCESS
          });
        },
        fail() {
          dispatch(
            addMessage({
              text: 'Error loading stripe'
            })
          );
          dispatch({
            type: types.LOAD_STRIPE_FAIL
          });
        }
      });
    }
  };
};

export const createToken = ({name, ccNumber, cvc, ccMonth, ccYear}) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      if (!window.Stripe) {
        dispatch(
          addMessage({
            text: 'Error loading stripe'
          })
        );
        reject();
      }
      dispatch({
        type: types.CREATE_TOKEN_REQUEST
      });
      const cardData = {
        name,
        number: ccNumber,
        exp_month: ccMonth,
        exp_year: ccYear,
        cvc
      };
      window.Stripe.createToken(cardData, (status, response) => {
        if (response.error) {
          dispatch(
            addMessage({
              text: response.error.message
            })
          );
          dispatch({
            type: types.CREATE_TOKEN_FAIL
          });
          return reject();
        }
        dispatch({
          type: types.CREATE_TOKEN_SUCCESS,
          token: response.id
        });
        resolve(response.id);
      });
    });
  };
};

export const updateCard = cardData => {
  return async (dispatch, getState) => {
    const {email} = getProfile(getState());
    dispatch({
      type: types.UPDATE_CARD_REQUEST
    });
    try {
      const token = await dispatch(createToken(cardData));
      const profile = await api.updateCard({token, email});
      dispatch({
        type: types.UPDATE_CARD_SUCCESS
      });
      dispatch(updateProfileLocally(profile));
      dispatch(
        addMessage({
          text: 'Your billing information was successfully saved',
          style: 'success'
        })
      );
    } catch (error) {
      //eslint-disable-next-line no-console
      console.error(error);
      dispatch({
        type: types.UPDATE_CARD_FAIL
      });
      const declined = 'Your card was declined.';
      const text = error.includes(declined) ? declined : error;
      dispatch(addMessage({text}));
    }
  };
};
