import module from './module';
import types from './types';
import * as api from 'lib/api';
import {getProfile, getTemplates} from 'modules/profile/selectors';
import {addMessage} from 'modules/messages/actions';
import templatesActions from 'modules/templates/actions';
import {updateProfileLocally} from 'modules/profile/actions';
import {mergeArrays} from 'lib/utils';

const {actions} = module;

actions.buyMarketTemplateStart = id => {
  return dispatch => {
    dispatch(actions.selectResource(id));
    dispatch({
      type: types.BUY_MARKET_TEMPLATE_START
    });
  };
};

actions.buyMarketTemplateCancel = () => {
  return dispatch => {
    dispatch({
      type: types.BUY_MARKET_TEMPLATE_CANCEL
    });
    dispatch(actions.clearSelectedResource());
  };
};

actions.buyMarketTemplate = template => {
  return async (dispatch, getState) => {
    const state = getState();
    const profile = getProfile(state);
    const purchasedTemplates = getTemplates(state);
    const isPaid = template.price > 0;
    const isFree = purchasedTemplates.includes(template.id) || !isPaid;
    const {email, clickId} = profile;
    dispatch({
      type: types.BUY_MARKET_TEMPLATE_REQUEST
    });
    try {
      const {body, thumbnail, name} = await api[
        isFree ? 'fetchMarketTemplate' : 'buyMarketTemplate'
      ](template.id, {
        email,
        clickId
      });
      await dispatch(templatesActions.createTemplate({body, thumbnail, name}));
      if (!isFree && isPaid) {
        const installedTemplates = mergeArrays(purchasedTemplates, [template.id]);
        dispatch(updateProfileLocally({installedTemplates}));
      }
      dispatch({
        type: types.BUY_MARKET_TEMPLATE_SUCCESS
      });
    } catch (error) {
      dispatch({
        type: types.BUY_MARKET_TEMPLATE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export default actions;
