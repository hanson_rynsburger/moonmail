import module from './module';
import {combineReducers} from 'redux';
import {createAsyncFlagReducer, createStartFlagReducer} from 'lib/reduxUtils';
import types from './types';

const {reducers, key} = module;

reducers.isBuying = createAsyncFlagReducer(types, 'buyMarketTemplate');
reducers.isBuyStarted = createStartFlagReducer(types, 'buyMarketTemplate');

export default {
  key,
  reducer: combineReducers(reducers)
};
