export default {
  'special-features': ['dynamic-images', 'animated-gif', 'dynamic-countdowns'],
  behavioral: ['notification', 'welcome'],
  'e-commerce': ['coupon', 'discount', 'product-display', 'special-promotion'],
  event: ['event-promotion'],
  holiday: ['black-friday'],
  inaugural: ['activation-message', 'download', 'opt-in'],
  newsletter: ['blog', 'corporate-news', 'magazine', 'product-news'],
  transactional: ['order'],
  industry: [
    'technology',
    'fashion',
    'news',
    'app',
    'b2b',
    'books',
    'travel',
    'photography',
    'food'
  ]
};
