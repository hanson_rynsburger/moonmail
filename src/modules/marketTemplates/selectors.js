import module from './module';
import {keySelectorFactory} from 'lib/reduxUtils';

const {selectors} = module;
const keySelector = keySelectorFactory(selectors.stateSelector);

selectors.getIsBuying = keySelector('isBuying');
selectors.getIsBuyStarted = keySelector('isBuyStarted');

export default selectors;
