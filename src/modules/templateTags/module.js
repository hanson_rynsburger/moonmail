import getCollection from 'modules/common';
import {schema} from 'normalizr';

export const resource = new schema.Entity('resources', {}, {idAttribute: 'key'});

export default getCollection('templateTag', {
  include: ['fetchAll'],
  schema: {
    resource,
    arrayOfResources: [resource]
  }
});
