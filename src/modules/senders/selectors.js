import module from './module';
import {createSelector, keySelectorFactory} from 'lib/reduxUtils';
import {getRouteQuery} from 'modules/route/selectors';
import {isInsecureSender} from 'modules/senders/utils';

const {selectors} = module;
const keySelector = keySelectorFactory(selectors.stateSelector);

selectors.getIsVerifying = keySelector('isVerifying');
selectors.getIsVerifyingDomain = keySelector('isVerifyingDomain');
selectors.getIsVerifyingDomain = keySelector('isVerifyingDomain');
selectors.getIsUpdatingStatus = keySelector('isUpdatingStatus');
selectors.getIsGeneratingDkim = keySelector('isGeneratingDkim');
selectors.getIsActivatingDkim = keySelector('isActivatingDkim');

selectors.getVerifiedSenders = createSelector(selectors.getResources, senders =>
  senders.filter(s => s.verified)
);

selectors.getInsecureSenders = createSelector(selectors.getVerifiedSenders, senders =>
  senders.filter(s => s._isUpToDate && isInsecureSender(s))
);

selectors.getFilteredSenders = createSelector(
  selectors.getSortedResources,
  getRouteQuery,
  (senders, {filter}) => {
    switch (filter) {
      case 'verified':
        return senders.filter(s => s.verified);
      case 'unverified':
        return senders.filter(s => !s.verified);
      case 'archived':
        return senders.filter(s => s.archived);
      default:
        return senders.filter(s => !s.archived);
    }
  }
);

// Because senders are fetch globally after profile on app load
// we check if profile is fetching to avoid ui flickering
selectors.getIsFetchingAll = createSelector(
  selectors.getIsFetchingAll,
  // because of imports order it seems to be impossible to import profile getIsFetching selector
  // checking if profile is fetching directly from state
  state => state.profile.isFetching,
  (isFetchingSenders, isFetchingProfile) => isFetchingSenders || isFetchingProfile
);

export default selectors;
