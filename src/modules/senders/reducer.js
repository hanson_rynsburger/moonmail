import module from './module';
import {combineReducers} from 'redux';
import {createAsyncFlagReducer} from 'lib/reduxUtils';
import types from './types';

const {reducers, key} = module;

reducers.isVerifying = createAsyncFlagReducer(types, 'verifySender');
reducers.isUpdatingStatus = createAsyncFlagReducer(types, 'updateSenderStatus');
reducers.isGeneratingDkim = createAsyncFlagReducer(types, 'generateSenderDkim');
reducers.isVerifyingDomain = createAsyncFlagReducer(types, 'verifySenderDomain');
reducers.isActivatingDkim = createAsyncFlagReducer(types, 'activateSenderDkim');

export default {
  key,
  reducer: combineReducers(reducers)
};
