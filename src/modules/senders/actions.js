import module from './module';
import * as api from 'lib/api';
import {createUpdateAction} from 'lib/reduxUtils';
import {getHasSenders} from 'modules/profile/selectors';
import {addMessage} from 'modules/messages/actions';
import selectors from './selectors';
import {push} from 'react-router-redux';
import {DEFAULT_SENDERS} from './constants';
import types from './types';
import {isInsecureSender} from 'modules/senders/utils';

const {actions} = module;

const updateAction = actionName => createUpdateAction(actions, types, actionName);

actions.checkInsecureSender = () => {
  return async (dispatch, getState) => {
    const state = getState();
    const senderIds = selectors.getIds(state);
    const getSender = selectors.getResourceGetter(state);
    let i = 0;
    let hasInsecureSender = false;
    while (i < senderIds.length && !hasInsecureSender) {
      let sender = getSender(senderIds[i]);
      if (sender._isUpToDate) {
        hasInsecureSender = isInsecureSender(sender);
      } else {
        //eslint-disable-next-line no-await-in-loop
        sender = await dispatch(actions.fetchSender(senderIds[i]));
        hasInsecureSender = isInsecureSender(sender);
      }
      i++;
    }
  };
};

actions.fetchSenders = (params, forceRefresh = false) => {
  return async (dispatch, getState) => {
    const state = getState();
    const hasSenders = getHasSenders(state);
    if (!hasSenders) {
      return dispatch(actions.loadResourcesLocally(DEFAULT_SENDERS));
    }
    await dispatch(actions.fetchResources(params, forceRefresh));
    dispatch(actions.checkInsecureSender());
  };
};

actions.updateSenderStatus = updateAction('updateSenderStatus');

actions.poolSenderStatus = senderId => {
  return dispatch => {
    let checks = 0;
    const interval = setInterval(async () => {
      let {verified} = await dispatch(actions.updateSenderStatus(senderId));
      checks++;
      if (checks > 10 || verified) clearInterval(interval);
    }, 5000);
  };
};

actions.createSender = data => {
  return async dispatch => {
    dispatch(actions.createResource(data, true)).then(sender => {
      dispatch(push(`/profile/senders/${sender.id}`));
      dispatch(
        addMessage({
          text: 'Please check you inbox, you have to receive email from Amazon',
          style: 'success'
        })
      );
      dispatch(actions.poolSenderStatus(sender.id));
    });
  };
};

actions.verifySender = senderId => {
  return async dispatch => {
    dispatch({
      type: types.VERIFY_SENDER_REQUEST
    });
    try {
      await api.verifySender(senderId);
      dispatch({
        type: types.VERIFY_SENDER_SUCCESS
      });
      dispatch(
        addMessage({
          text: 'Please check you inbox, you have to receive email from Amazon',
          style: 'success'
        })
      );
      dispatch(actions.poolSenderStatus(senderId));
    } catch (error) {
      dispatch({
        type: types.VERIFY_SENDER_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

actions.setSenderArchived = (senderId, archived) => {
  return async dispatch => {
    dispatch(actions.updateResourceLocally(senderId, {archived}));
    try {
      await dispatch(actions.updateResource(senderId, {archived}, true));
      dispatch(actions.deleteResourceLocally(senderId));
    } catch (error) {
      //eslint-disable-next-line no-console
      console.error(error);
      dispatch(actions.updateResourceLocally(senderId, {archived: !archived}));
    }
  };
};

actions.generateSenderDkim = updateAction('generateSenderDkim');
actions.verifySenderDomain = updateAction('verifySenderDomain');
actions.activateSenderDkim = updateAction('activateSenderDkim');

actions.checkSpfStatus = senderId => {
  return async dispatch => {
    const {spfEnabled} = await dispatch(actions.fetchSender(senderId, true));
    if (!spfEnabled) {
      dispatch(
        addMessage({
          text:
            'SPF TXT record is not detected. ' +
            'Please note that changes in DNS records may take up to 72 hours'
        })
      );
    }
  };
};

actions.checkDomainStatus = senderId => {
  return async dispatch => {
    const {domainVerified} = await dispatch(actions.fetchSender(senderId, true));
    if (!domainVerified) {
      dispatch(
        addMessage({
          text:
            'Domain verification TXT record is not detected. ' +
            'Please note that changes in DNS records may take up to 72 hours'
        })
      );
    }
  };
};

actions.checkDkimStatus = senderId => {
  return async dispatch => {
    const {dkimVerified} = await dispatch(actions.fetchSender(senderId, true));
    if (!dkimVerified) {
      dispatch(
        addMessage({
          text:
            'DKIM CNAME records are not detected. ' +
            'Please note that changes in DNS records may take up to 72 hours'
        })
      );
    }
  };
};

actions.checkDmarcStatus = senderId => {
  return async dispatch => {
    const {dmarcEnabled} = await dispatch(actions.fetchSender(senderId, true));
    if (!dmarcEnabled) {
      dispatch(
        addMessage({
          text:
            'DMARC TXT record is not detected. ' +
            'Please note that changes in DNS records may take up to 72 hours'
        })
      );
    }
  };
};

export default actions;
