export const isInsecureSender = (s = {}) => {
  if (s.archived) return false;
  if (!s.domainVerified) return true;
  return !(s.dkimVerified && s.dkimEnabled);
};
