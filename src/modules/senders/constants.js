export const DEFAULT_SENDER_EMAIL = 'no-reply@email-campaign-manager.com';
export const DEFAULT_SENDER_ID = 'default-sender';
export const DEFAULT_SENDER = {
  fromName: 'MoonMail Sender',
  domainVerified: true,
  domainVerificationToken: '12345',
  domainVerificationStatus: 'Success',
  dkimVerificationStatus: 'Success',
  dkimEnabled: true,
  dkimVerified: true,
  dmarcEnabled: true,
  dkimTokens: ['12345'],
  spfEnabled: true,
  id: DEFAULT_SENDER_ID,
  verified: true,
  emailAddress: DEFAULT_SENDER_EMAIL,
  default: true
};

export const DEFAULT_SENDERS = {
  items: [DEFAULT_SENDER]
};

export const BLACKLISTED_SENDER_DOMAINS = [
  'paypal',
  'amazon',
  'google',
  'microsoft',
  'outlook',
  'facebook',
  'jpmorganchase',
  'bankofamerica',
  'wellsfargo',
  'citigroup',
  'goldmansachs',
  'morganstanley',
  'hsbc',
  'stripe',
  'gmail',
  'hotmail',
  'icloud'
];

export const SENDERS_FILTERS = ['all', 'verified', 'unverified', 'archived'];
