import module from './module';
import types from './types';
import * as api from 'lib/api';
import {createUpdateAction} from 'lib/reduxUtils';
import {omitProps, isUnsupportedAttachment, humanize} from 'lib/utils';
import {ATTACHMENTS_LIMIT} from 'lib/constants';
import {push} from 'react-router-redux';
import {addMessage} from 'modules/messages/actions';
import * as profileSelectors from 'modules/profile/selectors';
import selectors from 'modules/campaigns/selectors';
import S3 from 'lib/s3Bucket';
import sendersSelectors from 'modules/senders/selectors';
import listsSelectors from 'modules/lists/selectors';
import {footer, getAddress} from 'lib/htmlUtils';
import {parse} from 'lib/parser';
import numeral from 'numeral';
import {getIsRecentlySent} from './utils';
import moment from 'moment';
import {DEMO_GRAPH_DATA} from './constants';
import {triggerEventAction} from 'lib/remoteUtils';

const {actions} = module;

const updateAction = actionName => createUpdateAction(actions, types, actionName);

actions.updateCampaign = (...args) => {
  return async dispatch => {
    const campaign = await dispatch(actions.updateResource(...args));
    dispatch(triggerEventAction('app.userUpdatedCampaign'));
    return campaign;
  };
};

actions.createCampaign = data => {
  return async dispatch => {
    const campaign = await dispatch(actions.createResource(data, true));
    dispatch(triggerEventAction('app.userCreatedCampaign'));
    dispatch(push(`/campaigns/${campaign.id}`));
    return campaign;
  };
};

actions.fetchCampaigns = (filter, forceRefresh = false) => {
  return async dispatch => {
    const params = {
      fields: ['body', 'template', 'listIds', 'userId', 'subject'].join(','),
      include_fields: false
    };
    if (filter === 'archived') params.archived = true;
    const items = await dispatch(actions.fetchResources(params, forceRefresh));

    // fetch reports for campaigns sent in last 24 hours
    const recentlySent = items.filter(getIsRecentlySent);
    recentlySent.forEach(async campaign => {
      if (campaign.sentCount && campaign.sentCount > 0) return;
      const {sentCount = 0} = await dispatch(actions.fetchCampaignReport(campaign.id));
      if (sentCount === 0) dispatch(actions.pollCampaignReport(campaign.id));
    });
    return items;
  };
};

actions.fetchCampaignGraphData = ({start, end, campaignId} = {}) => {
  return async dispatch => {
    if (!campaignId) {
      return dispatch(
        dispatch({
          type: types.FETCH_GRAPH_DATA_SUCCESS,
          data: DEMO_GRAPH_DATA.data
        })
      );
    }
    dispatch({
      type: types.FETCH_GRAPH_DATA_REQUEST
    });
    try {
      const [opensData, clicksData] = await Promise.all([
        api.fetchCampaignGraphData(campaignId, {start, end, source: 'opens'}),
        api.fetchCampaignGraphData(campaignId, {start, end, source: 'clicks'})
      ]);

      const opens = [];
      const clicks = [];
      const timestamps = [];

      opensData.items.sort((a, b) => a.timestamp - b.timestamp);
      clicksData.items.sort((a, b) => a.timestamp - b.timestamp);

      opensData.items.forEach((item, index) => {
        opens.push(item.count);
        clicks.push(clicksData.items[index].count);
        timestamps.push(moment.unix(item.timestamp).toDate());
      });

      dispatch({
        type: types.FETCH_GRAPH_DATA_SUCCESS,
        data: {opens, clicks, timestamps}
      });
    } catch (error) {
      dispatch({
        type: types.FETCH_GRAPH_DATA_FAIL
      });
      dispatch(
        addMessage({
          text: error
        })
      );
    }
  };
};

let pollReportIntervals = {};
actions.pollCampaignReport = campaignId => {
  return dispatch => {
    pollReportIntervals[campaignId] = setInterval(async () => {
      const {sentCount} = await dispatch(actions.fetchCampaignReport(campaignId));
      if (sentCount > 0) clearInterval(pollReportIntervals[campaignId]);
    }, 60000);
  };
};

actions.cancelCampaignsReportPolling = () => {
  return () => {
    Object.keys(pollReportIntervals).forEach(id => {
      clearInterval(pollReportIntervals[id]);
    });
  };
};

actions.setCampaignArchived = (campaignId, archived) => {
  return async dispatch => {
    dispatch(actions.updateResourceLocally(campaignId, {archived}));
    try {
      await dispatch(actions.updateResource(campaignId, {archived}, true));
      dispatch(actions.deleteResourceLocally(campaignId));
    } catch (error) {
      dispatch(actions.updateResourceLocally(campaignId, {archived: !archived}));
    }
  };
};

actions.duplicateCampaign = campaignId => {
  return async dispatch => {
    dispatch({
      type: types.CREATE_REQUEST
    });
    try {
      const campaign = await api.duplicateCampaign(campaignId);
      dispatch(actions.createResourceLocally(campaign));
      dispatch(push(`/campaigns/${campaign.id}/settings`));
    } catch (error) {
      dispatch({
        type: types.CREATE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

actions.fetchCampaignReport = updateAction('fetchCampaignReport');
actions.fetchCampaignLinks = updateAction('fetchCampaignLinks');

actions.sendCampaignStart = () => ({
  type: types.SEND_CAMPAIGN_START
});

actions.sendCampaignCancel = () => ({
  type: types.SEND_CAMPAIGN_CANCEL
});

actions.sendCampaign = (campaignId, data) => {
  return async dispatch => {
    dispatch({
      type: types.SEND_CAMPAIGN_REQUEST
    });
    try {
      const campaign = await api.sendCampaign(campaignId, omitProps(data, 'template'));
      dispatch({
        type: types.SEND_CAMPAIGN_SUCCESS
      });
      dispatch(actions.updateResourceLocally(campaignId, campaign));
      dispatch(push('/campaigns'));
      dispatch(
        addMessage({
          text: 'Campaign is being sent',
          style: 'success'
        })
      );
      setTimeout(() => {
        dispatch(actions.sendCampaignCancel());
      });
      dispatch(triggerEventAction('app.userSentCampaign'));
    } catch (error) {
      dispatch({
        type: types.SEND_CAMPAIGN_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

actions.testCampaignStart = () => ({
  type: types.TEST_CAMPAIGN_START
});

actions.testCampaignCancel = () => ({
  type: types.TEST_CAMPAIGN_CANCEL
});

actions.testCampaign = (campaignId, data) => {
  return async dispatch => {
    dispatch({
      type: types.TEST_CAMPAIGN_REQUEST
    });
    try {
      await api.testCampaign(campaignId, data);
      dispatch({
        type: types.TEST_CAMPAIGN_SUCCESS
      });
      dispatch(
        addMessage({
          text: 'Test email was sent',
          style: 'success'
        })
      );
    } catch (error) {
      dispatch({
        type: types.TEST_CAMPAIGN_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

actions.scheduleCampaignStart = () => ({
  type: types.SCHEDULE_CAMPAIGN_START
});

actions.scheduleCampaignCancel = () => ({
  type: types.SCHEDULE_CAMPAIGN_CANCEL
});

const scheduleCampaignAction = updateAction('scheduleCampaign');

actions.scheduleCampaign = (campaignId, data) => {
  return async dispatch => {
    await dispatch(scheduleCampaignAction(campaignId, data));
    dispatch(triggerEventAction('app.userScheduledCampaign'));
    dispatch(
      addMessage({
        text: 'Campaign was scheduled',
        style: 'success'
      })
    );
  };
};

const cancelScheduledCampaignAction = updateAction('cancelScheduledCampaign');

actions.cancelScheduledCampaign = campaignId => {
  return async dispatch => {
    await dispatch(cancelScheduledCampaignAction(campaignId));
    dispatch(
      addMessage({
        text: 'Scheduled campaign has been canceled',
        style: 'success'
      })
    );
  };
};

actions.validateAndParseCampaign = ({body, senderId, listIds = []}, metaData = [], isEditable) => {
  return async (dispatch, getState) => {
    const state = getState();
    const profile = profileSelectors.getProfile(state);
    const isFreeUser = profileSelectors.getIsFreeUser(state);
    const hasSenders = profileSelectors.getHasSenders(state);
    const sender = sendersSelectors.getResourceById(state, senderId);
    const getListById = listsSelectors.getResourceGetter(state);
    const recipientsPerCampaign = profileSelectors.getLimits(state).recipientsPerCampaign;
    const plan = profileSelectors.getPlan(state);
    const tags = {};
    const originalBody = body;
    const defaultTags = {
      unsubscribe_url: `${APP_CONFIG.staticCdnURL}/unsubscribed.html`,
      recipient_email: profile.email,
      name: 'John',
      surname: 'Doe',
      from_email: sender.emailAddress,
      from_name: sender.fromName,
      from_address: getAddress(profile.address)
    };
    metaData.forEach(f => {
      tags[f] = humanize(f);
    });
    const validateEditableCampaign = () => {
      if (!originalBody) {
        throw 'Please add at least some content';
      }
      if (hasSenders) {
        if (!senderId || senderId !== sender.id) {
          throw 'Sender does not exist! Please select sender in settings.';
        }
      } else if (senderId) {
        throw 'You can use only MoonMail sender. ' +
          'Please change sender in settings or upgrade your account.';
      }
      if (recipientsPerCampaign) {
        const totalRecipients = listIds.reduce(
          (total, id) => total + getListById(id).subscribedCount,
          0
        );
        if (totalRecipients > recipientsPerCampaign) {
          throw `You can only send this campaign to ${recipientsPerCampaign} or less subscribers. 
        You're on the ${plan.title} Plan`;
        }
      }
    };
    try {
      if (isEditable) validateEditableCampaign();
      const campaignBody = isFreeUser ? body + footer : body;
      const template = await parse(campaignBody, isEditable);
      const html = template.render({...tags, ...defaultTags});
      dispatch({
        type: types.VALIDATE_AND_PARSE_SUCCESS,
        html
      });
    } catch (error) {
      dispatch({type: types.VALIDATE_AND_PARSE_FAIL});
      dispatch(addMessage({text: error}));
    }
  };
};

actions.attachFile = (campaignId, file) => {
  return async (dispatch, getState) => {
    dispatch({type: types.ATTACH_FILE_REQUEST});
    try {
      if (isUnsupportedAttachment(file)) {
        throw 'Unsupported extension';
      }
      const state = getState();
      const userId = profileSelectors.getUserId(state);
      const bucket = new S3({userId});
      const attachments = selectors.getAttachments(state);
      const totalSize = attachments.reduce((a, b) => a + b.size, 0);
      if (totalSize + file.size >= ATTACHMENTS_LIMIT) {
        throw `Maximum email size including files could not exceed ${numeral(
          ATTACHMENTS_LIMIT
        ).format('0 b')}`;
      }
      const {fileName, fileUrl} = await bucket.uploadFile(file);
      await dispatch(
        actions.updateResource(
          campaignId,
          {
            attachments: [...attachments, {name: fileName, size: file.size, url: fileUrl}]
          },
          true
        )
      );

      dispatch({type: types.ATTACH_FILE_SUCCESS});
    } catch (error) {
      dispatch({type: types.ATTACH_FILE_FAIL});
      dispatch(addMessage({text: error}));
    }
  };
};

actions.detachFile = (campaignId, fileName) => {
  return async (dispatch, getState) => {
    dispatch({
      type: types.DETACH_FILE_REQUEST,
      fileName
    });
    try {
      const state = getState();
      const userId = profileSelectors.getUserId(state);
      const bucket = new S3({userId});
      const attachments = selectors.getAttachments(state);

      await bucket.deleteFile(fileName);
      await dispatch(
        actions.updateResource(
          campaignId,
          {
            attachments: attachments.filter(a => a.name !== fileName)
          },
          true
        )
      );

      dispatch({type: types.DETACH_FILE_SUCCESS});
    } catch (error) {
      dispatch({type: types.DETACH_FILE_FAIL});
      dispatch(addMessage({text: error}));
    }
  };
};

export default actions;
