import module from './module';
import {deCapitalize} from 'lib/utils';
import {getRouteQuery} from 'modules/route/selectors';
import {keySelectorFactory, createKeySelector, createSelector} from 'lib/reduxUtils';
import {getIsAvailable} from 'modules/campaigns/utils';

const {selectors} = module;
const keySelector = keySelectorFactory(selectors.stateSelector);

selectors.getGraphData = keySelector('graphData');
selectors.getIsFetchingGraphData = keySelector('isFetchingGraphData');
selectors.getIsFetchingReport = keySelector('isFetchingReport ');
selectors.getIsFetchingLinks = keySelector('isFetchingLinks');
selectors.getIsSending = keySelector('isSending');
selectors.getIsSendStarted = keySelector('isSendStarted');
selectors.getIsTesting = keySelector('isTesting');
selectors.getIsTestStarted = keySelector('isTestStarted');
selectors.getIsScheduling = keySelector('isScheduling');
selectors.getIsScheduleStarted = keySelector('isScheduleStarted');
selectors.getIsCanceling = keySelector('isCanceling');
selectors.getIsValid = keySelector('isValid');
selectors.getCampaignBody = keySelector('campaignBody');

selectors.getAttachments = createKeySelector(selectors.getActiveResource, 'attachments', []);
selectors.getIsAttachingFile = keySelector('isAttachingFile', false);
selectors.getIsDetachingFile = keySelector('isDetachingFile', false);
selectors.getDetachingFileName = keySelector('detachingFileName', null);

selectors.getAvaliableCampaigns = createSelector(selectors.getSortedResources, campaigns =>
  campaigns.filter(getIsAvailable)
);

selectors.getIsEditable = createSelector(
  selectors.getActiveResource,
  campaign =>
    !['pending', 'sent', 'sending', 'badReputation', 'scheduled'].includes(
      deCapitalize(campaign.status)
    )
);

selectors.getIsScheduled = createSelector(
  selectors.getActiveResource,
  campaign => campaign.status === 'scheduled'
);

selectors.getCampaignLinks = createSelector(selectors.getActiveResource, ({links = {}}) =>
  Object.keys(links).map(id => links[id])
);

selectors.getSortedCampaignLinks = createSelector(selectors.getCampaignLinks, links =>
  [...links].sort((l1, l2) => (l2.clicksCount || 0) - (l1.clicksCount || 0))
);

selectors.getFilteredCampaigns = createSelector(
  selectors.getSortedResources,
  getRouteQuery,
  (campaigns, {filter}) => {
    if (['draft', 'sent', 'scheduled'].includes(filter)) {
      return campaigns.filter(c => c.status === filter);
    }
    return campaigns;
  }
);

export default selectors;
