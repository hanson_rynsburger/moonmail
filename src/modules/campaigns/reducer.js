import module from './module';
import types from './types';
import {createAsyncFlagReducer, createStartFlagReducer} from 'lib/reduxUtils';
import {combineReducers} from 'redux';

const {reducers, key} = module;

reducers.graphData = (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_GRAPH_DATA_SUCCESS:
      return {...state, ...action.data};
    case types.FETCH_GRAPH_DATA_REQUEST:
    case types.FETCH_GRAPH_DATA_FAIL:
      return {};
    default:
      return state;
  }
};

reducers.campaignBody = (state = '', action) => {
  switch (action.type) {
    case types.VALIDATE_AND_PARSE_SUCCESS:
      return action.html;
    case types.VALIDATE_AND_PARSE_FAIL:
      return '';
    default:
      return state;
  }
};

reducers.isValid = (state = true, action) => {
  switch (action.type) {
    case types.VALIDATE_AND_PARSE_SUCCESS:
      return true;
    case types.VALIDATE_AND_PARSE_FAIL:
      return false;
    default:
      return state;
  }
};

reducers.isFetchingGraphData = createAsyncFlagReducer(types, 'fetchGraphData');
reducers.isFetchingReport = createAsyncFlagReducer(types, 'fetchCampaignReport');
reducers.isFetchingLinks = createAsyncFlagReducer(types, 'fetchCampaignLinks');
reducers.isSending = createAsyncFlagReducer(types, 'sendCampaign');
reducers.isSendStarted = createStartFlagReducer(types, 'sendCampaign');
reducers.isTesting = createAsyncFlagReducer(types, 'testCampaign');
reducers.isTestStarted = createStartFlagReducer(types, 'testCampaign');
reducers.isScheduling = createAsyncFlagReducer(types, 'scheduleCampaign');
reducers.isScheduleStarted = createStartFlagReducer(types, 'scheduleCampaign');
reducers.isCanceling = createAsyncFlagReducer(types, 'cancelScheduledCampaign');
reducers.isAttachingFile = createAsyncFlagReducer(types, 'attachFile');
reducers.isDetachingFile = createAsyncFlagReducer(types, 'detachFile');

reducers.detachingFileName = (state = null, action) => {
  switch (action.type) {
    case types.DETACH_FILE_REQUEST:
      return action.fileName;
    case types.DETACH_FILE_SUCCESS:
    case types.DETACH_FILE_FAIL:
      return null;
    default:
      return state;
  }
};

export default {
  key,
  reducer: combineReducers(reducers)
};
