import moment from 'moment';
import {humanize} from 'lib/utils';

export const getParamsFromFilter = filter => {
  switch (filter) {
    case 'draft':
    case 'sent':
    case 'scheduled':
      return {status: filter};
    case 'archived':
      return {archived: true};
    default:
      return {};
  }
};

export const getIsRecentlySent = ({sentAt}) => {
  if (!sentAt) return false;
  return moment().diff(moment.unix(sentAt), 'hours') < 24;
};

export const getStatuses = campaign => {
  const status = campaign.status || 'something went wrong';
  const result = {};
  const isRecentlySent = getIsRecentlySent(campaign);
  result.isSent = isRecentlySent ? campaign.sentCount > 0 : ['sent', 'sending'].includes(status);
  result.isPending = isRecentlySent ? !campaign.sentCount : status === 'pending';
  result.isDraft = status === 'draft';
  result.isScheduled = status === 'scheduled';
  result.isArchived = campaign.archived;
  result.isArchivable = !result.isArchived && result.isSent;
  result.isEditable = !result.isSent && !result.isPending && !result.isScheduled;
  result.isError = !result.isDraft && result.isEditable;
  result.isPaymentError = status.includes('paymentGatewayError');
  result.isBadReputation = status === 'badReputation';
  result.isLimitReached = status === 'limitReached';
  result.isNotReady = status === 'campaignNotReady';
  result.statusText = status.replace('stripe', '');
  if (!result.isSent && status === 'sent') result.statusText = 'sending';
  result.statusText = result.isArchived ? 'Archived' : humanize(result.statusText);
  return result;
};

export const getIsAvailable = ({sentAt, status}) => {
  return ['sent', 'sending'].includes(status) && moment().diff(moment.unix(sentAt), 'hours') > 1;
};
