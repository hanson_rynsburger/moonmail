import * as types from './types';
import * as schema from 'schema';
import {getMessages} from './selectors';
import {copyTextToClipboard} from 'lib/utils';
import {normalize} from 'normalizr';
import _debug from 'debug';

const logError = _debug('app:error');

let nId = 0;
export const addMessage = (props = {}) => {
  return (dispatch, getState) => {
    if (!props.text) return;
    if (props.text.constructor !== String) {
      throw props.text;
    }
    const activeMessages = getMessages(getState());
    if (activeMessages.some(msg => msg.text === props.text)) {
      return;
    }
    const message = {
      id: nId,
      delay: 5000,
      style: 'error',
      ...props
    };
    setTimeout(() => {
      dispatch(removeMessage(message.id));
    }, message.delay);
    const normalized = normalize(message, schema.message);
    dispatch({
      type: types.ADD_MESSAGE,
      byId: normalized.entities.messages,
      messageId: normalized.result
    });
    nId++;
    if (message.style === 'error') {
      logError(props.text);
    }
  };
};

export const removeMessage = messageId => ({
  type: types.REMOVE_MESSAGE,
  messageId
});

export const copyToClipboard = (text, name) => dispatch => {
  copyTextToClipboard(text);
  dispatch(
    addMessage({
      text: `${name} copied to clipboard`,
      style: 'success',
      delay: 1000
    })
  );
};
