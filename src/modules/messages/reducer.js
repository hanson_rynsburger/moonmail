import * as types from './types';
import {combineReducers} from 'redux';

export const key = 'messages';

const ids = (state = [], action) => {
  let index;
  switch (action.type) {
    case types.ADD_MESSAGE:
      return [...state, action.messageId];
    case types.REMOVE_MESSAGE:
      index = state.indexOf(action.messageId);
      return [...state.slice(0, index), ...state.slice(index + 1)];
    default:
      return state;
  }
};

const byId = (state = {}, action) => {
  let stateCopy;
  switch (action.type) {
    case types.ADD_MESSAGE:
      return {...state, ...action.byId};
    case types.REMOVE_MESSAGE:
      stateCopy = {...state};
      delete stateCopy[action.messageId];
      return stateCopy;
    default:
      return state;
  }
};

export const reducer = combineReducers({
  ids,
  byId
});

export default {key, reducer};
