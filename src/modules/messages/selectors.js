import {createSelector} from 'reselect';
import {key} from './reducer';

const messagesSelector = state => state[key];
export const getMessages = createSelector(messagesSelector, messages =>
  messages.ids.map(id => messages.byId[id])
);
