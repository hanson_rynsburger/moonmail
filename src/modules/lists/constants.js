/* eslint max-len:0 */
export const LIST_DEFAULT_METAFIELDS = ['name', 'surname'];
export const LIST_STATUS_DESCRIPTION = {
  processing:
    "MoonMail processes a background check to all recipients just after you have uploaded a list. This check is based on format validation of email addresses & artificial intelligence processes. While this check is on the works, you won't be able to use this list. This process can take up to 24 hours depending on the total amount of recipients to be processed.",
  importing:
    'Importing recipients. It can take a while, depending on the total amount of recipients to be processed.'
};
