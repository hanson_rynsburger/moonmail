import module from './module';
import {combineReducers} from 'redux';
import {createAsyncFlagReducer} from 'lib/reduxUtils';
import types from './types';

const {reducers, key} = module;

reducers.isExporting = createAsyncFlagReducer(types, 'export');

reducers.totalRecipients = (state = null, action) => {
  switch (action.type) {
    case types.COUNT_RECIPIENTS_SUCCESS:
      return action.totalRecipients || null;
    case types.COUNT_RECIPIENTS_FAIL:
      return null;
    default:
      return state;
  }
};

export default {
  key,
  reducer: combineReducers(reducers)
};
