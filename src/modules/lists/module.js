import getCollection from 'modules/common';
import {schema} from 'normalizr';
import {objectToArray} from 'lib/utils';

export const resource = new schema.Entity(
  'resources',
  {},
  {
    processStrategy(entity) {
      entity.status = null;
      if (entity.importStatus) {
        entity.imports = objectToArray(entity.importStatus).sort(
          (a, b) => b.createdAt - a.createdAt
        );
        entity.isImporting = entity.imports.some(i => i.importing);
        if (entity.isImporting) {
          entity.status = 'importing';
        }
      }
      if (entity.exports) {
        entity.exports = objectToArray(entity.exports).sort((a, b) => b.createdAt - a.createdAt);
      }
      entity.isProcessing = entity.processed === false;
      if (entity.isProcessing) entity.status = 'processing';
      return entity;
    }
  }
);

export default getCollection('list', {
  schema: {
    resource,
    arrayOfResources: [resource]
  }
});
