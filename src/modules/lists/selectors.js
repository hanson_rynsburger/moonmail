import {createSelector, keySelectorFactory} from 'lib/reduxUtils';
import module from './module';
import {mergeArrays} from 'lib/utils';
import campaignsSelectors from 'modules/campaigns/selectors';
import automationSelectors from 'modules/automations/selectors';
import {LIST_DEFAULT_METAFIELDS} from 'modules/lists/constants';
import {CAMPAIGN_DEFAULT_METAFIELDS} from 'modules/campaigns/constants';
import {EMPTY_ARRAY} from 'lib/constants';
import {getLimits} from 'modules/profile/selectors';

const {selectors} = module;
const keySelector = keySelectorFactory(selectors.stateSelector);

selectors.getResources = createSelector(selectors.getResources, lists =>
  lists.filter(l => !l.archived)
);

selectors.getSortedResources = createSelector(selectors.getSortedResources, lists =>
  lists.filter(l => !l.archived)
);

selectors.getActiveListMetaFields = createSelector(
  selectors.getActiveResource,
  list => list.metadataAttributes || EMPTY_ARRAY
);

selectors.getListMetaFields = createSelector(selectors.getActiveListMetaFields, fields =>
  mergeArrays(LIST_DEFAULT_METAFIELDS, fields)
);

selectors.getAvailableLists = createSelector(selectors.getSortedResources, lists =>
  lists.filter(l => l.subscribedCount > 0 && !l.isProcessing)
);

selectors.getActiveCampaignLists = createSelector(
  selectors.getResources,
  campaignsSelectors.getActiveResource,
  (lists, campaign) => {
    if (!campaign.listIds) return EMPTY_ARRAY;
    return lists.filter(l => campaign.listIds.includes(l.id));
  }
);

selectors.getActiveCampaignMetaData = createSelector(selectors.getActiveCampaignLists, lists =>
  mergeArrays(CAMPAIGN_DEFAULT_METAFIELDS, ...lists.map(l => l.metadataAttributes))
);

selectors.getActiveAutomationLists = createSelector(
  selectors.getResources,
  automationSelectors.getActiveResource,
  (lists, automation) => {
    if (!automation.listId) return EMPTY_ARRAY;
    return lists.filter(l => automation.listId === l.id);
  }
);

selectors.getActiveActionMetaData = createSelector(selectors.getActiveAutomationLists, lists =>
  mergeArrays(CAMPAIGN_DEFAULT_METAFIELDS, ...lists.map(l => l.metadataAttributes))
);

selectors.getIsExporting = keySelector('isExporting');
selectors.getTotalRecipients = keySelector('totalRecipients');

selectors.getRemainingRecipients = createSelector(
  getLimits,
  selectors.getTotalRecipients,
  (limits, totalRecipients) => {
    if (limits.recipientsInTotal === null) return Infinity;
    const limit = limits.recipientsInTotal || 2000;
    const count = limit - totalRecipients;
    return count > 0 ? count : 0;
  }
);

export default selectors;
