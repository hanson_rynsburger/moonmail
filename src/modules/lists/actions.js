import module from './module';
import {addMessage} from 'modules/messages/actions';
import {push} from 'react-router-redux';
import * as api from 'lib/api';
import types from './types';
import {triggerEventAction} from 'lib/remoteUtils';

const {actions} = module;

actions.fetchLists = (params, forceRefresh) =>
  actions.fetchResources(
    {
      fields: ['confirmationEmailBody', 'importStatus', 'userId', 'senderId'].join(','),
      include_fields: false,
      ...params
    },
    forceRefresh
  );

actions.fetchList = (listId, params, forceRefresh) =>
  actions.fetchResource(
    listId,
    {
      fields: 'confirmationEmailBody',
      include_fields: false,
      ...params
    },
    forceRefresh
  );

actions.createList = data => {
  return async dispatch => {
    const list = await dispatch(actions.createResource(data, true));
    dispatch(triggerEventAction('app.userCreatedList'));
    dispatch(push(`/lists/${list.id}`));
    return list;
  };
};

actions.exportList = listId => {
  return async dispatch => {
    dispatch({
      type: types.EXPORT_REQUEST
    });
    try {
      await api.exportList(listId);
      dispatch({
        type: types.EXPORT_SUCCESS
      });
      dispatch(triggerEventAction('app.userExportedList'));
      dispatch(
        addMessage({
          text: "Csv file will be available for download as soon as it's ready",
          style: 'success'
        })
      );
    } catch (error) {
      dispatch({
        type: types.EXPORT_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

actions.fetchRecipientsCount = () => {
  return async dispatch => {
    dispatch({
      type: types.COUNT_RECIPIENTS_REQUEST
    });
    try {
      const {totalRecipients} = await api.fetchRecipientsCount();
      dispatch({
        type: types.COUNT_RECIPIENTS_SUCCESS,
        totalRecipients
      });
    } catch (error) {
      dispatch({
        type: types.COUNT_RECIPIENTS_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export default actions;
