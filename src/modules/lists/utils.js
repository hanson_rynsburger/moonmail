import {RATE_LIMITS} from 'lib/constants';

export const negativeStatuses = ['unsubscribed', 'complained', 'bounced'];

export const obfuscateEmail = (email, status) => {
  if (email && negativeStatuses.includes(status)) {
    return `${email.split('@')[0]}@${status}-invalid-recipient`;
  }

  return email;
};

export const getIsBadReputation = ({bouncedCount = 0, complainedCount = 0, total = 0}) => {
  return (
    bouncedCount / total * 100 >= RATE_LIMITS.bounceRate ||
    complainedCount / total * 100 >= RATE_LIMITS.complaintRate
  );
};
