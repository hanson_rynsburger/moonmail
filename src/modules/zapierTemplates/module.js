import getCollection from 'modules/common';

export default getCollection('zapierTemplate', {
  include: ['fetchAll']
});
