import {getRouteParams, getRouteQuery} from 'modules/route/selectors';
import {EMPTY_OBJECT} from 'lib/constants';
import {includesAny, capitalize} from 'lib/utils';
import {createSelector, createStateSelector, keySelectorFactory} from 'lib/reduxUtils';
import {DEFAULT_ACTIONS} from './constants';

export default ({name = 'resource', include = DEFAULT_ACTIONS}) => {
  const resource = capitalize(name);
  const stateSelector = createStateSelector(`${name}s`);
  const selectors = {stateSelector};
  const keySelector = keySelectorFactory(stateSelector);
  if (include.includes('fetchAll')) {
    selectors.getIds = keySelector('ids');
    selectors.getResourcesMap = keySelector('byId');
    selectors.getIsFetchingAll = keySelector('isFetchingAll');
    selectors.getIsUpToDate = keySelector('isUpToDate');
    selectors.getParams = keySelector('params');

    selectors.getResources = createSelector(stateSelector, state =>
      state.ids.map(id => state.byId[id])
    );
    selectors.getSortedResources = createSelector(selectors.getResources, resources =>
      resources.sort((a, b) => b.createdAt - a.createdAt)
    );
    selectors.getReverseSortedResources = createSelector(selectors.getSortedResources, resources =>
      resources.reverse()
    );
    selectors.getFilteredResources = createSelector(
      selectors.getSortedResources,
      getRouteQuery,
      (resources, {status}) => (status ? resources.filter(c => c.status === status) : resources)
    );
    selectors.getResourceById = (state, resourceId) =>
      selectors.getResourcesMap(state)[resourceId] || EMPTY_OBJECT;
    selectors.getResourceGetter = state => resourceId =>
      selectors.getResourcesMap(state)[resourceId] || EMPTY_OBJECT;
    selectors.getActiveId = createSelector(getRouteParams, params => params[`${name}Id`]);

    selectors.getActiveResource = createSelector(
      stateSelector,
      selectors.getActiveId,
      (state, id) => state.byId[id] || EMPTY_OBJECT
    );

    selectors.getDefaultResource = createSelector(
      selectors.getResources,
      resources => resources[0] || EMPTY_OBJECT
    );

    selectors.getDefaultResourceId = createSelector(
      selectors.getDefaultResource,
      resource => resource.id
    );

    selectors.getPagination = createSelector(
      selectors.getParams,
      ({size, from = 0, total, nextPage, prevPage}) => ({
        next: nextPage || from + size < total ? from + size : null,
        prev: prevPage || from - size >= 0 ? from - size : null,
        total
      })
    );

    selectors[`get${resource}sMap`] = selectors.getResourcesMap;
    selectors[`get${resource}s`] = selectors.getResources;
    selectors[`getSorted${resource}s`] = selectors.getSortedResources;
    selectors[`getReverseSorted${resource}s`] = selectors.getReverseSortedResources;
    selectors[`get${resource}ById`] = selectors.getResourceById;
    selectors[`get${resource}Getter`] = selectors.getResourceGetter;
    selectors[`getDefault${resource}`] = selectors.getDefaultResource;
    selectors[`getDefault${resource}Id`] = selectors.getDefaultResourceId;
  }

  if (include.includes('fetch')) {
    selectors.getIsFetching = keySelector('isFetching');
  }

  if (include.includes('create')) {
    selectors.getIsCreating = keySelector('isCreating');
    selectors.getIsCreateStarted = keySelector('isCreateStarted');
  }

  if (include.includes('update')) {
    selectors.getIsUpdating = keySelector('isUpdating');
    selectors.getIsUpdateStarted = keySelector('isUpdateStarted');
  }

  if (include.includes('delete')) {
    selectors.getIsDeleting = keySelector('isDeleting');
    selectors.getIsDeleteStarted = keySelector('isDeleteStarted');
  }

  if (includesAny(include, 'delete', 'update', 'select')) {
    selectors.getSelectedId = keySelector('selectedId');
    selectors.getIsSelected = createSelector(selectors.getSelectedId, id => !!id);
    selectors.getSelectedResource = createSelector(
      stateSelector,
      selectors.getSelectedId,
      (state, id) => state.byId[id] || EMPTY_OBJECT
    );

    selectors[`getSelected${resource}`] = selectors.getSelectedResource;
  }

  return selectors;
};
