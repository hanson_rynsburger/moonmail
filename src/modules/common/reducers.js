import {mergeArrays, includesAny, removeFromArray} from 'lib/utils';
import {createStartFlagReducer, createAsyncFlagReducer} from 'lib/reduxUtils';
import {EMPTY_OBJECT, EMPTY_ARRAY} from 'lib/constants';
import {DEFAULT_ACTIONS} from './constants';

export default ({types, include = DEFAULT_ACTIONS}) => {
  const reducers = {};
  const createAsyncReducer = (actionName, initialState) =>
    createAsyncFlagReducer(types, actionName, initialState);
  const createStartReducer = actionName => createStartFlagReducer(types, actionName);
  if (include.includes('fetchAll')) {
    reducers.isFetchingAll = createAsyncReducer('fetchAll');
    reducers.ids = (state = EMPTY_ARRAY, action) => {
      switch (action.type) {
        case types.FETCH_ALL_SUCCESS:
          return [...action.ids];
        case types.FETCH_NEXT_SUCCESS:
          return state.concat(action.ids);
        case types.FETCH_SUCCESS:
        case types.CREATE_SUCCESS:
          return mergeArrays(state, [action.resourceId]);
        case types.DELETE_SUCCESS:
          return removeFromArray(state, action.resourceId);
        case types.CLEAR_ALL:
          return EMPTY_ARRAY;
        default:
          return state;
      }
    };
    reducers.byId = (state = EMPTY_OBJECT, action) => {
      let stateCopy;
      switch (action.type) {
        case types.FETCH_ALL_SUCCESS:
          return action.byId || EMPTY_OBJECT;
        case types.FETCH_NEXT_SUCCESS:
          return {...state, ...action.byId};
        case types.CREATE_SUCCESS:
          return {
            ...state,
            [action.resourceId]: {
              ...action.byId[action.resourceId],
              _isUpToDate: true
            }
          };
        case types.FETCH_SUCCESS:
          return {
            ...state,
            [action.resourceId]: {
              ...state[action.resourceId],
              ...action.byId[action.resourceId],
              _isUpToDate: true
            }
          };
        case types.UPDATE_SUCCESS:
          return {
            ...state,
            [action.resourceId]: {
              ...state[action.resourceId],
              ...action.byId[action.resourceId]
            }
          };
        case types.DELETE_SUCCESS:
          stateCopy = {...state};
          delete stateCopy[action.resourceId];
          return stateCopy;
        case types.CLEAR_ALL:
          return EMPTY_OBJECT;
        default:
          return state;
      }
    };
    reducers.isUpToDate = (state = false, action) => {
      switch (action.type) {
        case types.FETCH_ALL_SUCCESS:
          return true;
        case types.CLEAR_ALL:
          return false;
        default:
          return state;
      }
    };

    reducers.params = (state = EMPTY_OBJECT, action) => {
      switch (action.type) {
        case types.FETCH_ALL_SUCCESS:
          return action.params || EMPTY_OBJECT;
        case types.CLEAR_ALL:
          return EMPTY_OBJECT;
        default:
          return state;
      }
    };
  }

  if (include.includes('fetch')) {
    reducers.isFetching = createAsyncReducer('fetch');
  }

  if (include.includes('create')) {
    reducers.isCreating = createAsyncReducer('create');
    reducers.isCreateStarted = createStartReducer('create');
  }

  if (include.includes('update')) {
    reducers.isUpdating = createAsyncReducer('update');
    reducers.isUpdateStarted = createStartReducer('update');
  }

  if (include.includes('delete')) {
    reducers.isDeleting = createAsyncReducer('delete');
    reducers.isDeleteStarted = createStartReducer('delete');
  }

  if (includesAny(include, 'delete', 'update', 'select')) {
    reducers.selectedId = (state = null, action) => {
      switch (action.type) {
        case types.SELECT:
          return action.resourceId;
        case types.CLEAR_SELECTED:
          return null;
        default:
          return state;
      }
    };
  }

  return reducers;
};
