import {getTypeName} from 'lib/reduxUtils';
import {includesAny} from 'lib/utils';
import {DEFAULT_ACTIONS} from './constants';

export default ({name = 'resource', include = DEFAULT_ACTIONS}) => {
  const typeName = getTypeName(name);
  const types = {};

  if (include.includes('fetchAll')) {
    types.FETCH_ALL_REQUEST = `FETCH_${typeName}S_REQUEST`;
    types.FETCH_ALL_SUCCESS = `FETCH_${typeName}S_SUCCESS`;
    types.FETCH_ALL_FAIL = `FETCH_${typeName}S_FAIL`;
    types.FETCH_NEXT_REQUEST = `FETCH_NEXT_${typeName}S_REQUEST`;
    types.FETCH_NEXT_SUCCESS = `FETCH_NEXT_${typeName}S_SUCCESS`;
    types.FETCH_NEXT_FAIL = `FETCH_NEXT_${typeName}S_FAIL`;
    types.CLEAR_ALL = `CLEAR_ALL_${typeName}S`;
  }

  if (include.includes('fetch')) {
    types.FETCH_REQUEST = `FETCH_${typeName}_REQUEST`;
    types.FETCH_SUCCESS = `FETCH_${typeName}_SUCCESS`;
    types.FETCH_FAIL = `FETCH_${typeName}_FAIL`;
  }

  if (include.includes('create')) {
    types.CREATE_START = `CREATE_${typeName}_START`;
    types.CREATE_CANCEL = `CREATE_${typeName}_CANCEL`;
    types.CREATE_REQUEST = `CREATE_${typeName}_REQUEST`;
    types.CREATE_SUCCESS = `CREATE_${typeName}_SUCCESS`;
    types.CREATE_FAIL = `CREATE_${typeName}_FAIL`;
  }

  if (include.includes('update')) {
    types.UPDATE_START = `UPDATE_${typeName}_START`;
    types.UPDATE_CANCEL = `UPDATE_${typeName}_CANCEL`;
    types.UPDATE_REQUEST = `UPDATE_${typeName}_REQUEST`;
    types.UPDATE_SUCCESS = `UPDATE_${typeName}_SUCCESS`;
    types.UPDATE_FAIL = `UPDATE_${typeName}_FAIL`;
  }

  if (include.includes('delete')) {
    types.DELETE_START = `DELETE_${typeName}_START`;
    types.DELETE_CANCEL = `DELETE_${typeName}_CANCEL`;
    types.DELETE_REQUEST = `DELETE_${typeName}_REQUEST`;
    types.DELETE_SUCCESS = `DELETE_${typeName}_SUCCESS`;
    types.DELETE_FAIL = `DELETE_${typeName}_FAIL`;
  }

  if (includesAny(include, 'delete', 'update', 'select')) {
    types.SELECT = `SELECT_${typeName}`;
    types.CLEAR_SELECTED = `CLEAR_SELECTED_${typeName}`;
  }

  return types;
};
