import getTypes from './types';
import getActions from './actions';
import getSelectors from './selectors';
import getReducers from './reducers';
import {combineReducers} from 'redux';

export default function(name = 'resource', {include, schema, customName, fetchNext = true} = {}) {
  const types = getTypes({name, include});
  const selectors = getSelectors({name, include});
  const actions = getActions({name, types, selectors, include, schema, fetchNext, customName});
  const reducers = getReducers({types, include});
  return {
    types,
    selectors,
    actions,
    reducers,
    reducer: combineReducers(reducers),
    key: `${name}s`
  };
}
