import * as api from 'lib/api';
import {addMessage} from 'modules/messages/actions';
import {capitalize, includesAny, humanize, isObject} from 'lib/utils';
import {normalizeResource, normalizeResources} from 'lib/reduxUtils';
import {DEFAULT_ACTIONS} from './constants';
import {EMPTY_OBJECT} from 'lib/constants';

export default ({
  name = 'resource',
  types,
  selectors,
  fetchNext,
  customName,
  include = DEFAULT_ACTIONS,
  schema = {}
}) => {
  const actions = {};
  const resource = capitalize(name);
  const resourceName = customName || humanize(name);
  const getLastFlag = args => {
    const lastArg = args.slice(-1).pop();
    return typeof lastArg === 'boolean' ? lastArg : false;
  };
  const getParams = args => args.find(isObject) || EMPTY_OBJECT;

  const fetchNextResources = (page, args) => {
    return async dispatch => {
      if (!page) return;
      dispatch({
        type: types.FETCH_NEXT_REQUEST
      });
      try {
        const params = getParams(args);
        params.page = page;
        const res = await api[`fetch${resource}s`](...args);
        dispatch({
          type: types.FETCH_NEXT_SUCCESS,
          ...normalizeResources(res)
        });
        if (res.nextPage) {
          dispatch(fetchNextResources(res.nextPage, args));
        }
      } catch (error) {
        dispatch({
          type: types.FETCH_NEXT_FAIL
        });
        dispatch(
          addMessage({
            text: error,
            onRetry: () => {
              dispatch(actions.fetchResources(...args));
            }
          })
        );
        return Promise.reject(error);
      }
    };
  };

  if (include.includes('fetchAll')) {
    actions.loadResourcesLocally = (res, params) => ({
      type: types.FETCH_ALL_SUCCESS,
      params: {
        total: res.total,
        nexPage: res.nextPage,
        prevPage: res.prevPage,
        from: 0,
        ...params
      },
      ...normalizeResources(res, schema.arrayOfResources)
    });

    actions.fetchResources = (...args) => {
      const needsToRefresh = getLastFlag(args);
      return async (dispatch, getState) => {
        const state = getState();
        const isUpToDate = selectors.getIsUpToDate(state);
        if (!needsToRefresh && isUpToDate) {
          return selectors.getResources(state);
        }
        dispatch({
          type: types.FETCH_ALL_REQUEST
        });
        try {
          const params = getParams(args);
          const res = await api[`fetch${resource}s`](...args);
          dispatch(actions.loadResourcesLocally(res, params));
          if (fetchNext) {
            dispatch(fetchNextResources(res.nextPage, args));
          }
          return res.items;
        } catch (error) {
          dispatch({
            type: types.FETCH_ALL_FAIL
          });
          dispatch(
            addMessage({
              text: error,
              onRetry: () => {
                dispatch(actions.fetchResources(...args));
              }
            })
          );
          return Promise.reject(error);
        }
      };
    };

    actions.clearResources = () => ({
      type: types.CLEAR_ALL
    });

    actions[`load${resource}sLocally`] = actions.loadResourcesLocally;
    actions[`fetch${resource}s`] = actions.fetchResources;
    actions[`clear${resource}s`] = actions.clearResources;
  }

  if (include.includes('fetch')) {
    actions.fetchResource = (id, ...args) => {
      const needsToRefresh = getLastFlag(args);
      return async (dispatch, getState) => {
        const state = getState();
        const localResource = selectors.getResourceById(state, id);
        if (!needsToRefresh && localResource._isUpToDate) {
          return localResource;
        }
        dispatch({
          type: types.FETCH_REQUEST
        });
        try {
          const res = await api[`fetch${resource}`](id, ...args);
          dispatch({
            type: types.FETCH_SUCCESS,
            ...normalizeResource(res, schema.resource)
          });
          return res;
        } catch (error) {
          dispatch({
            type: types.FETCH_FAIL
          });
          dispatch(
            addMessage({
              text: error,
              onRetry: () => {
                dispatch(actions.fetchResource(id, ...args));
              }
            })
          );
          return Promise.reject(error);
        }
      };
    };

    actions[`fetch${resource}`] = actions.fetchResource;
  }

  if (include.includes('create')) {
    actions.createResourceStart = () => ({
      type: types.CREATE_START
    });

    actions.createResourceCancel = () => ({
      type: types.CREATE_CANCEL
    });

    actions.createResourceLocally = res => ({
      type: types.CREATE_SUCCESS,
      ...normalizeResource(res, schema.resource)
    });

    actions.createResource = (...args) => {
      const isSilent = getLastFlag(args);
      return async dispatch => {
        dispatch({
          type: types.CREATE_REQUEST
        });
        try {
          const res = await api[`create${resource}`](...args);
          dispatch(actions.createResourceLocally(res));
          !isSilent &&
            dispatch(
              addMessage({
                text: `${resourceName} has been created.`,
                style: 'success'
              })
            );
          return res;
        } catch (error) {
          dispatch({
            type: types.CREATE_FAIL
          });
          dispatch(
            addMessage({
              text: error,
              onRetry: () => {
                dispatch(actions.createResource(...args));
              }
            })
          );
          return Promise.reject(error);
        }
      };
    };

    actions[`create${resource}Start`] = actions.createResourceStart;
    actions[`create${resource}Cancel`] = actions.createResourceCancel;
    actions[`create${resource}`] = actions.createResource;
  }

  if (include.includes('update')) {
    actions.updateResourceStart = id => {
      return dispatch => {
        dispatch(actions.selectResource(id));
        dispatch({type: types.UPDATE_START});
      };
    };

    actions.updateResourceCancel = () => {
      return dispatch => {
        dispatch({type: types.UPDATE_CANCEL});
        dispatch(actions.clearSelectedResource());
      };
    };
    actions.updateResourceDataLocally = res => {
      return {
        type: types.UPDATE_SUCCESS,
        ...normalizeResource(res, schema.resource)
      };
    };

    actions.updateResourceLocally = (id, res) => actions.updateResourceDataLocally({id, ...res});

    actions.updateResource = (id, ...args) => {
      const isSilent = getLastFlag(args);
      return async dispatch => {
        dispatch({
          type: types.UPDATE_REQUEST
        });
        try {
          const res = await api[`update${resource}`](id, ...args);
          dispatch(actions.updateResourceDataLocally(res));
          dispatch(actions.clearSelectedResource());
          !isSilent &&
            dispatch(
              addMessage({
                text: `${resourceName} has been updated.`,
                style: 'success'
              })
            );
          return res;
        } catch (error) {
          dispatch({
            type: types.UPDATE_FAIL
          });
          dispatch(
            addMessage({
              text: error,
              onRetry: () => {
                dispatch(actions.updateResource(id, ...args));
              }
            })
          );
          return Promise.reject(error);
        }
      };
    };

    actions[`update${resource}Start`] = actions.updateResourceStart;
    actions[`update${resource}Cancel`] = actions.updateResourceCancel;
    actions[`update${resource}Locally`] = actions.updateResourceLocally;
    actions[`update${resource}DataLocally`] = actions.updateResourceDataLocally;
    actions[`update${resource}`] = actions.updateResource;
  }

  if (include.includes('delete')) {
    actions.deleteResourceStart = id => {
      return dispatch => {
        dispatch(actions.selectResource(id));
        dispatch({type: types.DELETE_START});
      };
    };

    actions.deleteResourceCancel = () => {
      return dispatch => {
        dispatch({type: types.DELETE_CANCEL});
        dispatch(actions.clearSelectedResource());
      };
    };

    actions.deleteResourceLocally = resourceId => ({
      type: types.DELETE_SUCCESS,
      resourceId
    });

    actions.deleteResource = (id, ...args) => {
      const isSilent = getLastFlag(args);
      return async dispatch => {
        dispatch({
          type: types.DELETE_REQUEST
        });
        try {
          const res = await api[`delete${resource}`](id, ...args);
          dispatch(actions.deleteResourceLocally(id));
          dispatch(actions.clearSelectedResource());
          !isSilent &&
            dispatch(
              addMessage({
                text: `${resourceName} has been deleted.`,
                style: 'success'
              })
            );
          return res;
        } catch (error) {
          dispatch({
            type: types.DELETE_FAIL
          });
          dispatch(
            addMessage({
              text: error,
              onRetry: () => {
                dispatch(actions.deleteResource(id, ...args));
              }
            })
          );
          return Promise.reject(error);
        }
      };
    };

    actions[`delete${resource}Start`] = actions.deleteResourceStart;
    actions[`delete${resource}Cancel`] = actions.deleteResourceCancel;
    actions[`delete${resource}Locally`] = actions.deleteResourceLocally();
    actions[`delete${resource}`] = actions.deleteResource;
  }
  if (includesAny(include, 'delete', 'update', 'select')) {
    actions.selectResource = id => ({
      type: types.SELECT,
      resourceId: id
    });
    actions.clearSelectedResource = () => ({
      type: types.CLEAR_SELECTED
    });

    actions[`select${resource}`] = actions.selectResource;
    actions[`clearSelected${resource}`] = actions.clearSelectedResource;
  }
  return actions;
};
