import module from './module';
import {createSelector, keySelectorFactory} from 'lib/reduxUtils';
import {getRouteQuery} from 'modules/route/selectors';

const {selectors} = module;
const keySelector = keySelectorFactory(selectors.stateSelector);
selectors.getIsPausing = keySelector('isPausing');
selectors.getIsActivating = keySelector('isActivating');
selectors.getIsActivateStarted = keySelector('isActivateStarted');
selectors.getIsActive = createSelector(
  selectors.getActiveResource,
  automation => automation.status === 'active'
);
selectors.getIsEditable = createSelector(
  selectors.getActiveResource,
  automation => automation.status !== 'active'
);

selectors.getFilteredAutomations = createSelector(
  selectors.getSortedResources,
  getRouteQuery,
  (automations, query) => {
    switch (query.filter) {
      case 'archived':
        return automations.filter(a => a.archived);
      case 'active':
        return automations.filter(a => !a.archived && a.status === 'active');
      case 'paused':
        return automations.filter(a => !a.archived && a.status === 'paused');
      default:
        return automations.filter(a => !a.archived);
    }
  }
);

export default selectors;
