import module from './module';
import * as api from 'lib/api';
import types from './types';
import actionActions from 'modules/automationActions/actions';
import {addMessage} from 'modules/messages/actions';

const {actions} = module;

actions.activateAutomationStart = () => ({
  type: types.ACTIVATE_START
});

actions.activateAutomationCancel = () => ({
  type: types.ACTIVATE_CANCEL
});

actions.activateAutomation = automationId => {
  return async dispatch => {
    dispatch({
      type: types.ACTIVATE_REQUEST
    });
    try {
      const automation = await api.activateAutomation(automationId);
      dispatch(actions.updateResourceLocally(automationId, automation));
      dispatch(actionActions.activateAllActions());
      dispatch({
        type: types.ACTIVATE_SUCCESS
      });
      dispatch(
        addMessage({
          text: 'Automation has been activated',
          style: 'success'
        })
      );
    } catch (error) {
      dispatch({
        type: types.ACTIVATE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

actions.pauseAutomation = automationId => {
  return async dispatch => {
    dispatch({
      type: types.PAUSE_REQUEST
    });
    try {
      const automation = await api.pauseAutomation(automationId);
      dispatch(actions.updateResourceLocally(automationId, automation));
      dispatch(actionActions.pauseAllActions());
      dispatch({
        type: types.PAUSE_SUCCESS
      });
      dispatch(
        addMessage({
          text: 'Automation has been paused',
          style: 'success'
        })
      );
    } catch (error) {
      dispatch({
        type: types.PAUSE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

actions.setAutomationArchived = (automationId, archived) => {
  return dispatch => {
    dispatch(actions.updateResource(automationId, {archived}, true));
  };
};

export default actions;
