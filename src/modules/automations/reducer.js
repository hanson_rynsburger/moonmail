import module from './module';
import types from './types';
import {combineReducers} from 'redux';
import {createAsyncFlagReducer, createStartFlagReducer} from 'lib/reduxUtils';

const {reducers, key} = module;
reducers.isPausing = createAsyncFlagReducer(types, 'pause');
reducers.isActivating = createAsyncFlagReducer(types, 'activate');
reducers.isActivateStarted = createStartFlagReducer(types, 'activate');

export default {
  key,
  reducer: combineReducers(reducers)
};
