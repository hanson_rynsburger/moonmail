import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as selectors from 'modules/profile/selectors';

export default function(ComposedComponent) {
  class RequireSes extends Component {
    static contextTypes = {
      router: PropTypes.object
    };

    static propTypes = {
      isUnverifiedSesUser: PropTypes.bool,
      isProfileFetching: PropTypes.bool
    };

    componentWillMount() {
      const {isUnverifiedSesUser, isProfileFetching} = this.props;
      if (!isProfileFetching && isUnverifiedSesUser) {
        this.context.router.replace('/getting-started-aws-ses');
      }
    }

    componentWillUpdate(nextProps) {
      const {isUnverifiedSesUser, isProfileFetching} = nextProps;
      if (!isProfileFetching && isUnverifiedSesUser) {
        this.context.router.replace('/getting-started-aws-ses');
      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  const mapStateToProps = state => ({
    isUnverifiedSesUser: selectors.getIsUnverifiedSesUser(state),
    isProfileFetching: selectors.getIsFetching(state)
  });

  return connect(mapStateToProps)(RequireSes);
}
