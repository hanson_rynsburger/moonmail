import React, {Component} from 'react';
import StaticMessage from 'components/StaticMessage';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false
    };
  }

  componentDidCatch(error) {
    this.setState({hasError: true});
    //eslint-disable-next-line no-console
    console.error(error);
  }

  render() {
    if (this.state.hasError) {
      return (
        <StaticMessage type="error" active header="Oops! Something went wrong.">
          We are already working to fix this problem.
        </StaticMessage>
      );
    }
    return this.props.children;
  }
}

export default ErrorBoundary;
