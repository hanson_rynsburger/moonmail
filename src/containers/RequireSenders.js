import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as selectors from 'modules/profile/selectors';
import NotFound from 'routes/NotFound/components/NotFoundView';

export default function(ComposedComponent) {
  class RequireSenders extends Component {
    static propTypes = {
      hasSenders: PropTypes.bool.isRequired
    };

    render() {
      if (!this.props.hasSenders) {
        return <NotFound />;
      }
      return <ComposedComponent {...this.props} />;
    }
  }

  const mapStateToProps = state => ({
    hasSenders: selectors.getHasSenders(state)
  });

  return connect(mapStateToProps)(RequireSenders);
}
