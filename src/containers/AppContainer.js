import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Router from 'react-router/lib/Router';
import applyRouterMiddleware from 'react-router/lib/applyRouterMiddleware';
import useScroll from 'react-router-scroll/lib/useScroll';
import {Provider} from 'react-redux';
import {syncRouteParams} from 'modules/route/utils';

class AppContainer extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired,
    routerKey: PropTypes.number,
    store: PropTypes.object.isRequired
  };

  render() {
    const {history, routes, routerKey, store} = this.props;
    syncRouteParams(store, routes, history);
    return (
      <Provider store={store}>
        <Router history={history} key={routerKey} render={applyRouterMiddleware(useScroll())}>
          {routes}
        </Router>
      </Provider>
    );
  }
}

export default AppContainer;
