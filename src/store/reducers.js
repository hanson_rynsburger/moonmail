import {combineReducers} from 'redux';
import {reducer as form} from 'redux-form';
import modules from 'modules';

export const makeRootReducer = asyncReducers => {
  const reducers = {
    // Add sync reducers here
    form,
    ...asyncReducers
  };
  modules.forEach(({reducer, key}) => (reducers[key] = reducer));
  const appReducer = combineReducers(reducers);

  // Clear state on sign out
  return (state, action) => appReducer(state, action);
};

export const injectReducer = (store, {key, reducer}) => {
  store.asyncReducers[key] = reducer;
  store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
