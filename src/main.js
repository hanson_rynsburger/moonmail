import React from 'react';
import ReactDOM from 'react-dom';
import {browserHistory} from 'react-router';
import moment from 'moment/src/moment';
import {NO_LOCALIZE} from 'lib/constants';
import * as api from 'lib/api';
import createStore from './store/createStore';
import AppContainer from './containers/AppContainer';
import registerServiceWorker from './registerServiceWorker';
import _debug from 'debug';
// import Semantic UI
import '../semantic/dist/components/transition';
import '../semantic/dist/components/dropdown';
import '../semantic/dist/components/popup';
import '../semantic/dist/semantic.css';
// load locales
import 'lib/locales/de';
import 'lib/locales/es';
import 'lib/locales/fr';
import 'lib/locales/it';
import 'lib/locales/pt';

const info = _debug('app:info');

// ========================================================
// Enable debug
// ========================================================
localStorage.debug = __DEBUG__ || __STAGE__ === 'development' ? 'app:*' : 'app:info*,app:error*';

info(`v${__VERSION__}`);

// ========================================================
// Developer info
// ========================================================
window.developer = 'Dmitriy Nevzorov, https://github.com/jimmyn';

// ========================================================
// Store and History Instantiation
// ========================================================
const initialState = window.___INITIAL_STATE__;
const store = createStore(initialState, browserHistory);

// ========================================================
// Track url changes
// ========================================================
browserHistory.listen(() => {
  window.Intercom('update');
});

// ========================================================
// Routing for development stage
// ========================================================
if (__STAGE__ === 'development' && location.hash && !location.hash.includes('access_token')) {
  browserHistory.push(location.hash.substring(1));
}

// ========================================================
// Setup api interceptors
// ========================================================
api.addInterceptors(store);

// ========================================================
// Developer Tools Setup
// ========================================================
if (__DEBUG__) {
  if (window.devToolsExtension) {
    window.devToolsExtension();
  }
}

// ========================================================
// Localize And Moment Setup
// ========================================================
moment.locale('en');
if (true) {
  window.Localize.initialize({
    key: 'zyjZ2H97VXDaE',
    rememberLanguage: true,
    blockedClasses: ['intercom-container', NO_LOCALIZE]
  });
  moment.locale(window.Localize.getLanguage());
  window.Localize.on('setLanguage', ({to}) => moment.locale(to));
}

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('root');

let render = (routerKey = null) => {
  const routes = require('./routes/index').default(store);
  ReactDOM.render(
    <AppContainer store={store} history={browserHistory} routes={routes} key={routerKey} />,
    MOUNT_NODE
  );
};

// Enable HMR
if (__DEV__ && module.hot) {
  // Setup hot module replacement
  module.hot.accept(['./containers/AppContainer', './routes/index', './modules/index'], () =>
    setImmediate(() => {
      ReactDOM.unmountComponentAtNode(MOUNT_NODE);
      render();
    })
  );
}

// ========================================================
// Go!
// ========================================================
render();
registerServiceWorker();
