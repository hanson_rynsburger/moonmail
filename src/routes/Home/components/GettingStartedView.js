import React from 'react';
import PropTypes from 'prop-types';
import numeral from 'numeral';
import cx from 'classnames';
import {Link} from 'components/Links';

const Item = ({to, header, children, icon, active, disabled}) => {
  if (!active) return null;
  return (
    <div className="item">
      <div className="icon-wrapper">
        <i className={cx('circular inverted large icon', icon, {grey: disabled})} />
      </div>
      <div className="content">
        <Link to={to} className={cx('header', {disabled})}>
          {header}
        </Link>
        <div className={cx({'text grey': disabled})}>{children}</div>
      </div>
    </div>
  );
};

const GettingStartedView = ({
  isPhoneVerified,
  isContactInfoProvided,
  limits,
  plan,
  isFreeUser,
  isUnverifiedSesUser,
  lists,
  hasNoSender,
  isAllowedToSend,
  isFreeMessUser
}) => {
  const recipientsPerCampaign = numeral(limits.recipientsPerCampaign).format();
  const recipientsInTotal = isNaN(limits.recipientsInTotal)
    ? 'unlimited'
    : numeral(limits.recipientsInTotal).format('0a');

  return (
    <div className="ui basic segment padded-bottom">
      <div className="ui very padded segment">
        <div className="ui large dividing header">
          Welcome to MoonMail! Let's get started!
          <div className="sub header">
            Here are the steps to get your account ready to send your first campaign.
          </div>
        </div>
        <div className="ui stackable grid">
          <div className="ten wide column">
            <div className="ui large very relaxed list">
              <Item
                to="/profile/settings"
                header="Verify your phone number"
                icon="mobile"
                active={!isPhoneVerified}>
                It is needed for security reasons before you can send any email. <br />
                We will not use it nor reveal it to the others.
              </Item>
              <Item
                to="/profile/contact-info"
                header="Provide your contact information"
                icon="address card outline"
                active={!isContactInfoProvided}>
                It is needed for security reasons before you can send any email. <br />
                We will not use it nor reveal it to the others.
              </Item>
              <Item
                to="/getting-started-aws-ses"
                header="Add your AWS credentials"
                icon="amazon"
                active={isUnverifiedSesUser}>
                We need you to authorize MoonMail to use your AWS account to send emails.
              </Item>
              <Item
                to="/profile/senders/new"
                header="Create your first sender"
                icon="send outline"
                disabled={isFreeMessUser || isUnverifiedSesUser}
                active={isFreeMessUser || hasNoSender}>
                {`An email address that will be used when sending emails through MoonMail. ${
                  isFreeMessUser
                    ? 'Default sender will be used. You need to  upgrade to create a custom sender.'
                    : ''
                }`}
              </Item>
              <Item
                to="/lists/new"
                header="Create your first list"
                icon="address book outline"
                disabled={hasNoSender || isUnverifiedSesUser}
                active={lists.length === 0}>
                {`You can save up to ${recipientsInTotal} subscribers on the ${plan.title} plan`}
              </Item>
              <Item
                to="/campaigns/new"
                header="Create & send your first email campaign"
                icon="mail outline"
                disabled={!isAllowedToSend || lists.length === 0 || hasNoSender}
                active>
                {`You can send ${
                  isFreeUser
                    ? `1 campaign a day to ${recipientsPerCampaign} subscribers`
                    : 'unlimited campaigns'
                } on the ${plan.title} plan`}
              </Item>
            </div>
            {isFreeUser && (
              <h3>
                Need more? <Link to="/profile/plan">Upgrade now and get a 20% discount!</Link>
              </h3>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

GettingStartedView.propTypes = {
  isPhoneVerified: PropTypes.bool,
  isContactInfoProvided: PropTypes.bool,
  limits: PropTypes.object,
  plan: PropTypes.object,
  isFreeUser: PropTypes.bool,
  lists: PropTypes.array.isRequired,
  isUnverifiedSesUser: PropTypes.bool,
  hasNoSender: PropTypes.bool,
  isAllowedToSend: PropTypes.bool,
  isFreeMessUser: PropTypes.bool
};

export default GettingStartedView;
