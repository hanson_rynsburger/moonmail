import React from 'react';
import PropTypes from 'prop-types';
import CampaignGraph from 'components/CampaignGraph';
import Select, {SelectItem} from 'components/Select';
import DropDownMenu from 'components/DropDownMenu';
import classNames from './HomeView.scss';
import GettingStarted from '../containers/GettingStartedContainer';
import cx from 'classnames';

const INTERVALS = {
  '12AfterSent': '12 hours after delivery',
  '24AfterSent': '24 hours after delivery',
  '12BeforeNow': 'Last 12 hours',
  '24BeforeNow': 'Last 24 hours'
};

export const HomeView = ({
  campaigns,
  isFetching,
  onCampaignChange,
  onIntervalChange,
  interval,
  isDemo,
  isGettingStarted,
  params
}) => {
  if (!isFetching && isGettingStarted) {
    return <GettingStarted />;
  }
  return (
    <div className="ui basic segment padded-bottom">
      {campaigns.length > 0 && (
        <div className={classNames.filters}>
          <Select
            name="campaignId"
            fluid
            fieldClass={classNames.campaignSelector}
            value={params.campaignId}
            label={false}
            search
            onChange={onCampaignChange}>
            {campaigns.map((campaign, i) => (
              <SelectItem key={i} value={campaign.id}>
                {campaign.name}
              </SelectItem>
            ))}
          </Select>
          <DropDownMenu className={cx('floating labeled icon button', classNames.filter)}>
            <i className="clock outline icon" />
            <span className="text">{INTERVALS[interval]}</span>
            <div className="menu">
              <div className="scrolling menu">
                {Object.keys(INTERVALS).map(i => (
                  <div
                    key={i}
                    className={cx('item', {'active selected': i === interval})}
                    onClick={() => onIntervalChange(i)}>
                    {INTERVALS[i]}
                  </div>
                ))}
              </div>
            </div>
          </DropDownMenu>
        </div>
      )}
      <CampaignGraph isDemo={isDemo} isLoading={isFetching} params={params} />
    </div>
  );
};

HomeView.propTypes = {
  campaigns: PropTypes.array.isRequired,
  isFetching: PropTypes.bool,
  onCampaignChange: PropTypes.func.isRequired,
  onIntervalChange: PropTypes.func.isRequired,
  interval: PropTypes.string.isRequired,
  isDemo: PropTypes.bool,
  params: PropTypes.object.isRequired,
  isGettingStarted: PropTypes.bool
};

export default HomeView;
