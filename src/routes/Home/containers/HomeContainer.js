import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import HomeView from '../components/HomeView';
import actions from 'modules/campaigns/actions';
import listsActions from 'modules/lists/actions';
import selectors from 'modules/campaigns/selectors';
import {getIsFetching} from 'modules/profile/selectors';
import {EMPTY_OBJECT} from 'lib/constants';

class HomeContainer extends Component {
  static propTypes = {
    params: PropTypes.object.isRequired,
    buildGraphParams: PropTypes.func.isRequired,
    getCampaign: PropTypes.func.isRequired,
    fetchCampaigns: PropTypes.func.isRequired,
    fetchLists: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      interval: '12AfterSent',
      params: props.params
    };
  }

  componentWillReceiveProps({params}) {
    if (!this.props.params.campaignId) {
      this.setState({params});
    }
  }

  onCampaignChange = campaignId => {
    this.setState({
      interval: '12AfterSent',
      params: this.props.buildGraphParams(campaignId)
    });
  };

  onIntervalChange = interval => {
    const campaign = this.props.getCampaign(this.state.params.campaignId);
    const params = {...this.state.params};
    const now = new Date().valueOf() / 1000;
    switch (interval) {
      case '12AfterSent':
        params.start = campaign.sentAt;
        params.end = Math.min(campaign.sentAt + 43200, now);
        break;
      case '24AfterSent':
        params.start = campaign.sentAt;
        params.end = Math.min(campaign.sentAt + 86400, now);
        break;
      case '12BeforeNow':
        params.start = Math.max(now - 43200, campaign.sentAt);
        params.end = now;
        break;
      case '24BeforeNow':
        params.start = Math.max(now - 86400, campaign.sentAt);
        params.end = now;
        break;
    }
    this.setState({
      params,
      interval
    });
  };

  componentDidMount() {
    this.props.fetchCampaigns();
    this.props.fetchLists();
  }

  render() {
    return (
      <HomeView
        {...this.props}
        params={this.state.params}
        interval={this.state.interval}
        onIntervalChange={this.onIntervalChange}
        onCampaignChange={this.onCampaignChange}
      />
    );
  }
}

const mapStateToProps = state => {
  const campaigns = selectors.getAvaliableCampaigns(state);
  const defaultCampaign = campaigns[0] || EMPTY_OBJECT;
  const getCampaign = selectors.getResourceGetter(state);
  const buildGraphParams = campaignId => {
    const campaign = getCampaign(campaignId);
    if (!campaign.id) return EMPTY_OBJECT;
    const now = new Date().valueOf() / 1000;
    return {
      campaignId: campaign.id,
      campaignName: campaign.name,
      start: campaign.sentAt,
      end: Math.round(Math.min(campaign.sentAt + 43200, now))
    };
  };

  const isDemo = selectors.getIsUpToDate(state) && campaigns.length === 0;
  return {
    campaigns,
    buildGraphParams,
    getCampaign,
    isDemo,
    isGettingStarted: isDemo,
    isFetching: getIsFetching(state) || selectors.getIsFetchingAll(state),
    params: buildGraphParams(defaultCampaign.id)
  };
};

export default connect(mapStateToProps, {
  fetchCampaigns: actions.fetchCampaigns,
  fetchLists: listsActions.fetchLists
})(HomeContainer);
