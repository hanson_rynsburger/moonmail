import {connect} from 'react-redux';
import * as selectors from 'modules/profile/selectors';
import listsSelectors from 'modules/lists/selectors';
import sendersSelectors from 'modules/senders/selectors';
import GettingStartedView from '../components/GettingStartedView';
import {getIsNewFreeUser} from 'modules/profile/selectors';
const mapStateToProps = state => {
  return {
    isPhoneVerified: selectors.getIsPhoneVerified(state),
    isContactInfoProvided: selectors.getIsContactInfoProvided(state),
    limits: selectors.getLimits(state),
    isFreeUser: selectors.getIsFreeUser(state),
    isFreeMessUser: selectors.getIsFreeMessUser(state),
    plan: selectors.getPlan(state),
    lists: listsSelectors.getAvailableLists(state),
    isUnverifiedSesUser: selectors.getIsUnverifiedSesUser(state),
    hasNoSender:
      selectors.getHasSenders(state) && sendersSelectors.getVerifiedSenders(state).length === 0,
    isAllowedToSend: selectors.getIsAllowedToSend(state),
    hasDiscount: getIsNewFreeUser(state)
  };
};

export default connect(mapStateToProps)(GettingStartedView);
