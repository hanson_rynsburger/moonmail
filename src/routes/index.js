// We only need to import the modules necessary for initial render
import CoreLayout from 'layouts/CoreLayout';
import Home from './Home';
import Campaigns from './Campaigns';
import NewCampaign from './NewCampaign';
import Campaign from './Campaign';
import Automations from './Automations';
import NewAutomation from './NewAutomation';
import Automation from './Automation';
import Lists from './Lists';
import NewList from './NewList';
import List from './List';
import Profile from './Profile';
import Template from './TemplateEditor';
import GettingStarted from './GettingStarted';
import AuthCallback from './AuthCallback';
import CancelledAccount from './CancelledAccount';
import Extensions from './Extensions';
import NotFound from './NotFound';
import Templates from './Templates';
import PublishTemplate from './PublishTemplate';
import MarketTemplate from './MarketTemplate';
import MarketTemplates from './MarketTemplates';
import IntegrationsRoute from './Integrations';

export const createRoutes = store => ({
  path: '/',
  component: CoreLayout,
  indexRoute: Home,
  childRoutes: [
    AuthCallback,
    CancelledAccount,
    Profile(store),
    Campaigns,
    NewCampaign,
    Campaign,
    Automations,
    NewAutomation,
    Automation,
    Lists,
    NewList,
    List(store),
    PublishTemplate,
    Template(store),
    MarketTemplate,
    MarketTemplates,
    Templates,
    GettingStarted,
    Extensions,
    IntegrationsRoute(store),
    {
      path: 'profile/api',
      onEnter: (params, replace) => replace('/integrations')
    },
    NotFound
  ]
});

export default createRoutes;
