import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import RequirePermission from 'routes/Campaign/containers/RequirePermission';
import TemplateSelector from 'routes/Templates/containers/TemplateSelectorContainer';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';

class Template extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  static propTypes = {
    campaignId: PropTypes.string.isRequired,
    updateCampaignLocally: PropTypes.func.isRequired
  };

  onSelect = async template => {
    const {updateCampaignLocally, campaignId} = this.props;
    updateCampaignLocally(campaignId, {template});
    this.context.router.push(`/campaigns/${campaignId}/editor`);
  };

  render() {
    return <TemplateSelector {...this.props} onSelect={this.onSelect} />;
  }
}

const mapStateToProps = (state, ownProps) => {
  const isEditable = selectors.getIsEditable(state);
  return {
    campaignId: ownProps.params.campaignId,
    campaign: selectors.getActiveResource(state),
    isEditable,
    isPermitted: isEditable
  };
};

export default connect(mapStateToProps, actions)(RequirePermission(Template));
