import Template from './containers/TemplateContainer';

export default {
  path: 'template',
  component: Template
};
