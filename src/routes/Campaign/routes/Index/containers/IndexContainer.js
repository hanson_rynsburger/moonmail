import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';

class Index extends Component {
  constructor(props) {
    super(props);
    this.urlBase = `/campaigns/${props.campaignId}`;
  }

  static contextTypes = {
    router: PropTypes.object
  };

  static propTypes = {
    campaignId: PropTypes.string.isRequired
  };

  handleRedirect({isEditable}) {
    if (isEditable) {
      return this.context.router.replace(`${this.urlBase}/html`);
    }
    this.context.router.replace(`${this.urlBase}/preview`);
  }

  componentWillReceiveProps(nextProps) {
    this.handleRedirect(nextProps);
  }

  componentWillMount() {
    this.handleRedirect(this.props);
  }

  render() {
    return <div />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  campaignId: ownProps.params.campaignId,
  isEditable: selectors.getIsEditable(state),
  isScheduled: selectors.getIsScheduled(state)
});

export default connect(mapStateToProps, actions)(Index);
