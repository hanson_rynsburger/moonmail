import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as actions from 'routes/TemplateEditor/modules/actions';
import {addMessage} from 'modules/messages/actions';
import * as selectors from 'routes/TemplateEditor/modules/selectors';
import campaignActions from 'modules/campaigns/actions';
import campaignSelectors from 'modules/campaigns/selectors';
import listSelectors from 'modules/lists/selectors';
import {getUserIdBase64, getIsFreeUser} from 'modules/profile/selectors';
import EditorView from '../components/EditorView';

class Editor extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  static propTypes = {
    loadEditor: PropTypes.func.isRequired,
    campaign: PropTypes.object,
    campaignId: PropTypes.string.isRequired
  };

  componentWillMount() {
    const {campaign, campaignId} = this.props;
    if (campaign.id && !campaign.template) {
      this.context.router.push(`/campaigns/${campaignId}/templates`);
    }
  }

  componentDidMount() {
    this.props.loadEditor();
  }

  render() {
    return <EditorView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  isEditorLoading: selectors.getIsEditorLoading(state),
  isFetchingToken: selectors.getIsFetchingToken(state),
  isFreeUser: getIsFreeUser(state),
  campaign: campaignSelectors.getActiveResource(state),
  campaignId: ownProps.params.campaignId,
  token: selectors.getToken(state),
  uid: getUserIdBase64(state),
  metaData: listSelectors.getActiveCampaignMetaData(state)
});

export default connect(mapStateToProps, {
  ...actions,
  ...campaignActions,
  addMessage
})(Editor);
