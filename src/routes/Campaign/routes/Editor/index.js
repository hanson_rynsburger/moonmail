import Editor from './containers/EditorContainer';

export default {
  path: 'editor',
  component: Editor,
  isPlain: true
};
