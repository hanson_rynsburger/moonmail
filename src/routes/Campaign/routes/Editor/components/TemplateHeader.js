import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router/lib/Link';
import classNames from 'routes/TemplateEditor/components/TemplateHeader.scss';

const TemplateHeader = ({campaignId}) => (
  <header className={classNames.container}>
    <Link to={`/campaigns/${campaignId}/html`} className={classNames.link}>
      Edit code
    </Link>
    <Link to={`/campaigns/${campaignId}/preview`} className={classNames.link}>
      Preview and send
    </Link>
    <Link to={`/campaigns/${campaignId}/settings`} className={classNames.link}>
      Settings
    </Link>
  </header>
);

TemplateHeader.propTypes = {
  campaignId: PropTypes.string.isRequired
};

export default TemplateHeader;
