import PropTypes from 'prop-types';
import React, {Component} from 'react';
import fs from 'browser-filesaver';
import TemplateEditor from 'routes/TemplateEditor/containers/TemplateEditorContainer';
import TemplateHeader from './TemplateHeader';
import TestModal from 'routes/Campaign/containers/TestCampaignContainer';
import {parse} from 'lib/parser';

class EditorView extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  static propTypes = {
    updateCampaign: PropTypes.func.isRequired,
    updateCampaignLocally: PropTypes.func.isRequired,
    addMessage: PropTypes.func.isRequired,
    testCampaignStart: PropTypes.func.isRequired,
    validateAndParseCampaign: PropTypes.func.isRequired,
    campaignId: PropTypes.string.isRequired,
    metaData: PropTypes.array.isRequired,
    campaign: PropTypes.shape({
      template: PropTypes.string,
      name: PropTypes.string
    }),
    isFreeUser: PropTypes.bool.isRequired
  };

  onSave = (template, body) => {
    this.setState({isSaving: true});
    const {updateCampaign, updateCampaignLocally, campaign, isFreeUser, addMessage} = this.props;
    updateCampaignLocally(campaign.id, {template, body});
    parse(body, !isFreeUser)
      .then(() => {
        this.context.router.push(`/campaigns/${campaign.id}/preview`);
        updateCampaign(campaign.id, {template, body}, true);
      })
      .catch(error => {
        addMessage({text: error});
      });
  };

  onSaveAsTemplate = jsonFile => {
    fs.saveAs(
      new Blob([jsonFile], {type: 'application/json;charset=utf-8'}),
      this.props.campaign.name.replace(' ', '_') + '.json'
    );
  };

  onAutoSave = template => {
    const {updateCampaign, campaign} = this.props;
    updateCampaign(campaign.id, {template}, true);
  };

  onSend = body => {
    const {
      updateCampaignLocally,
      testCampaignStart,
      campaign,
      validateAndParseCampaign,
      metaData
    } = this.props;
    updateCampaignLocally(campaign.id, {body});
    validateAndParseCampaign(campaign, metaData, true);
    testCampaignStart();
  };

  render() {
    const options = {
      mergeTags: this.props.metaData.map(field => ({
        name: field,
        value: `{{${field}}}`
      }))
    };
    return (
      <div>
        <TemplateHeader campaignId={this.props.campaignId} />
        <TemplateEditor
          template={this.props.campaign.template}
          options={options}
          onSave={this.onSave}
          onSaveAsTemplate={this.onSaveAsTemplate}
          onAutoSave={this.onAutoSave}
          onSend={this.onSend}
        />
        <TestModal campaignId={this.props.campaignId} />
      </div>
    );
  }
}

export default EditorView;
