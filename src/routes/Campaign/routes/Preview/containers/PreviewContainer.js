import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import PreviewView from '../components/PreviewView';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import listSelectors from 'modules/lists/selectors';
import segmentsActions from 'modules/segments/actions';
import sendersSelectors from 'modules/senders/selectors';
import * as profileSelectors from 'modules/profile/selectors';

class Preview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: false
    };
  }

  static propTypes = {
    fetchCampaignLinks: PropTypes.func.isRequired,
    fetchSegments: PropTypes.func.isRequired,
    validateAndParseCampaign: PropTypes.func.isRequired,
    campaign: PropTypes.shape({
      body: PropTypes.string
    }).isRequired,
    campaignId: PropTypes.string.isRequired,
    metaData: PropTypes.array.isRequired,
    isFreeUser: PropTypes.bool.isRequired,
    sendersUpToDate: PropTypes.bool.isRequired,
    isEditable: PropTypes.bool.isRequired,
    isSending: PropTypes.bool.isRequired,
    profile: PropTypes.object.isRequired,
    sender: PropTypes.object
  };

  switchMode = () => {
    this.setState({
      isMobile: !this.state.isMobile
    });
  };

  componentDidMount() {
    const {
      campaignId,
      campaign,
      fetchCampaignLinks,
      metaData,
      isEditable,
      validateAndParseCampaign,
      sendersUpToDate,
      fetchSegments
    } = this.props;
    if (campaign.segmentId) {
      fetchSegments(campaign.listIds[0]);
    }
    if (campaign.status === 'sent') {
      fetchCampaignLinks(campaignId);
    }
    if (sendersUpToDate && campaign._isUpToDate) {
      validateAndParseCampaign(campaign, metaData, isEditable);
    }
  }

  componentWillReceiveProps({sendersUpToDate, campaign, metaData, isEditable}) {
    if (sendersUpToDate && campaign._isUpToDate) {
      this.props.validateAndParseCampaign(campaign, metaData, isEditable);
    }
  }

  render() {
    return (
      <PreviewView
        html={this.state.html}
        switchMode={this.switchMode}
        isMobile={this.state.isMobile}
        invalid={this.state.invalid}
        {...this.props}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const campaign = selectors.getActiveResource(state);
  return {
    campaignId: ownProps.params.campaignId,
    campaign,
    campaignBody: selectors.getCampaignBody(state),
    isValid: selectors.getIsValid(state),
    sender: sendersSelectors.getResourceById(state, campaign.senderId),
    links: selectors.getCampaignLinks(state),
    isScheduled: selectors.getIsScheduled(state),
    isEditable: selectors.getIsEditable(state),
    isUpdating: selectors.getIsUpdating(state),
    isSending: selectors.getIsSending(state),
    isCanceling: selectors.getIsCanceling(state),
    isAllowedToSend: profileSelectors.getIsAllowedToSend(state),
    metaData: listSelectors.getActiveCampaignMetaData(state),
    isFreeUser: profileSelectors.getIsFreeUser(state),
    profile: profileSelectors.getProfile(state),
    sendersUpToDate: sendersSelectors.getIsUpToDate(state)
  };
};

export default connect(mapStateToProps, {
  ...actions,
  fetchSegments: segmentsActions.fetchSegments
})(Preview);
