import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactFrame from 'react-frame-component';
import TestModal from 'routes/Campaign/containers/TestCampaignContainer';
import ConfirmSend from 'routes/Campaign/containers/ConfirmSendContainer';
import ScheduleCampaign from 'routes/Campaign/containers/ScheduleCampaignContainer';
import Button from 'components/Button';
import NoLocalize, {NO_LOCALIZE} from 'components/NoLocalize';
import classNames from './PreviewView.scss';
import {formatDate} from 'lib/utils';
import pluralize from 'pluralize';
import cx from 'classnames';

const TOOLTIP_STYLE = {
  'pointer-events': 'none',
  display: 'inline-block',
  visibility: 'hidden',
  'background-color': '#FFF',
  color: 'rgba(0,0,0,.87)',
  border: '1px solid #D4D4D5',
  'border-radius': '.28571429rem',
  position: 'absolute',
  'z-index': '1000',
  transform: 'translate(0,35px)',
  padding: '.433em 0.8em',
  'font-weight': '400',
  'font-style': 'normal',
  'font-size': '1rem',
  'line-height': '1rem',
  'font-family': "Lato,'Helvetica Neue',Arial,Helvetica,sans-serif",
  'box-shadow': '0 2px 4px 0 rgba(34,36,38,.12),0 2px 10px 0 rgba(34,36,38,.15)'
};

const ARROW_STYLE = {
  content: "''",
  width: '.71428571em',
  height: '.71428571em',
  'pointer-events': 'none',
  position: 'absolute',
  background: '#FFF',
  'z-index': '999',
  transform: 'translate(0,-0.75286em) rotate(135deg)',
  'box-shadow': '-1px 1px 0 0 #bababc'
};

class PreviewView extends Component {
  static propTypes = {
    campaignBody: PropTypes.string,
    campaign: PropTypes.object.isRequired,
    links: PropTypes.array.isRequired,
    isMobile: PropTypes.bool.isRequired,
    isValid: PropTypes.bool.isRequired,
    isEditable: PropTypes.bool.isRequired,
    isScheduled: PropTypes.bool.isRequired,
    isCanceling: PropTypes.bool.isRequired,
    switchMode: PropTypes.func.isRequired,
    cancelScheduledCampaign: PropTypes.func.isRequired,
    campaignId: PropTypes.string.isRequired,
    testCampaignStart: PropTypes.func.isRequired,
    sendCampaignStart: PropTypes.func.isRequired,
    scheduleCampaignStart: PropTypes.func.isRequired,
    isAllowedToSend: PropTypes.bool
  };

  setFrame = instance => (this.frame = instance);
  toggleShown = () => null;

  constructor(...args) {
    super(...args);

    this.state = {
      // This hack is needed to make sure the links inside the iframe receive heatmaps
      heatmapSetup: false,
      allShown: false
    };
  }

  setupHeatmap = () => {
    const {campaign: {status}, links} = this.props;
    if (!this.state.heatmapSetup && status === 'sent' && links.length > 0) {
      const totalClicks = links.map(link => link.clicksCount || 0).reduce((p, c) => p + c);

      const frameDocument = this.frame.getDoc();
      const linkElements = $(frameDocument)
        .find('a')
        .toArray();

      const tinyTips = [];

      linkElements.forEach((linkElement, index) => {
        if (!links[index]) {
          return;
        }

        const {clicksCount = 0} = links[index];
        const percentage = totalClicks > 0 ? Math.ceil(100 * clicksCount / totalClicks) : 0;

        $(linkElement).attr('target', '_blank');

        const tooltip = frameDocument.createElement('div');
        const tooltipArrow = frameDocument.createElement('div');

        $(tooltip).css(TOOLTIP_STYLE);
        $(tooltipArrow).css(ARROW_STYLE);

        $(tooltip).html(`
          <div style="display: inline-block;">
            <p style="font-weight: bold; font-size: 20px; margin: 0 0 5px 0; line-height: 24px; text-align: left;">
              ${percentage}%
            </p>
            <p style="font-weight: 400; font-size: 12px; margin: 0; line-height: 14px; text-align: left;">
              ${clicksCount} of ${pluralize('click', totalClicks, true)}
            </p>
          </div>
        `);

        $(linkElement).prepend(tooltip);
        $(tooltip).prepend(tooltipArrow);

        const tinyTooltip = frameDocument.createElement('div');
        const tinyTooltipArrow = frameDocument.createElement('div');

        $(tinyTooltip).css({
          ...TOOLTIP_STYLE,
          padding: '.433em 0.5em'
        });
        $(tinyTooltipArrow).css(ARROW_STYLE);

        $(tinyTooltip).html(`
          <div style="display: inline-block">
            <p style="font-weight: 400; font-size: 16px; margin: 0; line-height: 20px; text-align: left;">
              ${percentage}%
            </p>
          </div>
        `);

        $(linkElement).prepend(tinyTooltip);
        $(tinyTooltip).prepend(tinyTooltipArrow);

        tinyTips.push(tinyTooltip);

        $(linkElement).on(
          'mouseenter',
          () => !this.state.allShown && $(tooltip).css('visibility', 'visible')
        );
        $(linkElement).on('mouseleave', () => $(tooltip).css('visibility', 'hidden'));
      });

      this.toggleShown = () => {
        tinyTips.forEach(tip =>
          $(tip).css('visibility', this.state.allShown ? 'hidden' : 'visible')
        );
        this.setState({allShown: !this.state.allShown});
      };

      this.setState({heatmapSetup: true});
    }
  };

  // Workaround assuring the frame is ready before injecting heatmap tooltips
  setupHeatmapDelayed = () => setTimeout(this.setupHeatmap, 100);

  render() {
    const {
      campaignBody,
      isMobile,
      switchMode,
      isValid,
      sendCampaignStart,
      testCampaignStart,
      cancelScheduledCampaign,
      scheduleCampaignStart,
      campaignId,
      campaign,
      isEditable,
      isCanceling,
      isAllowedToSend,
      isScheduled
    } = this.props;

    const frameClass = cx(classNames.frame, {
      [classNames.mobileFrame]: isMobile
    });

    const disabled = !isValid || !isAllowedToSend;

    return (
      <section className={classNames.wrapper}>
        <ReactFrame
          ref={this.setFrame}
          contentDidMount={this.setupHeatmapDelayed}
          contentDidUpdate={this.setupHeatmap}
          className={cx(frameClass, NO_LOCALIZE)}>
          <div dangerouslySetInnerHTML={{__html: campaignBody || campaign.body}} />
        </ReactFrame>
        <div className="page-actions">
          <div className="ui basic buttons">
            <Button onClick={switchMode} active={!isMobile}>
              <i className="icon desktop" />Desktop
            </Button>
            <Button onClick={switchMode} active={isMobile}>
              <i className="icon mobile alternate" />Mobile
            </Button>
          </div>
          <div className="spacer" />
          {isEditable &&
            !isScheduled && (
              <div>
                <Button disabled={disabled} onClick={testCampaignStart}>
                  Send test email
                </Button>
                <Button disabled={disabled} onClick={scheduleCampaignStart}>
                  <i className="clock outline icon" />
                  Schedule
                </Button>
                <Button primary onClick={sendCampaignStart} disabled={disabled}>
                  <i className="send icon" />
                  Send
                </Button>
              </div>
            )}
          {isScheduled && (
            <div>
              <div className="ui basic teal right pointing large label">
                Scheduled for
                <NoLocalize> {formatDate(campaign.scheduledAt, {showTime: true})}</NoLocalize>
              </div>
              <Button
                primary
                loading={isCanceling}
                onClick={() => cancelScheduledCampaign(campaignId)}>
                <i className="pause icon" />
                Pause
              </Button>
            </div>
          )}
          {this.state.heatmapSetup && (
            <div className="ui basic buttons">
              <Button onClick={this.toggleShown} disabled={!this.state.allShown}>
                <i className="icon bar chart" />Clicks Statistics
              </Button>
              <Button onClick={this.toggleShown} disabled={this.state.allShown}>
                <i className="icon map" />Clicks Map
              </Button>
            </div>
          )}
        </div>
        <TestModal campaignId={campaignId} />
        <ConfirmSend campaignId={campaignId} />
        <ScheduleCampaign campaignId={campaignId} />
      </section>
    );
  }
}

export default PreviewView;
