import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {reduxForm} from 'redux-form';
import HtmlView from '../components/HtmlView';
import {createValidator} from 'lib/validator';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import listsSelectors from 'modules/lists/selectors';
import {parse} from 'lib/parser';
import {getIsFreeUser} from 'modules/profile/selectors';
import RequirePermission from 'routes/Campaign/containers/RequirePermission';
import {compose} from 'recompose';

class Html extends Component {
  static propTypes = {
    updateCampaignLocally: PropTypes.func.isRequired,
    updateCampaign: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
    invalid: PropTypes.bool.isRequired,
    isEditable: PropTypes.bool,
    campaignId: PropTypes.string.isRequired,
    campaign: PropTypes.shape({
      body: PropTypes.string,
      subject: PropTypes.string,
      listIds: PropTypes.array
    }).isRequired
  };

  beforeUnload = e => {
    const {campaign, isEditable} = this.props;
    if (campaign._isPendingUpdate && isEditable) {
      const s = 'You have unsaved changes.';
      e.returnValue = s;
      return s;
    }
  };

  componentDidMount() {
    window.onbeforeunload = this.beforeUnload;
  }

  componentWillUnmount() {
    window.onbeforeunload = null;
    const {updateCampaign, values, invalid, campaignId, isEditable, campaign} = this.props;
    if (isEditable && !invalid && campaign._isPendingUpdate) {
      updateCampaign(campaignId, values, true);
    }
  }

  render() {
    return <HtmlView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => {
  const campaign = selectors.getActiveResource(state);
  const isFreeUser = getIsFreeUser(state);
  const isEditable = selectors.getIsEditable(state);
  const asyncValidate = values => {
    return new Promise((resolve, reject) => {
      parse(values.body, !isFreeUser).then(
        () => {
          resolve({});
        },
        error => {
          reject({body: error});
        }
      );
    });
  };
  return {
    campaignId: ownProps.params.campaignId,
    isUpdating: selectors.getIsUpdating(state),
    isPermitted: isEditable,
    isEditable,
    campaign,
    asyncValidate,
    initialValues: campaign,
    metaData: listsSelectors.getActiveCampaignMetaData(state)
  };
};

export default compose(
  reduxForm(
    {
      form: 'html',
      fields: ['body'],
      asyncBlurFields: ['body'],
      validate: createValidator({
        body: 'required'
      })
    },
    mapStateToProps,
    actions
  ),
  RequirePermission
)(Html);
