import {connect} from 'react-redux';
import * as profileSelectors from 'modules/profile/selectors';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import FileAttachment from '../components/FileAttachment';

const EXTENSION_ID = 'file-attachment';

const mapStateToProps = state => ({
  isSupported: profileSelectors.getExtensions(state).includes(EXTENSION_ID),
  attachments: selectors.getAttachments(state),
  isAttachingFile: selectors.getIsAttachingFile(state),
  isDetachingFile: selectors.getIsDetachingFile(state),
  detachingFileName: selectors.getDetachingFileName(state)
});

export default connect(mapStateToProps, actions)(FileAttachment);
