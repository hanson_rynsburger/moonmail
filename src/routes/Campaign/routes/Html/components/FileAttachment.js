import PropTypes from 'prop-types';
import React, {Component} from 'react';
import Button from 'components/Button';
import FileButton from 'components/FileButton';
import Modal from 'components/Modal';
import classNames from './FileAttachment.scss';
import cx from 'classnames';
import numeral from 'numeral';
import Link from 'react-router/lib/Link';
import {Aux} from 'lib/utils';
import PopUp from 'components/PopUp';

class FileAttachment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false
    };
  }

  toggleModal = () => {
    this.setState({isModalOpen: !this.state.isModalOpen});
  };

  handleAttach = e => {
    const {files} = e.target;
    const {attachFile, campaignId} = this.props;
    if (!files.length) return;
    attachFile(campaignId, files[0]);
  };

  handleDetach = fileName => {
    if (window.confirm('Remove this file?')) {
      this.props.detachFile(this.props.campaignId, fileName);
    }
  };

  render() {
    const {
      isSupported,
      attachments,
      isAttachingFile,
      isDetachingFile,
      detachingFileName
    } = this.props;
    const {isModalOpen} = this.state;
    return (
      <div className={classNames.holder}>
        <PopUp
          on="click"
          showDelay={500}
          closable={false}
          variation="small"
          lastResort="right center"
          disabled={isSupported}
          content={
            <Aux>
              <Link to="/extensions">Install extension</Link> to attach files
            </Aux>
          }>
          <Button onClick={this.toggleModal} disabled={!isSupported}>
            Attach files {attachments.length ? `(${attachments.length})` : null}
          </Button>
        </PopUp>
        <Modal size="small" isOpen={isModalOpen}>
          <div className="header">Attachments ({attachments.length})</div>
          <div className="content">
            <div className="ui relaxed large list">
              {attachments.length ? (
                attachments.map((file, i) => (
                  <div key={i} className="item">
                    <Button
                      onClick={() => this.handleDetach(file.name)}
                      className="tiny icon basic">
                      <i
                        className={cx(
                          'icon',
                          classNames.deleteIcon,
                          isDetachingFile && detachingFileName === file.name
                            ? 'notched circle grey loading'
                            : 'trash red'
                        )}
                      />
                    </Button>
                    <a href={file.url} target="_blank" rel="noopener noreferrer">
                      {file.name}
                    </a>{' '}
                    <span className="text grey">({numeral(file.size).format('0.0 b')})</span>
                  </div>
                ))
              ) : (
                <div className="item">No files attached so far.</div>
              )}
            </div>
          </div>
          <div className="actions">
            <div className="actions">
              <FileButton onChange={this.handleAttach} loading={isAttachingFile}>
                <i className="icon cloud upload" />
                Attach a file
              </FileButton>
              <Button primary onClick={this.toggleModal}>
                Done
              </Button>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

FileAttachment.propTypes = {
  attachFile: PropTypes.func.isRequired,
  detachFile: PropTypes.func.isRequired,
  campaignId: PropTypes.string.isRequired,
  isSupported: PropTypes.bool.isRequired,
  attachments: PropTypes.array.isRequired,
  isAttachingFile: PropTypes.bool.isRequired,
  isDetachingFile: PropTypes.bool.isRequired,
  detachingFileName: PropTypes.string
};

export default FileAttachment;
