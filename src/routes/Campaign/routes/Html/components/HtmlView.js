import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import CodeEditor from 'components/CodeEditor';
import cx from 'classnames';
import classNames from './HtmlView.scss';
import DotHint from 'components/DotHint';
import FileAttachment from '../containers/FileAttachmentContainer';

const HtmlView = ({
  fields: {body},
  handleSubmit,
  invalid,
  updateCampaign,
  updateCampaignLocally,
  isUpdating,
  campaignId,
  metaData
}) => {
  const submit = ({body}) => {
    updateCampaign(campaignId, {body}).then(() => {
      updateCampaignLocally(campaignId, {_isPendingUpdate: false});
    });
  };

  const handleChange = html => {
    body.onChange(html);
    updateCampaignLocally(campaignId, {body: html, _isPendingUpdate: true});
  };

  const tags = metaData.map(tag => `{{${tag}}}`).join('</br>');
  return (
    <section className={cx('ui vertical segment full height', classNames.section)}>
      <form className={cx('ui form', classNames.form)} onSubmit={handleSubmit(submit)}>
        <CodeEditor
          label={
            <DotHint id="campaign.html.body" variables={{tags}}>
              Email body (paste html or drag your html file here)
            </DotHint>
          }
          {...body}
          onChange={handleChange}
          className={classNames.editorField}
          editorClass={classNames.editor}
        />
        <div className="page-actions">
          <Button loading={isUpdating} primary type="submit" disabled={invalid || isUpdating}>
            Save draft
          </Button>
          <FileAttachment campaignId={campaignId} />
          <div className="spacer" />
          <Button primary to={`/campaigns/${campaignId}/preview`}>
            Save & Preview
          </Button>
        </div>
      </form>
    </section>
  );
};

HtmlView.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  updateCampaign: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isUpdating: PropTypes.bool.isRequired,
  campaignId: PropTypes.string.isRequired,
  fields: PropTypes.shape({
    body: PropTypes.object.isRequired
  }).isRequired,
  metaData: PropTypes.array.isRequired,
  updateCampaignLocally: PropTypes.func.isRequired
};

export default HtmlView;
