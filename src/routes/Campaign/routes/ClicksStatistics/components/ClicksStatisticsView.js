import React from 'react';
import PropTypes from 'prop-types';
import {ResourceTable} from 'components/Table';
import {NO_LOCALIZE} from 'lib/constants';

const ClicksStatisticsView = ({isFetching, links}) => {
  return (
    <section className="padded-bottom">
      <ResourceTable
        resourceName="links"
        isFetching={isFetching && links.length === 0}
        numColumns={3}
        className="large striped fixed single line"
        header={
          <tr>
            <th className="one wide center aligned">Clicks</th>
            <th>Link</th>
            <th className="eight wide">Text</th>
          </tr>
        }>
        {links.map(({url, text, clicksCount = 0}, i) => (
          <tr key={i}>
            <td className="center aligned">{clicksCount}</td>
            <td className={NO_LOCALIZE}>
              <a href={url} title={url} target="_blank" rel="noopener noreferrer">
                {decodeURI(url)}
              </a>
            </td>
            <td className={NO_LOCALIZE}>
              <span title={text}>{text}</span>
            </td>
          </tr>
        ))}
      </ResourceTable>
    </section>
  );
};

ClicksStatisticsView.propTypes = {
  links: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired
};

export default ClicksStatisticsView;
