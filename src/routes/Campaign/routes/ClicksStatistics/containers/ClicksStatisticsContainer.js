import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import ClicksStatisticsView from '../components/ClicksStatisticsView';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import RequirePermission from 'routes/Campaign/containers/RequirePermission';

class ClicksStatistics extends Component {
  static propTypes = {
    fetchCampaignLinks: PropTypes.func.isRequired,
    campaignId: PropTypes.string.isRequired
  };

  componentDidMount() {
    const {fetchCampaignLinks, campaignId} = this.props;
    fetchCampaignLinks(campaignId);
  }

  render() {
    return <ClicksStatisticsView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  campaignId: ownProps.params.campaignId,
  links: selectors.getSortedCampaignLinks(state),
  isFetching: selectors.getIsFetchingLinks(state),
  isPermitted: !selectors.getIsEditable(state)
});

export default connect(mapStateToProps, actions)(RequirePermission(ClicksStatistics));
