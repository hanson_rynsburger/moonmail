import ClicksStatistics from './containers/ClicksStatisticsContainer';

export default {
  path: 'clicks-statistics',
  component: ClicksStatistics
};
