import React from 'react';
import PropTypes from 'prop-types';
import Progress from 'components/Progress';
import DotHint from 'components/DotHint';
import NL, {NO_LOCALIZE} from 'components/NoLocalize';
import Button from 'components/Button';
import {formatDate, deCapitalize, formatRate, formatStat, Aux, popupWindow} from 'lib/utils';
import {RATE_LIMITS} from 'lib/constants';

const ReportView = ({
  campaign: {
    opensCount = 0,
    sentCount = 0,
    unsubscribeCount = 0,
    bouncesCount = 0,
    complaintsCount = 0,
    clicksCount = 0,
    sentAt,
    status,
    name
  }
}) => {
  const openRate = formatRate(opensCount / sentCount);
  const unsubscribeRate = formatRate(unsubscribeCount / sentCount);
  const bounceRate = formatRate(bouncesCount / sentCount);
  const bouncePercent = formatRate(bounceRate / RATE_LIMITS.bounceRate);
  const complaintRate = formatRate(complaintsCount / sentCount, 3);
  const complaintPercent = formatRate(complaintRate / RATE_LIMITS.complaintRate, 3);
  const deliveryBase = sentCount - bouncesCount - complaintsCount;
  const emailDeliveryRate = formatRate(deliveryBase / sentCount);
  const clickOpenRate = formatRate(clicksCount / opensCount);

  const printReport = () => {
    const popupWin = popupWindow('', 'to_print', 800, 400);
    popupWin.document.open();
    popupWin.document.write(`<html>
  <head>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css" />
  </head>
  <body>
    <section class="ui padded basic segment">
      <h1>${name}</h1>
      <h3>Sent at: ${formatDate(sentAt, {showTime: true})}</h3>
      <table class="ui basic celled table">
        <thead>
          <tr>
            <th>Sent emails</th>
            <th>Opened</th>
            <th>Clicked</th>
            <th>Unsubscribed</th>
            <th>Bounced</th>
            <th>Complained</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>${formatStat(sentCount)}</td>
            <td>${formatStat(opensCount)}</td>
            <td>${formatStat(clicksCount)}</td>
            <td>${formatStat(unsubscribeCount)}</td>
            <td>${formatStat(bouncesCount)}</td>
            <td>${formatStat(complaintsCount)}</td>
          </tr>
        </tbody>
      </table>
      <table class="ui basic celled table">
        <thead>
          <tr>
            <th>Email delivery rate</th>
            <th>Click open rate</th>
            <th>Unsubscribe rate</th>
            <th>Open rate</th>
            <th>Bounce rate</th>
            <th>Complaint rate</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>${emailDeliveryRate}%</td>
            <td>${clickOpenRate}%</td>
            <td>${unsubscribeRate}%</td>
            <td>${openRate}%</td>
            <td className="${NO_LOCALIZE}">
              ${bounceRate}% out of ${RATE_LIMITS.bounceRate.toFixed(2)}%.
            </td>
            <td className="${NO_LOCALIZE}">
              ${complaintRate}% out of ${RATE_LIMITS.complaintRate}%.
            </td>
          </tr>
        </tbody>
      </table>
    </section>
    <script type="text/javascript">
      setTimeout(window.print, 500);
    </script>
  </body>
</html>
    `);
    popupWin.document.close();
  };

  if (deCapitalize(status) === 'badReputation') {
    return (
      <div className="ui icon error message">
        <i className="ban icon" />
        <div className="content">
          <div className="header">
            This campaign was blocked because of the bad reputation (too high bounce or complaint
            rate)
          </div>
          <p>
            You can not send any more campaigns. Please contact support if you think its a mistake.
          </p>
        </div>
      </div>
    );
  }

  const readMore = (
    <a
      target="_blank"
      rel="noopener noreferrer"
      href="http://support.moonmail.io/faq/validation-and-reputation/what-are-the-acceptable-complaint-and-bounce-rates">
      Read more
    </a>
  );

  return (
    <section className="padded-bottom">
      <div className="header-with-actions">
        <h3 className="ui header">
          Sent
          <NL> {formatDate(sentAt, {fromNow: true})}</NL>
        </h3>
        <Button onClick={printReport}>
          <i className="ui print icon" />Print report
        </Button>
      </div>
      <div className="ui vertical segment">
        <div className="ui small statistics">
          <div className="grey statistic">
            <div className="value">{formatStat(sentCount)}</div>
            <div className="label">Sent emails</div>
          </div>
          <div className="teal statistic">
            <div className="value">{formatStat(opensCount)}</div>
            <div className="label">Opened</div>
          </div>
          <div className="teal statistic">
            <div className="value">{formatStat(clicksCount)}</div>
            <div className="label">Clicked</div>
          </div>
          <div className="red statistic">
            <div className="value">{formatStat(unsubscribeCount)}</div>
            <div className="label">Unsubscribed</div>
          </div>
          <div className="red statistic">
            <div className="value">{formatStat(bouncesCount)}</div>
            <div className="label">Bounced</div>
          </div>
          <div className="red statistic">
            <div className="value">{formatStat(complaintsCount)}</div>
            <div className="label">Complained</div>
          </div>
        </div>
      </div>
      <div className="ui vertical segment">
        <h3 className="ui header">
          Email delivery rate
          <DotHint id="campaign.report.delivery_rate" />
        </h3>
        <Progress percent={emailDeliveryRate} className="teal" />
        <h3 className="ui header">
          Open rate
          <DotHint id="campaign.report.open_rate" />
        </h3>
        <Progress percent={openRate} className="teal" />
        <h3 className="ui header">
          Click open rate
          <DotHint id="campaign.report.click_rate" />
        </h3>
        <Progress percent={clickOpenRate} className="teal" />
        <h3 className="ui header">
          Unsubscribe rate
          <DotHint id="campaign.report.unsubscribe_rate" />
        </h3>
        <Progress className="error" percent={unsubscribeRate} indicating negative />
        <h3 className="ui header">
          <NL>
            Bounce rate {bounceRate}% out of {RATE_LIMITS.bounceRate.toFixed(2)}%
          </NL>.
          <DotHint id="campaign.report.bounce_rate" />
        </h3>
        <Progress
          percent={bouncePercent}
          label={<Aux>{readMore} about bounce rate restrictions.</Aux>}
          value={false}
          indicating
          negative
        />
        <h3 className="ui header">
          <NL>
            Complaint rate{complaintRate}% out of {RATE_LIMITS.complaintRate}%
          </NL>.
          <DotHint id="campaign.report.complaint_rate" />
        </h3>
        <Progress
          percent={complaintPercent}
          label={<Aux>{readMore} about complaint rate restrictions.</Aux>}
          value={false}
          indicating
          negative
        />
      </div>
    </section>
  );
};

ReportView.propTypes = {
  campaign: PropTypes.object.isRequired
};

export default ReportView;
