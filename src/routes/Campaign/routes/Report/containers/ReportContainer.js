import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import ReportView from '../components/ReportView';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import RequirePermission from 'routes/Campaign/containers/RequirePermission';

class Report extends Component {
  static propTypes = {
    fetchCampaignReport: PropTypes.func.isRequired,
    campaignId: PropTypes.string.isRequired
  };

  componentDidMount() {
    const {fetchCampaignReport, campaignId} = this.props;
    fetchCampaignReport(campaignId);
  }

  render() {
    return <ReportView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  campaignId: ownProps.params.campaignId,
  campaign: selectors.getActiveResource(state),
  isPermitted: !selectors.getIsEditable(state)
});

export default connect(mapStateToProps, actions)(RequirePermission(Report));
