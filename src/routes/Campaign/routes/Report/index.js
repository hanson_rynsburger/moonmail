import Report from './containers/ReportContainer';

export default {
  path: 'report',
  component: Report
};
