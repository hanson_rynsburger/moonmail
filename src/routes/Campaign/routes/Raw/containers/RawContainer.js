import {connect} from 'react-redux';
import RawView from '../components/RawView';
import selectors from 'modules/campaigns/selectors';
import RequirePermission from 'routes/Campaign/containers/RequirePermission';

const mapStateToProps = (state, ownProps) => ({
  campaignId: ownProps.params.campaignId,
  campaign: selectors.getActiveResource(state),
  isPermitted: !selectors.getIsEditable(state)
});

export default connect(mapStateToProps)(RequirePermission(RawView));
