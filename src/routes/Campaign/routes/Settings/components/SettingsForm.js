import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import EmojiInput from 'components/EmojiInput';
import Select, {SelectItem} from 'components/Select';
import Button from 'components/Button';
import Link from 'react-router/lib/Link';
import SenderHint from 'components/SenderHint';
import DotHint from 'components/DotHint';

const SettingsForm = ({
  fields: {name, subject, listIds, senderId, segmentId},
  lists,
  senders,
  handleSubmit,
  invalid,
  isLoading,
  isFetchingLists,
  isFetchingSenders,
  hasSenders,
  isEditable,
  onFormSubmit,
  buttonText,
  segments = [],
  isFetchingSegments,
  values
}) => {
  const isCreateListHint = isEditable && lists.length === 0 && !isFetchingLists;
  const isSegmentVisible =
    segments.length > 0 && values.listIds.length === 1 && values.listIds[0] !== '';

  const submit = formProps => {
    if (formProps.segmentId === '') {
      formProps.segmentId = null;
    }
    if (!hasSenders) {
      formProps.senderId = null;
    }
    onFormSubmit(formProps);
  };

  return (
    <form className="ui form nine wide column" onSubmit={handleSubmit(submit)}>
      <Input type="text" {...name} />
      <EmojiInput
        type="text"
        {...subject}
        disabled={!isEditable}
        label={<DotHint id="campaign.settings.subject">Subject</DotHint>}
      />
      <Select
        label="Sender"
        search
        loading={isFetchingSenders}
        disabled={!isEditable}
        {...senderId}>
        {senders.map((sender, i) => (
          <SelectItem key={i} disabled={!hasSenders && !sender.default} value={sender.id}>
            {sender.fromName ? (
              <span>
                <b>{sender.fromName}</b> {`<${sender.emailAddress}>`}
              </span>
            ) : (
              sender.emailAddress
            )}
          </SelectItem>
        ))}
      </Select>
      {isEditable && <SenderHint resource="a campaign" />}
      <Select
        multiple
        label={<DotHint id="campaign.settings.lists">Lists</DotHint>}
        search
        loading={isFetchingLists}
        disabled={!isEditable}
        {...listIds}>
        {lists.map((list, i) => (
          <SelectItem key={i} value={list.id}>
            {list.name} <span className="color grey">({list.subscribedCount || 0})</span>
          </SelectItem>
        ))}
      </Select>
      {isCreateListHint && (
        <div className="ui info small message">
          To create a campaign you need a list. <Link to="/lists/new">Create list</Link>
        </div>
      )}
      {isSegmentVisible && (
        <Select
          label="List segment"
          search
          loading={isFetchingSegments}
          disabled={!isEditable}
          placeholder="Select segment"
          {...segmentId}>
          <SelectItem value={''}>Select segment</SelectItem>
          {segments.map((segment, i) => (
            <SelectItem key={i} value={segment.id}>
              {segment.name}
            </SelectItem>
          ))}
        </Select>
      )}
      <Button primary loading={isLoading} type="submit" disabled={invalid || isLoading}>
        {buttonText || 'Update'}
      </Button>
    </form>
  );
};

SettingsForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isFetchingLists: PropTypes.bool.isRequired,
  isEditable: PropTypes.bool.isRequired,
  isFetchingSenders: PropTypes.bool.isRequired,
  hasSenders: PropTypes.bool.isRequired,
  fields: PropTypes.shape({
    name: PropTypes.object.isRequired,
    subject: PropTypes.object.isRequired,
    listIds: PropTypes.object.isRequired
  }).isRequired,
  lists: PropTypes.arrayOf(PropTypes.object).isRequired,
  senders: PropTypes.arrayOf(PropTypes.object).isRequired,
  buttonText: PropTypes.string,
  recipientsPerCampaign: PropTypes.number,
  segments: PropTypes.array,
  isFetchingSegments: PropTypes.bool,
  values: PropTypes.object.isRequired
};

export default SettingsForm;
