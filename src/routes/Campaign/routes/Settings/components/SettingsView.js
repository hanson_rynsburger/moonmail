import React from 'react';
import PropTypes from 'prop-types';
import SettingsForm from '../containers/SettingsFromContainer';

const Settings = ({campaignId, updateCampaign, isUpdating, ...formProps}) => {
  const submit = formProps => {
    updateCampaign(campaignId, formProps);
  };
  return (
    <section className="ui vertical segment">
      <div className="ui stackable grid">
        <SettingsForm {...formProps} onFormSubmit={submit} isLoading={isUpdating} />
      </div>
    </section>
  );
};

Settings.propTypes = {
  isUpdating: PropTypes.bool.isRequired,
  updateCampaign: PropTypes.func.isRequired,
  campaignId: PropTypes.string.isRequired
};

export default Settings;
