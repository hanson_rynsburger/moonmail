import {connect} from 'react-redux';
import SettingsView from '../components/SettingsView';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';

const mapStateToProps = (state, ownProps) => {
  return {
    campaignId: ownProps.params.campaignId,
    isUpdating: selectors.getIsUpdating(state)
  };
};

export default connect(mapStateToProps, actions)(SettingsView);
