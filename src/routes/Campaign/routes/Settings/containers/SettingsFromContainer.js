import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {reduxForm} from 'redux-form';
import SettingsForm from '../components/SettingsForm';
import {createValidator} from 'lib/validator';
import * as profileSelectors from 'modules/profile/selectors';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import listSelectors from 'modules/lists/selectors';
import segmentsSelectors from 'modules/segments/selectors';
import segmentsActions from 'modules/segments/actions';
import sendersSelectors from 'modules/senders/selectors';

class SettingsFromContainer extends Component {
  static propTypes = {
    fetchSegments: PropTypes.func.isRequired,
    clearSegments: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
    fields: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {fetchSegments, values} = this.props;
    const listIds = values.listIds || [];
    if (listIds.length === 1) {
      fetchSegments(listIds[0]);
    }
  }

  componentWillReceiveProps({values: {listIds = []}}) {
    const oldListIds = this.props.values.listIds || [];
    if (listIds[0] !== oldListIds[0]) {
      this.props.fields.segmentId.onChange(null);
      if (listIds.length === 1 && listIds[0] !== '') {
        this.props.fetchSegments(listIds[0], true);
      } else {
        this.props.clearSegments();
      }
    }
  }

  render() {
    return <SettingsForm {...this.props} />;
  }
}

const validateCampaign = createValidator({
  name: 'required|max:140',
  subject: 'required',
  listIds: 'required',
  senderId: 'required'
});

const mapStateToProps = state => {
  const campaign = selectors.getActiveResource(state);
  const hasSenders = profileSelectors.getHasSenders(state);
  const plan = profileSelectors.getPlan(state);
  const recipientsPerCampaign = profileSelectors.getLimits(state).recipientsPerCampaign;
  const getListById = listSelectors.getResourceGetter(state);

  const validate = data => {
    const errors = validateCampaign(data);
    if (!recipientsPerCampaign) return errors;
    const totalRecipients = data.listIds.reduce(
      (total, id) => total + getListById(id).subscribedCount,
      0
    );
    if (totalRecipients > recipientsPerCampaign) {
      errors.listIds = [
        `You can only send this campaign to ${recipientsPerCampaign} or less subscribers. 
        You're on the ${plan.title} Plan`
      ];
    }
    return errors;
  };

  return {
    isFetchingLists: listSelectors.getIsFetchingAll(state),
    isEditable: selectors.getIsEditable(state),
    recipientsPerCampaign,
    lists: listSelectors.getAvailableLists(state),
    senders: sendersSelectors.getVerifiedSenders(state),
    segments: segmentsSelectors.getSortedResources(state),
    isFetchingSegments: segmentsSelectors.getIsFetchingAll(state),
    isFetchingSenders: sendersSelectors.getIsFetchingAll(state),
    hasSenders,
    initialValues: {
      listIds: [],
      senderId: sendersSelectors.getDefaultResourceId(state),
      ...campaign
    },
    validate
  };
};

export default reduxForm(
  {
    form: 'campaignSettings',
    fields: ['name', 'subject', 'listIds', 'senderId', 'segmentId']
  },
  mapStateToProps,
  {
    ...actions,
    fetchSegments: segmentsActions.fetchSegments,
    clearSegments: segmentsActions.clearSegments
  }
)(SettingsFromContainer);
