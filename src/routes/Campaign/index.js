import Campaign from './containers/CampaignContainer';
import Index from './routes/Index';
import Settings from './routes/Settings';
import Editor from './routes/Editor';
import Html from './routes/Html';
import Preview from './routes/Preview';
import Raw from './routes/Raw';
import Report from './routes/Report';
import ClicksStatistics from './routes/ClicksStatistics';
import Template from './routes/Template';

export default {
  path: '/campaigns/:campaignId',
  indexRoute: Index,
  component: Campaign,
  hideFooter: true,
  childRoutes: [Settings, Html, Editor, Preview, Raw, Report, ClicksStatistics, Template]
};
