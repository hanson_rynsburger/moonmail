import {reduxForm} from 'redux-form';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import listsSelectors from 'modules/lists/selectors';
import sendersSelectors from 'modules/senders/selectors';
import segmentsSelectors from 'modules/segments/selectors';
import ScheduleCampaignView from '../components/ScheduleCampaign';

const validate = values => {
  const errors = {};
  if (!values.scheduleAt) {
    errors.scheduleAt = ['Delivery date and time are required'];
  } else if (typeof values.scheduleAt === 'string') {
    errors.scheduleAt = ['Invalid date format'];
  } else if (values.scheduleAt * 1000 <= new Date().valueOf()) {
    errors.scheduleAt = ['Delivery date already passed'];
  }
  return errors;
};

const mapStateToProps = state => {
  const campaign = selectors.getActiveResource(state);
  return {
    isScheduling: selectors.getIsScheduling(state),
    isOpen: selectors.getIsScheduleStarted(state),
    lists: listsSelectors.getActiveCampaignLists(state),
    segment: segmentsSelectors.getResourceById(state, campaign.segmentId),
    campaign,
    sender: sendersSelectors.getResourceById(state, campaign.senderId)
  };
};

export default reduxForm(
  {
    form: 'scheduleCampaign',
    fields: ['scheduleAt'],
    validate
  },
  mapStateToProps,
  actions
)(ScheduleCampaignView);
