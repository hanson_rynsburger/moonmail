import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import selectors from 'modules/campaigns/selectors';

export default function(ComposedComponent) {
  class RequirePermission extends Component {
    static contextTypes = {
      router: PropTypes.object
    };

    static propTypes = {
      campaignId: PropTypes.string.isRequired,
      isPermitted: PropTypes.bool.isRequired,
      campaign: PropTypes.object.isRequired
    };

    handleRedirect({campaign, isPermitted}) {
      if (campaign._isUpToDate && !isPermitted) {
        this.context.router.replace('/campaigns');
      }
    }

    componentWillReceiveProps(nextProps) {
      this.handleRedirect(nextProps);
    }

    componentWillMount() {
      this.handleRedirect(this.props);
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  const mapStateToProps = state => ({
    campaign: selectors.getActiveResource(state)
  });

  return connect(mapStateToProps)(RequirePermission);
}
