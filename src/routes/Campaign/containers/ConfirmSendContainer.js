import {connect} from 'react-redux';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import listsSelectors from 'modules/lists/selectors';
import sendersSelectors from 'modules/senders/selectors';
import segmentsSelectors from 'modules/segments/selectors';
import ConfirmSend from '../components/ConfirmSend';

const mapStateToProps = state => {
  const campaign = selectors.getActiveResource(state);
  return {
    isSending: selectors.getIsSending(state),
    isOpen: selectors.getIsSendStarted(state),
    lists: listsSelectors.getActiveCampaignLists(state),
    segment: segmentsSelectors.getResourceById(state, campaign.segmentId),
    campaign,
    sender: sendersSelectors.getResourceById(state, campaign.senderId)
  };
};

export default connect(mapStateToProps, actions)(ConfirmSend);
