import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import listsActions from 'modules/lists/actions';
import {getIsVerified} from 'modules/profile/selectors';
import CampaignView from '../components/CampaignView';

class Campaign extends Component {
  static propTypes = {
    fetchCampaign: PropTypes.func.isRequired,
    fetchLists: PropTypes.func.isRequired,
    campaignId: PropTypes.string.isRequired
  };

  componentDidMount() {
    const {fetchCampaign, fetchLists, campaignId} = this.props;
    fetchCampaign(campaignId);
    fetchLists();
  }

  render() {
    return <CampaignView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  campaign: selectors.getActiveResource(state),
  isEditable: selectors.getIsEditable(state),
  isFetching: selectors.getIsFetching(state),
  isScheduled: selectors.getIsScheduled(state),
  campaignId: ownProps.params.campaignId,
  isVerified: getIsVerified(state)
});

export default connect(mapStateToProps, {
  ...actions,
  fetchLists: listsActions.fetchLists
})(Campaign);
