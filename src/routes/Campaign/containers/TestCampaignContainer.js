import {reduxForm} from 'redux-form';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import {stringToArray} from 'lib/utils';
import Validator from 'lib/validator';
import TestCampaign from '../components/TestCampaign';

const mapStateToProps = state => ({
  isTesting: selectors.getIsTesting(state),
  isOpen: selectors.getIsTestStarted(state),
  campaign: selectors.getActiveResource(state),
  body: selectors.getCampaignBody(state)
});

const rules = {
  email: 'required|email'
};

const validate = values => {
  let errors = {};
  stringToArray(values.email).forEach(email => {
    const validator = new Validator({email}, rules);
    validator.passes();
    errors = validator.errors.all();
  });
  return errors;
};

export default reduxForm(
  {
    form: 'testCampaign',
    fields: ['email'],
    validate
  },
  mapStateToProps,
  actions
)(TestCampaign);
