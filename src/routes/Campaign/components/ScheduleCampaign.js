import React from 'react';
import PropTypes from 'prop-types';
import Confirm from 'components/Modal/Confirm';
import CampaignSummary from './CampaignSummary';
import DateTimeInput from 'components/DateTimeInput';
import DotHint from 'components/DotHint';
import moment from 'moment';

const ScheduleCampaign = ({
  fields: {scheduleAt},
  resetForm,
  invalid,
  handleSubmit,
  isOpen,
  isScheduling,
  campaign,
  lists,
  scheduleCampaignCancel,
  scheduleCampaign,
  sender,
  segment
}) => {
  const onSubmit = async ({scheduleAt}) => {
    await scheduleCampaign(campaign.id, {scheduleAt});
  };
  const onCancel = () => {
    scheduleCampaignCancel();
    resetForm();
  };
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isScheduling}
      isDisabled={invalid}
      onCancel={onCancel}
      onConfirm={handleSubmit(onSubmit)}
      scrolling={false}
      headerText="Set up your schedule"
      confirmText="Schedule campaign"
      confirmClass="primary">
      <form className="ui form" onSubmit={handleSubmit(onSubmit)}>
        <DateTimeInput
          {...scheduleAt}
          dateFormat={'MMMM D, YYYY'}
          isValidDate={date => date.isAfter(moment().subtract(1, 'day'))}
          label={<DotHint id="campaign.preview.schedule_datetime">Delivery date and time</DotHint>}
        />
      </form>
      <h5>Review the feedback below before scheduling your campaign.</h5>
      <CampaignSummary campaign={campaign} sender={sender} lists={lists} segment={segment} />
    </Confirm>
  );
};

ScheduleCampaign.propTypes = {
  campaign: PropTypes.object.isRequired,
  scheduleCampaign: PropTypes.func.isRequired,
  scheduleCampaignCancel: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  lists: PropTypes.array.isRequired,
  isScheduling: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  sender: PropTypes.object,
  segment: PropTypes.object,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired
};

export default ScheduleCampaign;
