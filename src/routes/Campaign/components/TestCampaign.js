import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import {stringToArray} from 'lib/utils';
import Confirm from 'components/Modal/Confirm';

const TestCampaign = ({
  isOpen,
  isTesting,
  fields: {email},
  handleSubmit,
  invalid,
  campaign,
  body,
  testCampaign,
  campaignId,
  resetForm,
  testCampaignCancel
}) => {
  const onSubmit = async formProps => {
    const emails = stringToArray(formProps.email);
    const {subject, senderId} = campaign;
    await testCampaign(campaignId, {subject, senderId, body, emails});
    resetForm();
  };
  const onCancel = () => {
    testCampaignCancel();
    resetForm();
  };
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isTesting}
      isDisabled={invalid}
      onCancel={onCancel}
      onConfirm={handleSubmit(onSubmit)}
      headerText="Send test email"
      confirmText="Send test email"
      confirmClass="primary">
      <form className="ui form" onSubmit={handleSubmit(onSubmit)}>
        <Input type="text" label="Emails separated by comma" {...email} />
      </form>
    </Confirm>
  );
};

TestCampaign.propTypes = {
  campaignId: PropTypes.string.isRequired,
  campaign: PropTypes.shape({
    body: PropTypes.string,
    subject: PropTypes.string
  }).isRequired,
  fields: PropTypes.object.isRequired,
  testCampaign: PropTypes.func.isRequired,
  testCampaignCancel: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  isTesting: PropTypes.bool.isRequired,
  body: PropTypes.string.isRequired
};

export default TestCampaign;
