import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/Loader';
import CampaignHeader from './CampaignHeader';
import {NO_LOCALIZE} from 'lib/constants';
import cx from 'classnames';

const CampaignView = ({campaign, isEditable, isFetching, isScheduled, isVerified, children}) => {
  if (isFetching || !campaign.id) {
    return (
      <section className="ui basic segment">
        <Loader active dimmer />
      </section>
    );
  }
  return (
    <section className="ui basic segment" style={{height: '100%'}}>
      <h1 className={cx('ui align left header', NO_LOCALIZE)}>{campaign.name}</h1>
      <CampaignHeader {...{campaign, isEditable, isVerified, isScheduled}} />
      {children}
    </section>
  );
};

CampaignView.propTypes = {
  campaign: PropTypes.shape({
    id: PropTypes.string,
    status: PropTypes.string
  }),
  isEditable: PropTypes.bool.isRequired,
  isScheduled: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  children: PropTypes.element.isRequired,
  isVerified: PropTypes.bool
};

export default CampaignView;
