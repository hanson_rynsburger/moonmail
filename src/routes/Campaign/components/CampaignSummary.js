import React from 'react';
import PropTypes from 'prop-types';
import {DEFAULT_SENDER_EMAIL} from 'modules/senders/constants';
import pluralize from 'pluralize';
import NL from 'components/NoLocalize';
import {joinComponents} from 'lib/utils';

const CampaignSummary = ({campaign, sender = {}, lists, segment}) => {
  const listNames = lists.map(l => l && l.name).join(', ');
  const attachments = campaign.attachments || [];
  const recipientsCount = lists.reduce((a, {subscribedCount = 0}) => a + subscribedCount, 0);
  const formatSender = ({name, emailAddress}) => {
    if (!name) return emailAddress;
    return `${name} <${emailAddress}>`;
  };
  return (
    <div className="ui list">
      <div className="item">
        Subject:{' '}
        <NL>
          <b>{campaign.subject}</b>
        </NL>
      </div>
      <div className="item">
        Sender:{' '}
        <b>
          {campaign.senderId ? (
            <NL>{formatSender(sender)}</NL> || 'Loading...'
          ) : (
            <NL>{DEFAULT_SENDER_EMAIL}</NL>
          )}
        </b>
      </div>
      <div className="item">
        {pluralize('List', lists.length)}: <NL el="b">{listNames}</NL>
      </div>
      {!segment.id && (
        <div className="item">
          Recipients: <NL el="b">{recipientsCount}</NL>
        </div>
      )}
      {segment.id && (
        <div className="item">
          Segment: <NL el="b">{segment.name}</NL>
        </div>
      )}
      {attachments.length > 0 && (
        <div className="item">
          {pluralize('Attachment', campaign.attachments.length)}:{' '}
          <NL el="b">
            {joinComponents(campaign.attachments, (file, i) => (
              <a key={i} target="_blank" rel="noopener noreferrer" href={file.url}>
                {file.name}
              </a>
            ))}
          </NL>
        </div>
      )}
    </div>
  );
};

CampaignSummary.propTypes = {
  campaign: PropTypes.shape({
    subject: PropTypes.string
  }).isRequired,
  segment: PropTypes.object,
  sender: PropTypes.shape({
    emailAddress: PropTypes.string
  }),
  lists: PropTypes.array.isRequired
};

export default CampaignSummary;
