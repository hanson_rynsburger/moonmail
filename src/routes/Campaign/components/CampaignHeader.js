import React from 'react';
import PropTypes from 'prop-types';
import {getRelativeItemLink} from 'components/Links';
import DropDownMenu from 'components/DropDownMenu';
import {Desktop, Mobile} from 'components/MediaQueries';

const CampaignHeader = ({campaign, isEditable, isVerified}) => {
  const Link = getRelativeItemLink(`/campaigns/${campaign.id}/`);
  const isSent = ['sent', 'sending'].includes(campaign.status);
  const links = {};

  links.settings = <Link to="settings">Settings</Link>;
  links.preview = <Link to="preview">{`Preview ${isEditable ? '& send' : ''}`}</Link>;

  if (isSent) {
    links.clicksStatistics = <Link to="clicks-statistics">Clicks Statistics</Link>;
    links.report = <Link to="report">Report</Link>;
  }

  if (isEditable) {
    links.template = (
      <Link to="template" disabled={!isVerified}>
        Select template
      </Link>
    );
    links.html = <Link to="html">Edit code</Link>;
    if (campaign.template) {
      links.editor = (
        <Link to="editor" disabled={!isVerified}>
          Edit template
        </Link>
      );
    }
  } else {
    links.raw = <Link to="raw">HTML</Link>;
  }
  return (
    <div className="ui secondary pointing large menu no-padding">
      <Desktop>
        {links.report}
        {links.clicksStatistics}
        {links.template}
        {links.editor}
        {links.html}
        {links.raw}
        {links.settings}
        {links.preview}
      </Desktop>
      <Mobile>
        {links.html}
        {links.preview}
        {links.report}
        <DropDownMenu className="item">
          More
          <i className="dropdown icon" />
          <div className="left menu transition hidden">
            {links.clicksStatistics}
            {links.template}
            {links.editor}
            {links.raw}
            {links.settings}
          </div>
        </DropDownMenu>
      </Mobile>
    </div>
  );
};

CampaignHeader.propTypes = {
  campaign: PropTypes.object.isRequired,
  isEditable: PropTypes.bool.isRequired,
  isVerified: PropTypes.bool
};

export default CampaignHeader;
