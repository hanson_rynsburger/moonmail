import React from 'react';
import PropTypes from 'prop-types';
import Confirm from 'components/Modal/Confirm';
import CampaignSummary from './CampaignSummary';

const ConfirmSend = ({
  isOpen,
  isSending,
  campaign,
  lists,
  sendCampaignCancel,
  sendCampaign,
  sender,
  segment
}) => {
  const send = () => {
    sendCampaign(campaign.id, campaign);
  };
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isSending}
      onCancel={sendCampaignCancel}
      onConfirm={send}
      headerText="You’re all set to send!"
      confirmText="Send campaign"
      confirmClass="primary">
      <h5>Review the feedback below before sending your campaign.</h5>
      <CampaignSummary {...{campaign, sender, lists, segment}} />
    </Confirm>
  );
};

ConfirmSend.propTypes = {
  campaign: PropTypes.object.isRequired,
  sendCampaign: PropTypes.func.isRequired,
  sendCampaignCancel: PropTypes.func.isRequired,
  lists: PropTypes.array.isRequired,
  isSending: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  sender: PropTypes.object,
  segment: PropTypes.object
};

export default ConfirmSend;
