import React from 'react';
import PropTypes from 'prop-types';
import PublishForm from '../containers/PublishTemplateFormContainer';

const PublishTemplateView = ({template, isPublishing, handlePublish}) => {
  return (
    <section className="ui basic segment padded-bottom">
      <h1>Sell your template in template market</h1>
      <PublishForm initialValues={template} isPublishing={isPublishing} onPublish={handlePublish} />
    </section>
  );
};

PublishTemplateView.propTypes = {
  template: PropTypes.object.isRequired,
  isPublishing: PropTypes.bool.isRequired,
  handlePublish: PropTypes.func.isRequired
};

export default PublishTemplateView;
