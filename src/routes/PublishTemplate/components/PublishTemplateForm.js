import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Select, {SelectItem} from 'components/Select';
import CATEGORIES from 'modules/marketTemplates/categories';
import classNames from './PublishTemplateForm.scss';
import Button from 'components/Button';
import FileButton from 'components/FileButton';
import {Aux, humanize} from 'lib/utils';

const PublishTemplateForm = ({
  isPublishing,
  buttonText = 'Publish',
  fields: {name, description, categories, tags, price, designer},
  isFetchingTags,
  templateTags,
  handleSubmit,
  onPublish,
  values,
  invalid,
  onThumbChange,
  isUploadingThumb,
  onImageChange,
  isUploadingImage,
  onImageDelete
}) => {
  const onSubmit = formProps => {
    onPublish({...formProps, approved: true});
  };
  const onDelete = (e, i) => {
    e.preventDefault();
    onImageDelete(i);
  };
  return (
    <div className="ui stackable grid">
      <form className="ui form ten wide column" onSubmit={handleSubmit(onSubmit)}>
        <div className="field">
          <label>Thumbnail</label>
          <a href={values.thumbnail} target="_blank" className={classNames.previewImage}>
            <img className="ui fluid image" src={values.thumbnail} />
          </a>
          <div className="hint">JPEG or PNG 250x250px</div>
        </div>
        <div className="field">
          <FileButton accept="image/*" onChange={onThumbChange} loading={isUploadingThumb}>
            Upload custom thumbnail
          </FileButton>
        </div>
        <div className="field">
          <div className={classNames.images}>
            {values.images.map((img, i) => (
              <a href={img} target="_blank" key={i} className={classNames.previewImage}>
                <i className="icon red close" onClick={e => onDelete(e, i)} />
                <img className="ui fluid image" src={img} />
              </a>
            ))}
          </div>
        </div>
        <div className="field">
          <FileButton accept="image/*" onChange={onImageChange} loading={isUploadingImage}>
            Upload preview image
          </FileButton>
        </div>
        <Input type="text" {...name} />
        <Input type="number" {...price} leftLabel="$" leftLabelClass="basic" />
        <Input component="textarea" rows={5} {...description} />
        <Select {...categories} multiple>
          {Object.keys(CATEGORIES).map(key => (
            <Aux key={key}>
              <div className="header">{humanize(key)}</div>
              {CATEGORIES[key].map(sub => (
                <SelectItem key={sub} value={sub}>
                  {humanize(sub)}
                </SelectItem>
              ))}
            </Aux>
          ))}
        </Select>
        <Select
          {...tags}
          multiple
          allowAdditions
          hint="type a tag and press enter"
          loading={isFetchingTags}>
          {templateTags.map((tag, i) => (
            <SelectItem key={i} value={tag.key}>
              {tag.key} <span className="color grey">({tag.doc_count || 0})</span>
            </SelectItem>
          ))}
        </Select>
        <h3>Designer</h3>
        <Input type="text" {...designer.name} label="Name" />
        <Input type="email" {...designer.email} label="Email" />
        <Input type="url" {...designer.url} label="Website" />
        <Button primary disabled={invalid} loading={isPublishing} onClick={handleSubmit(onSubmit)}>
          {buttonText}
        </Button>
      </form>
    </div>
  );
};

PublishTemplateForm.propTypes = {
  values: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  onPublish: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isPublishing: PropTypes.bool.isRequired,
  buttonText: PropTypes.string,
  isFetchingTags: PropTypes.bool.isRequired,
  templateTags: PropTypes.array.isRequired,
  onThumbChange: PropTypes.func.isRequired,
  isUploadingThumb: PropTypes.bool.isRequired,
  onImageChange: PropTypes.func.isRequired,
  isUploadingImage: PropTypes.bool.isRequired,
  onImageDelete: PropTypes.func.isRequired
};

export default PublishTemplateForm;
