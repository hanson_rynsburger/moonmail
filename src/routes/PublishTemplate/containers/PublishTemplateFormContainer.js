import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {reduxForm} from 'redux-form';
import selectors from 'modules/templateTags/selectors';
import actions from 'modules/templateTags/actions';
import PublishTemplateForm from '../components/PublishTemplateForm';
import {getProfile, getAddress, getUserId} from 'modules/profile/selectors';
import {addMessage} from 'modules/messages/actions';
import S3 from 'lib/s3Bucket';
import cuid from 'cuid';

class PublishTemplateFormContainer extends Component {
  static propTypes = {
    fetchTags: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
    values: PropTypes.object.isRequired,
    userId: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    this.bucket = new S3({
      userId: props.userId,
      bucketName: APP_CONFIG.templateImagesBucket
    });
    this.state = {
      isUploadingThumb: false,
      isUploadingImage: false
    };
  }

  onThumbChange = async e => {
    const file = e.target.files[0];
    if (!file) return;
    const {values, fields, addMessage} = this.props;
    this.setState({isUploadingThumb: true});
    try {
      const {fileUrl} = await this.bucket.uploadFile(file, {
        customName: `thumb_${values.id}`
      });
      fields.thumbnail.onChange(`${fileUrl}?v=${+new Date()}`);
      this.setState({isUploadingThumb: false});
    } catch (error) {
      addMessage({text: error});
    }
  };

  onImageChange = async e => {
    const file = e.target.files[0];
    if (!file) return;
    const {values, fields, addMessage} = this.props;
    this.setState({isUploadingImage: true});
    try {
      const {fileUrl} = await this.bucket.uploadFile(file, {
        customName: `img_${cuid()}_${values.id}`
      });
      fields.images.addField(fileUrl);
      this.setState({isUploadingImage: false});
    } catch (error) {
      addMessage({text: error});
    }
  };

  onImageDelete = async index => {
    if (window.confirm('Remove this image?')) {
      const {values, fields, addMessage} = this.props;
      const imgUrl = values.images[index];
      const imgName = imgUrl.split('/').pop();
      try {
        await this.bucket.deleteFile(imgName.split('?')[0]);
        fields.images.removeField(index);
      } catch (error) {
        addMessage({text: error});
      }
    }
  };

  componentDidMount() {
    this.props.fetchTags();
  }

  render() {
    return (
      <PublishTemplateForm
        {...this.props}
        isUploadingThumb={this.state.isUploadingThumb}
        isUploadingImage={this.state.isUploadingImage}
        onThumbChange={this.onThumbChange}
        onImageChange={this.onImageChange}
        onImageDelete={this.onImageDelete}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const profile = getProfile(state);
  const address = getAddress(state);
  return {
    templateTags: selectors.getResources(state),
    isFetchingTags: selectors.getIsFetchingAll(state),
    userId: getUserId(state),
    initialValues: {
      designer: {
        name: address.company || profile.name,
        email: profile.email,
        url: address.websiteUrl
      },
      ...ownProps.initialValues
    }
  };
};

export default reduxForm(
  {
    form: 'publishTemplate',
    fields: [
      'name',
      'description',
      'categories',
      'tags',
      'thumbnail',
      'id',
      'body',
      'price',
      'images[]',
      'designer.name',
      'designer.email',
      'designer.url'
    ]
  },
  mapStateToProps,
  {
    fetchTags: actions.fetchResources,
    addMessage
  }
)(PublishTemplateFormContainer);
