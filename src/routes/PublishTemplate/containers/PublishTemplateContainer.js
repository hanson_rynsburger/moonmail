import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import PublishTemplateView from '../components/PublishTemplateView';
import selectors from 'modules/templates/selectors';
import actions from 'modules/templates/actions';
import marketSelectors from 'modules/marketTemplates/selectors';
import marketActions from 'modules/marketTemplates/actions';

class PublishTemplateContainer extends Component {
  static propTypes = {
    fetchTemplate: PropTypes.func.isRequired,
    templateId: PropTypes.string.isRequired,
    router: PropTypes.object.isRequired
  };

  handlePublish = async data => {
    const {publishTemplate, router} = this.props;
    await publishTemplate(data);
    router.push('/templates');
  };

  componentDidMount() {
    const {fetchTemplate, templateId} = this.props;
    fetchTemplate(templateId);
  }

  render() {
    return <PublishTemplateView {...this.props} handlePublish={this.handlePublish} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  templateId: ownProps.params.templateId,
  template: selectors.getResourceById(state, ownProps.params.templateId),
  isPublishing: marketSelectors.getIsCreating(state)
});

export default connect(mapStateToProps, {
  fetchTemplate: actions.fetchResource,
  publishTemplate: marketActions.createResource
})(PublishTemplateContainer);
