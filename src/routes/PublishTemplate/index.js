import PublishTemplate from './containers/PublishTemplateContainer';

export default {
  path: 'templates/:templateId/publish',
  component: PublishTemplate
};
