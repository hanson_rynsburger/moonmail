import React, {Component} from 'react';
import {isAuthenticated} from 'modules/app/auth';
import {withRouter} from 'react-router';
import PropTypes from 'prop-types';
import Loader from 'components/Loader';

class SignInContainer extends Component {
  static propTypes = {
    router: PropTypes.object.isRequired
  };

  componentDidMount() {
    if (isAuthenticated()) {
      this.props.router.replace('/');
    }
  }

  render() {
    return <Loader active inline={false} />;
  }
}

export default withRouter(SignInContainer);
