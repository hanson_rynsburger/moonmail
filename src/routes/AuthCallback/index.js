import AuthCallback from './AuthCallback';
import {CALLBACK} from 'modules/app/auth';

export default {
  path: CALLBACK,
  component: AuthCallback,
  isPlain: true
};
