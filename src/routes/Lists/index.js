import Lists from './containers/ListsContainer';

export default {
  path: '/lists',
  component: Lists
};
