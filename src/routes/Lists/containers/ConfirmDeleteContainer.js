import {reduxForm} from 'redux-form';
import actions from 'modules/lists/actions';
import selectors from 'modules/lists/selectors';
import ConfirmDelete from '../components/ConfirmDelete';

const mapStateToProps = state => ({
  isDeleting: selectors.getIsDeleting(state),
  isOpen: selectors.getIsDeleteStarted(state),
  list: selectors.getSelectedResource(state)
});

const validate = ({name}, props) => {
  const errors = {};
  if (name !== props.list.name) {
    errors.name = ['Please enter correct name of the list you want to delete'];
  }
  return errors;
};

export default reduxForm(
  {
    form: 'deleteList',
    fields: ['name'],
    validate
  },
  mapStateToProps,
  actions
)(ConfirmDelete);
