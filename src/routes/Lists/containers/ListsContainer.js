import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/lists/actions';
import selectors from 'modules/lists/selectors';
import ListsView from '../components/ListsView';

class Lists extends Component {
  static propTypes = {
    fetchLists: PropTypes.func.isRequired,
    lists: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      searchString: ''
    };
  }

  componentDidMount() {
    this.props.fetchLists();
  }

  handleSearch = searchString => {
    this.setState({searchString});
  };

  filter = ({name = ''}) => {
    const itemName = name.toLowerCase();
    const str = this.state.searchString.toLowerCase();
    return itemName.includes(str);
  };

  render() {
    const lists =
      this.state.searchString.length > 1 ? this.props.lists.filter(this.filter) : this.props.lists;
    return (
      <ListsView
        {...this.props}
        handleSearch={this.handleSearch}
        searchString={this.state.searchString}
        lists={lists}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    lists: selectors.getSortedResources(state),
    isFetching: selectors.getIsFetchingAll(state),
    isDeleting: selectors.getIsDeleting(state)
  };
};

export default connect(mapStateToProps, actions)(Lists);
