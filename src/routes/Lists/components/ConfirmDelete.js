import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Confirm from 'components/Modal/Confirm';
import NL from 'components/NoLocalize';

const ConfirmDelete = ({
  isOpen,
  isDeleting,
  fields: {name},
  handleSubmit,
  deleteListCancel,
  deleteList,
  list,
  invalid,
  resetForm
}) => {
  const onSubmit = async () => {
    await deleteList(list.id);
    resetForm();
  };
  const onCancel = () => {
    deleteListCancel();
    resetForm();
  };
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isDeleting}
      isDisabled={invalid}
      onCancel={onCancel}
      onConfirm={handleSubmit(onSubmit)}
      confirmText="Delete list"
      confirmClass="negative">
      <p>
        This action will delete <NL el="b">{list.name}</NL> and all subscribers in it. Please type
        the list name to confirm.
      </p>
      <form className="ui form" onSubmit={handleSubmit(onSubmit)}>
        <Input label="List name" type="text" {...name} />
      </form>
    </Confirm>
  );
};

ConfirmDelete.propTypes = {
  list: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  deleteList: PropTypes.func.isRequired,
  deleteListCancel: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired
};

export default ConfirmDelete;
