import React from 'react';
import PropTypes from 'prop-types';
import DropDownMenu from 'components/DropDownMenu';
import NoLocalize from 'components/NoLocalize';
import {formatDate, formatRate} from 'lib/utils';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';
import {LIST_STATUS_DESCRIPTION} from 'modules/lists/constants';
import PopUp from 'components/PopUp';
import {getRelativeItemLink, getRelativeLink} from 'components/Links';

const ListItem = ({list, onDelete}) => {
  const ItemLink = getRelativeItemLink(`lists/${list.id}/`);
  const Link = getRelativeLink(`lists/${list.id}/`);
  const unsubscribeRate = formatRate(list.unsubscribedCount / list.total);
  const bounceRate = formatRate(list.bouncedCount / list.total);
  const iconClass = cx('large icon', {
    'inverted grey file alternate outline': !list.isProcessing,
    'teal refresh loading': list.isProcessing
  });
  return (
    <div className="item">
      <div className="right floated content">
        <Link to="add" className="ui icon button">
          <i className="add user icon" />
        </Link>
        <div className="ui buttons">
          <Link to="stats" className="ui button">
            Stats
          </Link>
          <DropDownMenu className="button icon floating">
            <i className="dropdown icon" />
            <div className="menu">
              <ItemLink>Subscribers</ItemLink>
              {!list.isProcessing && <ItemLink to="import">Import</ItemLink>}
              {!list.isProcessing && <ItemLink to="exports">Exports</ItemLink>}
              <ItemLink to="forms">Signup forms</ItemLink>
              <ItemLink to="settings">Settings</ItemLink>
              {!list.isProcessing && (
                <a className="item" onClick={() => onDelete(list.id)}>
                  Delete
                </a>
              )}
            </div>
          </DropDownMenu>
        </div>
      </div>
      <PopUp
        content={LIST_STATUS_DESCRIPTION[list.status] || 'Is ready for use'}
        position="bottom left"
        variation="wide"
        className="icon-wrapper">
        <i className={iconClass} />
      </PopUp>
      <div className="content">
        <div className="content flex">
          <div>
            <h3 className={cx('header truncate', NO_LOCALIZE)}>
              <Link title={list.name}>{list.name}</Link>
            </h3>
            <small className="description">
              Created
              <NoLocalize> {formatDate(list.createdAt, {fromNow: true})}</NoLocalize>
            </small>
          </div>
          <div className="ui relaxed large horizontal list mobile hidden" style={{padding: 0}}>
            <div className="item text grey">
              <h3 className="ui header grey">{list.subscribedCount || 0}</h3>
              Subscribers
            </div>
            <div className="item text grey">
              <h3 className="ui header grey">{unsubscribeRate || 0}%</h3>
              Unsubscribe rate
            </div>
            <div className="item text grey">
              <h3 className="ui header grey">{bounceRate || 0}%</h3>
              Bounce rate
            </div>
          </div>
          <div />
        </div>
      </div>
    </div>
  );
};

ListItem.propTypes = {
  list: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    createdAt: PropTypes.number.isRequired
  }),
  onDelete: PropTypes.func.isRequired
};

export default ListItem;
