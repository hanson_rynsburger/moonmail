import React from 'react';
import PropTypes from 'prop-types';
import ListItem from './ListItem';
import ConfirmDelete from '../containers/ConfirmDeleteContainer';
import ResourceList from 'components/ResourceList';
import Button from 'components/Button';
import Search from 'components/Search';

const ListsView = ({lists, isFetching, deleteListStart, handleSearch, searchString}) => {
  return (
    <section className="ui basic segment padded-bottom">
      <div className="header-with-actions">
        <h1 className="ui header">Lists</h1>
        {(searchString.length > 0 || lists.length > 10) && (
          <Search searchDelay={0} onSearch={handleSearch} value={searchString} />
        )}
        <Button to="lists/new" primary>
          Create List
        </Button>
      </div>
      <ResourceList
        isFetching={isFetching}
        resources={lists}
        richPlaceholder={searchString.length === 0}
        richPlaceholderCTA={
          <Button to="lists/new" primary>
            Create a List to fill the Void
          </Button>
        }>
        {(list, i) => <ListItem list={list} key={i} onDelete={deleteListStart} />}
      </ResourceList>
      <ConfirmDelete />
    </section>
  );
};

ListsView.propTypes = {
  lists: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  deleteListStart: PropTypes.func.isRequired,
  handleSearch: PropTypes.func.isRequired,
  searchString: PropTypes.string
};

export default ListsView;
