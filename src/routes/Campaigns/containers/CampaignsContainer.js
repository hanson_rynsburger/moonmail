import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import CampaignsView from '../components/CampaignsView';

class Campaigns extends Component {
  static propTypes = {
    fetchCampaigns: PropTypes.func.isRequired,
    updateCampaign: PropTypes.func.isRequired,
    clearCampaigns: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    filter: PropTypes.string,
    campaigns: PropTypes.array.isRequired,
    cancelCampaignsReportPolling: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      searchString: ''
    };
  }

  componentDidMount() {
    const {fetchCampaigns, filter} = this.props;
    fetchCampaigns(filter);
  }

  componentWillUnmount() {
    if (this.props.filter === 'archived') {
      this.props.clearCampaigns();
    }
    this.props.cancelCampaignsReportPolling();
  }

  updateQuery(filter) {
    this.props.router.push({
      pathname: this.props.location.pathname,
      query: {filter}
    });
  }

  handleFilterChange = async filter => {
    await this.props.fetchCampaigns(filter, filter === 'archived');
    this.updateQuery(filter);
  };

  componentWillReceiveProps({filter}) {
    if (filter !== this.props.filter && this.props.filter === 'archived') {
      this.props.fetchCampaigns(filter, true);
    }
  }

  handleSearch = searchString => {
    this.setState({searchString});
  };

  filter = ({name = ''}) => {
    const itemName = name.toLowerCase();
    const str = this.state.searchString.toLowerCase();
    return itemName.includes(str);
  };

  render() {
    const campaigns =
      this.state.searchString.length > 1
        ? this.props.campaigns.filter(this.filter)
        : this.props.campaigns;
    return (
      <CampaignsView
        {...this.props}
        handleFilterChange={this.handleFilterChange}
        handleSearch={this.handleSearch}
        searchString={this.state.searchString}
        campaigns={campaigns}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const {filter = 'all'} = ownProps.location.query || {};
  return {
    campaigns: selectors.getFilteredCampaigns(state),
    isFetching: selectors.getIsFetchingAll(state),
    isCreating: selectors.getIsCreating(state),
    filter
  };
};

export default connect(mapStateToProps, actions)(Campaigns);
