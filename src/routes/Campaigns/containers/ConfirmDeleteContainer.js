import {connect} from 'react-redux';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';
import ConfirmDelete from '../components/ConfirmDelete';

const mapStateToProps = state => ({
  isDeleting: selectors.getIsDeleting(state),
  isOpen: selectors.getIsDeleteStarted(state),
  campaign: selectors.getSelectedResource(state)
});

export default connect(mapStateToProps, actions)(ConfirmDelete);
