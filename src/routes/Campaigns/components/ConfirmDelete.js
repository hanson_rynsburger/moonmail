import React from 'react';
import PropTypes from 'prop-types';
import Confirm from 'components/Modal/Confirm';
import NoLocalize from 'components/NoLocalize';

const ConfirmDelete = ({isOpen, isDeleting, campaign, deleteCampaignCancel, deleteCampaign}) => (
  <Confirm
    isOpen={isOpen}
    isLoading={isDeleting}
    onCancel={deleteCampaignCancel}
    onConfirm={() => deleteCampaign(campaign.id)}
    confirmText="Delete campaign"
    confirmClass="negative">
    <p>
      This action will completely delete <NoLocalize el="b">"{campaign.name}"</NoLocalize>
    </p>
  </Confirm>
);

ConfirmDelete.propTypes = {
  campaign: PropTypes.object.isRequired,
  deleteCampaign: PropTypes.func.isRequired,
  deleteCampaignCancel: PropTypes.func.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired
};

export default ConfirmDelete;
