import React from 'react';
import PropTypes from 'prop-types';
import DropDownMenu from 'components/DropDownMenu';
import NoLocalize from 'components/NoLocalize';
import {formatDate} from 'lib/utils';
import classNames from './CampaignItem.scss';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';
import {getStatuses} from 'modules/campaigns/utils';
import PopUp from 'components/PopUp';
import {getRelativeItemLink, getRelativeLink, Link} from 'components/Links';

const Aux = props => props.children;

const CampaignItem = ({campaign, onDelete, onDuplicate, onArchiveChange}) => {
  const ItemLink = getRelativeItemLink(`/campaigns/${campaign.id}/`);
  const RelativeLink = getRelativeLink(`/campaigns/${campaign.id}/`);
  const statuses = getStatuses(campaign);
  const iconClass = cx(classNames.icon, 'large icon', {
    'green check': statuses.isSent,
    'grey envelope outline': statuses.isDraft,
    'teal refresh loading': statuses.isPending,
    'red warning circle': statuses.isError,
    'teal clock outline': statuses.isScheduled
  });

  const getHint = () => {
    if (statuses.isBadReputation) {
      return (
        <Aux>
          Bad reputation. One of your lists in this campaign has a bad reputation.{' '}
          <a
            href="http://support.moonmail.io/faq/validation-and-reputation/what-are-the-acceptable-complaint-and-bounce-rates"
            rel="noopener noreferrer"
            target="_blank">
            More details
          </a>
        </Aux>
      );
    }
    if (statuses.isLimitReached) {
      return (
        <Aux>
          You can only send one campaign per day.{' '}
          <Link to="/profile/plan">
            <b>Upgrade your account</b>
          </Link>{' '}
          to send this campaign right now.
        </Aux>
      );
    }
    if (statuses.isNotReady) {
      return 'Campaign cannot be sent. Please check campaign settings. Maybe sender is missing?';
    }
    return (
      <Aux>
        {statuses.statusText}{' '}
        {statuses.isPaymentError && <Link to="/profile/billing-history">More details</Link>}
      </Aux>
    );
  };

  let dateString;
  switch (campaign.status) {
    case 'sent':
    case 'sending':
      dateString = (
        <span>
          Sent
          <NoLocalize> {formatDate(campaign.sentAt, {fromNow: true})}</NoLocalize>
        </span>
      );
      break;
    case 'scheduled':
      dateString = (
        <span>
          Scheduled for
          <NoLocalize> {formatDate(campaign.scheduledAt, {showTime: true})}</NoLocalize>
        </span>
      );
      break;
    default:
      dateString = (
        <span>
          Created
          <NoLocalize> {formatDate(campaign.createdAt, {fromNow: true})}</NoLocalize>
        </span>
      );
  }

  return (
    <div className="item">
      {!statuses.isPending && (
        <div className="right floated content">
          <div className="ui buttons">
            {statuses.isSent && (
              <RelativeLink to="report" className="ui button">
                View report
              </RelativeLink>
            )}
            {statuses.isEditable && (
              <RelativeLink to="html" className="ui button">
                Edit
              </RelativeLink>
            )}
            {statuses.isScheduled && (
              <RelativeLink to="preview" className="ui button">
                Preview & pause
              </RelativeLink>
            )}
            <DropDownMenu action="hide" className="button icon floating">
              <i className="dropdown icon" />
              <div className="menu">
                {!statuses.isScheduled && (
                  <ItemLink to="preview">Preview {statuses.isEditable && '& send'}</ItemLink>
                )}
                <ItemLink to="settings">Settings</ItemLink>
                <a className="item" onClick={() => onDuplicate(campaign.id)}>
                  Duplicate
                </a>
                {statuses.isArchivable && (
                  <a className="item" onClick={() => onArchiveChange(campaign.id, true)}>
                    Archive
                  </a>
                )}
                {statuses.isArchived && (
                  <a className="item" onClick={() => onArchiveChange(campaign.id, false)}>
                    Unarchive
                  </a>
                )}
                {(statuses.isEditable || __DEV__) && (
                  <a className="item" onClick={() => onDelete(campaign.id)}>
                    Delete
                  </a>
                )}
              </div>
            </DropDownMenu>
          </div>
        </div>
      )}
      <PopUp content={getHint()} position="bottom left" className="icon-wrapper">
        <i className={cx(iconClass, {disabled: statuses.isArchived})} />
      </PopUp>
      <div className={cx('content', NO_LOCALIZE)}>
        <h3 className={cx('header', {disabled: statuses.isArchived})}>
          <RelativeLink title={campaign.name}>{campaign.name}</RelativeLink>
        </h3>
        <small className={cx('description', {disabled: statuses.isArchived})}>{dateString}</small>
      </div>
    </div>
  );
};

CampaignItem.propTypes = {
  campaign: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    createdAt: PropTypes.number,
    sentAt: PropTypes.number
  }),
  onDelete: PropTypes.func.isRequired,
  onDuplicate: PropTypes.func.isRequired,
  onArchiveChange: PropTypes.func.isRequired
};

export default CampaignItem;
