import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import DropDownMenu from 'components/DropDownMenu';
import CampaignItem from './CampaignItem';
import ConfirmDelete from '../containers/ConfirmDeleteContainer';
import cx from 'classnames';
import {CAMPAIGN_FILTERS} from 'modules/campaigns/constants';
import {humanize} from 'lib/utils';
import ResourceList from 'components/ResourceList';
import Search from 'components/Search';

const CampaignsView = ({
  campaigns,
  isFetching,
  filter = 'all',
  deleteCampaignStart,
  duplicateCampaign,
  setCampaignArchived,
  handleFilterChange,
  isCreating,
  handleSearch,
  searchString
}) => {
  return (
    <section className="ui basic segment padded-bottom">
      <div className="header-with-actions">
        <h1 className="ui header">Campaigns</h1>
        {(searchString.length > 0 || campaigns.length > 10) && (
          <Search searchDelay={0} onSearch={handleSearch} value={searchString} />
        )}
        <DropDownMenu className={cx('floating labeled icon button', {teal: filter !== 'all'})}>
          <i className="filter icon" />
          <span className="text">{humanize(filter)}</span>
          <div className="menu">
            <div className="scrolling menu">
              {CAMPAIGN_FILTERS.map(f => (
                <div
                  key={f}
                  className={cx('item', {'active selected': f === filter})}
                  onClick={() => handleFilterChange(f)}>
                  {humanize(f)}
                </div>
              ))}
            </div>
          </div>
        </DropDownMenu>
        <Button to="campaigns/new" primary>
          Create Campaign
        </Button>
      </div>
      <ResourceList
        resources={campaigns}
        resourceName="campaigns"
        richPlaceholder={searchString.length === 0 && filter === 'all'}
        richPlaceholderCTA={
          <Button to="campaigns/new" primary>
            Create a Campaign to fill the Void
          </Button>
        }
        isFetching={isFetching || isCreating}>
        {campaign => (
          <CampaignItem
            key={campaign.id}
            campaign={campaign}
            onDelete={deleteCampaignStart}
            onDuplicate={duplicateCampaign}
            onArchiveChange={setCampaignArchived}
          />
        )}
      </ResourceList>
      <ConfirmDelete />
    </section>
  );
};

CampaignsView.propTypes = {
  campaigns: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  isCreating: PropTypes.bool.isRequired,
  filter: PropTypes.string,
  deleteCampaignStart: PropTypes.func.isRequired,
  duplicateCampaign: PropTypes.func.isRequired,
  setCampaignArchived: PropTypes.func.isRequired,
  handleFilterChange: PropTypes.func.isRequired,
  handleSearch: PropTypes.func.isRequired,
  searchString: PropTypes.string
};

export default CampaignsView;
