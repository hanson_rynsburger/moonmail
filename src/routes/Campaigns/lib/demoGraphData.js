const opens = [
  {timestamp: '1480457700', count: 231},
  {timestamp: '1480458600', count: 384},
  {
    timestamp: '1480459500',
    count: 428
  },
  {timestamp: '1480460400', count: 308},
  {timestamp: '1480461300', count: 196},
  {
    timestamp: '1480462200',
    count: 121
  },
  {timestamp: '1480463100', count: 120},
  {timestamp: '1480464000', count: 85},
  {
    timestamp: '1480464900',
    count: 65
  },
  {timestamp: '1480465800', count: 67},
  {timestamp: '1480466700', count: 57},
  {
    timestamp: '1480467600',
    count: 45
  },
  {timestamp: '1480468500', count: 52},
  {timestamp: '1480469400', count: 44},
  {
    timestamp: '1480470300',
    count: 36
  },
  {timestamp: '1480471200', count: 53},
  {timestamp: '1480472100', count: 37},
  {
    timestamp: '1480473000',
    count: 29
  },
  {timestamp: '1480473900', count: 24},
  {timestamp: '1480474800', count: 31},
  {
    timestamp: '1480475700',
    count: 24
  },
  {timestamp: '1480476600', count: 20},
  {timestamp: '1480477500', count: 8},
  {
    timestamp: '1480478400',
    count: 19
  },
  {timestamp: '1480479300', count: 18}
];
const clicks = [
  {timestamp: '1480457700', count: 32},
  {
    timestamp: '1480458600',
    count: 54
  },
  {timestamp: '1480459500', count: 70},
  {timestamp: '1480460400', count: 55},
  {
    timestamp: '1480461300',
    count: 21
  },
  {timestamp: '1480462200', count: 29},
  {timestamp: '1480463100', count: 18},
  {
    timestamp: '1480464000',
    count: 19
  },
  {timestamp: '1480464900', count: 12},
  {timestamp: '1480465800', count: 25},
  {
    timestamp: '1480466700',
    count: 19
  },
  {timestamp: '1480467600', count: 10},
  {timestamp: '1480468500', count: 9},
  {
    timestamp: '1480469400',
    count: 8
  },
  {timestamp: '1480470300', count: 3},
  {timestamp: '1480471200', count: 6},
  {
    timestamp: '1480472100',
    count: 7
  },
  {timestamp: '1480473000', count: 8},
  {timestamp: '1480473900', count: 7},
  {
    timestamp: '1480474800',
    count: 14
  },
  {timestamp: '1480475700', count: 4},
  {timestamp: '1480476600', count: 6},
  {
    timestamp: '1480477500',
    count: 2
  },
  {timestamp: '1480478400', count: 4},
  {timestamp: '1480479300', count: 5}
];

const campaign = {
  id: '12345',
  name: 'Demo campaign'
};

const items = [];
const graphData = {};
opens.forEach((item, index) => {
  items.push({
    count: opens[index].count,
    clicks: clicks[index].count,
    timestamp: opens[index].timestamp
  });
});

graphData[campaign.id] = items;

export default {
  graphData,
  campaigns: [campaign]
};
