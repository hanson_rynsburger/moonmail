import Campaigns from './containers/CampaignsContainer';

export default {
  path: '/campaigns',
  component: Campaigns
};
