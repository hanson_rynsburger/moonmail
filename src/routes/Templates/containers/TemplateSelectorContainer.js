import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import templateActions from 'modules/templates/actions';
import templateSelectors from 'modules/templates/selectors';
import marketActions from 'modules/marketTemplates/actions';
import marketSelectors from 'modules/marketTemplates/selectors';
import TemplateSelectorView from '../components/TemplateSelectorView';
import {getIsVerified} from 'modules/profile/selectors';

class TemplateSelector extends Component {
  static propTypes = {
    fetchTemplates: PropTypes.func.isRequired,
    fetchTemplate: PropTypes.func.isRequired,
    selectTemplate: PropTypes.func.isRequired,
    clearSelectedTemplate: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    userTemplates: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      searchString: '',
      isLoadingEmptyTemplate: false
    };
  }

  componentDidMount() {
    this.props.fetchTemplates();
  }

  selectUserTemplate = async templateId => {
    const {fetchTemplate, selectTemplate, clearSelectedTemplate, onSelect} = this.props;
    selectTemplate(templateId);
    const {body} = await fetchTemplate(templateId);
    await onSelect(body);
    clearSelectedTemplate();
  };

  loadEmptyTemplate = async () => {
    this.setState({isLoadingEmptyTemplate: true});
    const json = await import('modules/templates/blank-template.json');
    await this.props.onSelect(JSON.stringify(json));
  };

  handleSearch = searchString => {
    this.setState({searchString});
  };

  filter = ({name = ''}) => {
    const itemName = name.toLowerCase();
    const str = this.state.searchString.toLowerCase();
    return itemName.includes(str);
  };

  render() {
    const userTemplates =
      this.state.searchString.length > 1
        ? this.props.userTemplates.filter(this.filter)
        : this.props.userTemplates;

    return (
      <TemplateSelectorView
        {...this.props}
        userTemplates={userTemplates}
        handleSearch={this.handleSearch}
        searchString={this.state.searchString}
        selectUserTemplate={this.selectUserTemplate}
        loadEmptyTemplate={this.loadEmptyTemplate}
        isLoadingEmptyTemplate={this.state.isLoadingEmptyTemplate}
      />
    );
  }
}

const mapStateToProps = state => ({
  userTemplates: templateSelectors.getSortedResources(state),
  marketTemplates: marketSelectors.getSortedResources(state),
  isFetchingUserTemplates: templateSelectors.getIsFetchingAll(state),
  isFetchingMarketTemplates: marketSelectors.getIsFetchingAll(state),
  marketTemplateId: marketSelectors.getSelectedId(state),
  userTemplateId: templateSelectors.getSelectedId(state),
  isDeleting: templateSelectors.getIsDeleting(state),
  isVerified: getIsVerified(state)
});

export default connect(mapStateToProps, {...templateActions, ...marketActions})(TemplateSelector);
