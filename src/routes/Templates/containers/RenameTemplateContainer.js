import {reduxForm} from 'redux-form';
import RenameTemplate from '../components/RenameTemplate';

export default reduxForm({
  form: 'renameTemplate',
  fields: ['name', 'thumbnail']
})(RenameTemplate);
