import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/templates/actions';
import selectors from 'modules/templates/selectors';
import TemplatesView from '../components/TemplatesView';
import {getIsTemplateManager, getIsVerified} from 'modules/profile/selectors';
import {addMessage} from 'modules/messages/actions';

class Templates extends Component {
  static propTypes = {
    fetchTemplates: PropTypes.func.isRequired,
    createTemplate: PropTypes.func.isRequired,
    templates: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      searchString: '',
      isLoadingEmptyTemplate: false
    };
  }

  componentDidMount() {
    this.props.fetchTemplates();
  }

  handleSearch = searchString => {
    this.setState({searchString});
  };

  filter = ({name = ''}) => {
    const itemName = name.toLowerCase();
    const str = this.state.searchString.toLowerCase();
    return itemName.includes(str);
  };

  loadEmptyTemplate = async () => {
    this.setState({isLoadingEmptyTemplate: true});
    const json = await import(/* webpackChunkName: "blank-template" */ 'modules/templates/blank-template.json');
    this.props.createTemplate({body: JSON.stringify(json)});
  };

  render() {
    const templates =
      this.state.searchString.length > 1
        ? this.props.templates.filter(this.filter)
        : this.props.templates;
    return (
      <TemplatesView
        {...this.props}
        handleSearch={this.handleSearch}
        searchString={this.state.searchString}
        loadEmptyTemplate={this.loadEmptyTemplate}
        isLoadingEmptyTemplate={this.state.isLoadingEmptyTemplate}
        templates={templates}
      />
    );
  }
}

const mapStateToProps = state => ({
  templates: selectors.getSortedResources(state),
  isFetching: selectors.getIsFetchingAll(state),
  selectedTemplate: selectors.getSelectedResource(state),
  isDeleting: selectors.getIsDeleting(state),
  isDeleteStarted: selectors.getIsDeleteStarted(state),
  isCreatingUserTemplate: selectors.getIsCreating(state),
  isTemplateManager: getIsTemplateManager(state),
  isUpdating: selectors.getIsUpdating(state),
  isUpdateStarted: selectors.getIsUpdateStarted(state),
  isDuplicating: selectors.getIsDuplicating(state),
  isImporting: selectors.getIsImporting(state),
  isVerified: getIsVerified(state)
});

export default connect(mapStateToProps, {
  ...actions,
  addMessage
})(Templates);
