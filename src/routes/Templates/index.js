import Templates from './containers/TemplatesContainer';

export default {
  path: '/templates',
  component: Templates
};
