import React from 'react';
import PropTypes from 'prop-types';
import TemplateItem from './TemplateItem';
import ResourceList from 'components/ResourceList';
import RenameTemplate from '../containers/RenameTemplateContainer';
import Confirm from 'components/Modal/Confirm';
import {DEFAULT_NAME} from 'modules/templates/constants';
import NL from 'components/NoLocalize';
import FileButton from 'components/FileButton';
import Button from 'components/Button';
import Search from 'components/Search';
import classNames from './TemplatesView.scss';
import cx from 'classnames';

const Templates = ({
  isFetching,
  templates,
  deleteTemplateStart,
  updateTemplateStart,
  updateTemplateCancel,
  updateTemplate,
  isUpdating,
  isUpdateStarted,
  duplicateTemplate,
  selectedTemplate,
  isDeleteStarted,
  isDeleting,
  deleteTemplate,
  deleteTemplateCancel,
  isTemplateManager,
  importTemplate,
  isImporting,
  isDuplicating,
  handleSearch,
  searchString,
  isLoadingEmptyTemplate,
  loadEmptyTemplate,
  isVerified
}) => {
  return (
    <div className="ui basic segment padded-bottom ">
      <div className="header-with-actions">
        <h2 className="ui header">My Email Templates</h2>
        {(searchString.length > 0 || templates.length > 5) && (
          <Search searchDelay={0} onSearch={handleSearch} value={searchString} />
        )}
        <Button onClick={loadEmptyTemplate} disabled={!isVerified} loading={isLoadingEmptyTemplate}>
          <i className="icon file outline" />
          Start from scratch
        </Button>
        {isTemplateManager && (
          <FileButton
            onChange={e => importTemplate(e.target.files[0])}
            loading={isImporting}
            accept=".json">
            <i className="icon upload" />
            Import template
          </FileButton>
        )}
        <Button to="/market-templates" primary>
          Templates Marketplace
        </Button>
      </div>
      <ResourceList
        isFetching={isFetching}
        resources={templates}
        pageSize={50}
        className={cx('ui cards', classNames.list)}
        richPlaceholder={searchString.length === 0}
        richPlaceholderCTA={
          <Button to="/market-templates" primary>
            Select a template from Marketplace
          </Button>
        }
        resourceName="templates">
        {(template, i) => (
          <TemplateItem
            key={i}
            onDelete={deleteTemplateStart}
            onDuplicate={duplicateTemplate}
            isLoading={selectedTemplate.id === template.id && isDuplicating}
            onRename={updateTemplateStart}
            isTemplateManager={isTemplateManager}
            isDisabled={!isVerified}
            template={template}
          />
        )}
      </ResourceList>
      <Confirm
        isOpen={isDeleteStarted}
        isLoading={isDeleting}
        onCancel={deleteTemplateCancel}
        onConfirm={() => deleteTemplate(selectedTemplate.id)}
        confirmText="Delete template"
        confirmClass="negative">
        <p>
          This action will completely delete{' '}
          {selectedTemplate.name ? (
            <NL el="b">"{selectedTemplate.name}"</NL>
          ) : (
            <b>"{DEFAULT_NAME}"</b>
          )}
        </p>
      </Confirm>
      <RenameTemplate
        initialValues={selectedTemplate}
        isOpen={isUpdateStarted}
        isSaving={isUpdating}
        onCancel={updateTemplateCancel}
        onSave={data => updateTemplate(selectedTemplate.id, data)}
      />
    </div>
  );
};

Templates.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  templates: PropTypes.array.isRequired,
  deleteTemplateStart: PropTypes.func.isRequired,
  updateTemplateStart: PropTypes.func.isRequired,
  duplicateTemplate: PropTypes.func.isRequired,
  selectedTemplate: PropTypes.object.isRequired,
  isDeleteStarted: PropTypes.bool.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  deleteTemplate: PropTypes.func.isRequired,
  deleteTemplateCancel: PropTypes.func.isRequired,
  isTemplateManager: PropTypes.bool,
  updateTemplateCancel: PropTypes.func.isRequired,
  updateTemplate: PropTypes.func.isRequired,
  isUpdating: PropTypes.bool,
  isUpdateStarted: PropTypes.bool,
  importTemplate: PropTypes.func.isRequired,
  isImporting: PropTypes.bool,
  isDuplicating: PropTypes.bool,
  handleSearch: PropTypes.func.isRequired,
  searchString: PropTypes.string,
  isLoadingEmptyTemplate: PropTypes.bool,
  loadEmptyTemplate: PropTypes.func.isRequired,
  isVerified: PropTypes.bool.isRequired
};

export default Templates;
