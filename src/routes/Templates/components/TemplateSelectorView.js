import React from 'react';
import PropTypes from 'prop-types';
import SelectTemplateItem from './SelectTemplateItem';
import classNames from './TemplatesView.scss';
import cx from 'classnames';
import ResourceList from 'components/ResourceList';
import Search from 'components/Search';
import Button from 'components/Button';

const Templates = ({
  userTemplates,
  selectUserTemplate,
  isFetchingUserTemplates,
  userTemplateId,
  handleSearch,
  searchString,
  loadEmptyTemplate,
  isLoadingEmptyTemplate,
  isVerified
}) => (
  <section className="padded-bottom">
    <div className="ui vertical segment">
      <div className="header-with-actions">
        <h2 className="ui header">My Templates</h2>
        <Search searchDelay={0} onSearch={handleSearch} value={searchString} />
        <Button
          onClick={loadEmptyTemplate}
          disabled={!isVerified}
          loading={isLoadingEmptyTemplate}
          primary>
          <i className="icon file outline" />
          Start from scratch
        </Button>
      </div>
      <ResourceList
        isFetching={isFetchingUserTemplates}
        resources={userTemplates}
        pageSize={50}
        className={cx('ui cards', classNames.list)}
        richPlaceholder
        richPlaceholderCTA={
          <Button to="/market-templates" primary>
            Select a template from Marketplace
          </Button>
        }
        resourceName="templates">
        {(template, i) => (
          <SelectTemplateItem
            key={i}
            onSelect={selectUserTemplate}
            isDisabled={!isVerified}
            isLoading={userTemplateId === template.id}
            template={template}
          />
        )}
      </ResourceList>
    </div>
  </section>
);

Templates.propTypes = {
  userTemplates: PropTypes.array,
  selectUserTemplate: PropTypes.func.isRequired,
  userTemplateId: PropTypes.string,
  isFetchingUserTemplates: PropTypes.bool,
  isLoadingEmptyTemplate: PropTypes.bool,
  handleSearch: PropTypes.func.isRequired,
  loadEmptyTemplate: PropTypes.func.isRequired,
  searchString: PropTypes.string,
  isVerified: PropTypes.bool.isRequired
};

export default Templates;
