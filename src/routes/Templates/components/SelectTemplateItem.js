import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import placeholder from 'static/blank-template.png';
import {NO_LOCALIZE} from 'lib/constants';
import classNames from './TemplatesView.scss';
import Loader from 'components/Loader';
import cx from 'classnames';
import path from 'object-path';
import {DEFAULT_NAME} from 'modules/templates/constants';

const TemplateItem = ({template, onSelect, isLoading, isDisabled}) => {
  const templateUrl = path.get(template, 'thumbnail', placeholder);
  return (
    <div className={cx('ui card', classNames.item)}>
      <Loader active={isLoading} inline={false} />
      <div className={cx('image', classNames.image)}>
        <img src={templateUrl} />
      </div>
      <div className="content">
        <div className="description">
          <h4 className={cx('truncate', NO_LOCALIZE)}>{template.name || DEFAULT_NAME}</h4>
        </div>
      </div>
      <div className="ui bottom attached buttons">
        <Button onClick={() => onSelect(template.id)} disabled={isDisabled}>
          Select
        </Button>
      </div>
    </div>
  );
};

TemplateItem.propTypes = {
  template: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }),
  onSelect: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool,
  isLoading: PropTypes.bool
};

export default TemplateItem;
