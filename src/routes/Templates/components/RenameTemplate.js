import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Confirm from 'components/Modal/Confirm';

const RenameTemplate = ({
  isOpen,
  isSaving,
  fields: {name},
  handleSubmit,
  onSave,
  onCancel,
  invalid
}) => {
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isSaving}
      isDisabled={invalid}
      onCancel={onCancel}
      onConfirm={handleSubmit(onSave)}
      headerText="Enter your template name"
      confirmText="Save"
      confirmClass="primary">
      <form className="ui form" onSubmit={handleSubmit(onSave)}>
        <Input label={false} type="text" {...name} />
      </form>
    </Confirm>
  );
};

RenameTemplate.propTypes = {
  fields: PropTypes.object.isRequired,
  onSave: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isSaving: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired
};

export default RenameTemplate;
