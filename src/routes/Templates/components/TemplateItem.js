import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router/lib/Link';
import {formatDate} from 'lib/utils';
import DropDownMenu from 'components/DropDownMenu';
import NoLocalize from 'components/NoLocalize';
import placeholder from 'static/blank-template.png';
import classNames from './TemplatesView.scss';
import {DEFAULT_NAME} from 'modules/templates/constants';
import Loader from 'components/Loader';
import Button from 'components/Button';
import cx from 'classnames';
import path from 'object-path';

const TemplateItem = ({
  template,
  onDelete,
  onDuplicate,
  onRename,
  isLoading,
  isDisabled,
  isTemplateManager
}) => {
  const templateUrl = path.get(template, 'thumbnail', placeholder);
  return (
    <div className={cx('ui card', classNames.item)}>
      <Loader active={isLoading} inline={false} />
      <Link className={cx('image', classNames.image)} to={`templates/${template.id}`}>
        <img src={templateUrl} />
      </Link>
      <div className="content">
        <div className="description">
          <h4 className="truncate">
            {template.name ? <NoLocalize>{template.name}</NoLocalize> : DEFAULT_NAME}
          </h4>
        </div>
        <div className="meta">
          Created
          <NoLocalize> {formatDate(template.createdAt, {fromNow: true})}</NoLocalize>
        </div>
      </div>
      <div className="ui bottom attached buttons">
        <Button to={`templates/${template.id}`} disabled={isDisabled}>
          Edit
        </Button>
        <DropDownMenu className="button right labeled icon floating">
          Actions
          <i className="dropdown icon" />
          <div className="menu">
            <a className="item" onClick={() => onRename(template.id)}>
              Rename
            </a>
            {!isDisabled && (
              <a className="item" onClick={() => onDuplicate(template.id)}>
                Duplicate
              </a>
            )}
            {isTemplateManager && (
              <Link className="item" to={`/templates/${template.id}/publish`}>
                Sell template
              </Link>
            )}
            <a className="item" onClick={() => onDelete(template.id)}>
              Delete
            </a>
          </div>
        </DropDownMenu>
      </div>
    </div>
  );
};

TemplateItem.propTypes = {
  template: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
    createdAt: PropTypes.number
  }),
  onDelete: PropTypes.func.isRequired,
  onRename: PropTypes.func.isRequired,
  onDuplicate: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isTemplateManager: PropTypes.bool
};

export default TemplateItem;
