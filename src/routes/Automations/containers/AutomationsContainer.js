import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/automations/actions';
import selectors from 'modules/automations/selectors';
import {getIsFreeUser} from 'modules/profile/selectors';
import AutomationsView from '../components/AutomationsView';

class Automations extends Component {
  static propTypes = {
    fetchResources: PropTypes.func.isRequired,
    filter: PropTypes.string,
    router: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    automations: PropTypes.array.isRequired,
    isFreeUser: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      searchString: ''
    };
  }

  componentDidMount() {
    this.props.fetchResources({limit: 9999, archived: false});
  }

  handleFilterChange = filter => {
    const {router, location} = this.props;
    if (filter !== this.props.filter) {
      router.push({
        pathname: location.pathname,
        query: {filter}
      });
    }
  };

  handleSearch = searchString => {
    this.setState({searchString});
  };

  filter = item => {
    const name = item.name.toLowerCase();
    const str = this.state.searchString.toLowerCase();
    return name.includes(str);
  };

  render() {
    const automations =
      this.state.searchString.length > 1
        ? this.props.automations.filter(this.filter)
        : this.props.automations;
    return (
      <AutomationsView
        {...this.props}
        handleSearch={this.handleSearch}
        searchString={this.state.searchString}
        handleFilterChange={this.handleFilterChange}
        automations={automations}
      />
    );
  }
}

const mapStateToProps = (state, props) => ({
  filter: props.location.query.filter,
  isFreeUser: getIsFreeUser(state),
  automations: selectors.getFilteredAutomations(state),
  isDeleteStarted: selectors.getIsDeleteStarted(state),
  isDeleting: selectors.getIsDeleting(state),
  isFetching: selectors.getIsFetchingAll(state),
  selectedAutomation: selectors.getSelectedResource(state)
});

export default connect(mapStateToProps, actions)(Automations);
