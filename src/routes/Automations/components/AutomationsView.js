import React from 'react';
import PropTypes from 'prop-types';
import ResourceList from 'components/ResourceList';
import AutomationItem from './AutomationItem';
import Button from 'components/Button';
import Link from 'react-router/lib/Link';
import Confirm from 'components/Modal/Confirm';
import DropDownMenu from 'components/DropDownMenu';
import NoLocalize from 'components/NoLocalize';
import {humanize, Aux} from 'lib/utils';
import {AUTOMATION_FILTERS} from 'modules/automations/constants';
import cx from 'classnames';
import Search from 'components/Search';
import PopUp from 'components/PopUp';

const AutomationsView = ({
  isFetching,
  automations,
  isDeleteStarted,
  isDeleting,
  deleteResourceStart,
  deleteResourceCancel,
  deleteResource,
  selectedAutomation,
  setAutomationArchived,
  handleFilterChange,
  isFreeUser,
  filter = 'all',
  handleSearch,
  searchString
}) => {
  const isSearchVisible = searchString.length > 0 || automations.length > 10;
  return (
    <section className="ui basic segment padded-bottom">
      <div className="header-with-actions">
        <h1 className="ui header">Automations</h1>
        {!isFreeUser &&
          isSearchVisible && (
            <Search searchDelay={0} onSearch={handleSearch} value={searchString} />
          )}
        {!isFreeUser && (
          <DropDownMenu className={cx('floating labeled icon button', {teal: filter !== 'all'})}>
            <i className="filter icon" />
            <span className="text">{humanize(filter)}</span>
            <div className="menu">
              <div className="scrolling menu">
                {AUTOMATION_FILTERS.map(f => (
                  <div
                    key={f}
                    className={cx('item', {'active selected': f === filter})}
                    onClick={() => handleFilterChange(f)}>
                    {humanize(f)}
                  </div>
                ))}
              </div>
            </div>
          </DropDownMenu>
        )}
        <PopUp
          on="click"
          showDelay={500}
          closable={false}
          variation="small"
          disabled={!isFreeUser}
          content={
            <Aux>
              <Link to="/profile/plan">Upgrade</Link> to create automations
            </Aux>
          }>
          <Button disabled={isFreeUser} primary to="/automations/new">
            Create Automation
          </Button>
        </PopUp>
      </div>
      <ResourceList
        className="ui big very relaxed divided list"
        isFetching={isFetching}
        resources={automations}
        resourceName="automations"
        richPlaceholder={searchString.length === 0 && filter === 'all'}
        richPlaceholderCTA={
          <Button to="/automations/new" primary>
            Create an Automation to fill the Void
          </Button>
        }>
        {(automation, i) => (
          <AutomationItem
            key={i}
            automation={automation}
            onArchiveChange={setAutomationArchived}
            onDelete={deleteResourceStart}
          />
        )}
      </ResourceList>
      <Confirm
        isOpen={isDeleteStarted}
        isLoading={isDeleting}
        onCancel={deleteResourceCancel}
        onConfirm={() => deleteResource(selectedAutomation.id)}
        confirmText="Delete automation"
        confirmClass="negative">
        <p>
          This action will completely delete{' '}
          <NoLocalize el="b">"{selectedAutomation.name}"</NoLocalize>
        </p>
      </Confirm>
    </section>
  );
};

AutomationsView.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  automations: PropTypes.array.isRequired,
  isDeleteStarted: PropTypes.bool.isRequired,
  isFreeUser: PropTypes.bool.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  deleteResourceStart: PropTypes.func.isRequired,
  deleteResourceCancel: PropTypes.func.isRequired,
  deleteResource: PropTypes.func.isRequired,
  setAutomationArchived: PropTypes.func.isRequired,
  handleFilterChange: PropTypes.func.isRequired,
  selectedAutomation: PropTypes.object,
  filter: PropTypes.string,
  handleSearch: PropTypes.func.isRequired,
  searchString: PropTypes.string
};

export default AutomationsView;
