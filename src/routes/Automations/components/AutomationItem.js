import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router/lib/Link';
import DropDownMenu from 'components/DropDownMenu';
import NL from 'components/NoLocalize';
import {humanize, formatDate, Aux} from 'lib/utils';
import {NO_LOCALIZE} from 'lib/constants';
import PopUp from 'components/PopUp';
import cx from 'classnames';

const AutomationItem = ({automation = {}, onDelete, onArchiveChange}) => {
  const isActive = automation.status === 'active';
  const isPaused = automation.status === 'paused';
  const isArchived = automation.archived;
  const isArchivable = !isArchived && automation.status === 'paused';
  const isError = !isPaused && !isActive;
  const status = automation.status.replace('stripe', '');
  const iconClass = cx('large icon', {
    'green play': isActive,
    'grey pause': isPaused,
    'red warning circle': isError
  });
  return (
    <div className="item">
      <div className="right floated content">
        <div className="ui buttons">
          <Link to={`/automations/${automation.id}`} className="ui button">
            Actions
          </Link>
          <DropDownMenu automation="hide" className="button icon floating">
            <i className="dropdown icon" />
            <div className="menu">
              <Link to={`/automations/${automation.id}/settings`} className="item">
                Settings
              </Link>
              {__DEV__ && (
                <Aux>
                  {isArchivable && (
                    <a className="item" onClick={() => onArchiveChange(automation.id, true)}>
                      Archive
                    </a>
                  )}
                  {isArchived && (
                    <a className="item" onClick={() => onArchiveChange(automation.id, false)}>
                      Unarchive
                    </a>
                  )}
                  {isPaused && (
                    <a className="item" onClick={() => onDelete(automation.id)}>
                      Delete
                    </a>
                  )}
                </Aux>
              )}
            </div>
          </DropDownMenu>
        </div>
      </div>
      <PopUp
        content={humanize(isArchived ? 'Archived' : status)}
        position="bottom left"
        className="icon-wrapper">
        <i className={cx(iconClass, {disabled: isArchived})} />
      </PopUp>
      <NL className="content">
        <h3 className={cx('header truncate', NO_LOCALIZE, {disabled: isArchived})}>
          <Link to={`/automations/${automation.id}`} title={automation.name}>
            {automation.name}
          </Link>
        </h3>
        <small className={cx('description', {disabled: isArchived})}>
          Created {formatDate(automation.createdAt, {fromNow: true})}
        </small>
      </NL>
    </div>
  );
};

AutomationItem.propTypes = {
  automation: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    createdAt: PropTypes.number
  }),
  onDelete: PropTypes.func.isRequired,
  onArchiveChange: PropTypes.func
};

export default AutomationItem;
