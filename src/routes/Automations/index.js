import Automations from './containers/AutomationsContainer';

export default {
  path: 'automations',
  component: Automations
};
