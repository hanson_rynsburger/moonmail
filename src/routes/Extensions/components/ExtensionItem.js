import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import {joinComponents} from 'lib/utils';

const ExtensionItem = ({extension = {}, isInstalled, viewDetails}) => {
  const tags = extension.tags.split(',');
  return (
    <div className="card">
      {isInstalled && (
        <label className="ui green right corner label">
          <i className="check icon" />
        </label>
      )}
      <div className="content">
        <div className="header">{extension.name}</div>
        <div className="meta">made by {extension.developer}</div>
        <div className="description">
          <p>{extension.description}</p>
          <p>
            {joinComponents(tags, (tag, i) => (
              <a key={i} href="#" onClick={e => e.preventDefault()}>
                <b>{tag}</b>
              </a>
            ))}
          </p>
        </div>
      </div>
      <Button className="bottom attached" primary onClick={viewDetails}>
        {isInstalled ? (
          <div>Uninstall</div>
        ) : (
          <div>
            Install for{' '}
            {extension.price ? `$${extension.price} ${extension.recurring && '/mo'}` : 'free'}
          </div>
        )}
      </Button>
    </div>
  );
};

ExtensionItem.propTypes = {
  extension: PropTypes.object.isRequired,
  isInstalled: PropTypes.bool,
  viewDetails: PropTypes.func.isRequired
};

export default ExtensionItem;
