import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/Loader';
import ExtensionItem from './ExtensionItem';
import Payment from 'components/Payment';

const ExtensionsView = ({
  extensions,
  isFetching,
  installedExtensionIds,
  selectExtension,
  installExtension,
  uninstallExtension,
  clearSelectedExtension,
  isPaymentOpen,
  selectedExtension,
  isSelectedExtensionInstalled,
  isExtensionUpdating
}) => {
  const getConfirmText = () => {
    if (isSelectedExtensionInstalled) return 'I really want to uninstall';
    if (selectedExtension.price)
      return `Install for $${selectedExtension.price} ${selectedExtension.recurring && '/ mo'}`;
    return 'Install for free';
  };
  const process = async () => {
    if (isSelectedExtensionInstalled) return uninstallExtension(selectedExtension);
    if (selectedExtension.url) return window.open(selectedExtension.url, '_blank');
    return installExtension(selectedExtension);
  };
  return (
    <section className="ui basic segment padded-bottom">
      <h1 className="ui header">Extensions</h1>
      <Loader active={isFetching} inline />
      <div className="ui cards">
        {extensions.map((extension, i) => (
          <ExtensionItem
            extension={extension}
            viewDetails={() => selectExtension(extension.id)}
            isInstalled={installedExtensionIds.includes(extension.id)}
            key={i}
          />
        ))}
      </div>
      <Payment
        isOpen={isPaymentOpen}
        isProcessing={isExtensionUpdating}
        process={process}
        processCancel={clearSelectedExtension}
        price={isSelectedExtensionInstalled ? null : selectedExtension.price}
        headerText={selectedExtension.name}
        confirmText={getConfirmText()}
        isHandledBySupport={selectedExtension.isHandledBySupport}
        hasSeparatePreview>
        <div className="content">
          {selectedExtension.fullDescription ? (
            <div dangerouslySetInnerHTML={{__html: selectedExtension.fullDescription}} />
          ) : (
            <p>{selectedExtension.description}</p>
          )}
        </div>
      </Payment>
    </section>
  );
};

ExtensionsView.propTypes = {
  extensions: PropTypes.array.isRequired,
  installedExtensionIds: PropTypes.array,
  isFetching: PropTypes.bool.isRequired,
  selectExtension: PropTypes.func.isRequired,
  installExtension: PropTypes.func.isRequired,
  uninstallExtension: PropTypes.func.isRequired,
  clearSelectedExtension: PropTypes.func.isRequired,
  isPaymentOpen: PropTypes.bool.isRequired,
  selectedExtension: PropTypes.object.isRequired,
  isSelectedExtensionInstalled: PropTypes.bool.isRequired,
  isExtensionUpdating: PropTypes.bool.isRequired
};

export default ExtensionsView;
