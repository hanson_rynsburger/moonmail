import Extensions from './containers/ExtensionsContainer';

export default {
  path: 'extensions',
  component: Extensions
};
