import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import actions from 'modules/extensions/actions';
import selectors from 'modules/extensions/selectors';
import * as profileActions from 'modules/profile/actions';
import * as profileSelectors from 'modules/profile/selectors';
import ExtensionsView from '../components/ExtensionsView';

class Extensions extends Component {
  static propTypes = {
    fetchExtensions: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchExtensions();
  }

  render() {
    return <ExtensionsView {...this.props} />;
  }
}

const mapStateToProps = function(state) {
  const installedExtensionIds = profileSelectors.getExtensions(state);
  const selectedExtension = selectors.getSelectedResource(state);
  return {
    extensions: selectors.getResources(state),
    isFetching: selectors.getIsFetchingAll(state),
    installedExtensionIds,
    isPaymentOpen: selectors.getIsSelected(state),
    selectedExtension,
    isSelectedExtensionInstalled: installedExtensionIds.includes(selectedExtension.id),
    isExtensionUpdating: profileSelectors.getIsUpdating(state)
  };
};

export default connect(mapStateToProps, {
  ...actions,
  fetchExtensions: actions.fetchResources,
  selectExtension: actions.selectResource,
  installExtension: profileActions.installExtension,
  uninstallExtension: profileActions.uninstallExtension,
  clearSelectedExtension: actions.clearSelectedResource
})(Extensions);
