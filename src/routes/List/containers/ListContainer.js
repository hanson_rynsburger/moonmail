import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/lists/actions';
import selectors from 'modules/lists/selectors';
import segmentsActions from 'modules/segments/actions';
import recipientsActions from 'modules/recipients/actions';
import ListView from '../components/ListView';

class List extends Component {
  static propTypes = {
    fetchList: PropTypes.func.isRequired,
    fetchRecipientsCount: PropTypes.func.isRequired,
    fetchSegments: PropTypes.func.isRequired,
    clearSegments: PropTypes.func.isRequired,
    clearRecipients: PropTypes.func.isRequired,
    listId: PropTypes.string.isRequired
  };

  componentWillUnmount() {
    this.props.clearSegments();
    this.props.clearRecipients();
  }

  componentDidMount() {
    const {fetchList, listId, fetchRecipientsCount, fetchSegments} = this.props;
    fetchList(listId);
    fetchRecipientsCount();
    fetchSegments(listId);
  }

  render() {
    return <ListView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  list: selectors.getActiveResource(state),
  isFetching: selectors.getIsFetching(state),
  uploadLimit: selectors.getRemainingRecipients(state),
  listId: ownProps.params.listId
});

export default connect(mapStateToProps, {
  ...actions,
  fetchSegments: segmentsActions.fetchResources,
  clearSegments: segmentsActions.clearResources,
  clearRecipients: recipientsActions.clearResources
})(List);
