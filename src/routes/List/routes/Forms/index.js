import FormsView from './components/FormsView';

// Sync route definition
export default {
  path: 'forms',
  component: FormsView
};
