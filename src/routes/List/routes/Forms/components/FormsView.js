import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router/lib/Link';

const FormsView = ({params}) => (
  <section className="ui vertical segment">
    <div className="ui big very relaxed divided list">
      <div className="item">
        <div className="right floated content">
          <Link to={`/lists/${params.listId}/forms/embedded`} className="ui button">
            Select
          </Link>
        </div>
        <i className="big brown icon code" />
        <div className="content">
          <h3 className="header">
            <Link to={`/lists/${params.listId}/forms/embedded`}>Embedded form</Link>
          </h3>
          <small className="description">
            Generate HTML code to embed in your site or blog to collect signups.
          </small>
        </div>
      </div>
    </div>
  </section>
);

FormsView.propTypes = {
  params: PropTypes.shape({
    listId: PropTypes.string.isRequired
  })
};

export default FormsView;
