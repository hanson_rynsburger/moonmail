import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CodeEditor from 'components/CodeEditor';
import Button from 'components/Button';
import DotHint from 'components/DotHint';
import cx from 'classnames';
import {NO_LOCALIZE} from 'components/NoLocalize';
import classNames from './CopyPasteView.scss';
import UploadLimitHint from 'components/UploadLimitHint';
import {emailRegEx2} from 'lib/validator';

const csvMode = () => ({
  token(stream) {
    if (stream.match(emailRegEx2)) {
      return 'tag';
    } else {
      stream.next();
      return null;
    }
  }
});

class CopyPasteView extends Component {
  static propTypes = {
    initListImporter: PropTypes.func.isRequired,
    parseListClean: PropTypes.func.isRequired,
    parseList: PropTypes.func.isRequired,
    importList: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    isImporting: PropTypes.bool.isRequired,
    invalid: PropTypes.bool.isRequired,
    rows: PropTypes.array,
    headers: PropTypes.array,
    uploadLimit: PropTypes.number,
    fields: PropTypes.object.isRequired,
    listId: PropTypes.string.isRequired
  };

  componentDidMount() {
    this.props.initListImporter();
  }

  componentWillUnmount() {
    this.onReset();
  }

  onReset = () => {
    this.props.parseListClean();
  };

  submit = ({body}) => {
    let data = body.replace(/ /g, '').replace(/,/g, ';');
    if (!data.split('\n')[0].includes('email')) {
      data = `email\n${data}`;
    }
    this.props.fields.body.onChange(data);
    this.props.parseList(data);
  };

  onImport = () => {
    const {importList, listId, headers, fields: {body}} = this.props;
    const meta = {};
    headers.forEach(key => (meta[key] = key));
    importList(listId, body.value, meta);
  };

  render() {
    const {handleSubmit, fields: {body}, isImporting, invalid, rows, headers} = this.props;
    const isPreview = rows.length > 0;
    const label = (
      <span>
        Paste subscribers, each starting from a new line.
        <DotHint id="list.copy_paste.body" />
      </span>
    );
    return (
      <section className="padded-bottom">
        <UploadLimitHint />
        {!isPreview && (
          <form className={cx('ui form')} onSubmit={handleSubmit(this.submit)}>
            <CodeEditor
              editorClass={classNames.editorField}
              defineMode={{name: 'csv', fn: csvMode}}
              options={{lineNumbers: true, mode: 'csv'}}
              label={label}
              {...body}
            />
            <div>
              <Button loading={isImporting} primary type="submit" disabled={invalid || isImporting}>
                Import subscribers
              </Button>
            </div>
          </form>
        )}
        {isPreview && (
          <div className={classNames.hintWrapper}>
            <div className={cx('ui segment', classNames.previewWrapper)}>
              <table
                className={cx('ui large striped selectable single line table', classNames.table)}>
                <thead>
                  <tr>
                    <th width={50} />
                    {headers.map((key, i) => (
                      <th className={NO_LOCALIZE} key={i}>
                        {key}
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {rows.map((row, i) => (
                    <tr className={NO_LOCALIZE} key={i}>
                      <td>{i + 1}</td>
                      {headers.map((field, i) => <td key={i}>{row[field]}</td>)}
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        )}
        {isPreview && (
          <div>
            <Button primary loading={isImporting} onClick={this.onImport} disabled={isImporting}>
              Import subscribers
            </Button>
            <Button onClick={this.onReset} disabled={isImporting}>
              Cancel
            </Button>
          </div>
        )}
      </section>
    );
  }
}

export default CopyPasteView;
