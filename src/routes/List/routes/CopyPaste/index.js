import CopyPaste from './containers/CopyPasteContainer';

export default {
  path: 'copy-paste',
  component: CopyPaste
};
