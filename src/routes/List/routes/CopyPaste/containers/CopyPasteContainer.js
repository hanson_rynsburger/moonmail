import {reduxForm} from 'redux-form';
import CopyPasteView from '../components/CopyPasteView';
import * as actions from 'routes/List/routes/Import/modules/actions';
import * as selectors from 'routes/List/routes/Import/modules/selectors';
import {createValidator} from 'lib/validator';
import {getIsFreeUser} from 'modules/profile/selectors';
import {LIST_DEFAULT_METAFIELDS} from 'modules/lists/constants';

const mapStateToProps = (state, ownProps) => {
  const isFreeUser = getIsFreeUser(state);
  let headers = selectors.getFields(state);
  if (isFreeUser) {
    headers = headers.filter(h => ['email', ...LIST_DEFAULT_METAFIELDS].includes(h));
  }
  return {
    listId: ownProps.params.listId,
    isImporting: selectors.getIsImporting(state),
    rows: selectors.getRows(state),
    headers
  };
};

const defaultBody = `email,name,surname\njohn@moonmail.io,john,doe`;

export default reduxForm(
  {
    form: 'copyPasteRecipients',
    fields: ['body'],
    asyncBlurFields: ['body'],
    validate: createValidator({
      body: 'required'
    }),
    initialValues: {
      body: defaultBody
    }
  },
  mapStateToProps,
  actions
)(CopyPasteView);
