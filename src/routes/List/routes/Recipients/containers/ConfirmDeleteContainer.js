import {connect} from 'react-redux';
import actions from 'modules/recipients/actions';
import selectors from 'modules/recipients/selectors';
import ConfirmDelete from '../components/ConfirmDelete';

const mapStateToProps = state => ({
  isDeleting: selectors.getIsDeleting(state),
  isOpen: selectors.getIsDeleteStarted(state),
  recipientIds: selectors.getSelectedIds(state)
});

export default connect(mapStateToProps, actions)(ConfirmDelete);
