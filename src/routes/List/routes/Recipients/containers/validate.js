import {createValidator} from 'lib/validator';

export default data => {
  const validator = createValidator({
    name: 'required|max:40'
  });
  const conditionsValidator = createValidator({
    recipientField: 'required',
    filters: {
      gte: 'timestamp',
      lte: 'timestamp'
    }
  });
  const errors = validator(data);

  if (!data.conditions.length) {
    errors._error = 'Conditions required';
  } else {
    errors.conditions = data.conditions.map(conditionsValidator);
  }

  return errors;
};
