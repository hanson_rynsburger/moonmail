import {connect} from 'react-redux';
import actions from 'modules/segments/actions';
import selectors from 'modules/segments/selectors';
import Segmentation from '../components/Segmentation';

const mapStateToProps = state => ({
  segments: selectors.getResources(state),
  selectedSegment: selectors.getSelectedResource(state),
  isSelected: selectors.getIsSelected(state),
  isExpanded: selectors.getIsExpanded(state)
});

export default connect(mapStateToProps, actions)(Segmentation);
