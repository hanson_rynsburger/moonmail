import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {reduxForm} from 'redux-form';
import actions from 'modules/segments/actions';
import selectors from 'modules/segments/selectors';
import SegmentForm from '../components/SegmentForm';
import {conditionsToFormFields} from 'modules/segments/utils';
import validate from './validate';

class SegmentFormContainer extends Component {
  static propTypes = {
    listId: PropTypes.string.isRequired,
    fetchRecipientFields: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchRecipientFields(this.props.listId);
  }

  render() {
    return <SegmentForm {...this.props} />;
  }
}

const mapStateToProps = state => {
  const {name, conditions} = selectors.getSelectedResource(state);
  return {
    isExpanded: selectors.getIsExpanded(state),
    isSaving: selectors.getIsSaving(state),
    isDeleting: selectors.getIsDeleting(state),
    isProcessing: selectors.getIsProcessing(state),
    recipientFieldIds: selectors.getRecipientFieldIds(state),
    recipientFields: selectors.getRecipientFields(state),
    getRecipientField: selectors.recipientFieldSelector(state),
    initialValues: {
      name,
      conditions: conditionsToFormFields(conditions)
    }
  };
};

export default reduxForm(
  {
    form: 'segmentDetails',
    fields: [
      'name',
      'conditions[].recipientField',
      'conditions[].filters.match',
      'conditions[].filters.gte',
      'conditions[].filters.lte'
    ],
    validate
  },
  mapStateToProps,
  {
    ...actions,
    deleteSegment: actions.deleteResource
  }
)(SegmentFormContainer);
