import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/recipients/actions';
import selectors from 'modules/recipients/selectors';
import segmentSelectors from 'modules/segments/selectors';
import segmentActions from 'modules/segments/actions';
import RecipientsView from '../components/RecipientsView';
import {withRouter} from 'react-router';
import {compact} from 'lib/utils';

class Recipients extends Component {
  static propTypes = {
    q: PropTypes.string,
    page: PropTypes.string,
    pages: PropTypes.object,
    isSegmentSelected: PropTypes.bool,
    recipients: PropTypes.array.isRequired,
    router: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    listId: PropTypes.string.isRequired,
    isDeleting: PropTypes.bool.isRequired,
    fetchRecipients: PropTypes.func.isRequired,
    deleteRecipientsStart: PropTypes.func.isRequired,
    clearRecipients: PropTypes.func.isRequired,
    selectedIds: PropTypes.array.isRequired,
    selectSegment: PropTypes.func.isRequired,
    clearSegments: PropTypes.func.isRequired,
    segmentId: PropTypes.string,
    status: PropTypes.string
  };

  updateQuery({q, status}) {
    const {selectSegment, router, listId} = this.props;
    selectSegment(null);
    router.push({
      pathname: `/lists/${listId}`,
      query: compact({q, status})
    });
  }

  componentDidMount() {
    const {q, status} = this.props;
    this.fetch({q, status});
  }

  fetch = ({from = 0, segmentId, q, status}) => {
    const {listId, fetchRecipients} = this.props;
    fetchRecipients(listId, segmentId, {q, status, from, size: 10}, true);
  };

  getPage = from => {
    const {segmentId, q, status} = this.props;
    this.fetch({q, status, from, segmentId});
  };

  viewDetails = recipientId => {
    const {router, listId} = this.props;
    router.push(`/lists/${listId}/recipients/${recipientId}`);
  };

  onBulkDelete = () => {
    this.props.deleteRecipientsStart();
  };

  handleSearchChange = q => {
    const {status} = this.props;
    this.updateQuery({q, status});
    this.fetch({q, status});
  };

  handleSelectSegment = segmentId => {
    this.updateQuery({});
    this.props.selectSegment(segmentId);
    this.fetch({segmentId});
  };

  handleStatusChange = status => {
    this.updateQuery({status});
    this.fetch({status});
  };

  render() {
    return (
      <RecipientsView
        getPage={this.getPage}
        onBulkDelete={this.onBulkDelete}
        selectedCount={this.props.selectedIds.length}
        viewDetails={this.viewDetails}
        handleSearchChange={this.handleSearchChange}
        handleSelectSegment={this.handleSelectSegment}
        handleStatusChange={this.handleStatusChange}
        {...this.props}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const {status, q} = ownProps.location.query || {};
  return {
    recipients: selectors.getRecipients(state),
    selectedIds: selectors.getSelectedIds(state),
    isFetching: selectors.getIsFetchingAll(state),
    params: selectors.getParams(state),
    listId: ownProps.params.listId,
    isDeleting: selectors.getIsDeleting(state),
    segmentId: segmentSelectors.getSelectedId(state),
    segment: segmentSelectors.getSelectedResource(state),
    pagination: selectors.getPagination(state),
    status,
    q
  };
};

export default connect(mapStateToProps, {
  ...actions,
  selectSegment: segmentActions.selectResource,
  clearSegments: segmentActions.clearResources
})(withRouter(Recipients));
