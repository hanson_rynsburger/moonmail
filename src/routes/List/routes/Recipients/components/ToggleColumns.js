import React from 'react';
import PropTypes from 'prop-types';
import DropDownMenu from 'components/DropDownMenu';
import CheckBox from 'components/CheckBox';
import {humanize} from 'lib/utils';

const ToggleColumns = ({columns, onToggle}) => {
  const toggle = e => {
    onToggle({...columns, [e.target.name]: e.target.value !== 'true'});
  };
  return (
    <DropDownMenu className="button floating" action="nothing">
      <span className="text">Toggle columns</span>
      <div className="menu">
        {Object.keys(columns).map((column, i) => (
          <div className="item" key={i}>
            <CheckBox
              value={columns[column]}
              checked={columns[column]}
              name={column}
              label={humanize(column)}
              onChange={toggle}
            />
          </div>
        ))}
      </div>
    </DropDownMenu>
  );
};

ToggleColumns.propTypes = {
  columns: PropTypes.object.isRequired,
  onToggle: PropTypes.func.isRequired
};

export default ToggleColumns;
