import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import DropDownMenu from 'components/DropDownMenu';
import Button from 'components/Button';
import SegmentForm from '../containers/SegmentFormContainer';

class Segmentation extends Component {
  static propTypes = {
    listId: PropTypes.string.isRequired,
    segments: PropTypes.array.isRequired,
    selectedSegment: PropTypes.object.isRequired,
    isSelected: PropTypes.bool.isRequired,
    onSelectSegment: PropTypes.func.isRequired,
    addNew: PropTypes.func.isRequired,
    expand: PropTypes.func.isRequired,
    clearSelectedSegment: PropTypes.func.isRequired
  };

  componentWillUnmount() {
    this.props.clearSelectedSegment();
  }

  render() {
    const {
      listId,
      segments,
      selectedSegment,
      isSelected,
      onSelectSegment,
      addNew,
      expand
    } = this.props;

    const dropDownOptions = [
      {
        text: 'All segments',
        onClick: () => onSelectSegment(null)
      },
      ...segments.filter(s => !s.archived).map(segment => ({
        text: segment.name || '[No name]',
        onClick: () => onSelectSegment(segment.id)
      }))
    ];
    return (
      <div>
        <div className="ui buttons">
          {segments.length > 0 && (
            <DropDownMenu
              className={cx('floating labeled icon button', {teal: selectedSegment.name})}>
              <i className="pie chart icon" />
              <span className="text">{selectedSegment.name || 'All segments'}</span>
              <div className="menu">
                <div className="scrolling menu">
                  {dropDownOptions.map(({text, onClick}, i) => (
                    <div
                      key={i}
                      className={cx('item', {'active selected': text === selectedSegment.name})}
                      onClick={onClick}>
                      {text}
                    </div>
                  ))}
                </div>
              </div>
            </DropDownMenu>
          )}
          {isSelected && (
            <Button primary icon onClick={expand}>
              <i className="setting icon" />
            </Button>
          )}
          {segments.length > 0 ? (
            <Button icon onClick={addNew}>
              <i className="plus icon" />
            </Button>
          ) : (
            <Button onClick={addNew}>
              <i className="pie chart icon" />
              Create segment
            </Button>
          )}
        </div>
        <SegmentForm
          listId={listId}
          segmentId={selectedSegment.id}
          onSelectSegment={onSelectSegment}
        />
      </div>
    );
  }
}

export default Segmentation;
