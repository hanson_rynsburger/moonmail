import React from 'react';
import PropTypes from 'prop-types';
import CheckBox from 'components/CheckBox';
import {humanize, formatDate} from 'lib/utils';
import {NO_LOCALIZE} from 'lib/constants';
import cx from 'classnames';
import {negativeStatuses, obfuscateEmail} from 'modules/lists/utils';

const RecipientRow = ({
  recipient: {
    metadata: {name = '', surname = ''} = {},
    isSelected = false,
    status,
    id,
    email,
    createdAt
  },
  onSelect,
  onClick
}) => {
  const rowClass = cx('clickable', {
    active: isSelected,
    negative: negativeStatuses.includes(status),
    warning: status === 'awaitingConfirmation'
  });
  return (
    <tr className={rowClass} onClick={() => onClick(id)} title="view details">
      <td onClick={e => e.stopPropagation()}>
        <CheckBox onChange={() => onSelect(id)} fitted checked={isSelected} />
      </td>
      <td className={NO_LOCALIZE}>{obfuscateEmail(email, status)}</td>
      <td className={NO_LOCALIZE}>
        {name} {surname}
      </td>
      <td>{humanize(status)}</td>
      <td className={NO_LOCALIZE}>{formatDate(createdAt)}</td>
    </tr>
  );
};

RecipientRow.propTypes = {
  recipient: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
  onClick: PropTypes.func.isRequired
};

export default RecipientRow;
