import React from 'react';
import PropTypes from 'prop-types';
import RecipientRow from './RecipientRow';
import ConfirmDelete from '../containers/ConfirmDeleteContainer';
import CheckBox from 'components/CheckBox';
import Loader from 'components/Loader';
import classNames from './RecipientsView.scss';
import Pagination from 'components/Pagination';
import Search from 'components/Search';
import Button from 'components/Button';
import NL from 'components/NoLocalize';
import Segmentation from '../containers/SegmentationContainer';
import cx from 'classnames';
import {RECIPIENT_STATUSES} from 'lib/constants';
import DropDownMenu from 'components/DropDownMenu';
import {humanize, quote, Aux} from 'lib/utils';

const RecipientsView = ({
  selectRecipient,
  selectAllRecipients,
  recipients,
  selectedCount,
  isFetching,
  getPage,
  onBulkDelete,
  listId,
  q,
  status,
  viewDetails,
  handleSelectSegment,
  handleSearchChange,
  handleStatusChange,
  segment,
  pagination
}) => {
  const allSelected = recipients.length > 0 && recipients.length === selectedCount;
  const isRecipientsVisible = isFetching || recipients.length > 0;
  const isPlaceholderVisible = isFetching && recipients.length === 0;
  const colSpan = 5;

  const getText = term => <Aux>No subscribers found matching {term}.</Aux>;
  const getEmptyText = () => {
    if (q) return getText(<NL>{quote(q)}</NL>);
    if (segment.id)
      return getText(
        <span>
          <NL>{quote(segment.name)}</NL> segment
        </span>
      );
    if (status) return getText(<span>{quote(humanize(status))} status</span>);
    return "You don't have any subscribers in this list yet.";
  };

  const EmptyRow = () => (
    <tr>
      <td colSpan={colSpan} className={classNames.placeholderRow}>
        <h3 className="ui header centered">{getEmptyText()}</h3>
      </td>
    </tr>
  );

  return (
    <section className={classNames.section}>
      <div className="page-actions">
        <Search
          value={q}
          placeholder="Search by email, name, surname"
          onSearch={handleSearchChange}
          inputClass={classNames.searchInput}
          className={classNames.search}
        />
        <div className="spacer" />
        <DropDownMenu className={cx('floating labeled icon button', {teal: status})}>
          <i className="filter icon" />
          <span className="text">{humanize(status || 'any status')}</span>
          <div className="menu">
            <div className="scrolling menu">
              <div className="item" onClick={() => handleStatusChange()}>
                Any status
              </div>
              {RECIPIENT_STATUSES.map(s => (
                <div
                  key={s}
                  className={cx('item', {'active selected': s === status})}
                  onClick={() => handleStatusChange(s)}>
                  {humanize(s)}
                </div>
              ))}
            </div>
          </div>
        </DropDownMenu>
        <Segmentation listId={listId} onSelectSegment={handleSelectSegment} />
      </div>
      <div className={classNames.dimmerWrapper}>
        <Loader active={isFetching} inline={false} dimmerClassName={classNames.dimmer} />
        <div className={cx('ui top attached segment', classNames.tableWrapper)}>
          <table className={cx('ui large striped selectable single line table', classNames.table)}>
            <thead>
              <tr>
                <th className={classNames.checkboxColumn}>
                  <CheckBox
                    init
                    onChange={() => selectAllRecipients(!allSelected)}
                    checked={allSelected}
                    fitted
                  />
                </th>
                <th>Email</th>
                <th>Name</th>
                <th>Status</th>
                <th>Created at</th>
              </tr>
            </thead>
            <tbody>
              {isPlaceholderVisible && <tr className={classNames.placeholderRow} />}
              {isRecipientsVisible ? (
                recipients.map((r, i) => (
                  <RecipientRow
                    recipient={r}
                    key={i}
                    onClick={viewDetails}
                    onSelect={selectRecipient}
                  />
                ))
              ) : (
                <EmptyRow />
              )}
            </tbody>
          </table>
        </div>
        <div className="ui secondary bottom attached segment clearing">
          {selectedCount > 0 && (
            <Button className="labeled icon red" onClick={onBulkDelete}>
              <i className="trash outline icon" />
              Delete ( {selectedCount} )
            </Button>
          )}
          {pagination.total > 0 && (
            <span className={classNames.totalCount}>
              Total: <NL>{pagination.total}</NL>
            </span>
          )}
          <Pagination onPageChange={getPage} {...pagination} />
        </div>
      </div>
      <ConfirmDelete listId={listId} />
    </section>
  );
};

RecipientsView.propTypes = {
  getPage: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired,
  onBulkDelete: PropTypes.func.isRequired,
  selectRecipient: PropTypes.func.isRequired,
  selectAllRecipients: PropTypes.func.isRequired,
  recipients: PropTypes.array.isRequired,
  selectedCount: PropTypes.number,
  isFetching: PropTypes.bool.isRequired,
  listId: PropTypes.string.isRequired,
  status: PropTypes.string,
  q: PropTypes.string,
  viewDetails: PropTypes.func.isRequired,
  handleSearchChange: PropTypes.func.isRequired,
  handleSelectSegment: PropTypes.func.isRequired,
  segment: PropTypes.object,
  handleStatusChange: PropTypes.func.isRequired
};

export default RecipientsView;
