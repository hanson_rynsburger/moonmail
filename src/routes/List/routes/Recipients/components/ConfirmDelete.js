import React from 'react';
import PropTypes from 'prop-types';
import Confirm from 'components/Modal/Confirm';
import NL from 'components/NoLocalize';
import pluralize from 'pluralize';

const ConfirmDelete = ({
  isOpen,
  isDeleting,
  listId,
  recipientIds,
  deleteRecipientsCancel,
  deleteRecipients
}) => (
  <Confirm
    isOpen={isOpen}
    isLoading={isDeleting}
    onCancel={deleteRecipientsCancel}
    onConfirm={() => deleteRecipients(listId, recipientIds)}
    confirmText={`Delete ${pluralize('subscriber', recipientIds.length)}`}
    confirmClass="negative">
    <p>
      This action will completely delete{' '}
      <b>
        <NL>{recipientIds.length} </NL>
        {pluralize('subscriber', recipientIds.length)}
      </b>
    </p>
  </Confirm>
);

ConfirmDelete.propTypes = {
  recipientIds: PropTypes.array.isRequired,
  deleteRecipients: PropTypes.func.isRequired,
  deleteRecipientsCancel: PropTypes.func.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  listId: PropTypes.string.isRequired
};

export default ConfirmDelete;
