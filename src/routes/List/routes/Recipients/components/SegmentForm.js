import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import {humanize} from 'lib/utils';
import Modal from 'components/Modal';
import Button from 'components/Button';
import Input from 'components/Input';
import Select, {SelectItem} from 'components/Select';
import classNames from './SegmentForm.scss';
import Filter from './Filter';

class SegmentForm extends Component {
  static propTypes = {
    listId: PropTypes.string.isRequired,
    segmentId: PropTypes.string,
    isExpanded: PropTypes.bool.isRequired,
    isSaving: PropTypes.bool.isRequired,
    isDeleting: PropTypes.bool.isRequired,
    isProcessing: PropTypes.bool.isRequired,
    recipientFieldIds: PropTypes.array.isRequired,
    getRecipientField: PropTypes.func.isRequired,
    collapse: PropTypes.func.isRequired,
    saveSegment: PropTypes.func.isRequired,
    deleteSegment: PropTypes.func.isRequired,
    onSelectSegment: PropTypes.func.isRequired,
    fields: PropTypes.object.isRequired,
    invalid: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired
  };

  addCondition = (value, index) => {
    this.props.fields.conditions.addField(value, index);
  };

  removeCondition = index => {
    this.props.fields.conditions.removeField(index);
  };

  deleteSegment = () => {
    const {deleteSegment, segmentId, listId, onSelectSegment} = this.props;
    deleteSegment(segmentId, listId).then(() => {
      onSelectSegment(null);
    });
  };

  saveSegment = formProps => {
    const {saveSegment, listId, onSelectSegment} = this.props;
    saveSegment(listId, formProps).then(id => {
      onSelectSegment(id);
    });
  };

  render() {
    const {
      segmentId,
      isExpanded,
      isSaving,
      isDeleting,
      isProcessing,
      recipientFieldIds,
      getRecipientField,
      collapse,
      fields: {name, conditions},
      invalid,
      handleSubmit
    } = this.props;

    const usedRecipientFieldIds = conditions.map(c => c.recipientField.value);
    const unusedRecipientFieldIds = recipientFieldIds.filter(
      id => !usedRecipientFieldIds.includes(id)
    );

    return (
      <Modal isOpen={isExpanded} size="small">
        <div className="header">{segmentId ? 'Edit' : 'New'} segment</div>
        <div className="content">
          <form onSubmit={handleSubmit} className="ui form">
            <Input {...name} label="Segment name" />
            <div className="field">
              <label>Conditions</label>
              {conditions.length ? (
                conditions.map(({recipientField, filters}, index) => {
                  const availableFieldIds = [...unusedRecipientFieldIds];
                  const currentField = getRecipientField(recipientField.value);
                  if (!recipientField.value) {
                    recipientField.value = unusedRecipientFieldIds[0];
                  } else {
                    availableFieldIds.unshift(recipientField.value);
                  }
                  return (
                    <div key={index} className={cx('fields', classNames.filterFields)}>
                      <div className="five wide field">
                        <Select
                          search
                          {...recipientField}
                          label={false}
                          invalid={false}
                          className="ui selection dropdown">
                          {availableFieldIds.map(fieldId => (
                            <SelectItem key={fieldId} value={fieldId}>
                              {getRecipientField(fieldId).name || humanize(fieldId)}
                            </SelectItem>
                          ))}
                        </Select>
                      </div>
                      <Filter field={currentField} filters={filters} />
                      <div>
                        <Button basic className="icon" onClick={() => this.removeCondition(index)}>
                          <i className="remove icon link" />
                        </Button>
                      </div>
                    </div>
                  );
                })
              ) : (
                <div className={classNames.info}>Please, start adding conditions.</div>
              )}
            </div>
          </form>
        </div>
        <div className="actions">
          {unusedRecipientFieldIds.length > 0 && (
            <Button className="left floated" basic onClick={() => this.addCondition()}>
              <i className="plus icon" />Add condition
            </Button>
          )}
          {segmentId && (
            <Button
              basic
              icon
              loading={isDeleting}
              disabled={isProcessing}
              onClick={this.deleteSegment}>
              <i className="trash alternate outline icon" />
            </Button>
          )}
          <Button disabled={isProcessing} onClick={collapse}>
            Cancel
          </Button>
          <Button
            primary
            loading={isSaving}
            disabled={invalid || isProcessing}
            onClick={handleSubmit(this.saveSegment)}>
            Save
          </Button>
        </div>
      </Modal>
    );
  }
}

export default SegmentForm;
