import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Select, {SelectItem} from 'components/Select';
import DateTimeInput from 'components/DateTimeInput';
import classNames from './Filter.scss';
import cx from 'classnames';

const Filter = ({filters, field: {type, options, placeholder, mask, search}}) => {
  switch (type) {
    case 'select':
      return (
        <Select
          {...filters.match}
          fieldClass="eleven wide"
          placeholder={placeholder}
          search={search}
          label={false}>
          {options.map(option => (
            <SelectItem key={option.value} value={option.value}>
              {option.name}
            </SelectItem>
          ))}
        </Select>
      );
    case 'date':
      return (
        <div className={cx('eleven wide field', classNames.dateRange)}>
          <DateTimeInput
            {...filters.gte}
            label={false}
            showError={false}
            inputProps={{placeholder: 'Start'}}
          />
          <span className={classNames.separator}>and</span>
          <DateTimeInput
            {...filters.lte}
            showError={false}
            label={false}
            inputProps={{placeholder: 'End'}}
          />
        </div>
      );
    default:
      return (
        <Input
          fieldClass="eleven wide"
          placeholder={placeholder}
          mask={mask}
          label={false}
          {...filters.match}
        />
      );
  }
};

Filter.propTypes = {
  filters: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired
};

export default Filter;
