import React from 'react';
import Liquid from 'liquid.js';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import {stringToArray} from 'lib/utils';
import Confirm from 'components/Modal/Confirm';

const TestConfirmationEmail = ({
  isOpen,
  isTesting,
  fields: {emails},
  handleSubmit,
  invalid,
  list,
  testConfirmationEmail,
  listId,
  resetForm,
  testConfirmationEmailCancel
}) => {
  const onSubmit = async formProps => {
    const emails = stringToArray(formProps.emails);
    const body = Liquid.parse(list.confirmationEmailBody).render({list_name: list.name});
    const data = {emails, body, subject: 'Test Confirmation Email'};
    await testConfirmationEmail(listId, data);
    resetForm();
  };
  const onCancel = () => {
    testConfirmationEmailCancel();
    resetForm();
  };
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isTesting}
      isDisabled={invalid}
      onCancel={onCancel}
      onConfirm={handleSubmit(onSubmit)}
      headerText="Send test confirmation email"
      confirmText="Send"
      confirmClass="primary">
      <form className="ui form" onSubmit={handleSubmit(onSubmit)}>
        <Input type="text" label="Emails separated by comma" {...emails} />
      </form>
    </Confirm>
  );
};

TestConfirmationEmail.propTypes = {
  listId: PropTypes.string.isRequired,
  list: PropTypes.shape({
    confirmationEmailBody: PropTypes.string
  }).isRequired,
  fields: PropTypes.object.isRequired,
  testConfirmationEmailCancel: PropTypes.func.isRequired,
  testConfirmationEmail: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  isTesting: PropTypes.bool.isRequired
};

export default TestConfirmationEmail;
