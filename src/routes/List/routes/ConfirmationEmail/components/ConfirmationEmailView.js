import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import Frame from 'react-frame-component';
import Link from 'react-router/lib/Link';
import CodeEditor from 'components/CodeEditor';
import Loader from 'components/Loader';
import classNames from './ConfirmationEmailView.scss';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';
import DotHint from 'components/DotHint';
import TestModal from '../containers/TestConfirmationEmailContainer';
import ConfirmReset from '../containers/ConfirmResetContainer';
import {Aux} from 'lib/utils';
import PopUp from 'components/PopUp';

const ConfirmationEmailView = ({
  fields: {confirmationEmailBody},
  handleSubmit,
  updateList,
  list,
  listId,
  html,
  isFreeUser,
  isUpdating,
  isFetching,
  invalid,
  resetForm,
  switchMode,
  setIsEditing,
  isEditing,
  testConfirmationEmailStart,
  resetConfirmationEmailStart,
  isMobile
}) => {
  const frameClass = cx(classNames.frame, {
    [classNames.mobileFrame]: isMobile
  });

  const submit = formProps => {
    updateList(listId, formProps).then(() => setIsEditing(false));
  };

  const cancel = e => {
    if (e) {
      e.preventDefault();
    }
    resetForm();
    setIsEditing(false);
  };

  return (
    <section className={classNames.section}>
      {!isEditing && (
        <div className={cx('ui form', classNames.form)}>
          <Loader active={isFetching} inline={false} dimmerClassName={classNames.dimmer} />
          <Frame className={cx(frameClass, NO_LOCALIZE)}>
            <div dangerouslySetInnerHTML={{__html: html}} />
          </Frame>
          <div className="page-actions">
            <div className="ui basic buttons">
              <Button onClick={switchMode} active={!isMobile}>
                <i className="icon desktop" />Desktop
              </Button>
              <Button onClick={switchMode} active={isMobile}>
                <i className="icon mobile alternate" />Mobile
              </Button>
            </div>
            <div className="spacer" />
            <div>
              <Button onClick={testConfirmationEmailStart}>Send test email</Button>
              <PopUp
                on="click"
                showDelay={500}
                closable={false}
                variation="small"
                disabled={!isFreeUser}
                content={
                  <Aux>
                    <Link to="/profile/plan">upgrade your account</Link> to edit this email
                  </Aux>
                }>
                <Button primary disabled={isFreeUser} onClick={() => setIsEditing(true)}>
                  <i className="icon edit" /> Edit
                </Button>
              </PopUp>
            </div>
          </div>
        </div>
      )}
      {isEditing && (
        <form className={cx('ui form', classNames.form)} onSubmit={handleSubmit(submit)}>
          <Loader active={isFetching} inline={false} dimmerClassName={classNames.dimmer} />
          <CodeEditor
            label={
              <DotHint id="list.confirmation_email.body">
                Confirmation email body (paste html or drag your html file here)
              </DotHint>
            }
            {...confirmationEmailBody}
            className={classNames.editorField}
            editorClass={classNames.editor}
          />
          <div className="page-actions">
            <Button onClick={resetConfirmationEmailStart} disabled={isUpdating}>
              Reset to default
            </Button>
            <div className="spacer" />
            <Button negative onClick={cancel} disabled={isUpdating}>
              Cancel
            </Button>
            <Button loading={isUpdating} primary type="submit" disabled={invalid || isUpdating}>
              Save
            </Button>
          </div>
        </form>
      )}
      <TestModal listId={listId} />
      <ConfirmReset list={list} onConfirm={cancel} />
    </section>
  );
};

ConfirmationEmailView.propTypes = {
  list: PropTypes.object.isRequired,
  listId: PropTypes.string.isRequired,
  html: PropTypes.string.isRequired,
  isFreeUser: PropTypes.bool,
  isUpdating: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  isEditing: PropTypes.bool.isRequired,
  isMobile: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  updateList: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  switchMode: PropTypes.func.isRequired,
  setIsEditing: PropTypes.func.isRequired,
  testConfirmationEmailStart: PropTypes.func.isRequired,
  resetConfirmationEmailStart: PropTypes.func.isRequired,
  fields: PropTypes.shape({
    confirmationEmailBody: PropTypes.object.isRequired
  }).isRequired
};

export default ConfirmationEmailView;
