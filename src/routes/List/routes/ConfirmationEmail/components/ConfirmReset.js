import React from 'react';
import PropTypes from 'prop-types';
import Confirm from 'components/Modal/Confirm';
import NoLocalize from 'components/NoLocalize';

const ConfirmReset = ({
  isOpen,
  isResetting,
  list,
  resetConfirmationEmailCancel,
  resetConfirmationEmail,
  onConfirm
}) => {
  const reset = async () => {
    await resetConfirmationEmail(list.id);
    if (onConfirm) {
      onConfirm();
    }
  };

  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isResetting}
      onCancel={resetConfirmationEmailCancel}
      onConfirm={reset}
      confirmText="Reset confirmation email"
      confirmClass="negative">
      <p>
        This action will reset confirmation email to default for the list{' '}
        <NoLocalize el="b">"{list.name}"</NoLocalize>
      </p>
    </Confirm>
  );
};

ConfirmReset.propTypes = {
  list: PropTypes.object.isRequired,
  resetConfirmationEmail: PropTypes.func.isRequired,
  resetConfirmationEmailCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func,
  isResetting: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired
};

export default ConfirmReset;
