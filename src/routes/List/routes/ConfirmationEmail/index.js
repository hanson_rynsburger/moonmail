import {injectReducer} from 'store/reducers';
import ConfirmationEmail from './containers/ConfirmationEmailContainer';
import reducer, {stateKey} from './modules/reducer';

export default store => {
  injectReducer(store, {key: stateKey, reducer});
  return {
    path: 'confirmation-email',
    component: ConfirmationEmail
  };
};
