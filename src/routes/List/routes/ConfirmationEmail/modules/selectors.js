import {createSelector} from 'reselect';
import {stateKey} from './reducer';

const confirmationEmailSelector = state => state[stateKey];

export const getIsTesting = createSelector(
  confirmationEmailSelector,
  confirmationEmail => confirmationEmail.isTesting
);

export const getIsTestModalOpen = createSelector(
  confirmationEmailSelector,
  confirmationEmail => confirmationEmail.isTestModalOpen
);

export const getIsResetting = createSelector(
  confirmationEmailSelector,
  confirmationEmail => confirmationEmail.isResetting
);

export const getIsResetModalOpen = createSelector(
  confirmationEmailSelector,
  confirmationEmail => confirmationEmail.isResetModalOpen
);

export const getIsEditing = createSelector(
  confirmationEmailSelector,
  confirmationEmail => confirmationEmail.isEditing
);
