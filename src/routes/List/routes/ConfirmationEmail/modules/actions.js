import * as api from 'lib/api';
import * as types from './types';
import {addMessage} from 'modules/messages/actions';
import listsActions from 'modules/lists/actions';

import listsTypes from 'modules/lists/types';

export const testConfirmationEmailStart = () => ({
  type: types.TEST_CONFIRMATION_EMAIL_START
});

export const testConfirmationEmailCancel = () => ({
  type: types.TEST_CONFIRMATION_EMAIL_CANCEL
});

export const testConfirmationEmail = (listId, data) => {
  return async dispatch => {
    dispatch({
      type: types.TEST_CONFIRMATION_EMAIL_REQUEST
    });
    try {
      await api.sendTestConfirmationEmail(listId, data);
      dispatch({
        type: types.TEST_CONFIRMATION_EMAIL_SUCCESS
      });
      dispatch(
        addMessage({
          text: 'Test email was sent',
          style: 'success'
        })
      );
    } catch (error) {
      dispatch({
        type: types.TEST_CONFIRMATION_EMAIL_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const resetConfirmationEmailStart = () => ({
  type: types.RESET_CONFIRMATION_EMAIL_START
});

export const resetConfirmationEmailCancel = () => ({
  type: types.RESET_CONFIRMATION_EMAIL_CANCEL
});

export const resetConfirmationEmail = listId => {
  return async dispatch => {
    dispatch({
      type: listsTypes.UPDATE_REQUEST
    });
    try {
      const list = await api.resetConfirmationEmail(listId);
      dispatch(listsActions.updateResourceDataLocally(list));
      dispatch(
        addMessage({
          text: 'List was updated',
          style: 'success'
        })
      );
    } catch (error) {
      dispatch({
        type: listsTypes.UPDATE_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const setIsEditing = isEditing => ({
  type: types.SET_EDITING_CONFIRMATION_EMAIL,
  isEditing
});
