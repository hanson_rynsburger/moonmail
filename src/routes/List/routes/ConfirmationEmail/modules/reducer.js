import * as types from './types';
import {combineReducers} from 'redux';
import listsTypes from 'modules/lists/types';

export const stateKey = 'confirmationEmail';

const isTesting = (state = false, action) => {
  switch (action.type) {
    case types.TEST_CONFIRMATION_EMAIL_REQUEST:
      return true;
    case types.TEST_CONFIRMATION_EMAIL_SUCCESS:
    case types.TEST_CONFIRMATION_EMAIL_FAIL:
      return false;
    default:
      return state;
  }
};

const isTestModalOpen = (state = false, action) => {
  switch (action.type) {
    case types.TEST_CONFIRMATION_EMAIL_START:
      return true;
    case types.TEST_CONFIRMATION_EMAIL_SUCCESS:
    case types.TEST_CONFIRMATION_EMAIL_FAIL:
    case types.TEST_CONFIRMATION_EMAIL_CANCEL:
      return false;
    default:
      return state;
  }
};

const isResetting = (state = false, action) => {
  switch (action.type) {
    case listsTypes.UPDATE_REQUEST:
      return true;
    case listsTypes.UPDATE_SUCCESS:
    case listsTypes.UPDATE_FAIL:
      return false;
    default:
      return state;
  }
};

const isResetModalOpen = (state = false, action) => {
  switch (action.type) {
    case types.RESET_CONFIRMATION_EMAIL_START:
      return true;
    case listsTypes.UPDATE_SUCCESS:
    case listsTypes.UPDATE_FAIL:
    case types.RESET_CONFIRMATION_EMAIL_CANCEL:
      return false;
    default:
      return state;
  }
};

const isEditing = (state = false, action) => {
  switch (action.type) {
    case types.SET_EDITING_CONFIRMATION_EMAIL:
      return !!action.isEditing;
    default:
      return state;
  }
};

export default combineReducers({
  isTesting,
  isTestModalOpen,
  isResetting,
  isEditing,
  isResetModalOpen
});
