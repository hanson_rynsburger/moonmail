import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {reduxForm} from 'redux-form';
import ConfirmationEmailView from '../components/ConfirmationEmailView';
import * as actions from '../modules/actions';
import * as selectors from '../modules/selectors';
import listsActions from 'modules/lists/actions';
import listsSelectors from 'modules/lists/selectors';
import {addMessage} from 'modules/messages/actions';
import {parse} from 'lib/parser';
import {createValidator} from 'lib/validator';
import {getIsFreeUser} from 'modules/profile/selectors';

class ConfirmationEmail extends Component {
  static propTypes = {
    addMessage: PropTypes.func.isRequired,
    fetchList: PropTypes.func.isRequired,
    isEditing: PropTypes.bool.isRequired,
    updateListLocally: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
    list: PropTypes.object.isRequired,
    listId: PropTypes.string.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      html: props.list.confirmationEmailBody || '',
      isMobile: false
    };
  }

  parseLiquid = async props => {
    const {list, addMessage} = props;

    if (!list.confirmationEmailBody) {
      return;
    }

    const data = {list_name: list.name};
    try {
      const template = await parse(list.confirmationEmailBody, true, 'verify_url');
      const html = template.render(data);
      this.setState({html});
      return html;
    } catch (error) {
      //eslint-disable-next-line no-console
      console.error(error);
      if (error) addMessage({text: error});
      this.setState({invalid: true});
      return list.confirmationEmailBody;
    }
  };

  beforeUnload = e => {
    const {values, list} = this.props;
    if (list.confirmationEmailBody !== values.confirmationEmailBody) {
      const confirmationMessage = 'You have unsaved changes.';
      e.returnValue = confirmationMessage;
      return confirmationMessage;
    }
  };

  switchMode = () => {
    this.setState({
      isMobile: !this.state.isMobile
    });
  };

  componentDidMount() {
    const {fetchList, listId, isEditing, list} = this.props;
    if (!list.confirmationEmailBody && !isEditing) {
      fetchList(
        listId,
        {
          fields: ['id', 'confirmationEmailBody', 'processed'].join(','),
          include_fields: true
        },
        true
      );
    }
    window.onbeforeunload = this.beforeUnload;
  }

  componentWillReceiveProps(nextProps) {
    this.parseLiquid(nextProps);
  }

  componentWillUnmount() {
    const {updateListLocally, values, list, listId} = this.props;
    if (list.confirmationEmailBody !== values.confirmationEmailBody) {
      updateListLocally(listId, values);
    }
  }

  render() {
    return (
      <ConfirmationEmailView
        {...this.props}
        isMobile={this.state.isMobile}
        switchMode={this.switchMode}
        html={this.state.html}
      />
    );
  }
}

const asyncValidate = values => {
  return new Promise((resolve, reject) => {
    parse(values.confirmationEmailBody, true, 'verify_url').then(
      () => {
        resolve({});
      },
      error => {
        reject({confirmationEmailBody: error});
      }
    );
  });
};

const mapStateToProps = (state, ownProps) => {
  const list = listsSelectors.getActiveResource(state);
  return {
    list,
    listId: ownProps.params.listId,
    isFreeUser: getIsFreeUser(state),
    isUpdating: listsSelectors.getIsUpdating(state),
    isFetching: listsSelectors.getIsFetching(state),
    isEditing: selectors.getIsEditing(state),
    initialValues: list,
    asyncValidate
  };
};

export default reduxForm(
  {
    form: 'confirmationEmail',
    fields: ['confirmationEmailBody'],
    asyncBlurFields: ['confirmationEmailBody'],
    validate: createValidator({
      confirmationEmailBody: 'required'
    })
  },
  mapStateToProps,
  {...actions, ...listsActions, addMessage}
)(ConfirmationEmail);
