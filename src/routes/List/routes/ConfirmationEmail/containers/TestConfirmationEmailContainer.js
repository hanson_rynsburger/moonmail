import {reduxForm} from 'redux-form';
import * as actions from '../modules/actions';
import * as selectors from '../modules/selectors';
import listsSelectors from 'modules/lists/selectors';
import {stringToArray} from 'lib/utils';
import Validator from 'lib/validator';
import TestConfirmationEmail from '../components/TestConfirmationEmail';

const mapStateToProps = state => ({
  isTesting: selectors.getIsTesting(state),
  isOpen: selectors.getIsTestModalOpen(state),
  list: listsSelectors.getActiveResource(state)
});

const rules = {
  email: 'required|email'
};

const validate = values => {
  let errors = {};
  stringToArray(values.emails).forEach(email => {
    const validator = new Validator({email}, rules);
    validator.passes();
    errors = validator.errors.all();
  });
  return errors;
};

export default reduxForm(
  {
    form: 'testConfirmationEmail',
    fields: ['emails'],
    validate
  },
  mapStateToProps,
  actions
)(TestConfirmationEmail);
