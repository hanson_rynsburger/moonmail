import {connect} from 'react-redux';
import * as actions from '../modules/actions';
import * as selectors from '../modules/selectors';
import ConfirmReset from '../components/ConfirmReset';

const mapStateToProps = state => ({
  isResetting: selectors.getIsResetting(state),
  isOpen: selectors.getIsResetModalOpen(state)
});

export default connect(mapStateToProps, actions)(ConfirmReset);
