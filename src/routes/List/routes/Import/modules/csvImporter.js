import S3 from 'aws-sdk/clients/s3';
import storage from 'store';
import Papa from 'papaparse';
import {formatStat} from 'lib/utils';

class CsvImporter {
  constructor() {
    this.creds = storage.get('credentials') || {};
    this.bucket = new S3({
      params: {Bucket: APP_CONFIG.csvImportBucket},
      region: APP_CONFIG.region,
      accessKeyId: this.creds.AccessKeyId,
      secretAccessKey: this.creds.SecretAccessKey,
      sessionToken: this.creds.SessionToken
    });
  }

  parse(csv, uploadLimit = 0) {
    const allowedExtensions = ['csv'];
    const preview = uploadLimit !== Infinity ? uploadLimit + 1 : 251;
    const isFile = !!csv.name;
    return new Promise((resolve, reject) => {
      if (isFile) {
        const ext = csv.name.split('.').pop();
        if (!allowedExtensions.includes(ext)) {
          reject('Not a valid file type. We accept only .csv files');
        }
      }
      Papa.parse(csv, {
        header: true,
        preview,
        dynamicTyping: true,
        skipEmptyLines: true,
        complete: results => {
          const validHeaders = results.meta.fields.every(f => /^[a-zA-Z0-9_-]*$/.test(f));
          if (results.errors.length > 0) {
            const error = results.errors[0];
            if (error.row !== undefined) {
              return reject(`Row: ${error.row + 1}; Error: ${error.message}`);
            }
          }
          if (!validHeaders) {
            return reject(
              'Please add header with field names as a first row to your file. ' +
                'Header can not contain special characters.'
            );
          }
          if (uploadLimit !== Infinity && results.data.length > uploadLimit) {
            return reject(`
              You can upload up to ${formatStat(uploadLimit)} subscribers, 
              please upgrade to increase this limit.
            `);
          }
          if (isFile) {
            results.data = results.data.slice(0, 6);
          }
          resolve(results);
        },
        error: reject
      });
    });
  }

  upload(name, file, headers = {}) {
    const params = {
      Key: name,
      ContentType: file.type || 'text/csv',
      Body: file,
      Metadata: {
        headers: JSON.stringify(headers)
      }
    };
    return this.bucket.putObject(params).promise();
  }
}

export default CsvImporter;
