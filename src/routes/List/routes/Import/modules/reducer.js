import * as types from './types';
import {combineReducers} from 'redux';

export const stateKey = 'importList';

const isImporting = (state = false, action) => {
  switch (action.type) {
    case types.IMPORT_LIST_REQUEST:
      return true;
    case types.IMPORT_LIST_SUCCESS:
    case types.IMPORT_LIST_FAIL:
      return false;
    default:
      return state;
  }
};

const rows = (state = [], action) => {
  switch (action.type) {
    case types.PARSE_LIST_SUCCESS:
      return action.rows;
    case types.IMPORT_LIST_SUCCESS:
    case types.IMPORT_LIST_FAIL:
    case types.PARSE_LIST_CLEAN:
      return [];
    default:
      return state;
  }
};

const fields = (state = [], action) => {
  switch (action.type) {
    case types.PARSE_LIST_SUCCESS:
      return action.fields;
    case types.IMPORT_LIST_SUCCESS:
    case types.IMPORT_LIST_FAIL:
    case types.PARSE_LIST_CLEAN:
      return [];
    default:
      return state;
  }
};

export default combineReducers({
  isImporting,
  rows,
  fields
});
