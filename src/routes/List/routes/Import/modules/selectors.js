import {createSelector} from 'reselect';
import {stateKey} from './reducer';

const importListSelector = state => state[stateKey];

export const getIsImporting = createSelector(
  importListSelector,
  importList => importList.isImporting
);

export const getRows = createSelector(importListSelector, importList => importList.rows);

export const getFields = createSelector(importListSelector, importList => importList.fields);
