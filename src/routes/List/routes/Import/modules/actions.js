import * as types from './types';
import CsvImporter from './csvImporter';
import {getIsDemoUser, getProfile} from 'modules/profile/selectors';
import {addMessage} from 'modules/messages/actions';
import listSelectors from 'modules/lists/selectors';
import {push} from 'react-router-redux';
import {slugRegex} from 'lib/validator';
import {DEMO_USER_ERROR} from 'lib/constants';
import {triggerEventAction} from 'lib/remoteUtils';

let csvImporter = {};

export const initListImporter = () => () => {
  csvImporter = new CsvImporter();
};

export const importList = (listId, csv, headers) => {
  return async (dispatch, getState) => {
    const state = getState();
    const profile = getProfile(state);
    const isDemoUser = getIsDemoUser(state);
    const isFile = !!csv.name;
    const timestamp = isFile ? csv.lastModified : new Date().getTime();
    const name = `${profile.user_id}.${listId}.${timestamp}.csv`;
    const fields = Object.keys(headers).map(key => headers[key]);
    const hasValidFields = fields.every(f => f.match(slugRegex));
    const hasEmail = fields.includes('email');
    if (isDemoUser) {
      return dispatch(
        addMessage({
          text: DEMO_USER_ERROR
        })
      );
    }
    if (!hasValidFields) {
      return dispatch(
        addMessage({
          text: 'Invalid field name. No spaces and special characters are allowed'
        })
      );
    }
    if (!hasEmail) {
      return dispatch(
        addMessage({
          text: 'Please select at least email field'
        })
      );
    }
    dispatch({
      type: types.IMPORT_LIST_REQUEST
    });
    try {
      await csvImporter.upload(name, csv, headers);
      dispatch({
        type: types.IMPORT_LIST_SUCCESS
      });
      dispatch(
        addMessage({
          text: 'Data was uploaded successfully. Please wait until import is finished',
          style: 'success',
          delay: 5000
        })
      );
      dispatch(triggerEventAction('app.userImportedList'));
      dispatch(push(`/lists/${listId}/stats`));
    } catch (error) {
      dispatch({
        type: types.IMPORT_LIST_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const parseListClean = () => ({
  type: types.PARSE_LIST_CLEAN
});

export const parseList = csv => {
  return async (dispatch, getState) => {
    const uploadLimit = listSelectors.getRemainingRecipients(getState());
    try {
      const results = await csvImporter.parse(csv, uploadLimit);
      dispatch({
        type: types.PARSE_LIST_SUCCESS,
        rows: results.data,
        fields: results.meta.fields
      });
    } catch (error) {
      dispatch({
        type: types.PARSE_LIST_FAIL
      });
      dispatch(
        addMessage({
          text: error,
          delay: 5000
        })
      );
    }
  };
};
