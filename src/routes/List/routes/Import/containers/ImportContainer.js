import {reduxForm} from 'redux-form';
import ImportView from '../components/ImportView';
import * as actions from '../modules/actions';
import * as selectors from '../modules/selectors';
import {mergeArrays} from 'lib/utils';
import listSelectors from 'modules/lists/selectors';
import {getIsFreeUser} from 'modules/profile/selectors';

const mapStateToProps = (state, ownProps) => {
  const fields = selectors.getFields(state);
  const availableFields = mergeArrays(
    ['email', 'name', 'surname'],
    listSelectors.getActiveListMetaFields(state)
  );
  const initialValues = {};
  fields.forEach(f => {
    const field = f.toLowerCase();
    initialValues[f] = availableFields.includes(field) ? field : 'false';
  });
  return {
    listId: ownProps.params.listId,
    isImporting: selectors.getIsImporting(state),
    rows: selectors.getRows(state),
    availableFields,
    isFreeUser: getIsFreeUser(state),
    fields,
    initialValues
  };
};

export default reduxForm(
  {
    form: 'importList',
    fields: []
  },
  mapStateToProps,
  actions
)(ImportView);
