import React from 'react';
import PropTypes from 'prop-types';
import Select, {SelectItem} from 'components/Select';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';

const ColumnHeader = ({field, selectedFields, allowAdditions}) => (
  <th width={130}>
    <div className={cx('ui form', NO_LOCALIZE)}>
      <Select {...field} allowAdditions={allowAdditions} fluid>
        <SelectItem value="false">disabled</SelectItem>
        {Object.keys(selectedFields).map((key, i) => (
          <SelectItem key={i} value={key} disabled={selectedFields[key]}>
            {key}
          </SelectItem>
        ))}
      </Select>
    </div>
  </th>
);

ColumnHeader.propTypes = {
  field: PropTypes.object.isRequired,
  selectedFields: PropTypes.object.isRequired,
  allowAdditions: PropTypes.bool.isRequired
};

export default ColumnHeader;
