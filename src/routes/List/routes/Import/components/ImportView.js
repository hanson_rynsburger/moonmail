import React, {Component} from 'react';
import PropTypes from 'prop-types';
import DropZone from './DropZone';
import ColumnHeader from './ColumnHeader';
import Button from 'components/Button';
import cx from 'classnames';
import classNames from './ImportView.scss';
import DotHint from 'components/DotHint';
import {NO_LOCALIZE} from 'components/NoLocalize';
import UploadLimitHint from 'components/UploadLimitHint';

const {staticCdnURL} = APP_CONFIG;

class ImportView extends Component {
  static propTypes = {
    parseList: PropTypes.func.isRequired,
    parseListClean: PropTypes.func.isRequired,
    initListImporter: PropTypes.func.isRequired,
    importList: PropTypes.func.isRequired,
    listId: PropTypes.string.isRequired,
    rows: PropTypes.array.isRequired,
    availableFields: PropTypes.array.isRequired,
    fields: PropTypes.object.isRequired,
    values: PropTypes.object.isRequired,
    isImporting: PropTypes.bool.isRequired,
    isFreeUser: PropTypes.bool.isRequired
  };

  constructor(props) {
    super(props);
    const selectedFields = {};
    props.availableFields.forEach(f => {
      selectedFields[f] = false;
    });
    this.state = {
      file: {},
      selectedFields
    };
  }

  componentDidMount() {
    this.props.initListImporter();
  }

  componentWillUnmount() {
    this.props.parseListClean();
  }

  onDrop = files => {
    const file = files[0];
    this.props.parseList(file).then(() => {
      this.setState({file});
    });
  };

  onImport = () => {
    const {importList, listId, values} = this.props;
    importList(listId, this.state.file, values);
  };

  onReset = () => {
    this.setState({file: {}});
    this.props.parseListClean();
  };

  checkSelected(values) {
    const fields = Object.keys(values).map(key => values[key]);
    const selected = this.state.selectedFields;
    Object.keys(selected).forEach(key => {
      selected[key] = fields.includes(key);
    });
    this.setState({selected});
  }

  componentWillReceiveProps(newProps) {
    this.checkSelected(newProps.values);
  }

  render() {
    const {rows, fields, isImporting, isFreeUser} = this.props;
    const headers = Object.keys(fields);
    const isPreview = headers.length > 0;
    return (
      <section className="padded-bottom">
        <UploadLimitHint />
        {!isPreview && (
          <div>
            <div className={classNames.dropZone}>
              <DotHint className={classNames.hint} id="list.import.csv" />
              <DropZone onDrop={this.onDrop} />
            </div>
            <p>
              You can download a <b>.csv</b> example file from{' '}
              <a href={`${staticCdnURL}/moonmail_import_example.csv`}>here</a>
            </p>
          </div>
        )}
        {isPreview && (
          <div className={classNames.hintWrapper}>
            <div className={cx('ui segment', classNames.previewWrapper)}>
              <table
                className={cx('ui large striped selectable single line table', classNames.table)}>
                <thead>
                  <tr>
                    {headers.map((key, i) => (
                      <ColumnHeader
                        key={i}
                        field={fields[key]}
                        selectedFields={this.state.selectedFields}
                        allowAdditions={!isFreeUser}
                      />
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {rows.map((row, i) => (
                    <tr className={NO_LOCALIZE} key={i}>
                      {headers.map((field, i) => <td key={i}>{row[field]}</td>)}
                    </tr>
                  ))}
                  <tr>{headers.map((field, i) => <td key={i}>...</td>)}</tr>
                </tbody>
              </table>
            </div>
            <DotHint
              className={classNames.hint}
              id={`list.import.${isFreeUser ? 'table' : 'table_paid'}`}
            />
          </div>
        )}
        {isPreview && (
          <div>
            <Button primary loading={isImporting} onClick={this.onImport} disabled={isImporting}>
              Import subscribers
            </Button>
            <Button onClick={this.onReset} disabled={isImporting}>
              Reset
            </Button>
          </div>
        )}
      </section>
    );
  }
}

export default ImportView;
