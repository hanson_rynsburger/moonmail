import React from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import classNames from './DropZone.scss';
import cx from 'classnames';

const DropZone = ({onDrop}) => (
  <Dropzone onDrop={onDrop} className="dropzone">
    {({isDragActive, isDragReject}) => (
      <div
        className={cx('ui icon message', classNames.dropzone, {
          green: isDragActive,
          red: isDragReject
        })}>
        <i className="cloud upload alternate icon" />
        <div className="content">
          <div className="header">Drag & drop .csv file here</div>
          <p>
            You can import csv files in any format.{' '}
            <b>The main requirement is that it contains header with field names as a first row.</b>
          </p>
        </div>
      </div>
    )}
  </Dropzone>
);

DropZone.propTypes = {
  onDrop: PropTypes.func.isRequired
};

export default DropZone;
