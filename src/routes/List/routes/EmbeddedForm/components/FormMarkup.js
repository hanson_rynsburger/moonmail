/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import PropTypes from 'prop-types';
import ReactDOMServer from 'react-dom/server';
import {humanize} from 'lib/utils';
import trim from 'trim';

const {apiBaseURL, staticCdnURL} = APP_CONFIG;

const FormMarkup = ({metaData, listId, userId, terms, termsUrl}) => (
  <form
    action={`${apiBaseURL}/lists/${listId}/subscribe`}
    method="post"
    id="mm-embedded-form"
    noValidate>
    <input type="hidden" name="u" value={userId} />
    <div className="mm-field">
      <label htmlFor="mm-email">
        Email <sup>*</sup>
      </label>
      <input id="mm-email" type="email" name="email" required />
    </div>
    {Object.keys(metaData).map((key, i) => {
      if (metaData[key] !== true) return;
      return (
        <div className="mm-field" key={i}>
          <label htmlFor={`mm-${key}`}>{humanize(key)}</label>
          <input id={`mm-${key}`} type="text" name={`metadata[${key}]`} />
        </div>
      );
    })}
    {terms && (
      <div className="mm-field">
        <input type="checkbox" id="mm-terms" name="metadata[acceptedTerms]" required />{' '}
        <label htmlFor="mm-terms">
          I agree to the{' '}
          <a href={termsUrl} target="_blank">
            Terms and Conditions
          </a>
          <sup>*</sup>
        </label>
      </div>
    )}
    <button type="submit">Subscribe</button>
  </form>
);

FormMarkup.propTypes = {
  metaData: PropTypes.object.isRequired,
  listId: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  terms: PropTypes.bool,
  termsUrl: PropTypes.string
};

function formatHtml(str) {
  const div = document.createElement('div');
  div.innerHTML = str.trim();

  return formatNode(div, 0).innerHTML.trim();
}

function formatNode(node, level) {
  if (node.innerHTML.length < 50) return node;
  const indentBefore = new Array(level++ + 1).join('  ');
  const indentAfter = new Array(level - 1).join('  ');
  for (let i = 0; i < node.children.length; i++) {
    node.insertBefore(document.createTextNode('\n' + indentBefore), node.children[i]);
    formatNode(node.children[i], level);
    if (node.lastElementChild === node.children[i]) {
      node.appendChild(document.createTextNode('\n' + indentAfter));
    }
  }
  return node;
}

export default props => {
  const formHtml = formatHtml(ReactDOMServer.renderToStaticMarkup(<FormMarkup {...props} />));
  const successMessage =
    props.list.sendEmailVerificationOnSubscribe === false
      ? "You've been successfully subscribed."
      : 'Please check your email to confirm subscription';

  const options = {
    config: {
      reCapcha: Boolean(props.config.reCapcha),
      redirectUrl: props.redirectUrl ? `"${props.redirectUrl}"` : false
    },
    translations: {
      required: 'This field is required.',
      email: 'Please enter a valid email address.',
      success: successMessage,
      error: 'Oops! Something went wrong...'
    }
  };
  return trim(`<!-- mm-embedded-form styles (for better performance put this link in html head of your page) -->
<link rel="stylesheet" href="${staticCdnURL}/mm-embedded-form.css">

<!-- mm-embedded-form start -->
${formHtml}
<!-- mm-embedded-form end -->

<!-- mm-embedded-form JavaScript, remove this script to use the plain form -->
<script src="${staticCdnURL}/mm-embedded-form.js"></script>

<!-- mm-embedded-form options, allows you to configure a form behaviour -->
<script>
  mmOptions = ${JSON.stringify(options, null, 2)}
</script>
`);
};
