import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ChechboxInput from 'components/CheckBoxInput';
import Chechbox from 'components/CheckBox';
import Input from 'components/Input';
import CodeMirror from 'components/CodeMirror';
import FormPreview from './FormPreview';
import renderFormHtml from './FormMarkup';
import {NO_LOCALIZE} from 'lib/constants';
import classNames from './EmbeddedFormView.scss';
import {humanize, propsChanged} from 'lib/utils';
import cx from 'classnames';
import DotHint from 'components/DotHint';

export default class EmbeddedFormView extends Component {
  static propTypes = {
    listId: PropTypes.string.isRequired,
    list: PropTypes.object.isRequired,
    userId: PropTypes.string.isRequired,
    fields: PropTypes.object.isRequired,
    values: PropTypes.object.isRequired,
    updateList: PropTypes.func.isRequired,
    copyToClipboard: PropTypes.func.isRequired,
    isUpdating: PropTypes.bool
  };

  constructor(props) {
    super(props);
    const {listId, list, userId, values} = props;
    this.state = {
      formHtml: renderFormHtml({listId, list, userId, ...values})
    };
  }

  componentWillReceiveProps({listId, list, userId, values}) {
    const formHtml = renderFormHtml({listId, list, userId, ...values});
    this.setState({formHtml});

    // save config to a list
    if (propsChanged(values.config, this.props.values.config)) {
      this.props.updateList(listId, values.config, true);
    }
  }

  render() {
    const {
      list,
      fields: {metaData, config, terms, termsUrl, redirectUrl, redirect},
      isUpdating,
      copyToClipboard
    } = this.props;
    const {formHtml} = this.state;

    const options = {
      mode: 'text/html',
      readOnly: true
    };
    return (
      <section className="ui vertical segment padded-bottom">
        <h2>Embedded form</h2>
        <div className="ui stackable grid">
          <div className="five wide column">
            <h3>Custom fields</h3>
            <form className="ui form">
              {Object.keys(metaData).map((key, i) => {
                metaData[key].checked = !!metaData[key].checked;
                metaData[key].value = !!metaData[key].value;
                return <ChechboxInput key={i} {...metaData[key]} label={humanize(key)} />;
              })}
              <ChechboxInput {...terms} label="Terms of Service" />
              {terms.value && <Input type="text" {...termsUrl} />}
            </form>
            <h3>Form settings</h3>
            <form className="ui form">
              <div className="field">
                <Chechbox {...config.reCapcha} disabled={isUpdating} label="Enable reCAPTCHA" />
                <DotHint id="list.forms.recapcha" />
              </div>
              <ChechboxInput {...redirect} label="Redirect on success" />
              {redirect.value && <Input type="text" {...redirectUrl} />}
            </form>
          </div>
          <div className="eleven wide column">
            <h3>Preview</h3>
            <section className={cx('ui segment', NO_LOCALIZE)}>
              <FormPreview html={formHtml} list={list} />
            </section>
            <h3>
              <a
                href="#"
                onClick={e => {
                  e.preventDefault();
                  copyToClipboard(formHtml, 'Form');
                }}>
                Copy/paste onto your site{' '}
              </a>
            </h3>
            <section className={cx('ui segment no padding', NO_LOCALIZE)}>
              <CodeMirror value={formHtml} options={options} className={classNames.code} />
            </section>
          </div>
        </div>
      </section>
    );
  }
}
