import PropTypes from 'prop-types';
import React, {Component} from 'react';
import Frame from 'react-frame-component';
import ReactDOM from 'react-dom';
import classNames from './FormPreview.scss';

class FormPreview extends Component {
  static propTypes = {
    html: PropTypes.string.isRequired,
    list: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      height: 0
    };
  }

  componentDidMount() {
    const formScript = /<script.*?src="(.*?)"/.exec(this.props.html)[1];
    this.loadScript(formScript).then(() => {
      if (this.props.list.sendEmailVerificationOnSubscribe === false) {
        this.iframe.contentWindow.mmOptions.translations.success =
          "You've been successfully subscribed.";
      }
    });
    setTimeout(this.updateHeight);
  }

  loadScript(src) {
    //eslint-disable-next-line react/no-find-dom-node
    this.iframe = ReactDOM.findDOMNode(this.frame);
    const context = this.iframe.contentDocument;
    const script = context.createElement('script');
    script.type = 'text/javascript';
    script.src = src;
    context.body.appendChild(script);
    return new Promise(resolve => {
      script.onload = resolve;
    });
  }

  updateHeight = () => {
    const height = this.iframe.contentWindow.document.body.offsetHeight + 16;
    if (height !== this.state.height) {
      this.setState({height});
    }
  };

  componentDidUpdate() {
    setTimeout(this.updateHeight);
    window.frame = this.iframe.contentWindow;
    const context = this.iframe.contentWindow;
    context.mmInit && context.mmInit();
  }

  render() {
    return (
      <Frame
        height={this.state.height}
        className={classNames.frame}
        ref={i => {
          this.frame = i;
        }}>
        <div dangerouslySetInnerHTML={{__html: this.props.html}} />
      </Frame>
    );
  }
}

export default FormPreview;
