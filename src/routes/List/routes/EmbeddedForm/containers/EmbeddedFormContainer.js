import {reduxForm, getValues} from 'redux-form';
import EmbeddedFormView from '../components/EmbeddedFormView';
import {getUserIdBase64} from 'modules/profile/selectors';
import listSelectors from 'modules/lists/selectors';
import listActions from 'modules/lists/actions';
import {createValidator} from 'lib/validator';
import {copyToClipboard} from 'modules/messages/actions';

const mapStateToProps = (state, ownProps) => {
  const values = getValues(state.form.embedded);
  listSelectors.getListMetaFields(state).forEach(f => {
    ownProps.fields.push(`metaData.${f}`);
  });
  const list = listSelectors.getActiveResource(state);
  const initialValues = {
    ...values,
    config: list,
    termsUrl: 'http://support.moonmail.io/terms-and-policies/terms-of-service'
  };
  return {
    listId: ownProps.params.listId,
    list,
    userId: getUserIdBase64(state),
    initialValues,
    isUpdating: listSelectors.getIsUpdating(state)
  };
};

export default reduxForm(
  {
    form: 'embedded',
    fields: ['terms', 'termsUrl', 'config.reCapcha', 'redirectUrl', 'redirect'],
    validate: createValidator({termsUrl: 'url', redirectUrl: 'url'})
  },
  mapStateToProps,
  {...listActions, copyToClipboard}
)(EmbeddedFormView);
