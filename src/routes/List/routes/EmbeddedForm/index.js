import EmbeddedForm from './containers/EmbeddedFormContainer';

// Sync route definition
export default {
  path: 'forms/embedded',
  component: EmbeddedForm
};
