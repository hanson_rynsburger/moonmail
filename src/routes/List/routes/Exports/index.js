import Exports from './containers/ExportsContainer';

export default {
  path: 'exports',
  component: Exports
};
