import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from 'lib/utils';
import {NO_LOCALIZE} from 'lib/constants';
import {ResourceTable} from 'components/Table';
import cx from 'classnames';
import Button from 'components/Button';
import moment from 'moment/src/moment';
import {parse} from 'url';
import {parse as parseQuery} from 'querystring';

const ExportsView = ({list, isExporting, isFetching, exportList}) => {
  const {exports = []} = list;
  return (
    <section className="padded-bottom">
      <ResourceTable
        resourceName="exports"
        className="large striped single line"
        isFetching={isFetching}
        numColumns={3}
        header={
          <tr>
            <th>Exported on</th>
            <th width={150}>Status</th>
            <th width={100}>Download</th>
          </tr>
        }>
        {exports.map(listExport => {
          const isPending = listExport.status === 'pending';

          const query = parse(listExport.url).query;
          const expiresValue = query && parseQuery(query)['Expires'];
          const expiresDate = expiresValue && moment.unix(Number(expiresValue));
          const isExpired = !isPending && expiresDate && moment().isAfter(expiresDate);

          let downloadIcon = (
            <a href={listExport.url} target="_blank" rel="noopener noreferrer" download>
              <i className="download icon" />
            </a>
          );

          if (isPending) {
            downloadIcon = <i className="clock outline icon" />;
          }

          if (isExpired) {
            downloadIcon = null;
          }

          return (
            <tr key={listExport.createdAt} className={cx({disabled: isPending || isExpired})}>
              <td className={NO_LOCALIZE}>{formatDate(listExport.createdAt, {fromNow: true})}</td>
              <td>{isExpired ? 'Expired' : listExport.status}</td>
              <td className="center aligned">{downloadIcon}</td>
            </tr>
          );
        })}
      </ResourceTable>
      <div>
        <Button primary loading={isExporting} onClick={() => exportList(list.id)}>
          New Export
        </Button>
      </div>
    </section>
  );
};

ExportsView.propTypes = {
  list: PropTypes.object.isRequired,
  isExporting: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  exportList: PropTypes.func.isRequired
};

export default ExportsView;
