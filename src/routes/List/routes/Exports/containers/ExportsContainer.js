import {connect} from 'react-redux';
import ExportsView from '../components/ExportsView';
import actions from 'modules/lists/actions';
import selectors from 'modules/lists/selectors';

const mapStateToProps = (state, ownProps) => ({
  list: selectors.getActiveResource(state),
  isExporting: selectors.getIsExporting(state),
  isFetching: selectors.getIsFetching(state),
  listId: ownProps.params.listId
});

export default connect(mapStateToProps, actions)(ExportsView);
