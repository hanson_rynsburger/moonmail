import React from 'react';
import PropTypes from 'prop-types';
import {trim, compact, Aux} from 'lib/utils';
import Input from 'components/Input';
import Button from 'components/Button';
import CheckBox from 'components/CheckBox';
import Select, {SelectItem} from 'components/Select';
import SenderHint from 'components/SenderHint';
import KeyInput from 'components/KeyInput';
import {NO_LOCALIZE} from 'components/NoLocalize';

const {staticCdnURL} = APP_CONFIG;

const SettingsForm = ({
  fields: {
    name,
    senderId,
    sendEmailVerificationOnSubscribe,
    successConfirmationUrl,
    errorConfirmationUrl
  },
  listId,
  handleSubmit,
  invalid,
  onFormSubmit,
  isLoading,
  hasSenders,
  isFreeUser,
  isFetchingSenders,
  copyToClipboard,
  senders,
  buttonText
}) => {
  const successUrl = `${staticCdnURL}/subscribed.html`;
  const errorUrl = `${staticCdnURL}/error.html`;
  const submit = formProps => {
    const data = compact(formProps);
    if (!hasSenders) {
      data.senderId = null;
    }
    data.name = trim(data.name);
    onFormSubmit(data);
  };

  return (
    <div className="ui stackable grid">
      <form className="ui form nine wide column" onSubmit={handleSubmit(submit)}>
        {listId && (
          <div className="field">
            <label htmlFor="listId">List ID</label>
            <KeyInput
              id="listId"
              onCopy={() => copyToClipboard(listId, 'List ID')}
              value={listId}
            />
          </div>
        )}
        <Input type="text" {...name} />
        <Select
          label="Sender"
          hint="this email address will be used to send confirm subscription emails"
          search
          loading={isFetchingSenders}
          {...senderId}>
          {senders.map((sender, i) => (
            <SelectItem key={i} disabled={!hasSenders && !sender.default} value={sender.id}>
              {sender.fromName ? (
                <span>
                  <b>{sender.fromName}</b> {`<${sender.emailAddress}>`}
                </span>
              ) : (
                sender.emailAddress
              )}
            </SelectItem>
          ))}
        </Select>
        <SenderHint resource="a list" />
        <div className="field">
          <CheckBox
            disabled={isFreeUser}
            label="Send confirmation email when recipient subscribes using the form"
            {...sendEmailVerificationOnSubscribe}
          />
        </div>
        {sendEmailVerificationOnSubscribe.value && (
          <Aux>
            <Input
              label={
                <span>
                  Subscription success redirect url.{' '}
                  <a
                    style={{float: 'right'}}
                    target="_blank"
                    rel="noopener noreferrer"
                    href={`${successUrl}?listName=${name.value}`}>
                    default url
                  </a>
                </span>
              }
              className={NO_LOCALIZE}
              placeholder={successUrl}
              disabled={isFreeUser}
              hint="User is redirected to this url if he successfully confirms subscription."
              {...successConfirmationUrl}
            />
            <Input
              label={
                <span>
                  Subscription error redirect url.{' '}
                  <a
                    style={{float: 'right'}}
                    target="_blank"
                    rel="noopener noreferrer"
                    href={errorUrl}>
                    default url
                  </a>
                </span>
              }
              className={NO_LOCALIZE}
              placeholder={errorUrl}
              disabled={isFreeUser}
              hint="User is redirected to this url if something went wrong and subscription was not confirmed."
              {...errorConfirmationUrl}
            />
          </Aux>
        )}
        <Button primary loading={isLoading} type="submit" disabled={invalid || isLoading}>
          {buttonText || 'Update'}
        </Button>
      </form>
    </div>
  );
};

SettingsForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  copyToClipboard: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  hasSenders: PropTypes.bool.isRequired,
  fields: PropTypes.object.isRequired,
  message: PropTypes.object,
  buttonText: PropTypes.string,
  senders: PropTypes.arrayOf(PropTypes.object).isRequired,
  isFetchingSenders: PropTypes.bool.isRequired,
  listId: PropTypes.string,
  isFreeUser: PropTypes.bool
};

export default SettingsForm;
