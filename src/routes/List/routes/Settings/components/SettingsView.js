import React from 'react';
import PropTypes from 'prop-types';
import SettingsForm from '../containers/SettingsFormContainer';

const SettingsView = ({updateList, listId, isUpdating, list}) => {
  const submit = formProps => {
    updateList(listId, formProps);
  };
  return (
    <section className="ui vertical segment">
      <SettingsForm
        onFormSubmit={submit}
        isLoading={isUpdating}
        initialValues={list}
        listId={listId}
      />
    </section>
  );
};

SettingsView.propTypes = {
  list: PropTypes.object.isRequired,
  updateList: PropTypes.func.isRequired,
  isUpdating: PropTypes.bool.isRequired,
  listId: PropTypes.string.isRequired
};

export default SettingsView;
