import {connect} from 'react-redux';
import SettingsView from '../components/SettingsView';
import selectors from 'modules/lists/selectors';
import actions from 'modules/lists/actions';

const mapStateToProps = (state, ownProps) => ({
  listId: ownProps.params.listId,
  isUpdating: selectors.getIsUpdating(state),
  list: selectors.getActiveResource(state)
});

export default connect(mapStateToProps, actions)(SettingsView);
