import {reduxForm} from 'redux-form';
import {createValidator} from 'lib/validator';
import SettingsFormView from '../components/SettingsForm';
import selectors from 'modules/lists/selectors';
import sendersSelectors from 'modules/senders/selectors';
import * as profileSelectors from 'modules/profile/selectors';
import {copyToClipboard} from 'modules/messages/actions';

const mapStateToProps = (state, {fields = [], initialValues = {}}) => {
  const hasSenders = profileSelectors.getHasSenders(state);
  const rules = {
    name: 'required|max:140',
    successConfirmationUrl: 'url',
    errorConfirmationUrl: 'url',
    senderId: 'required'
  };
  initialValues.senderId = sendersSelectors.getDefaultResourceId(state);
  return {
    isUpdating: selectors.getIsUpdating(state),
    isUnverifiedSesUser: profileSelectors.getIsUnverifiedSesUser(state),
    hasSenders,
    isFreeUser: profileSelectors.getIsFreeUser(state),
    senders: sendersSelectors.getVerifiedSenders(state),
    isFetchingSenders: sendersSelectors.getIsFetchingAll(state),
    fields,
    initialValues,
    validate: createValidator(rules)
  };
};

export default reduxForm(
  {
    form: 'listSettings',
    fields: [
      'name',
      'senderId',
      'successConfirmationUrl',
      'errorConfirmationUrl',
      'sendEmailVerificationOnSubscribe'
    ]
  },
  mapStateToProps,
  {
    copyToClipboard
  }
)(SettingsFormView);
