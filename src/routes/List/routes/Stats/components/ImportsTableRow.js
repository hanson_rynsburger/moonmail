import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from 'lib/utils';
import cx from 'classnames';

const ImportsTableRow = ({file}) => {
  const rowClass = cx({
    negative: file.status === 'failed',
    positive: file.status === 'success'
  });
  return (
    <tr className={rowClass}>
      <td>{formatDate(file.createdAt)}</td>
      <td>{formatDate(file.finishedAt)}</td>
      <td>{file.status}</td>
    </tr>
  );
};
ImportsTableRow.propTypes = {
  file: PropTypes.object.isRequired
};

export default ImportsTableRow;
