import React from 'react';
import PropTypes from 'prop-types';
import ImportsTableRow from './ImportsTableRow';

const ImportsTable = ({files}) => (
  <table className="ui celled striped table">
    <thead>
      <tr>
        <th>Started at</th>
        <th>Finished at</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>{files.map(file => <ImportsTableRow file={file} key={file.createdAt} />)}</tbody>
  </table>
);

ImportsTable.propTypes = {
  files: PropTypes.array.isRequired
};

export default ImportsTable;
