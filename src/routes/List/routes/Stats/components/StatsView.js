import React from 'react';
import PropTypes from 'prop-types';
import Progress from 'components/Progress';
import NL from 'components/NoLocalize';
import ImportsTable from './ImportsTable';
import {formatDate, formatRate, formatStat, Aux} from 'lib/utils';
import {RATE_LIMITS} from 'lib/constants';
import DotHint from 'components/DotHint';

const StatsView = ({
  list: {
    bouncedCount = 0,
    complainedCount = 0,
    subscribedCount = 0,
    awaitingConfirmationCount = 0,
    unsubscribedCount = 0,
    total = 0,
    imports = [],
    createdAt
  }
}) => {
  const unsubscribeRate = formatRate(unsubscribedCount / total);
  const bounceRate = formatRate(bouncedCount / total);
  const bouncePercent = formatRate(bounceRate / RATE_LIMITS.bounceRate);
  const complaintRate = formatRate(complainedCount / total, 3);
  const complaintPercent = formatRate(complaintRate / RATE_LIMITS.complaintRate, 3);

  const readMore = (
    <a
      target="_blank"
      rel="noopener noreferrer"
      href="http://support.moonmail.io/faq/validation-and-reputation/what-are-the-acceptable-complaint-and-bounce-rates">
      Read more
    </a>
  );

  return (
    <section className="ui vertical segment padded-bottom">
      <h3 className="ui header">
        Created
        <NL> {formatDate(createdAt, {fromNow: true})}</NL>
      </h3>
      <div className="ui vertical segment">
        <div className="ui small statistics">
          <div className="teal statistic">
            <div className="value">{formatStat(subscribedCount)}</div>
            <div className="label">Subscribed</div>
          </div>
          <div className="grey statistic">
            <div className="value">{formatStat(awaitingConfirmationCount)}</div>
            <div className="label">Unconfirmed</div>
          </div>
          <div className="red statistic">
            <div className="value">{formatStat(unsubscribedCount)}</div>
            <div className="label">Unsubscribed</div>
          </div>
          <div className="red statistic">
            <div className="value">{formatStat(bouncedCount)}</div>
            <div className="label">Bounced</div>
          </div>
          <div className="red statistic">
            <div className="value">{formatStat(complainedCount)}</div>
            <div className="label">Complained</div>
          </div>
          <div className="grey statistic">
            <div className="value">{formatStat(total)}</div>
            <div className="label">Total subscribers</div>
          </div>
        </div>
      </div>
      <div className="ui vertical segment">
        <h3 className="ui header">
          <DotHint id="list.stats.unsubscribe_rate">Unsubscribe rate</DotHint>
        </h3>
        <Progress className="error" percent={unsubscribeRate} indicating negative />
        <h3 className="ui header">
          Bounce rate <NL>{bounceRate}%</NL> out of <NL>{RATE_LIMITS.bounceRate.toFixed(2)}%</NL>
          <DotHint id="list.stats.bounce_rate" />
        </h3>
        <Progress
          percent={bouncePercent}
          label={<Aux>{readMore} about bounce rate restrictions.</Aux>}
          value={false}
          indicating
          negative
        />
        <h3 className="ui header">
          Complaint rate <NL>{complaintRate}%</NL> out of <NL>{RATE_LIMITS.complaintRate}%</NL>.
          <DotHint id="list.stats.complaint_rate" />
        </h3>
        <Progress
          percent={complaintPercent}
          label={<Aux>{readMore} about complaint rate restrictions.</Aux>}
          value={false}
          indicating
          negative
        />
      </div>
      {imports.length > 0 && (
        <div className="ui vertical segment">
          <h3 className="ui header">Imports</h3>
          <ImportsTable files={imports} />
        </div>
      )}
    </section>
  );
};

StatsView.propTypes = {
  list: PropTypes.object.isRequired
};

export default StatsView;
