import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import StatsView from '../components/StatsView';
import actions from 'modules/lists/actions';
import selectors from 'modules/lists/selectors';

class Stats extends Component {
  static propTypes = {
    fetchList: PropTypes.func.isRequired,
    listId: PropTypes.string.isRequired
  };

  componentDidMount() {
    const {fetchList, listId} = this.props;
    fetchList(listId, undefined, true);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return <StatsView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  listId: ownProps.params.listId,
  list: selectors.getActiveResource(state)
});

export default connect(mapStateToProps, actions)(Stats);
