import Stats from './containers/StatsContainer';

export default {
  path: 'stats',
  component: Stats
};
