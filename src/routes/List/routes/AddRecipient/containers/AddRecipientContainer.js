import {connect} from 'react-redux';
import AddRecipientView from '../components/AddRecipientView';
import recipientsSelectors from 'modules/recipients/selectors';
import recipientsActions from 'modules/recipients/actions';
import listsSelectors from 'modules/lists/selectors';
import listsActions from 'modules/lists/actions';
import {createValidator} from 'lib/validator';
import {mergeArrays} from 'lib/utils';
import {LIST_DEFAULT_METAFIELDS} from 'modules/lists/constants';

const mapStateToProps = (state, ownProps) => ({
  listId: ownProps.params.listId,
  isCreating: recipientsSelectors.getIsCreating(state),
  fields: mergeArrays(
    ['email'],
    LIST_DEFAULT_METAFIELDS,
    listsSelectors.getActiveListMetaFields(state)
  ),
  validate: createValidator({
    email: 'required|email'
  })
});

export default connect(mapStateToProps, {
  createRecipient: recipientsActions.createRecipient,
  updateListLocally: listsActions.updateResourceLocally
})(AddRecipientView);
