import AddRecipient from './containers/AddRecipientContainer';

export default {
  path: 'add',
  component: AddRecipient
};
