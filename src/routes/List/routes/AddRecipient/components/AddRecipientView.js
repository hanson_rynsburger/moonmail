import React from 'react';
import PropTypes from 'prop-types';
import RecipientForm from '../../Recipient/containers/RecipientFormContainer';

const AddRecipientView = ({isCreating, fields, validate, createRecipient, listId}) => {
  return (
    <section className="ui vertical segment">
      <div className="ui stackable grid">
        <RecipientForm
          buttonText="Subscribe"
          className="nine wide column"
          onFormSubmit={data => createRecipient(listId, data)}
          isLoading={isCreating}
          fields={fields}
          validate={validate}
        />
      </div>
    </section>
  );
};

AddRecipientView.propTypes = {
  isCreating: PropTypes.bool,
  fields: PropTypes.array.isRequired,
  validate: PropTypes.func.isRequired,
  createRecipient: PropTypes.func.isRequired,
  listId: PropTypes.string.isRequired
};

export default AddRecipientView;
