import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import CustomField from './CustomField';
import DeletableInput from './DeletableInput';
import Link from 'react-router/lib/Link';
import Button from 'components/Button';
import cx from 'classnames';
import {compact} from 'lib/utils';

const RecipientFormView = ({
  fields,
  protectedFields,
  handleSubmit,
  resetForm,
  invalid,
  onFormSubmit,
  onFormCancel,
  isLoading,
  isFieldFormVisible,
  showCustomFieldForm,
  addCustomField,
  removeCustomField,
  isFreeUser,
  buttonText,
  className
}) => {
  const submit = async ({email, ...metadata}) => {
    await onFormSubmit({email, metadata: compact(metadata)});
    resetForm();
  };
  return (
    <form className={cx('ui form', className)} onSubmit={handleSubmit(submit)}>
      {Object.keys(fields).map(key => {
        if (protectedFields.includes(key)) {
          return <Input {...fields[key]} key={key} type="text" label={key} />;
        }
        return (
          <Input
            {...fields[key]}
            key={key}
            type="text"
            label={key}
            component={DeletableInput}
            onDelete={() => removeCustomField(key)}
          />
        );
      })}

      {isFieldFormVisible && (
        <CustomField onAdd={addCustomField} onCancel={() => showCustomFieldForm(false)} />
      )}
      {isFreeUser && (
        <div className="ui info small message">
          To add more custom fields <Link to="/profile/plan">upgrade your account</Link>.
        </div>
      )}
      <div className="nine wide column">
        {onFormCancel && <Button onClick={onFormCancel}>Cancel</Button>}
        {!isFreeUser && (
          <Button onClick={() => showCustomFieldForm(true)} disabled={isFieldFormVisible}>
            Add custom field
          </Button>
        )}
        <Button
          loading={isLoading}
          primary
          type="submit"
          onClick={handleSubmit(submit)}
          disabled={invalid || isLoading}>
          {buttonText}
        </Button>
      </div>
    </form>
  );
};

RecipientFormView.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  showCustomFieldForm: PropTypes.func.isRequired,
  addCustomField: PropTypes.func.isRequired,
  removeCustomField: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isFreeUser: PropTypes.bool.isRequired,
  fields: PropTypes.object.isRequired,
  isFieldFormVisible: PropTypes.bool.isRequired,
  onFormCancel: PropTypes.func,
  isNew: PropTypes.bool,
  protectedFields: PropTypes.array.isRequired,
  className: PropTypes.string,
  buttonText: PropTypes.string
};

export default RecipientFormView;
