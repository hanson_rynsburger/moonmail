import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';

const DeletableInput = ({onDelete, ...rest}) => (
  <div className="ui action input">
    <input {...rest} />
    <Button basic icon onClick={onDelete}>
      <i className="trash alternate outline icon" />
    </Button>
  </div>
);

DeletableInput.propTypes = {
  onDelete: PropTypes.func.isRequired
};

export default DeletableInput;
