import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Button from 'components/Button';
import DotHint from 'components/DotHint';
import {reduxForm} from 'redux-form';
import Validator from 'lib/validator';

const ActionInput = ({onCancel, onSubmit, ...rest}) => (
  <div className="ui action input">
    <input {...rest} />
    <Button
      basic
      onClick={e => {
        e.preventDefault();
        onSubmit();
      }}>
      <i className="plus icon" /> Add
    </Button>
    <Button
      icon
      basic
      onClick={e => {
        e.preventDefault();
        onCancel();
      }}>
      <i className="close red icon" />
    </Button>
  </div>
);

ActionInput.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

const CustomField = ({fields: {name}, handleSubmit, onAdd, onCancel}) => {
  return (
    <Input
      {...name}
      label={<DotHint id="list.add.custom_field">Custom field</DotHint>}
      onCancel={onCancel}
      onSubmit={handleSubmit(({name}) => onAdd(name))}
      component={ActionInput}
    />
  );
};

CustomField.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onAdd: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  fields: PropTypes.shape({
    name: PropTypes.object.isRequired
  }).isRequired
};

const rules = {
  name: 'required|slug'
};

const validate = values => {
  const validator = new Validator(values, rules);
  validator.passes();
  return validator.errors.all();
};

export default reduxForm({
  form: 'customField',
  fields: ['name'],
  validate
})(CustomField);
