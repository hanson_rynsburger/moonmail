import React from 'react';
import PropTypes from 'prop-types';
import EmailLink from 'components/EmailLink';
import {Table} from 'components/Table';
import {formatDate, isEmpty, humanize} from 'lib/utils';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';
import classNames from './RecipientView.scss';
import Gravatar from 'react-gravatar';
import {negativeStatuses, obfuscateEmail} from 'modules/lists/utils';
import NotFound from 'components/NotFound';
import DotHint from 'components/DotHint';
import Button from 'components/Button';
import RecipientForm from '../containers/RecipientFormContainer';

export const RecipientView = ({
  recipient: {metadata = {}, systemMetadata = {}, ...data},
  isFetching,
  isFormVisible,
  updateRecipient,
  isUpdating,
  listId,
  showForm
}) => {
  if (!data.id) {
    return <NotFound isFetching={isFetching} resource="recipient" />;
  }

  const onFormSubmit = async ({metadata}) => {
    await updateRecipient(data.id, listId, {metadata});
    showForm(false);
  };

  //eslint-disable-next-line no-unused-vars
  const {location, metroCode, ...systemData} = systemMetadata;
  const {name, surname, ...userData} = metadata;
  const hasUserData = !isEmpty(metadata);
  const hasSystemData = !isEmpty(systemData);
  const rowClass = cx({
    negative: negativeStatuses.includes(data.status),
    warning: data.status === 'awaitingConfirmation'
  });
  const userFields = Object.keys(userData).sort();
  surname && userFields.unshift('surname');
  name && userFields.unshift('name');
  return (
    <section className="ui vertical segment padded-bottom">
      <div className="ui stackable grid">
        <div className="ui nine wide column">
          <h2 className={cx('ui header', classNames.header, NO_LOCALIZE)}>
            <Gravatar
              className="circular ui image"
              email={obfuscateEmail(data.email, data.status)}
              size={120}
              default="mm"
            />
            <div className="content">
              {name ? (
                <span>
                  {name} {surname}
                </span>
              ) : (
                <EmailLink to={obfuscateEmail(data.email, data.status)} />
              )}
              <div className="sub header">
                {name && <EmailLink to={obfuscateEmail(data.email, data.status)} />}
                {systemData.city && (
                  <div className={classNames.location}>
                    <i className="map marker alternate icon" />
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      href={`http://maps.google.com/maps?q=${location.lat},${location.lon}`}>
                      {systemData.city}, {systemData.countryName}
                    </a>
                  </div>
                )}
              </div>
            </div>
          </h2>
          <h3>Subscriber details</h3>
          <Table className="definition">
            <tr>
              <td className="four wide">ID</td>
              <td className={NO_LOCALIZE}>{data.id}</td>
            </tr>
            <tr className={rowClass}>
              <td className="four wide">Status</td>
              <td>{humanize(data.status)}</td>
            </tr>
            <tr>
              <td className="four wide">Email</td>
              <td className={NO_LOCALIZE}>{obfuscateEmail(data.email, data.status)}</td>
            </tr>
            <tr>
              <td className="four wide">Created</td>
              <td className={NO_LOCALIZE}>
                {formatDate(data.createdAt, {fromNow: true, showTime: true})}
              </td>
            </tr>
            <tr>
              <td className="four wide">Risk score</td>
              <td className={NO_LOCALIZE}>{data.riskScore}</td>
            </tr>
            <tr>
              <td className="four wide">Subscription origin</td>
              <td className={NO_LOCALIZE}>{humanize(data.subscriptionOrigin || '')}</td>
            </tr>
            {data.unsubscribeReason && (
              <tr>
                <td className="four wide">Unsubscribe reason</td>
                <td className={NO_LOCALIZE}>{data.unsubscribeReason}</td>
              </tr>
            )}
          </Table>
          {hasSystemData && (
            <h3>
              System fields <DotHint id="recipient.fields" />
            </h3>
          )}
          {hasSystemData && (
            <Table className="definition">
              {Object.keys(systemData)
                .sort()
                .map(key => (
                  <tr key={key}>
                    <td className="four wide">{key}</td>
                    <td className={NO_LOCALIZE}>{systemData[key]}</td>
                  </tr>
                ))}
            </Table>
          )}
          <h3>
            Subscriber custom fields <DotHint id="recipient.fields" />
          </h3>
          {!isFormVisible &&
            hasUserData && (
              <Table className="definition">
                {userFields.map(key => (
                  <tr key={key}>
                    <td className="four wide">{key}</td>
                    <td className={NO_LOCALIZE}>{metadata[key]}</td>
                  </tr>
                ))}
              </Table>
            )}
          {isFormVisible && (
            <RecipientForm
              initialValues={metadata}
              isLoading={isUpdating}
              fields={userFields}
              onFormSubmit={onFormSubmit}
              onFormCancel={() => showForm(false)}
            />
          )}
          {!isFormVisible && <Button onClick={() => showForm(true)}>Edit custom fields</Button>}
        </div>
      </div>
    </section>
  );
};

RecipientView.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  recipient: PropTypes.object.isRequired,
  isFormVisible: PropTypes.bool.isRequired,
  showForm: PropTypes.func.isRequired,
  listId: PropTypes.string.isRequired,
  updateRecipient: PropTypes.func.isRequired,
  isUpdating: PropTypes.bool.isRequired
};

export default RecipientView;
