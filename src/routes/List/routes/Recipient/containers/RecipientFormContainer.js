import React, {Component} from 'react';
import {reduxForm} from 'redux-form';
import {removeFromArray, mergeArrays} from 'lib/utils';
import RecipientFormView from '../components/RecipientFormView';
import {getIsFreeUser} from 'modules/profile/selectors';
import {LIST_DEFAULT_METAFIELDS} from 'modules/lists/constants';

const mapStateToProps = state => ({
  isFreeUser: getIsFreeUser(state)
});

const ConnectedRecipientForm = reduxForm({form: 'recipientForm'}, mapStateToProps)(
  RecipientFormView
);

export default class RecipientForm extends Component {
  static defaultProps = {
    protectedFields: mergeArrays(['email'], LIST_DEFAULT_METAFIELDS),
    buttonText: 'Save'
  };

  constructor(props) {
    super(props);
    this.state = {
      fields: mergeArrays(props.fields, LIST_DEFAULT_METAFIELDS),
      isFieldFormVisible: false
    };
  }

  addCustomField = field => {
    this.setState(state => ({
      fields: [...state.fields, field],
      isFieldFormVisible: false
    }));
  };

  removeCustomField = field => {
    this.setState(state => ({
      fields: removeFromArray(state.fields, field)
    }));
  };

  showCustomFieldForm = isFieldFormVisible => {
    this.setState({isFieldFormVisible});
  };

  clearCustomFields = () => {
    this.setState({fields: [], isFieldFormVisible: false});
  };

  render() {
    return (
      <ConnectedRecipientForm
        {...this.props}
        addCustomField={this.addCustomField}
        removeCustomField={this.removeCustomField}
        showCustomFieldForm={this.showCustomFieldForm}
        clearCustomFields={this.clearCustomFields}
        fields={this.state.fields}
        isFieldFormVisible={this.state.isFieldFormVisible}
      />
    );
  }
}
