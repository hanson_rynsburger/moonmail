import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/recipients/actions';
import selectors from 'modules/recipients/selectors';
import RecipientView from '../components/RecipientView';

class Recipient extends Component {
  static propTypes = {
    fetchRecipient: PropTypes.func.isRequired,
    recipientId: PropTypes.string.isRequired,
    listId: PropTypes.string.isRequired
  };

  state = {
    isFormVisible: false
  };

  showForm = isFormVisible => {
    this.setState({isFormVisible});
  };

  componentDidMount() {
    const {fetchRecipient, recipientId, listId} = this.props;
    fetchRecipient(listId, recipientId);
  }

  render() {
    return (
      <RecipientView
        {...this.props}
        isFormVisible={this.state.isFormVisible}
        showForm={this.showForm}
      />
    );
  }
}

const mapStateToProps = (state, props) => ({
  recipient: selectors.getActiveResource(state),
  isFetching: selectors.getIsFetching(state),
  isUpdating: selectors.getIsUpdating(state),
  recipientId: props.params.recipientId,
  listId: props.params.listId
});

export default connect(mapStateToProps, actions)(Recipient);
