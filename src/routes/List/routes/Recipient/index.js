import Recipient from './containers/RecipientContainer';

export default {
  path: 'recipients/:recipientId',
  component: Recipient
};
