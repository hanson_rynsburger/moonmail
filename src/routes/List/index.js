import List from './containers/ListContainer';
import Recipient from './routes/Recipient';
import Recipients from './routes/Recipients';
import AddRecipient from './routes/AddRecipient';
import ConfirmationEmail from './routes/ConfirmationEmail';
import Settings from './routes/Settings';
import Stats from './routes/Stats';
import Import from './routes/Import';
import Exports from './routes/Exports';
import Forms from './routes/Forms';
import EmbeddedForm from './routes/EmbeddedForm';
import CopyPaste from './routes/CopyPaste';

export default store => {
  return {
    path: '/lists/:listId',
    indexRoute: Recipients,
    component: List,
    hideFooter: true,
    childRoutes: [
      AddRecipient,
      ConfirmationEmail(store),
      Settings,
      Stats,
      Import(store),
      Exports,
      Recipient,
      EmbeddedForm,
      Forms,
      CopyPaste
    ]
  };
};
