import React from 'react';
import PropTypes from 'prop-types';
import ListHeader from './ListHeader';
import NoLocalize from 'components/NoLocalize';
import {LIST_STATUS_DESCRIPTION} from 'modules/lists/constants';
import cx from 'classnames';
import Loader from 'components/Loader';
import PopUp from 'components/PopUp';

const ListView = ({list, listId, children, uploadLimit}) => {
  if (!list.id) {
    return (
      <section className="ui basic segment">
        <Loader active dimmer />
      </section>
    );
  }
  const iconClass = cx('icon', {'refresh loading': list.isProcessing});
  return (
    <section className="ui basic segment" style={{height: '100%', marginBottom: 0}}>
      <h1 className="ui header">
        <NoLocalize>{list.name}</NoLocalize>{' '}
        {list.isProcessing && (
          <PopUp
            content={LIST_STATUS_DESCRIPTION[list.status]}
            variation="very wide"
            position="bottom left">
            <span className="ui label teal">
              <i className={iconClass} />
              {list.status}...
            </span>
          </PopUp>
        )}
      </h1>
      <ListHeader id={listId} uploadLimit={uploadLimit} isProcessing={list.isProcessing} />
      {children}
    </section>
  );
};

ListView.propTypes = {
  list: PropTypes.object.isRequired,
  listId: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  uploadLimit: PropTypes.number
};

export default ListView;
