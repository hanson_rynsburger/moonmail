import React from 'react';
import PropTypes from 'prop-types';
import NL from 'components/NoLocalize';
import DropDownMenu from 'components/DropDownMenu';
import {getRelativeItemLink} from 'components/Links';
import {Aux, formatStat} from 'lib/utils';
import {Desktop, Mobile} from 'components/MediaQueries';

const ListHeader = ({id, uploadLimit, isProcessing}) => {
  const Link = getRelativeItemLink(`/lists/${id}/`);
  const limit = <NL>{formatStat(uploadLimit)}</NL>;

  const links = {};
  links.stats = <Link to="stats">Stats</Link>;
  links.index = <Link index>Subscribers</Link>;
  links.add = <Link to="add">Add a subscriber</Link>;
  links.import = <Link to="import">Import subscribers</Link>;
  links.export = <Link to="exports">Export subscribers</Link>;
  links.copyPaste = (
    <Link to="copy-paste">
      Paste {uploadLimit !== Infinity && <Aux>up to {limit}</Aux>} subscribers
    </Link>
  );
  links.zapier = <Link to="/integrations">Add subscribers from anywhere</Link>;
  links.forms = <Link to="forms">Signup forms</Link>;
  links.confirmationEmail = <Link to="confirmation-email">Confirmation email</Link>;
  links.settings = <Link to="settings">Settings</Link>;

  return (
    <div className="ui secondary pointing large menu no-padding">
      <Desktop>
        {links.stats}
        {links.index}
        {!isProcessing && (
          <DropDownMenu className="item">
            Manage subscribers
            <i className="dropdown icon" />
            <div className="menu">
              {links.add}
              {links.import}
              {links.export}
              {links.copyPaste}
              {links.zapier}
            </div>
          </DropDownMenu>
        )}
        {links.forms}
        {links.confirmationEmail}
        {links.settings}
      </Desktop>
      <Mobile>
        {links.stats}
        {links.index}
        <DropDownMenu className="item">
          More
          <i className="dropdown icon" />
          <div className="left menu transition hidden">
            {!isProcessing && (
              <Aux>
                {links.add}
                {links.import}
                {links.export}
                {links.copyPaste}
                {links.zapier}
              </Aux>
            )}
            {links.forms}
            {links.confirmationEmail}
            {links.settings}
          </div>
        </DropDownMenu>
      </Mobile>
    </div>
  );
};

ListHeader.propTypes = {
  id: PropTypes.string,
  uploadLimit: PropTypes.number,
  isProcessing: PropTypes.bool
};

export default ListHeader;
