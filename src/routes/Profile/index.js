import Profile from './containers/ProfileContainer';
import OverviewRoute from './routes/Overview';
import AffiliateRoute from './routes/Affiliate';
import AffiliateConversionsRoute from './routes/AffiliateConversions';
import AffiliatePayoutsRoute from './routes/AffiliatePayouts';
import PlansRoute from './routes/Plan';
import ReputationRoute from './routes/Reputation';
import BillingInfoRoute from './routes/BillingInfo';
import BillingHistoryRoute from './routes/BillingHistory';
import InvoiceRoute from './routes/Invoice';
import SettingsRoute from './routes/Settings';
import DeleteRoute from './routes/DeleteAccount';
import ContactInfoRoute from './routes/ContactInfo';
import ExpertsRoute from './routes/Experts';
import SendersRoute from './routes/Senders';
import SenderRoute from './routes/Sender';
import NewSenderRoute from './routes/NewSender';
import AuthorizationsRoute from './routes/Authorizations';

export default store => ({
  path: '/profile',
  indexRoute: OverviewRoute,
  component: Profile,
  childRoutes: [
    PlansRoute,
    ReputationRoute,
    BillingInfoRoute,
    BillingHistoryRoute(store),
    ContactInfoRoute,
    ExpertsRoute(store),
    SettingsRoute(store),
    DeleteRoute(store),
    NewSenderRoute,
    AuthorizationsRoute(store),
    SendersRoute,
    SenderRoute,
    AffiliateRoute,
    AffiliateConversionsRoute(store),
    AffiliatePayoutsRoute(store),
    InvoiceRoute
  ]
});
