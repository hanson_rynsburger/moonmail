import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import ProfileView from '../components/ProfileView';
import * as selectors from 'modules/profile/selectors';
import listsActions from 'modules/lists/actions';

class ProfileContainer extends Component {
  static propTypes = {
    fetchRecipientsCount: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchRecipientsCount();
  }

  render() {
    return <ProfileView {...this.props} />;
  }
}

const mapStateToProps = state => ({
  profile: selectors.getProfile(state),
  isExpert: selectors.getIsExpert(state),
  isAmazonUser: selectors.getIsAmazonUser(state),
  paymentMethod: selectors.getPaymentMethod(state)
});

export default connect(mapStateToProps, {fetchRecipientsCount: listsActions.fetchRecipientsCount})(
  ProfileContainer
);
