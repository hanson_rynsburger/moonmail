import {reduxForm} from 'redux-form';
import Validator from 'lib/validator';
import dot from 'dot-object';
import {validatePostalCode} from 'lib/postalCodes';
import {findByName} from 'lib/countries';
import ContactInfoView from '../components/ContactInfoView';
import * as actions from 'modules/profile/actions';
import * as selectors from 'modules/profile/selectors';

const rules = {
  address: {
    company: 'required',
    websiteUrl: 'required|url',
    address: 'required',
    city: 'required',
    state: 'required',
    country: 'required'
  }
};

const validate = values => {
  const validator = new Validator(values, rules);
  validator.passes();

  const errors = validator.errors.all();

  const country = findByName(values.address.country);
  const countryCode = country && country.code;
  const zipCodeError = validatePostalCode(countryCode, values.address.zipCode);

  if (zipCodeError) {
    errors.address = errors.address || {};
    errors.address.zipCode = zipCodeError;
  }

  return dot.object(validator.errors.all());
};

const mapStateToProps = state => ({
  initialValues: selectors.getProfile(state),
  isUpdating: selectors.getIsUpdating(state),
  disabled: selectors.getIsPaidUser(state) && selectors.getIsContactInfoProvided(state)
});

export default reduxForm(
  {
    form: 'contactInfo',
    fields: [
      'address.company',
      'address.websiteUrl',
      'address.address',
      'address.address2',
      'address.city',
      'address.state',
      'address.zipCode',
      'address.country',
      'vat'
    ],
    validate
  },
  mapStateToProps,
  actions
)(ContactInfoView);
