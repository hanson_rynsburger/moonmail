import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Select, {SelectItem} from 'components/Select';
import countries from 'lib/countries';
import Button from 'components/Button';
import cx from 'classnames';

const ContactInfoForm = ({
  updateProfile,
  invalid,
  isUpdating,
  handleSubmit,
  disabled,
  fields: {address: {company, websiteUrl, address, address2, city, state, zipCode, country}, vat}
}) => {
  const submit = formProps => {
    updateProfile(formProps);
  };
  return (
    <div className="ui stackable vertically padded grid">
      <div className="eight wide column">
        <form className="ui form" onSubmit={handleSubmit(submit)}>
          <Input readOnly={disabled} label="Company / Organization" {...company} />
          <Input readOnly={disabled} label="VAT id (only needed for EU companies)" {...vat} />
          <Input readOnly={disabled} label="Website URL" {...websiteUrl} />
          <Select disabled={disabled} label="Country" search {...country}>
            {countries.map(country => (
              <SelectItem key={country.code} value={country.name}>
                <i className={cx(country.code.toLowerCase(), 'flag')} />
                {country.name}
              </SelectItem>
            ))}
          </Select>
          <Input readOnly={disabled} label="State / Province / Region" {...state} />
          <Input readOnly={disabled} label="City" {...city} />
          <Input readOnly={disabled} label="Address" {...address} />
          <Input readOnly={disabled} label="Address 2" {...address2} />
          <Input readOnly={disabled} label="Zip / Postal code" {...zipCode} />
          {!disabled && (
            <Button primary type="submit" loading={isUpdating} disabled={invalid}>
              Save
            </Button>
          )}
        </form>
      </div>
    </div>
  );
};

ContactInfoForm.propTypes = {
  fields: PropTypes.shape({
    company: PropTypes.object,
    websiteUrl: PropTypes.object,
    address: PropTypes.object,
    address2: PropTypes.object,
    city: PropTypes.object,
    state: PropTypes.object,
    zipCode: PropTypes.object,
    country: PropTypes.object
  }).isRequired,
  updateProfile: PropTypes.func,
  handleSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isUpdating: PropTypes.bool,
  disabled: PropTypes.bool
};

export default ContactInfoForm;
