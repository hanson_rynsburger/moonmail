import ContactInfo from './containers/ContactInfoContainer';

export default {
  path: 'contact-info',
  component: ContactInfo,
  hideAlerts: true
};
