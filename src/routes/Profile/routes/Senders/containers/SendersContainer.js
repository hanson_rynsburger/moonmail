import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import SendersView from '../components/SendersView';
import selectors from 'modules/senders/selectors';
import actions from 'modules/senders/actions';
import {getIsLimited, getHasSenders} from 'modules/profile/selectors';

class Senders extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
    router: PropTypes.object.isRequired,
    filter: PropTypes.string.isRequired
  };

  handleFilterChange = filter => {
    this.props.router.push({
      pathname: this.props.location.pathname,
      query: {filter}
    });
  };

  render() {
    return <SendersView {...this.props} handleFilterChange={this.handleFilterChange} />;
  }
}

const mapStateToProps = (state, ownProps) => {
  const {filter = 'all'} = ownProps.location.query || {};
  return {
    senders: selectors.getFilteredSenders(state),
    selectedSender: selectors.getSelectedResource(state),
    isDeleting: selectors.getIsDeleting(state),
    isDeleteStarted: selectors.getIsDeleteStarted(state),
    isFetching: selectors.getIsFetchingAll(state),
    isUpToDate: selectors.getIsUpToDate(state),
    isLimited: getIsLimited(state),
    hasSenders: getHasSenders(state),
    filter
  };
};

export default connect(mapStateToProps, actions)(Senders);
