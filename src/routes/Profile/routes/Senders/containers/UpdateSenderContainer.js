import {reduxForm} from 'redux-form';
import actions from 'modules/senders/actions';
import selectors from 'modules/senders/selectors';
import UpdateSender from '../components/UpdateSender';
import {createValidator} from 'lib/validator';

const mapStateToProps = state => {
  const sender = selectors.getSelectedResource(state);
  return {
    isUpdating: selectors.getIsUpdating(state),
    isOpen: selectors.getIsUpdateStarted(state),
    sender,
    initialValues: sender
  };
};

export default reduxForm(
  {
    form: 'updateSender',
    fields: ['fromName'],
    validate: createValidator({
      fromName: 'fromName'
    })
  },
  mapStateToProps,
  actions
)(UpdateSender);
