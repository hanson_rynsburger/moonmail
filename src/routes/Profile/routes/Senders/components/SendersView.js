import React from 'react';
import PropTypes from 'prop-types';
import SenderItem from './SenderItem';
import UpdateSender from '../containers/UpdateSenderContainer';
import Confirm from 'components/Modal/Confirm';
import Link from 'react-router/lib/Link';
import DotHint from 'components/DotHint';
import ResourceList from 'components/ResourceList';
import DropDownMenu from 'components/DropDownMenu';
import {SENDERS_FILTERS} from 'modules/senders/constants';
import {humanize, Aux} from 'lib/utils';
import Button from 'components/Button';
import cx from 'classnames';
import PopUp from 'components/PopUp';

export const SendersView = ({
  senders,
  hasSenders,
  isFetching,
  isLimited,
  verifySender,
  filter = 'all',
  updateSenderStart,
  deleteSenderStart,
  deleteSenderCancel,
  setSenderArchived,
  handleFilterChange,
  deleteSender,
  isDeleteStarted,
  isDeleting,
  selectedSender
}) => {
  return (
    <section className="ui vertical segment">
      <div className="header-with-actions">
        <h2 className="ui header">
          <DotHint id="senders.list.header">Senders</DotHint>
        </h2>
        {hasSenders && (
          <DropDownMenu className={cx('floating labeled icon button', {teal: filter !== 'all'})}>
            <i className="filter icon" />
            <span className="text">{humanize(filter)}</span>
            <div className="menu">
              <div className="scrolling menu">
                {SENDERS_FILTERS.map(f => (
                  <div
                    key={f}
                    className={cx('item', {'active selected': f === filter})}
                    onClick={() => handleFilterChange(f)}>
                    {humanize(f)}
                  </div>
                ))}
              </div>
            </div>
          </DropDownMenu>
        )}
        <PopUp
          on="click"
          showDelay={500}
          closable={false}
          variation="small"
          disabled={hasSenders}
          content={
            <Aux>
              <Link to="/profile/plan">Upgrade</Link> to create custom senders
            </Aux>
          }>
          <Button to="/profile/senders/new" primary disabled={isLimited || !hasSenders}>
            Create Sender
          </Button>
        </PopUp>
      </div>
      <ResourceList isFetching={isFetching} resources={senders} resourceName="senders">
        {(sender, i) => (
          <SenderItem
            sender={sender}
            onVerify={verifySender}
            onEdit={updateSenderStart}
            onDelete={deleteSenderStart}
            hasSenders={hasSenders}
            onArchiveChange={setSenderArchived}
            key={i}
          />
        )}
      </ResourceList>
      <UpdateSender />
      <Confirm
        isOpen={isDeleteStarted}
        isLoading={isDeleting}
        onCancel={deleteSenderCancel}
        onConfirm={() => deleteSender(selectedSender.id)}
        confirmText="Delete sender"
        confirmClass="negative">
        <p>
          This action will completely delete <b>{selectedSender.emailAddress}</b>
        </p>
      </Confirm>
    </section>
  );
};

SendersView.propTypes = {
  senders: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  isLimited: PropTypes.bool.isRequired,
  hasSenders: PropTypes.bool.isRequired,
  isDeleteStarted: PropTypes.bool.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  verifySender: PropTypes.func.isRequired,
  updateSenderStart: PropTypes.func.isRequired,
  deleteSenderStart: PropTypes.func.isRequired,
  deleteSenderCancel: PropTypes.func.isRequired,
  deleteSender: PropTypes.func.isRequired,
  handleFilterChange: PropTypes.func.isRequired,
  setSenderArchived: PropTypes.func.isRequired,
  selectedSender: PropTypes.object,
  filter: PropTypes.string
};

export default SendersView;
