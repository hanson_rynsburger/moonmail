import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router/lib/Link';
import cx from 'classnames';
import classNames from './SenderItem.scss';
import Button from 'components/Button';
import DropDownMenu from 'components/DropDownMenu';
import {NO_LOCALIZE} from 'lib/constants';

const SenderItem = ({sender, onVerify, onEdit, onDelete, onArchiveChange, hasSenders}) => {
  const isDefault = sender.default;
  const isArchived = sender.archived;
  const isVerified = sender.verified;
  const isArchivable = !isArchived && isVerified;
  const iconClass = cx(classNames.icon, 'large middle aligned icon', {
    'green check': isVerified,
    'grey clock outline': !isVerified
  });
  return (
    <div className="item">
      {!isDefault &&
        hasSenders && (
          <div className="right floated content">
            <div className="ui buttons">
              {isVerified ? (
                <a className="ui button" onClick={() => onEdit(sender.id)}>
                  Edit
                </a>
              ) : (
                <Button onClick={() => onVerify(sender.id)}>Resend verification email</Button>
              )}
              <DropDownMenu className="button icon floating">
                <i className="dropdown icon" />
                <div className="menu">
                  {!isVerified && (
                    <a className="item" onClick={() => onEdit(sender.id)}>
                      Edit
                    </a>
                  )}
                  {!isVerified && (
                    <a className="item" onClick={() => onDelete(sender.id)}>
                      Delete
                    </a>
                  )}
                  {isArchivable && (
                    <a className="item" onClick={() => onArchiveChange(sender.id, true)}>
                      Archive
                    </a>
                  )}
                  {isArchived && (
                    <a className="item" onClick={() => onArchiveChange(sender.id, false)}>
                      Unarchive
                    </a>
                  )}
                </div>
              </DropDownMenu>
            </div>
          </div>
        )}
      <i className={iconClass} />
      <div className="content">
        <h3 className={cx('header truncate', {disabled: isArchived})}>
          <Link to={`/profile/senders/${sender.id}`} className={NO_LOCALIZE}>
            {sender.emailAddress}
          </Link>
        </h3>
        <small className={cx('description', NO_LOCALIZE)}>{sender.fromName}</small>
      </div>
    </div>
  );
};
SenderItem.propTypes = {
  sender: PropTypes.object.isRequired,
  onVerify: PropTypes.func.isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onArchiveChange: PropTypes.func.isRequired,
  hasSenders: PropTypes.bool
};

export default SenderItem;
