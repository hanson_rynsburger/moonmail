import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Confirm from 'components/Modal/Confirm';

const UpdateSender = ({
  isOpen,
  isUpdating,
  fields: {fromName},
  handleSubmit,
  updateSenderCancel,
  updateSender,
  sender,
  invalid,
  resetForm
}) => {
  const onSubmit = async formProps => {
    await updateSender(sender.id, formProps);
    resetForm();
  };
  const onCancel = () => {
    updateSenderCancel();
    resetForm();
  };
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isUpdating}
      isDisabled={invalid}
      onCancel={onCancel}
      onConfirm={handleSubmit(onSubmit)}
      headerText="Edit sender"
      confirmText="Save"
      confirmClass="primary">
      <form className="ui form" onSubmit={handleSubmit(onSubmit)}>
        <Input type="text" {...fromName} />
      </form>
    </Confirm>
  );
};

UpdateSender.propTypes = {
  sender: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  updateSender: PropTypes.func.isRequired,
  updateSenderCancel: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isUpdating: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired
};

export default UpdateSender;
