import * as api from 'lib/api';
import * as types from './types';
import {addMessage} from 'modules/messages/actions';
import {updateProfile} from 'modules/profile/actions';
import {getProfile} from 'modules/profile/selectors';

export const createAuthorizationStart = () => ({
  type: types.CREATE_AUTHORIZATION_START
});

export const createAuthorizationCancel = () => ({
  type: types.CREATE_AUTHORIZATION_CANCEL
});

export const createAuthorization = formProps => {
  return async (dispatch, getState) => {
    const state = getState();
    const {email, authorizations = []} = getProfile(state);
    if (email === formProps.email) {
      return dispatch(addMessage({text: 'You can not authorize your own user.'}));
    }
    if (authorizations.findIndex(a => a.email === formProps.email) > 0) {
      return dispatch(addMessage({text: 'This user is already authorized.'}));
    }
    dispatch({
      type: types.CREATE_AUTHORIZATION_REQUEST
    });
    try {
      const data = await api.createAuthorization(formProps);
      dispatch(updateProfile(data));
      dispatch({
        type: types.CREATE_AUTHORIZATION_SUCCESS
      });
    } catch (error) {
      dispatch({
        type: types.CREATE_AUTHORIZATION_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const deleteAuthorizationStart = userId => ({
  type: types.DELETE_AUTHORIZATION_START,
  userId
});

export const deleteAuthorizationCancel = () => ({
  type: types.DELETE_AUTHORIZATION_CANCEL
});

export const deleteAuthorization = userId => {
  return async dispatch => {
    dispatch({
      type: types.DELETE_AUTHORIZATION_REQUEST
    });
    try {
      const data = await api.deleteAuthorization(userId);
      dispatch(updateProfile(data));
      dispatch({
        type: types.DELETE_AUTHORIZATION_SUCCESS
      });
    } catch (error) {
      dispatch({
        type: types.DELETE_AUTHORIZATION_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};
