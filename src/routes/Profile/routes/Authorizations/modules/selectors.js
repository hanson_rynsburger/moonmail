import {createSelector} from 'reselect';
import {stateKey} from './reducer';

const authorizationsSelector = state => state[stateKey];

export const getActiveUserId = createSelector(
  authorizationsSelector,
  authorizations => authorizations.activeUserId
);

export const getIsDeleting = createSelector(
  authorizationsSelector,
  authorizations => authorizations.isDeleting
);

export const getIsCreating = createSelector(
  authorizationsSelector,
  authorizations => authorizations.isCreating
);

export const getIsCreateModalOpen = createSelector(
  authorizationsSelector,
  authorizations => authorizations.isCreateModalOpen
);

export const getIsDeleteModalOpen = createSelector(
  authorizationsSelector,
  authorizations => authorizations.isDeleteModalOpen
);
