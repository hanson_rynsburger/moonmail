import * as types from './types';
import {combineReducers} from 'redux';

export const stateKey = 'authorizations';

const activeUserId = (state = null, action) => {
  switch (action.type) {
    case types.DELETE_AUTHORIZATION_START:
      return action.userId;
    case types.DELETE_AUTHORIZATION_SUCCESS:
    case types.DELETE_AUTHORIZATION_CANCEL:
      return null;
    default:
      return state;
  }
};

const isDeleting = (state = false, action) => {
  switch (action.type) {
    case types.DELETE_AUTHORIZATION_REQUEST:
      return true;
    case types.DELETE_AUTHORIZATION_SUCCESS:
    case types.DELETE_AUTHORIZATION_FAIL:
      return false;
    default:
      return state;
  }
};

const isCreating = (state = false, action) => {
  switch (action.type) {
    case types.CREATE_AUTHORIZATION_REQUEST:
      return true;
    case types.CREATE_AUTHORIZATION_SUCCESS:
    case types.CREATE_AUTHORIZATION_FAIL:
      return false;
    default:
      return state;
  }
};

const isDeleteModalOpen = (state = false, action) => {
  switch (action.type) {
    case types.DELETE_AUTHORIZATION_START:
      return true;
    case types.DELETE_AUTHORIZATION_CANCEL:
    case types.DELETE_AUTHORIZATION_SUCCESS:
      return false;
    default:
      return state;
  }
};

const isCreateModalOpen = (state = false, action) => {
  switch (action.type) {
    case types.CREATE_AUTHORIZATION_START:
      return true;
    case types.CREATE_AUTHORIZATION_CANCEL:
    case types.CREATE_AUTHORIZATION_SUCCESS:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  isDeleting,
  isDeleteModalOpen,
  isCreateModalOpen,
  isCreating,
  activeUserId
});
