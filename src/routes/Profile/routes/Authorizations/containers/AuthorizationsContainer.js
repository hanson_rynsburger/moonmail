import {connect} from 'react-redux';
import AuthorizationsView from '../components/AuthorizationsView';
import * as selectors from 'modules/profile/selectors';
import * as actions from '../modules/actions';

const mapStateToProps = state => ({
  authorizations: selectors.getProfile(state).authorizations,
  isFreeUser: selectors.getIsFreeUser(state)
});

export default connect(mapStateToProps, actions)(AuthorizationsView);
