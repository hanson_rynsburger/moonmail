import {reduxForm} from 'redux-form';
import * as actions from '../modules/actions';
import * as selectors from '../modules/selectors';
import {createValidator} from 'lib/validator';
import CreateAuthorization from '../components/CreateAuthorization';

const mapStateToProps = state => ({
  isCreating: selectors.getIsCreating(state),
  isOpen: selectors.getIsCreateModalOpen(state)
});

export default reduxForm(
  {
    form: 'createAuthorization',
    fields: ['email'],
    validate: createValidator({
      email: 'required|email'
    })
  },
  mapStateToProps,
  actions
)(CreateAuthorization);
