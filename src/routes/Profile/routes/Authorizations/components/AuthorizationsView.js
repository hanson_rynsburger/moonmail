import PropTypes from 'prop-types';
import React from 'react';
import {ResourceTable} from 'components/Table';
import UserRow, {NUM_COLUMNS} from './UserRow';
import Button from 'components/Button';
import ConfirmDelete from '../containers/ConfirmDeleteContainer';
import CreateAuthorization from '../containers/CreateAuthorizationContainer';
import PopUp from 'components/PopUp';
import DotHint from 'components/DotHint';
import {Aux} from 'lib/utils';
import Link from 'react-router/lib/Link';

const AuthorizationsView = ({
  authorizations = [],
  deleteAuthorizationStart,
  createAuthorizationStart,
  isFreeUser
}) => (
  <section className="ui vertical segment">
    <h3>List of all users that have access to your account</h3>
    <div className="ui stackable grid">
      <div className="nine wide column">
        <ResourceTable numColumns={NUM_COLUMNS} resourceName="users">
          {authorizations.map((user, i) => (
            <UserRow key={i} user={user} onDelete={() => deleteAuthorizationStart(user.userId)} />
          ))}
        </ResourceTable>
        <PopUp
          on="click"
          showDelay={500}
          closable={false}
          variation="small"
          disabled={!isFreeUser}
          content={
            <Aux>
              <Link to="/profile/plan">Upgrade</Link> to add users
            </Aux>
          }>
          <Button onClick={createAuthorizationStart} disabled={isFreeUser} primary>
            Add user
          </Button>
        </PopUp>
        <DotHint id="profile.authorizations.add_user" />
      </div>
    </div>
    <ConfirmDelete />
    <CreateAuthorization />
  </section>
);

AuthorizationsView.propTypes = {
  authorizations: PropTypes.array,
  deleteAuthorizationStart: PropTypes.func.isRequired,
  createAuthorizationStart: PropTypes.func.isRequired,
  isFreeUser: PropTypes.bool.isRequired
};

export default AuthorizationsView;
