import PropTypes from 'prop-types';
import React from 'react';
import Input from 'components/Input';
import Confirm from 'components/Modal/Confirm';

const CreateAuthorization = ({
  isOpen,
  isCreating,
  fields: {email},
  handleSubmit,
  invalid,
  resetForm,
  createAuthorization,
  createAuthorizationCancel
}) => {
  const onSubmit = formProps => {
    createAuthorization(formProps);
  };
  const onCancel = () => {
    createAuthorizationCancel();
    resetForm();
  };
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isCreating}
      isDisabled={invalid}
      onCancel={onCancel}
      onConfirm={handleSubmit(onSubmit)}
      headerText="Authorize user to access your account"
      confirmText="Add user"
      confirmClass="primary">
      <form className="ui form" onSubmit={handleSubmit(onSubmit)}>
        <Input type="text" label="User email" {...email} />
      </form>
    </Confirm>
  );
};

CreateAuthorization.propTypes = {
  fields: PropTypes.shape({
    email: PropTypes.object.isRequired
  }),
  createAuthorization: PropTypes.func.isRequired,
  createAuthorizationCancel: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  isCreating: PropTypes.bool.isRequired
};

export default CreateAuthorization;
