import PropTypes from 'prop-types';
import React from 'react';
import Button from 'components/Button';
import {NO_LOCALIZE} from 'lib/constants';

const userPic = 'https://static.moonmail.io/user.png';
const UserRow = ({user, onDelete}) => (
  <tr>
    <td title={user.name || user.email} className={NO_LOCALIZE}>
      <h4 className="ui header">
        <img
          className="ui avatar image"
          src={user.picture || userPic}
          onError={e => (e.target.src = userPic)}
        />
        <div className="content">
          {user.name || user.email}
          {user.name !== user.email && <div className="sub header">{user.email}</div>}
        </div>
      </h4>
    </td>
    <td className="right aligned">
      <Button onClick={onDelete}>Revoke access</Button>
    </td>
  </tr>
);

UserRow.propTypes = {
  user: PropTypes.object,
  onDelete: PropTypes.func.isRequired
};

export const NUM_COLUMNS = 2;
export default UserRow;
