import PropTypes from 'prop-types';
import React from 'react';
import Confirm from 'components/Modal/Confirm';

const ConfirmDelete = ({
  isOpen,
  isDeleting,
  userId,
  deleteAuthorizationCancel,
  deleteAuthorization
}) => (
  <Confirm
    isOpen={isOpen}
    isLoading={isDeleting}
    onCancel={deleteAuthorizationCancel}
    onConfirm={() => deleteAuthorization(userId)}
    confirmText="Revoke access"
    confirmClass="negative">
    <p>This action will remove user access to your account</p>
  </Confirm>
);

ConfirmDelete.propTypes = {
  userId: PropTypes.string,
  deleteAuthorization: PropTypes.func.isRequired,
  deleteAuthorizationCancel: PropTypes.func.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired
};

export default ConfirmDelete;
