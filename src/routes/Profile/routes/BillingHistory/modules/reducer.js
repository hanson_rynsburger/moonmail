import * as types from './types';
import {combineReducers} from 'redux';

export const stateKey = 'charges';

const data = (state = [], action) => {
  switch (action.type) {
    case types.FETCH_CHARGES_SUCCESS:
      return action.data;
    default:
      return state;
  }
};

const pages = (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_CHARGES_SUCCESS:
      return action.pages;
    default:
      return state;
  }
};

const isFetching = (state = false, action) => {
  switch (action.type) {
    case types.FETCH_CHARGES_REQUEST:
      return true;
    case types.FETCH_CHARGES_SUCCESS:
    case types.FETCH_CHARGES_FAIL:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  data,
  isFetching,
  pages
});
