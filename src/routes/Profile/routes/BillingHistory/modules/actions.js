import * as api from 'lib/api';
import * as types from './types';
import {addMessage} from 'modules/messages/actions';

export const fetchCharges = (page, scanForward = true) => {
  return async dispatch => {
    const params = {};
    if (page) {
      if (scanForward) {
        params.startingAfter = page;
      } else {
        params.endingBefore = page;
      }
    }
    dispatch({
      type: types.FETCH_CHARGES_REQUEST
    });
    try {
      const charges = await api.fetchCharges(params);
      const pages = {};
      if (scanForward) {
        if (charges.has_more) pages.nextPage = charges.data.slice(-1)[0].id;
        if (page) pages.prevPage = charges.data[0].id;
      } else {
        if (charges.has_more) pages.prevPage = charges.data[0].id;
        if (page) pages.nextPage = charges.data.slice(-1)[0].id;
      }
      dispatch({
        type: types.FETCH_CHARGES_SUCCESS,
        data: charges.data,
        pages
      });
    } catch (error) {
      dispatch({
        type: types.FETCH_CHARGES_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};
