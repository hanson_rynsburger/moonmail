import {createSelector} from 'reselect';
import {stateKey} from './reducer';

const chargesSelector = state => state[stateKey];

export const getIsFetching = createSelector(chargesSelector, charges => charges.isFetching);

export const getPages = createSelector(chargesSelector, charges => charges.pages);

export const getCharges = createSelector(chargesSelector, charges => charges.data);
