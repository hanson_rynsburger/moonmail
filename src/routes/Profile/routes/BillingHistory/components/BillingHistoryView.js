import React from 'react';
import PropTypes from 'prop-types';
import ChargesTableRow, {NUM_COLUMNS} from './ChargesTableRow';
import {PaginatedTable} from 'components/Table';

export const BillingHistoryView = ({charges, isFetching, getPage, pages}) => (
  <section className="ui vertical segment">
    <PaginatedTable
      isFetching={isFetching}
      numColumns={NUM_COLUMNS}
      onNextPage={() => getPage(pages.nextPage)}
      onPrevPage={() => getPage(pages.prevPage, false)}
      isFirstPage={!pages.prevPage}
      isLastPage={!pages.nextPage}
      className="large striped"
      notFoundText="You don't have any billing history yet."
      header={<ChargesTableRow isHeader />}>
      {charges.map(charge => <ChargesTableRow charge={charge} key={charge.created} />)}
    </PaginatedTable>
    <div className="text grey">
      powered by{' '}
      <a href="https://monei.net" target="_blank" rel="noopener noreferrer">
        MONEI
      </a>
    </div>
  </section>
);

BillingHistoryView.propTypes = {
  charges: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  pages: PropTypes.object.isRequired,
  getPage: PropTypes.func.isRequired
};

export default BillingHistoryView;
