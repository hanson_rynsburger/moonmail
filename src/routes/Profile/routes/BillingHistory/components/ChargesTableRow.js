import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from 'lib/utils';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';
import NoLocalize from 'components/NoLocalize';
import Link from 'react-router/lib/Link';

const Label = ({children}) => <span className="ui left pointing small label">{children}</span>;

const ChargesTableRow = ({charge, isHeader = false}) => {
  if (isHeader) {
    return (
      <tr>
        <th>Amount</th>
        <th>Description</th>
        <th width={150}>Date</th>
        <th width={50} className="center aligned">
          Status
        </th>
        <th width={50} className="center aligned">
          Invoice
        </th>
      </tr>
    );
  }
  const rowClass = cx({
    negative: charge.status === 'failed',
    warning: charge.refunded
  });
  const iconClass = cx('icon', {
    'green checkmark': charge.status === 'succeeded',
    'teal clock outline': charge.status === 'pending',
    'red ban': charge.status === 'failed'
  });
  return (
    <tr className={rowClass}>
      <td style={{whiteSpace: 'nowrap'}}>
        <NoLocalize>
          <b>
            {charge.amount / 100} {charge.currency.toUpperCase()}
          </b>
        </NoLocalize>{' '}
        {charge.refunded && <Label>Refunded</Label>}
        {!charge.refunded && charge.refunds.total_count > 0 && <Label>Partially refunded</Label>}
        {!charge.paid && <Label>Insufficient Funds</Label>}
      </td>
      <td className={NO_LOCALIZE}>{charge.statement_descriptor || charge.description}</td>
      <td className={NO_LOCALIZE}>{formatDate(charge.created, {showTime: true})}</td>
      <td className={cx(NO_LOCALIZE, 'center aligned')}>
        <i className={iconClass} />
      </td>
      <td className={cx(NO_LOCALIZE, 'center aligned')}>
        {charge.status === 'succeeded' && (
          <Link to={`/profile/invoice/${charge.id}`} className="icon">
            <i className={'download icon'} />
          </Link>
        )}
      </td>
    </tr>
  );
};

ChargesTableRow.propTypes = {
  charge: PropTypes.object,
  isHeader: PropTypes.bool
};

export const NUM_COLUMNS = 5;

export default ChargesTableRow;
