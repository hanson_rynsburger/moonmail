import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import BillingHistoryView from '../components/BillingHistoryView';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';

class BillingHistory extends Component {
  static propTypes = {
    fetchCharges: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchCharges();
  }

  getPage = (page, scanForward = true) => {
    this.props.fetchCharges(page, scanForward);
  };

  render() {
    return <BillingHistoryView {...this.props} getPage={this.getPage} />;
  }
}

const mapStateToProps = state => ({
  charges: selectors.getCharges(state),
  pages: selectors.getPages(state),
  isFetching: selectors.getIsFetching(state)
});

export default connect(mapStateToProps, actions)(BillingHistory);
