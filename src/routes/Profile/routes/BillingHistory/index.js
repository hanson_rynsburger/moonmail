import {injectReducer} from 'store/reducers';
import BillingHistory from './containers/BillingHistoryContainer';
import reducer, {stateKey} from './modules/reducer';

export default store => {
  injectReducer(store, {key: stateKey, reducer});
  return {
    path: 'billing-history',
    component: BillingHistory
  };
};
