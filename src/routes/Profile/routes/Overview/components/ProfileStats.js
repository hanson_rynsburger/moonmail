import React from 'react';
import PropTypes from 'prop-types';
import {formatStat, formatPercents} from 'lib/utils';
import NL from 'components/NoLocalize';

const ProfileStats = ({
  totalRecipients,
  sentEmails,
  sentCampaigns,
  bounceRate,
  complaintsRate,
  totalComplaints,
  sendingQuota
}) => (
  <div className="ui relaxed large list">
    {sendingQuota && (
      <div className="item">
        Sending quota: <NL el="b">{sendingQuota} emails / day</NL>
      </div>
    )}
    <div className="item">
      Total subscribers: <NL el="b">{formatStat(totalRecipients)}</NL>
    </div>
    <div className="item">
      Sent campaigns: <NL el="b">{formatStat(sentCampaigns)}</NL>
    </div>
    <div className="item">
      Sent emails: <NL el="b">{formatStat(sentEmails)}</NL>
    </div>
    <div className="item">
      Bounce rate: <NL el="b">{formatPercents(bounceRate, 3)}%</NL>
    </div>
    <div className="item">
      Complaints rate: <NL el="b">{formatPercents(complaintsRate, 3)}%</NL>
    </div>
    <div className="item">
      Total complaints: <NL el="b">{formatStat(totalComplaints)}</NL>
    </div>
  </div>
);

ProfileStats.propTypes = {
  totalRecipients: PropTypes.number,
  sentEmails: PropTypes.number,
  sentCampaigns: PropTypes.number,
  bounceRate: PropTypes.number,
  complaintsRate: PropTypes.number,
  totalComplaints: PropTypes.number,
  sendingQuota: PropTypes.number
};

export default ProfileStats;
