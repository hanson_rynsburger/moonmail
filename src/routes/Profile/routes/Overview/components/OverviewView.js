import React from 'react';
import PropTypes from 'prop-types';
import {formatDate, formatPercents} from 'lib/utils';
import Link from 'react-router/lib/Link';
import FreePlanDesc from '../../Plan/components/FreePlanDesc';
import PaidPlanDesc from '../../Plan/components/PaidPlanDesc';
import NL from 'components/NoLocalize';
import cx from 'classnames';
import ProfileStats from './ProfileStats';

import {NO_LOCALIZE} from 'lib/constants';

const userPic = 'https://static.moonmail.io/user.png';

export const OverviewView = ({
  profile,
  plan,
  isSesUser,
  isExpert,
  isPaidUser,
  reputationData,
  totalRecipients
}) => {
  const reputation = formatPercents(reputationData.reputation);
  const reputationClass = cx('text', {
    green: reputation > 50,
    yellow: reputation <= 50 && reputation > 20,
    red: reputation <= 20
  });
  return (
    <section className="ui vertical segment">
      <div className="ui stackable grid">
        <div className="five wide column">
          <div className="ui card">
            <div className="image">
              {isExpert && (
                <Link to="/profile/experts" className="ui teal large ribbon label">
                  MoonMail Expert
                </Link>
              )}
              <img src={profile.picture} onError={e => (e.target.src = userPic)} />
            </div>
            <div className="content">
              <div className={cx('header', NO_LOCALIZE)}>{profile.name}</div>
              <div className="meta">
                <div className={NO_LOCALIZE}>{profile.email}</div>
                <div>
                  Joined
                  <NL> {formatDate(profile.created_at, {fromNow: true, unix: false})}</NL>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="five wide column">
          <h2 className="ui header">
            <div className="content">
              {profile.plan && <span>{plan.title} plan</span>}
              <div className="sub header">
                <Link to="/profile/plan">Change your plan</Link>
              </div>
            </div>
          </h2>
          {!isPaidUser && <FreePlanDesc isSesUser={isSesUser} limits={plan.limits} />}
          {isPaidUser && <PaidPlanDesc isSesUser={isSesUser} limits={plan.limits} />}
        </div>
        <div className="five wide column">
          <h2 className="ui header">
            <div className="content">
              Your reputation: <span className={reputationClass}>{reputation}%</span>
              <div className="sub header">
                <Link to="/profile/reputation">Learn more about the reputation system</Link>
              </div>
            </div>
          </h2>
          <ProfileStats
            {...reputationData}
            totalRecipients={totalRecipients}
            sendingQuota={profile.sendingQuota}
          />
        </div>
      </div>
    </section>
  );
};

OverviewView.propTypes = {
  profile: PropTypes.shape({
    name: PropTypes.string,
    plan: PropTypes.string
  }).isRequired,
  plan: PropTypes.object,
  isSesUser: PropTypes.bool,
  isPaidUser: PropTypes.bool,
  isExpert: PropTypes.bool,
  totalRecipients: PropTypes.number,
  reputationData: PropTypes.object
};

export default OverviewView;
