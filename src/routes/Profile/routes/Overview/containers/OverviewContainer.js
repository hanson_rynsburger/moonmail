import {connect} from 'react-redux';
import OverviewView from '../components/OverviewView';
import * as selectors from 'modules/profile/selectors';
import listsSelectors from 'modules/lists/selectors';

const mapStateToProps = state => ({
  profile: selectors.getProfile(state),
  isSesUser: selectors.getIsSesUser(state),
  isExpert: selectors.getIsExpert(state),
  isPaidUser: selectors.getIsPaidUser(state),
  plan: selectors.getPlan(state),
  reputationData: selectors.getReputationData(state),
  totalRecipients: listsSelectors.getTotalRecipients(state)
});

export default connect(mapStateToProps)(OverviewView);
