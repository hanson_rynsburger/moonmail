import {connect} from 'react-redux';
import ReputationView from '../components/ReputationView';
import * as selectors from 'modules/profile/selectors';

const mapStateToProps = state => ({
  profile: selectors.getProfile(state)
});

export default connect(mapStateToProps)(ReputationView);
