import Reputation from './containers/ReputationContainer';

// Sync route definition
export default {
  path: 'reputation',
  component: Reputation
};
