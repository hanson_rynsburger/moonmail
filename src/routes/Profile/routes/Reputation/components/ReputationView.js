import React from 'react';
import PropTypes from 'prop-types';
import Progress from 'components/Progress';
import path from 'object-path';
import {formatPercents} from 'lib/utils';

export const ReputationView = ({profile}) => {
  const reputation = formatPercents(path.get(profile, 'reputationData.reputation', 0));
  return (
    <section className="ui vertical segment">
      <h3>Your reputation is {reputation} out of 100</h3>
      <Progress percent={reputation} indicating />
      <div className="ui padded segment">
        <h3 className="ui header">What is this about?</h3>
        <p>
          As an email marketing provider, our best efforts are driven to ensure the highest
          deliverability of all email campaigns sent through MoonMail. To do so, we enforce our
          users to follow the best email marketing practices, specially when it comes to sending
          good content and only to people who want to receive it.
        </p>
        <p>
          This reputation system represents your performance, taking into account multiple factors
          such as bounce rate, number of complaints and Artificial Intelligence analysis to predict
          future behavior. The closer you are to 100%, the better you are performing, and we'd like
          to encourage you to keep your reputation percentage as high as possible, as this will have
          an impact on how fast we send your campaigns. If your reputation is lower than 15%, your
          account will be suspended and you won't be allowed to send more emails.
        </p>
        <p>
          Be aware that we constantly check this value, which means that we could stop the sending
          process of an email campaign before it was completed if we detect it's not performing
          well.
        </p>
        <p>
          <b>
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="http://support.moonmail.io/faq/validation-and-reputation/how-is-my-moonmail-reputation-calculated">
              Click here
            </a>
          </b>{' '}
          to better understand how your reputation is calculated
        </p>
      </div>
    </section>
  );
};

ReputationView.propTypes = {
  profile: PropTypes.shape({
    name: PropTypes.string,
    plan: PropTypes.string
  }).isRequired
};

export default ReputationView;
