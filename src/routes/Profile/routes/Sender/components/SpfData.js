import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';

const SpfData = ({checkStatus, isChecking, id}) => {
  return (
    <div>
      <h4>Adding a New SPF Record</h4>
      <p>
        If your custom MAIL FROM domain does not have an existing SPF record, publish a TXT record
        with the following value. The name of the record can be blank or @, depending on your DNS
        service.
      </p>
      <pre>"v=spf1 include:amazonses.com -all"</pre>
      <p>
        <b>Important</b>: If you use "-all" as shown in the example, ISPs might block email from IP
        addresses that are not listed in your SPF record. Your SPF record must therefore include
        every IP address that you use to send email. As a debugging aid, you can use "~all" instead.
        When you use "~all", ISPs will typically accept email from IP addresses that are not listed
        in the SPF record, but they might flag it. To maximize deliverability, use "-all" and add a
        record for each IP address. For examples of how to authorize multiple IP addresses, go to{' '}
        <a
          href="http://www.openspf.org/SPF_Record_Syntax"
          target="_blank"
          rel="noopener noreferrer">
          http://www.openspf.org/SPF_Record_Syntax
        </a>.
      </p>
      <h4>Adding to an Existing SPF Record</h4>
      <p>
        If your domain already has an SPF record, then you must add the following SPF mechanism to
        the existing record.
      </p>
      <pre>include:amazonses.com</pre>
      <p>
        <Button loading={isChecking} onClick={() => checkStatus(id)}>
          Check status
        </Button>
      </p>
    </div>
  );
};

SpfData.propTypes = {
  isChecking: PropTypes.bool,
  checkStatus: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired
};

export default SpfData;
