/* eslint-disable max-len */

import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import Loader from 'components/Loader';
import DomainData from './DomainData';
import DkimData from './DkimData';
import DmarcData from './DmarcData';
import SpfData from './SpfData';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';
import NL from 'components/NoLocalize';
import {contactSupport} from 'lib/remoteUtils';

const errorRegExp = new RegExp(/(error|fail)/i);

export const SenderView = ({
  sender,
  verifySender,
  updateSenderStatus,
  isVerifying,
  isUpdatingStatus,
  isVerifyingDomain,
  isGeneratingDkim,
  verifySenderDomain,
  generateSenderDkim,
  isActivatingDkim,
  isFetching,
  activateSenderDkim,
  checkSpfStatus,
  checkDomainStatus,
  checkDmarcStatus,
  checkDkimStatus
}) => {
  if (!sender.id) {
    return (
      <section className="ui vertical segment">
        <Loader active inline />
      </section>
    );
  }
  const {
    verified,
    domainVerificationToken,
    domainVerificationStatus,
    domainVerified,
    dkimTokens = [],
    dkimVerificationStatus,
    dkimEnabled,
    dmarcEnabled,
    dkimVerified,
    spfEnabled,
    emailAddress
  } = sender;

  const domainVerificationError = errorRegExp.test(domainVerificationStatus);
  const dkimVerificationError = errorRegExp.test(dkimVerificationStatus);
  const dkimReady = dkimVerified && dkimEnabled;

  const getDomainStatus = () => {
    if (domainVerified) return 'Verified';
    if (domainVerificationError) return 'DNS record is not detected.';
    if (domainVerificationToken) return 'Pending';
    return 'Unverified';
  };

  const getDkimStatus = () => {
    if (dkimTokens.length > 0) {
      if (dkimReady) return 'Enabled';
      if (dkimVerificationError) return 'DNS records are not detected.';
      return 'Pending';
    }
    return 'Disabled';
  };

  const getDomainIcon = () => {
    if (domainVerified) return 'icon green check';
    if (domainVerificationError) return 'icon red warning circle';
    return 'icon grey clock outline';
  };

  const getDkimIcon = () => {
    if (dkimTokens.length > 0) {
      if (dkimReady) return 'icon green check';
      if (dkimVerificationError) return 'icon red warning circle';
    }
    return 'icon grey clock outline';
  };

  const emailIcon = cx('icon', {
    'grey clock outline': !verified,
    'green check': verified
  });
  const spfIcon = cx('icon', {
    'grey clock outline': !spfEnabled,
    'green check': spfEnabled
  });
  const domainIcon = getDomainIcon();
  const dkimIcon = getDkimIcon();

  const dmarcIcon = cx('icon', {
    'grey clock outline': !dmarcEnabled,
    'green check': dmarcEnabled
  });

  const supportMessage = (
    <div className="ui message">
      <b>
        Not sure how to do this?{' '}
        <a href="#" onClick={contactSupport}>
          Contact our support
        </a>{' '}
        and we will gladly help!
      </b>
    </div>
  );

  return (
    <section className="ui vertical segment">
      <div className="ui stackable grid">
        <div className="fourteen wide column">
          <h2 className={cx('ui header', NO_LOCALIZE)}>
            {sender.fromName} {'<'}
            <a href={`mailto:${emailAddress}`}>{emailAddress}</a>
            {'>'}
          </h2>
          <section className="ui segment">
            <h3 className="ui header">
              <i className={emailIcon} />
              <div className="content">
                Email address verification
                <div className="sub header">
                  Status: <b>{verified ? 'Verified' : 'Pending'}</b>
                </div>
              </div>
            </h3>

            {verified ? (
              <p>
                You can now start sending campaigns from this email address. We recommend you to
                verify your domain and activate DKIM for it. <br />
                This will keep your campaigns out of spam folders and protect your reputation.
              </p>
            ) : (
              <p>
                Check your Inbox. Amazon sent an email to: <NL el="b">{emailAddress}</NL>. You need
                to click on the link provided by Amazon inside the body of that email. When clicking
                on that URL you authorize Amazon and MoonMail to send emails on behalf of
                <NL el="b"> {emailAddress}</NL>. <br />
                It's also a security check so no one can send emails from someone else's email
                address.
              </p>
            )}
            {!verified && (
              <div>
                <Button
                  primary
                  loading={isUpdatingStatus}
                  onClick={() => updateSenderStatus(sender.id)}>
                  Check status
                </Button>
                <Button loading={isVerifying} onClick={() => verifySender(sender.id)}>
                  Resend verification email
                </Button>
              </div>
            )}
          </section>
          {verified && (
            <div>
              <section className="ui segment">
                <h3 className="ui header">
                  {domainVerificationToken && <i className={domainIcon} />}
                  <div className="content">
                    Domain verification
                    <div className="sub header">
                      Status: <b>{getDomainStatus()}</b>
                    </div>
                  </div>
                </h3>
                {domainVerificationError && supportMessage}
                <p>
                  When you verify an entire domain, you are verifying all email addresses from that
                  domain, so you don't need to verify email addresses from that domain individually.
                  For example, if you verify the domain <i>example.com</i>, you can send email from{' '}
                  <i>user1@example.com</i>, <i>user2@example.com</i>, or any other user at{' '}
                  <i>example.com</i>.
                </p>
                {!domainVerified &&
                  (domainVerificationToken ? (
                    <DomainData
                      {...sender}
                      checkStatus={checkDomainStatus}
                      isChecking={isFetching}
                    />
                  ) : (
                    <p>
                      <Button
                        loading={isVerifyingDomain}
                        onClick={() => verifySenderDomain(sender.id)}>
                        Verify domain name
                      </Button>
                    </p>
                  ))}
              </section>
              <section className="ui segment">
                <h3 className="ui header">
                  <i className={spfIcon} />
                  <div className="content">
                    Sender Policy Framework (SPF)
                    <div className="sub header">
                      Status: <b>{spfEnabled ? 'Enabled' : 'Disabled'}</b>
                    </div>
                  </div>
                </h3>
                <p>
                  Sender Policy Framework (SPF) record indicates to ISPs that you have authorized
                  MoonMail to send email for your domain. When you use MoonMail, your decision about
                  whether to publish an SPF record depends on whether you only require your email to
                  pass an SPF check by the receiving mail server, or if you want your email to
                  comply with the additional requirements needed to pass Domain-based Message
                  Authentication, Reporting and Conformance (DMARC) authentication based on SPF. You
                  can use DKIM to achieve DMARC validation, but it is a best practice to use both
                  DKIM and SPF for maximum deliverability.{' '}
                  <a
                    href="http://support.moonmail.io/spf-dkim-setup-guides/about-txt-spf-dkim-records/authenticating-email-with-spf-in-moonmail"
                    target="_blank"
                    rel="noopener noreferrer">
                    Learn more about SPF.
                  </a>
                </p>
                {!spfEnabled && (
                  <SpfData {...sender} checkStatus={checkSpfStatus} isChecking={isFetching} />
                )}
              </section>
              <section className="ui segment">
                <h3 className="ui header">
                  {dkimTokens.length > 0 && <i className={dkimIcon} />}
                  <div className="content">
                    DomainKeys Identified Mail (DKIM)
                    <div className="sub header">
                      Status: <b>{getDkimStatus()}</b>
                    </div>
                  </div>
                </h3>
                {dkimVerificationError && supportMessage}
                <p>
                  DKIM provides proof that the email you send originates from your domain and is
                  authentic. DKIM signatures are stored in your domain's DNS system.{' '}
                  <a
                    href="http://support.moonmail.io/spf-dkim-setup-guides/about-txt-spf-dkim-records/authenticating-email-with-dkim-in-moonmail"
                    target="_blank"
                    rel="noopener noreferrer">
                    Learn more about DKIM
                  </a>.
                </p>
                {!dkimReady &&
                  (dkimTokens.length > 0 ? (
                    <DkimData
                      {...sender}
                      activate={activateSenderDkim}
                      checkStatus={checkDkimStatus}
                      isChecking={isFetching}
                      isActivating={isActivatingDkim}
                    />
                  ) : (
                    <p>
                      <Button
                        loading={isGeneratingDkim}
                        onClick={() => generateSenderDkim(sender.id)}>
                        Generate DKIM Settings
                      </Button>
                    </p>
                  ))}
              </section>
              <section className="ui segment">
                <h3 className="ui header">
                  <i className={dmarcIcon} />
                  <div className="content">
                    Domain-based Message Authentication, Reporting and Conformance (DMARC)
                    <div className="sub header">
                      Status: <b>{dmarcEnabled ? 'Enabled' : 'Disabled'}</b>
                    </div>
                  </div>
                </h3>
                <p>
                  DMARC is an email authentication, policy, and reporting protocol. It builds on the
                  widely deployed SPF and DKIM protocols, adding linkage to the author (“From:”)
                  domain name, published policies for recipient handling of authentication failures,
                  and reporting from receivers to senders, to improve and monitor protection of the
                  domain from fraudulent email.
                </p>
                {!dmarcEnabled && (
                  <DmarcData {...sender} checkStatus={checkDmarcStatus} isChecking={isFetching} />
                )}
              </section>
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

SenderView.propTypes = {
  sender: PropTypes.shape({
    emailAddress: PropTypes.string
  }).isRequired,
  verifySender: PropTypes.func.isRequired,
  updateSenderStatus: PropTypes.func.isRequired,
  isVerifying: PropTypes.bool.isRequired,
  isUpdatingStatus: PropTypes.bool.isRequired,
  isVerifyingDomain: PropTypes.bool.isRequired,
  isGeneratingDkim: PropTypes.bool.isRequired,
  verifySenderDomain: PropTypes.func.isRequired,
  generateSenderDkim: PropTypes.func.isRequired,
  isActivatingDkim: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  activateSenderDkim: PropTypes.func.isRequired,
  checkSpfStatus: PropTypes.func.isRequired,
  checkDomainStatus: PropTypes.func.isRequired,
  checkDmarcStatus: PropTypes.func.isRequired,
  checkDkimStatus: PropTypes.func.isRequired
};

export default SenderView;
