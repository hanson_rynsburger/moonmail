/* eslint max-len: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import {NO_LOCALIZE} from 'lib/constants';
import Button from 'components/Button';
import NoLocalize from 'components/NoLocalize';

const DomainData = ({domainVerificationToken, emailAddress, checkStatus, isChecking, id}) => {
  const domain = emailAddress.split('@')[1];
  return (
    <div>
      <p>
        To complete verification of{' '}
        <NoLocalize>
          <b>{domain}</b>
        </NoLocalize>, you must add the following TXT record to the domain's DNS settings:
      </p>
      <table className="ui celled striped table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          <tr className={NO_LOCALIZE}>
            <td>{domain}</td>
            <td>TXT</td>
            <td>{domainVerificationToken}</td>
          </tr>
        </tbody>
      </table>
      <p>
        A TXT record is a type of DNS record that provides additional information about your domain.
        The procedure for adding TXT records to your domain's DNS settings depends on who provides
        your DNS service. For general information, see{' '}
        <a
          href="http://support.moonmail.io/spf-dkim-setup-guides/about-txt-spf-dkim-records/how-to-get-txt-spf-and-dkim-records-from-your-moonmail-account"
          target="_blank"
          rel="noopener noreferrer">
          this article
        </a>.
      </p>
      <p>
        <b>NOTE:</b> Changes in DNS records may take up to 72 hours.
      </p>
      <p>
        <Button loading={isChecking} onClick={() => checkStatus(id)}>
          Check status
        </Button>
      </p>
    </div>
  );
};

DomainData.propTypes = {
  domainVerificationToken: PropTypes.string,
  isChecking: PropTypes.bool,
  emailAddress: PropTypes.string,
  checkStatus: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired
};

export default DomainData;
