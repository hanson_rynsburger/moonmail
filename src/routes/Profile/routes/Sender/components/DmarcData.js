/* eslint max-len: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import {NO_LOCALIZE} from 'lib/constants';

const DmarkData = ({checkStatus, isChecking, id, emailAddress}) => {
  const domain = emailAddress.split('@')[1];
  return (
    <div>
      <p>
        To activate DMARC signing for your domain (optional but recommended), you must add the
        following TXT record to the domain's DNS settings:
      </p>
      <table className="ui celled striped table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          <tr className={NO_LOCALIZE}>
            <td>_dmarc.{domain}</td>
            <td>TXT</td>
            <td>v=DMARC1; p=none</td>
          </tr>
        </tbody>
      </table>
      <p>
        A TXT record is a type of DNS record that provides additional information about your domain.
        The procedure for adding TXT records to your domain's DNS settings depends on who provides
        your DNS service. For general information, see{' '}
        <a
          href="http://support.moonmail.io/spf-dkim-setup-guides/about-txt-spf-dkim-records/how-to-get-txt-spf-and-dkim-records-from-your-moonmail-account"
          target="_blank"
          rel="noopener noreferrer">
          this article
        </a>.
      </p>
      <p>
        <b>NOTE:</b> Changes in DNS records may take up to 72 hours.
      </p>
      <p>
        <Button loading={isChecking} onClick={() => checkStatus(id)}>
          Check status
        </Button>
      </p>
    </div>
  );
};

DmarkData.propTypes = {
  isChecking: PropTypes.bool,
  checkStatus: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  emailAddress: PropTypes.string.isRequired
};

export default DmarkData;
