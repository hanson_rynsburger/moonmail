import React from 'react';
import PropTypes from 'prop-types';
import {NO_LOCALIZE} from 'lib/constants';
import Button from 'components/Button';

const DkimData = ({
  dkimTokens,
  domainVerified,
  dkimVerified,
  emailAddress,
  id,
  activate,
  checkStatus,
  isActivating,
  isChecking
}) => {
  const domain = emailAddress.split('@')[1];
  return (
    <div>
      <p>
        To activate DKIM signing for your domain (optional but recommended), you must add the
        following CNAME records to your domain's DNS settings:
      </p>
      <table className="ui celled striped table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Value</th>
          </tr>
        </thead>
        <tbody>
          {dkimTokens.map((token, i) => (
            <tr key={i} className={NO_LOCALIZE}>
              <td>
                {token}._domainkey.{domain}
              </td>
              <td>CNAME</td>
              <td>{token}.dkim.amazonses.com</td>
            </tr>
          ))}
        </tbody>
      </table>
      <p>
        How you update the DNS settings depends on who provides your DNS service; if your DNS
        service is provided by a domain name registrar, please contact that registrar to update your
        DNS records.
      </p>
      <p>
        <b>NOTE:</b> Changes in DNS records may take up to 72 hours.
      </p>
      {!domainVerified && (
        <p>
          <b>NOTE:</b> To activate DKIM you need to verify your domain first
        </p>
      )}
      <p>
        {!dkimVerified && (
          <Button loading={isChecking} onClick={() => checkStatus(id)}>
            Check status
          </Button>
        )}
        <Button
          loading={isActivating}
          disabled={!domainVerified && !dkimVerified}
          onClick={() => activate(id)}>
          Activate DKIM
        </Button>
      </p>
    </div>
  );
};

DkimData.propTypes = {
  dkimTokens: PropTypes.array,
  emailAddress: PropTypes.string,
  isActivating: PropTypes.bool,
  domainVerified: PropTypes.bool,
  dkimVerified: PropTypes.bool,
  isChecking: PropTypes.bool,
  activate: PropTypes.func.isRequired,
  checkStatus: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired
};

export default DkimData;
