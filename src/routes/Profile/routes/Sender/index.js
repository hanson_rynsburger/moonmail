import SenderContainer from './containers/SenderContainer';

export default {
  hideAlerts: true,
  path: 'senders/:senderId',
  component: SenderContainer
};
