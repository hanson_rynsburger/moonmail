import React, {Component} from 'react';
import {connect} from 'react-redux';
import SenderView from '../components/SenderView';
import actions from 'modules/senders/actions';
import selectors from 'modules/senders/selectors';

class Sender extends Component {
  componentDidMount() {
    this.props.fetchSender(this.props.senderId);
  }

  render() {
    return <SenderView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  senderId: ownProps.params.senderId,
  sender: selectors.getActiveResource(state),
  isVerifying: selectors.getIsVerifying(state),
  isFetching: selectors.getIsFetching(state),
  isVerifyingDomain: selectors.getIsVerifyingDomain(state),
  isUpdatingStatus: selectors.getIsUpdatingStatus(state),
  isGeneratingDkim: selectors.getIsGeneratingDkim(state),
  isActivatingDkim: selectors.getIsActivatingDkim(state)
});

export default connect(mapStateToProps, actions)(Sender);
