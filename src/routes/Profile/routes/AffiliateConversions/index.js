import {injectReducer} from 'store/reducers';
import Conversions from './containers/ConversionsContainer';
import reducer, {stateKey} from './modules/reducer';

export default store => {
  injectReducer(store, {key: stateKey, reducer});
  return {
    path: 'affiliate-conversions',
    component: Conversions
  };
};
