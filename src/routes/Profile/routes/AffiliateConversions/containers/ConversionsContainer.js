import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import ConversionsView from '../components/ConversionsView';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';

class Conversions extends Component {
  static propTypes = {
    fetchConversions: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchConversions();
  }

  getPage = page => {
    this.props.fetchConversions({page});
  };

  render() {
    return <ConversionsView {...this.props} getPage={this.getPage} />;
  }
}

const mapStateToProps = state => ({
  conversions: selectors.getConversions(state),
  pagination: selectors.getPagination(state),
  isFetching: selectors.getIsFetching(state)
});

export default connect(mapStateToProps, actions)(Conversions);
