import React from 'react';
import PropTypes from 'prop-types';
import ConversionRow, {NUM_COLUMNS} from './ConversionRow';
import {PaginatedTable} from 'components/Table';
import path from 'object-path';

export const ConversionsView = ({conversions, isFetching, pagination, getPage}) => {
  return (
    <section className="ui vertical segment">
      <PaginatedTable
        isFetching={isFetching}
        numColumns={NUM_COLUMNS}
        isFirstPage={!pagination.prev}
        isLastPage={!pagination.next}
        onNextPage={() => getPage(path.get(pagination, 'next.page'))}
        onPrevPage={() => getPage(path.get(pagination, 'prev.page'))}
        className="large striped"
        notFoundText="You don't have any conversions yet."
        header={<ConversionRow isHeader />}>
        {conversions.map((conversion, i) => <ConversionRow conversion={conversion} key={i} />)}
      </PaginatedTable>
    </section>
  );
};

ConversionsView.propTypes = {
  conversions: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  pagination: PropTypes.object.isRequired,
  getPage: PropTypes.func.isRequired
};

export default ConversionsView;
