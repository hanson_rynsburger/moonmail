import React from 'react';
import PropTypes from 'prop-types';
import path from 'object-path';
import {NO_LOCALIZE} from 'lib/constants';
import {formatDate} from 'lib/utils';
import cx from 'classnames';

const ConversionRow = ({conversion, isHeader = false}) => {
  if (isHeader) {
    return (
      <tr>
        <th>Commission</th>
        <th>Spent amount</th>
        <th>Date</th>
        <th width={50} className="center aligned">
          Status
        </th>
      </tr>
    );
  }
  const commission = conversion.commissions[0];
  const currency = path.get(conversion, 'program.currency');
  const iconClass = cx('icon', {
    'green checkmark': commission.approved,
    'teal clock outline': !commission.approved
  });
  return (
    <tr className={NO_LOCALIZE}>
      <td>
        <b>
          {commission.amount} {currency}
        </b>
      </td>
      <td>
        {conversion.amount} {currency}
      </td>
      <td>{formatDate(conversion.created_at, {fromNow: true, showTime: true, unix: false})}</td>
      <td className="center aligned">
        <span
          data-tooltip={commission.approved ? 'Approved' : 'Pending approval'}
          data-position="bottom right">
          <i className={iconClass} />
        </span>
      </td>
    </tr>
  );
};

ConversionRow.propTypes = {
  conversion: PropTypes.object,
  isHeader: PropTypes.bool
};

export const NUM_COLUMNS = 4;

export default ConversionRow;
