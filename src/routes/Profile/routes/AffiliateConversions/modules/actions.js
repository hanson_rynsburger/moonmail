import * as api from 'lib/api';
import * as types from './types';
import {addMessage} from 'modules/messages/actions';

export const fetchConversions = params => {
  return async dispatch => {
    dispatch({
      type: types.FETCH_CONVERSIONS_REQUEST
    });
    try {
      const {items, pagination} = await api.fetchConversions(params);
      dispatch({
        type: types.FETCH_CONVERSIONS_SUCCESS,
        items,
        pagination: pagination || {}
      });
    } catch (error) {
      dispatch({
        type: types.FETCH_CONVERSIONS_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};
