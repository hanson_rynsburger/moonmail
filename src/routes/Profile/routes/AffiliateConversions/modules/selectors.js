import {createSelector} from 'reselect';
import {stateKey} from './reducer';

const conversionsSelector = state => state[stateKey];

export const getIsFetching = createSelector(
  conversionsSelector,
  conversions => conversions.isFetching
);

export const getConversions = createSelector(conversionsSelector, conversions => conversions.data);

export const getPagination = createSelector(
  conversionsSelector,
  conversions => conversions.pagination
);
