import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import ConfirmDelete from '../containers/ConfirmDeleteContainer';

export const DeleteAccountView = ({deleteAccountStart}) => {
  return (
    <section className="ui vertical segment">
      <div className="ui stackable grid">
        <div className="eleven wide column">
          <h2 className="ui header">We’re sorry to see you go</h2>
          <p className="text red">Warning! Once confirmed, this operation can't be undone!</p>
          <p>If you need immediate support you can use our chat on the lower right corner</p>
          <Button
            primary
            negative
            onClick={() => {
              deleteAccountStart();
            }}>
            Delete Account
          </Button>
        </div>
      </div>
      <ConfirmDelete />
    </section>
  );
};

DeleteAccountView.propTypes = {
  deleteAccountStart: PropTypes.func.isRequired
};

export default DeleteAccountView;
