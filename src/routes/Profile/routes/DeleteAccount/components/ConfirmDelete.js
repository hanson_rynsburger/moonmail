import React from 'react';
import PropTypes from 'prop-types';
import Confirm from 'components/Modal/Confirm';
import Input from 'components/Input';

const ConfirmDelete = ({
  isOpen,
  isDeleting,
  deleteAccountCancel,
  deleteAccount,
  fields: {cancelReason},
  handleSubmit
}) => {
  const submit = data => {
    deleteAccount(data);
  };
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isDeleting}
      onCancel={deleteAccountCancel}
      onConfirm={handleSubmit(submit)}
      confirmText="Delete Account"
      confirmClass="negative">
      <p>This action will completely delete your account. Are you sure you want to continue?</p>
      <form className="ui form" onSubmit={handleSubmit(submit)}>
        <Input component="textarea" rows={4} {...cancelReason} />
      </form>
    </Confirm>
  );
};

ConfirmDelete.propTypes = {
  deleteAccount: PropTypes.func.isRequired,
  deleteAccountCancel: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  fields: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired
};

export default ConfirmDelete;
