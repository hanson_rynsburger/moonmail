import * as types from './types';
import {addMessage} from 'modules/messages/actions';
import * as api from 'lib/api';
import {getProfile, getAvailablePlans, getPlanId} from 'modules/profile/selectors';
import {signOut} from 'modules/app/actions';
import {triggerEvent} from 'lib/remoteUtils';

export const deleteAccountStart = () => ({
  type: types.DELETE_ACCOUNT_START
});

export const deleteAccountCancel = () => ({
  type: types.DELETE_ACCOUNT_CANCEL
});

export const deleteAccount = ({cancelReason}) => {
  return async (dispatch, getState) => {
    const state = getState();
    const {user_id: userId, email, clickId} = getProfile(state);
    const planId = getPlanId(state);
    const {free} = getAvailablePlans(state);

    dispatch({
      type: types.DELETE_ACCOUNT_REQUEST
    });
    try {
      await api.updatePlan({plan: free.id, email, clickId});
      await api.updateUserMetaData(userId, {
        planId: free.id,
        planSubscription: null,
        isUserAccountCanceled: true,
        cancelReason
      });
      triggerEvent({
        event: 'app.userCanceledAccount',
        email,
        userId,
        planId
      });
      dispatch(signOut());
    } catch (error) {
      //eslint-disable-next-line no-console
      console.error(error);
      dispatch({
        type: types.DELETE_ACCOUNT_FAIL
      });
      dispatch(addMessage({text: 'Account could not be deleted.'}));
    }
  };
};
