import {stateKey} from './reducer';
import {createStateSelector, createKeySelector} from 'lib/reduxUtils';

const stateSelector = createStateSelector(stateKey);

export const getIsDeleteModalOpen = createKeySelector(stateSelector, 'isDeleteModalOpen');
export const getIsDeleting = createKeySelector(stateSelector, 'isDeleting');
