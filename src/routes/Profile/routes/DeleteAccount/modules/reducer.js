import * as types from './types';
import {combineReducers} from 'redux';
import {createAsyncFlagReducer} from 'lib/reduxUtils';

export const stateKey = 'deleteAccount';

const isDeleteModalOpen = (state = false, action) => {
  switch (action.type) {
    case types.DELETE_ACCOUNT_START:
      return true;
    case types.DELETE_ACCOUNT_SUCCESS:
    case types.DELETE_ACCOUNT_FAIL:
    case types.DELETE_ACCOUNT_CANCEL:
      return false;
    default:
      return state;
  }
};

const isDeleting = createAsyncFlagReducer(types, 'deleteAccount');

export default combineReducers({
  isDeleteModalOpen,
  isDeleting
});
