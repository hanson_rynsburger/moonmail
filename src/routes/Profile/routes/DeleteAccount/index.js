import DeleteAccount from './containers/DeleteAccountContainer';
import {injectReducer} from 'store/reducers';
import reducer, {stateKey} from './modules/reducer';

export default store => {
  injectReducer(store, {key: stateKey, reducer});
  return {
    path: 'delete',
    component: DeleteAccount
  };
};
