import {reduxForm} from 'redux-form';
import * as actions from '../modules/actions';
import * as selectors from '../modules/selectors';
import ConfirmDelete from '../components/ConfirmDelete';
import {createValidator} from 'lib/validator';

const mapStateToProps = state => ({
  isOpen: selectors.getIsDeleteModalOpen(state),
  isDeleting: selectors.getIsDeleting(state)
});

export default reduxForm(
  {
    form: 'cancelAccount',
    fields: ['cancelReason'],
    validate: createValidator({
      cancelReason: 'required'
    })
  },
  mapStateToProps,
  actions
)(ConfirmDelete);
