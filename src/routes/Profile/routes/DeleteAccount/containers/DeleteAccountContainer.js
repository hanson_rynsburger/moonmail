import {connect} from 'react-redux';
import DeleteAccountView from '../components/DeleteAccountView';
import * as actions from '../modules/actions';

export default connect(null, actions)(DeleteAccountView);
