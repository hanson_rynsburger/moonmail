import React from 'react';
import PropTypes from 'prop-types';
import CreditCard from 'components/CreditCard';
import BillingForm from 'components/BillingForm';

export const BillingInfoView = ({paymentMethod, updateCard, isUpdating}) => (
  <section>
    <div className="ui stackable vertically padded grid">
      <div className="eight wide column">
        {paymentMethod && <CreditCard {...paymentMethod} />}
        <BillingForm onFormSubmit={updateCard} isLoading={isUpdating} buttonLabel="Save" />
      </div>
    </div>
  </section>
);

BillingInfoView.propTypes = {
  paymentMethod: PropTypes.object,
  updateCard: PropTypes.func.isRequired,
  isUpdating: PropTypes.bool.isRequired
};

export default BillingInfoView;
