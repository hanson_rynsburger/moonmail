import {PaymentContainer} from 'components/Payment';
import BillingInfoView from './components/BillingInfoView';

export default {
  path: 'billing-info',
  component: PaymentContainer(BillingInfoView)
};
