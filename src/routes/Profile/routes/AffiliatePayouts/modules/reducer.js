import * as types from './types';
import {combineReducers} from 'redux';

export const stateKey = 'affiliatePayouts';

const data = (state = [], action) => {
  switch (action.type) {
    case types.FETCH_PAYOUTS_SUCCESS:
      return action.items;
    default:
      return state;
  }
};

const pagination = (state = {}, action) => {
  switch (action.type) {
    case types.FETCH_PAYOUTS_SUCCESS:
      return action.pagination;
    default:
      return state;
  }
};

const isFetching = (state = false, action) => {
  switch (action.type) {
    case types.FETCH_PAYOUTS_REQUEST:
      return true;
    case types.FETCH_PAYOUTS_SUCCESS:
    case types.FETCH_PAYOUTS_FAIL:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  data,
  pagination,
  isFetching
});
