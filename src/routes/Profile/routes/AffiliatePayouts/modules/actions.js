import * as api from 'lib/api';
import * as types from './types';
import {addMessage} from 'modules/messages/actions';

export const fetchPayouts = params => {
  return async dispatch => {
    dispatch({
      type: types.FETCH_PAYOUTS_REQUEST
    });
    try {
      const {items, pagination} = await api.fetchPayouts(params);
      dispatch({
        type: types.FETCH_PAYOUTS_SUCCESS,
        items,
        pagination: pagination || {}
      });
    } catch (error) {
      dispatch({
        type: types.FETCH_PAYOUTS_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};
