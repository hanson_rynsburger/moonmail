import {createSelector} from 'reselect';
import {stateKey} from './reducer';

const payoutsSelector = state => state[stateKey];

export const getIsFetching = createSelector(payoutsSelector, payouts => payouts.isFetching);

export const getPayouts = createSelector(payoutsSelector, payouts => payouts.data);

export const getPagination = createSelector(payoutsSelector, payouts => payouts.pagination);
