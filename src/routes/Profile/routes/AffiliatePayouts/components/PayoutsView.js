import React from 'react';
import PropTypes from 'prop-types';
import PayoutRow, {NUM_COLUMNS} from './PayoutRow';
import {PaginatedTable} from 'components/Table';
import path from 'object-path';

export const PayoutsView = ({payouts, isFetching, pagination, getPage}) => (
  <section className="ui vertical segment">
    <PaginatedTable
      isFetching={isFetching}
      numColumns={NUM_COLUMNS}
      isFirstPage={!pagination.prev}
      isLastPage={!pagination.next}
      onNextPage={() => getPage(path.get(pagination, 'next.page'))}
      onPrevPage={() => getPage(path.get(pagination, 'prev.page'))}
      className="large striped"
      notFoundText="You don't have any payouts yet."
      header={<PayoutRow isHeader />}>
      {payouts.map((payout, i) => <PayoutRow payout={payout} key={i} />)}
    </PaginatedTable>
  </section>
);

PayoutsView.propTypes = {
  payouts: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  pagination: PropTypes.object.isRequired,
  getPage: PropTypes.func.isRequired
};

export default PayoutsView;
