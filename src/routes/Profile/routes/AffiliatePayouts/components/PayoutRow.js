import React from 'react';
import PropTypes from 'prop-types';
import {formatDate} from 'lib/utils';
import {NO_LOCALIZE} from 'lib/constants';
import cx from 'classnames';

const PayoutRow = ({payout, isHeader = false}) => {
  if (isHeader) {
    return (
      <tr>
        <th>Amount</th>
        <th>Created</th>
        <th width={50} className="center aligned">
          Status
        </th>
      </tr>
    );
  }
  const iconClass = cx('icon', {
    'green checkmark': payout.paid,
    'teal clock outline': !payout.paid
  });
  return (
    <tr className={NO_LOCALIZE}>
      <td>
        <b>{payout.amount} USD</b>
      </td>
      <td>{formatDate(payout.created_at, {showTime: true, fromNow: true, unix: false})}</td>
      <td className="center aligned">
        <span data-tooltip={payout.paid ? 'Paid' : 'Payment pending'} data-position="bottom right">
          <i className={iconClass} />
        </span>
      </td>
    </tr>
  );
};

PayoutRow.propTypes = {
  payout: PropTypes.object,
  isHeader: PropTypes.bool
};

export const NUM_COLUMNS = 3;

export default PayoutRow;
