import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import PayoutsView from '../components/PayoutsView';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';

class Payouts extends Component {
  static propTypes = {
    fetchPayouts: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchPayouts();
  }

  getPage = page => {
    this.props.fetchPayouts({page});
  };

  render() {
    return <PayoutsView {...this.props} getPage={this.getPage} />;
  }
}

const mapStateToProps = state => ({
  payouts: selectors.getPayouts(state),
  pagination: selectors.getPagination(state),
  isFetching: selectors.getIsFetching(state)
});

export default connect(mapStateToProps, actions)(Payouts);
