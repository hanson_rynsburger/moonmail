import {injectReducer} from 'store/reducers';
import Payouts from './containers/PayoutsContainer';
import reducer, {stateKey} from './modules/reducer';

export default store => {
  injectReducer(store, {key: stateKey, reducer});
  return {
    path: 'affiliate-payouts',
    component: Payouts
  };
};
