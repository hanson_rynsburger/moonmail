import Plans from './containers/PlansContainer';

export default {
  path: 'plan',
  component: Plans
};
