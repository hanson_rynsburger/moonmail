import {connect} from 'react-redux';
import PlansView from '../components/PlansView';
import actions from 'modules/plans/actions';
import selectors from 'modules/plans/selectors';
import {withRouter} from 'react-router';
import * as profileActions from 'modules/profile/actions';
import * as profileSelectors from 'modules/profile/selectors';
import RequireNotAmazon from 'containers/RequireNotAmazon';
import {compose} from 'recompose';
import listsSelectors from 'modules/lists/selectors';

const mapStateToProps = state => {
  const plans = profileSelectors.getAvailablePlans(state);
  const isSesUser = profileSelectors.getIsSesUser(state);
  return {
    pricingRate: profileSelectors.getPricingRate(state),
    isContactInfoProvided: profileSelectors.getIsContactInfoProvided(state),
    isSesUser,
    hasAnnualSubscription: profileSelectors.getHasAnnualSubscription(state),
    currentPlanId: profileSelectors.getPlanId(state),
    isFetching: selectors.getIsFetchingAll(state),
    plans,
    isPaymentOpen: selectors.getIsSelected(state),
    selectedPlan: selectors.getSelectedResource(state),
    isPlanUpdating: profileSelectors.getIsUpdating(state),
    hasExtensions: profileSelectors.getExtensions(state).length > 0,
    totalRecipients: listsSelectors.getTotalRecipients(state)
  };
};

export default compose(
  connect(mapStateToProps, {
    ...actions,
    updatePlanStart: actions.selectResource,
    updatePlan: profileActions.updatePlan,
    updatePlanCancel: actions.clearSelectedResource
  }),
  RequireNotAmazon,
  withRouter
)(PlansView);
