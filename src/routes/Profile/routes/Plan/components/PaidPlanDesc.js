import React from 'react';
import PropTypes from 'prop-types';
import numeral from 'numeral';

const PaidPlanDesc = ({limits = {}, isSesUser, planId}) => {
  const recipientsInTotal =
    limits.recipientsInTotal === null
      ? 'unlimited'
      : numeral(limits.recipientsInTotal).format('0a');

  return (
    <div className="ui relaxed large list">
      <div className="item">Unlimited campaigns per month</div>
      <div className="item">Unlimited recipients per campaign</div>
      <div className="item">
        Store up to <b>{recipientsInTotal}</b> recipients
      </div>
      <div className="item">Use any email as the send address</div>
      {isSesUser && (
        <div className="item">
          <b>Use your own AWS/SES account</b>
        </div>
      )}
      <div className="item">Unlimited Recipients Custom Fields</div>
      <div className="item">Premium support</div>
      <div className="item">Powerful Drag'n Drop Editor</div>
      <div className="item">Advanced Signup Forms</div>
      <div className="item">DKIM & SPF Configuration</div>
      {planId !== 'paid' && <div className="item">Free MailChimp Migration</div>}
    </div>
  );
};

PaidPlanDesc.propTypes = {
  limits: PropTypes.object,
  isSesUser: PropTypes.bool,
  planId: PropTypes.string
};

export default PaidPlanDesc;
