import React from 'react';
import PropTypes from 'prop-types';
import PlanButton from './PlanButton';
import classNames from './PlansView.scss';
import cx from 'classnames';
import PaidPlanDesc from './PaidPlanDesc';

const PaidPlan = ({
  id,
  title,
  price,
  pricingRate,
  isFeatured,
  subscriptions,
  isSesUser,
  isSelected,
  updatePlanStart,
  limits
}) => {
  const sub = subscriptions.find(s => s.primary) || {};
  const amount = (price * (1 - sub.discountFactor)).toFixed(2);
  return (
    <div className={cx('card', classNames.card)}>
      {isFeatured && (
        <div className="ui right corner label">
          <i className="star orange icon" />
        </div>
      )}
      <div className={cx('content', classNames.planHeader)}>
        <div className="header">
          <i className="rocket icon" />
          {title}{' '}
          <span className="text grey">
            ${amount}/mo<span className={classNames.ast}>*</span>
          </span>
        </div>
        <div className="meta">
          <a
            href="http://support.moonmail.io/faq/pricing/moonmail-pricing"
            target="_blank"
            rel="noopener noreferrer">
            usage fee ${pricingRate} for every 1000 emails**
          </a>
        </div>
      </div>
      <div className="content">
        <div className="description">
          <PaidPlanDesc limits={limits} isSesUser={isSesUser} planId={id} />
        </div>
      </div>
      <PlanButton planId={id} selected={isSelected} onClick={updatePlanStart} />
    </div>
  );
};

PaidPlan.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  price: PropTypes.number,
  pricingRate: PropTypes.number,
  isFeatured: PropTypes.bool,
  isSesUser: PropTypes.bool,
  isSelected: PropTypes.bool,
  subscriptions: PropTypes.array,
  updatePlanStart: PropTypes.func,
  limits: PropTypes.object.isRequired
};

export default PaidPlan;
