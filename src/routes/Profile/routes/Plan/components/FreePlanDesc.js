import React from 'react';
import PropTypes from 'prop-types';
import numeral from 'numeral';

const FreePlanDesc = ({limits = {}, isSesUser}) => {
  const recipientsPerCampaign = numeral(limits.recipientsPerCampaign).format();
  const recipientsInTotal = isNaN(limits.recipientsInTotal)
    ? 'unlimited'
    : numeral(limits.recipientsInTotal).format('0a');

  return (
    <div className="ui relaxed large list">
      <div className="item">1 campaign per day</div>
      {typeof limits.recipientsPerCampaign === 'number' && (
        <div className="item">
          <b>{recipientsPerCampaign}</b> recipients per campaign
        </div>
      )}
      <div className="item">
        Store up to <b>{recipientsInTotal}</b> recipients
      </div>
      <div className="item">
        <b>Our branding</b> in email footer
      </div>
      <div className="item">Use {isSesUser ? 'any email' : 'our domain'} as the send address</div>
      {isSesUser && (
        <div className="item">
          <b>Use your own AWS/SES account</b>
        </div>
      )}
      <div className="item">Basic Recipients Custom Fields</div>
      <div className="item">Basic support</div>
      <div className="item">Powerful Drag'n Drop Editor</div>
      <div className="item">Advanced Signup Forms</div>
    </div>
  );
};

FreePlanDesc.propTypes = {
  limits: PropTypes.object,
  isSesUser: PropTypes.bool
};

export default FreePlanDesc;
