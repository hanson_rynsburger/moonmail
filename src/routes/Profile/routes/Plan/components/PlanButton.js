import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Button from 'components/Button';

const PlanButton = ({selected, onClick, disabled, className, planId}) => {
  let label = 'Select';
  if (selected) label = 'Selected';
  if (disabled) label = 'Contact support to upgrade';
  return (
    <Button
      disabled={selected}
      primary={!selected}
      className={cx('bottom large attached', `${planId}_plan`, className)}
      onClick={() => onClick(planId)}>
      {label}
    </Button>
  );
};

PlanButton.propTypes = {
  selected: PropTypes.bool.isRequired,
  disabled: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  planId: PropTypes.string
};

export default PlanButton;
