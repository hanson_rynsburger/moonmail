import React from 'react';
import PropTypes from 'prop-types';
import PlanButton from './PlanButton';
import PaidPlan from './PaidPlan';
import Loader from 'components/Loader';
import Payment from 'components/Payment';
import FreePlanDesc from './FreePlanDesc';
import classNames from './PlansView.scss';
import cx from 'classnames';
import numeral from 'numeral';
import NL from 'components/NoLocalize';
import {contactSupport} from 'lib/remoteUtils';
import {formatStat} from 'lib/utils';

export const PlansView = ({
  plans,
  currentPlanId,
  updatePlanStart,
  updatePlan,
  updatePlanCancel,
  isSesUser,
  hasAnnualSubscription,
  pricingRate,
  isPaymentOpen,
  selectedPlan,
  isPlanUpdating,
  hasExtensions,
  totalRecipients,
  router
}) => {
  if (!plans.free.id) {
    return (
      <section className="ui vertical segment">
        <Loader inline />
      </section>
    );
  }
  const {paid, pro, enterprise, free} = plans || {};
  const limits = free.limits || {};
  const emailsPerMonth = numeral(limits.recipientsPerCampaign * limits.campaignsPerDay * 30).format(
    '0a'
  );
  const recipientsPerCampaign = numeral(limits.recipientsPerCampaign).format('0a');
  const totalRecipientsForPlan = selectedPlan.limits && selectedPlan.limits.recipientsInTotal;
  const isOutOfPlanLimits =
    totalRecipientsForPlan !== null && totalRecipients > totalRecipientsForPlan;

  const getPaymentText = () => {
    if (isOutOfPlanLimits) {
      return (
        <p>
          This plan allows only <b>{formatStat(totalRecipientsForPlan)}</b> recipients and you have{' '}
          <b>{formatStat(totalRecipients)}</b>. You need to delete some lists to downgrade.
        </p>
      );
    }
    if (!hasAnnualSubscription && hasExtensions) {
      return <p>You have installed extensions, please uninstall them first to downgrade</p>;
    }
    if (selectedPlan.price === 0) {
      return (
        <p>
          You will be able to send only <b>1 campaign a day</b> to no more than{' '}
          <NL el="b">{recipientsPerCampaign}</NL> subscribers.
        </p>
      );
    }
  };

  const handleUpdatePlan = args => {
    if (selectedPlan.price === 0 && hasExtensions) {
      updatePlanCancel();
      return router.push('/extensions');
    }
    return updatePlan(selectedPlan.id, args);
  };

  return (
    <section className="ui vertical segment">
      {plans && (
        <div>
          <div className="ui stackable cards">
            <div className={'card ' + cx('content', classNames.card)}>
              <div className={cx('content', classNames.planHeader)}>
                <div className="header">
                  <i className="send outline icon" /> Free forever
                </div>
                <div className="meta">send up to {emailsPerMonth} emails / month for free</div>
              </div>
              <div className="content">
                <div className="description">
                  <FreePlanDesc limits={limits} isSesUser={isSesUser} />
                </div>
              </div>
              <PlanButton
                planId={free.id}
                selected={currentPlanId === free.id}
                onClick={updatePlanStart}
              />
            </div>
            {[paid, pro, enterprise].map((plan, i) => (
              <PaidPlan
                {...plan}
                key={i}
                pricingRate={pricingRate}
                isSelected={currentPlanId === plan.id}
                {...{isSesUser, updatePlanStart}}
              />
            ))}
          </div>
          <div className={classNames.info}>
            <div>
              <p className={classNames.hint}>
                * If paid annually. <br />
                ** Amount of emails is rounded up to the nearest 1,000, starting from $4.99. <br />
              </p>
              <p className={classNames.stressedHint}>
                You'll be billed immediately after switching to a paid plan, but we offer a{' '}
                <em>30-day Refund Policy</em> if you're not happy.{' '}
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="http://support.moonmail.io/faq/pricing/moonmail-pricing">
                  Click here
                </a>{' '}
                to read our full Pricing Policy.
              </p>
            </div>
            <p>
              Need more recipients?{' '}
              <a
                href="#"
                onClick={e =>
                  contactSupport(e, 'Hello. I would like to increase recipients limit.')
                }>
                Contact us
              </a>!
            </p>
          </div>

          <Payment
            isOpen={isPaymentOpen}
            isProcessing={isPlanUpdating}
            process={handleUpdatePlan}
            processCancel={updatePlanCancel}
            price={selectedPlan.price}
            subscriptions={isOutOfPlanLimits ? null : selectedPlan.subscriptions}
            isHandledBySupport={isOutOfPlanLimits}
            headerText={`${selectedPlan.title} plan`}
            confirmText={
              selectedPlan.price === 0 && hasExtensions ? 'Uninstall extensions' : 'Change plan'
            }>
            {getPaymentText()}
          </Payment>
        </div>
      )}
    </section>
  );
};

PlansView.propTypes = {
  plans: PropTypes.object.isRequired,
  currentPlanId: PropTypes.string.isRequired,
  updatePlanStart: PropTypes.func.isRequired,
  updatePlan: PropTypes.func.isRequired,
  updatePlanCancel: PropTypes.func.isRequired,
  isSesUser: PropTypes.bool,
  hasAnnualSubscription: PropTypes.bool,
  pricingRate: PropTypes.number.isRequired,
  totalRecipients: PropTypes.number,
  isPaymentOpen: PropTypes.bool.isRequired,
  selectedPlan: PropTypes.object,
  router: PropTypes.object.isRequired,
  isPlanUpdating: PropTypes.bool.isRequired,
  hasExtensions: PropTypes.bool.isRequired
};

export default PlansView;
