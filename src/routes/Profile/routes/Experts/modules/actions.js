import * as api from 'lib/api';
import * as types from './types';
import S3 from 'lib/s3Bucket';
import {addMessage} from 'modules/messages/actions';
import {updateIntercom} from 'lib/remoteUtils';
import {getUserId, getExpertData, getIsDemoUser} from 'modules/profile/selectors';
import numeral from 'numeral';
import {DEMO_USER_ERROR} from 'lib/constants';

export const updateExpertSettings = (data, silent = false) => {
  return async (dispatch, getState) => {
    const expertData = {...getExpertData(getState()), ...data};
    !silent &&
      dispatch({
        type: types.UPDATE_EXPERT_SETTINGS_REQUEST
      });
    try {
      await api.updateExpertSettings(expertData);
      dispatch({
        type: types.UPDATE_EXPERT_SETTINGS_SUCCESS,
        data: expertData
      });
      !silent &&
        dispatch(
          addMessage({
            style: 'success',
            text: 'Your settings have been successfully updated'
          })
        );
      dispatch(updateIntercom());
    } catch (error) {
      dispatch({
        type: types.UPDATE_EXPERT_SETTINGS_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const uploadLogo = file => {
  return async (dispatch, getState) => {
    const sizeLimit = 256000;
    const state = getState();
    const userId = getUserId(state);
    const isDemoUser = getIsDemoUser(state);
    const {logo} = getExpertData(state);
    const bucketName = APP_CONFIG.expertLogosBucket;
    const bucket = new S3({userId, bucketName});
    if (isDemoUser) {
      return dispatch(
        addMessage({
          text: DEMO_USER_ERROR
        })
      );
    }
    if (file.size > sizeLimit) {
      return dispatch(
        addMessage({
          text: `File size must be less than ${numeral(sizeLimit).format('0 ib')}`
        })
      );
    }
    dispatch({type: types.UPLOAD_LOGO_REQUEST});
    try {
      if (logo) await bucket.deleteFile(logo);
      const {fileName} = await bucket.uploadFile(file, {ACL: 'public-read'});
      await dispatch(updateExpertSettings({logo: fileName}, true));
      dispatch({type: types.UPLOAD_LOGO_SUCCESS});
    } catch (error) {
      dispatch({type: types.UPLOAD_LOGO_FAIL});
      dispatch(
        addMessage({
          text: error
        })
      );
    }
  };
};
