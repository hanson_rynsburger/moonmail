import {createSelector} from 'reselect';
import {stateKey} from './reducer';

const settingsSelector = state => state[stateKey];

export const getIsUpdatingExpertSettings = createSelector(
  settingsSelector,
  settings => settings.isUpdatingExpertSettings
);

export const getIsLogoUploading = createSelector(
  settingsSelector,
  settings => settings.isLogoUploading
);
