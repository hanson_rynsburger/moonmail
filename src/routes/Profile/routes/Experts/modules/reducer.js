import * as types from './types';
import {combineReducers} from 'redux';

export const stateKey = 'expertSettings';

const isUpdatingExpertSettings = (state = false, action) => {
  switch (action.type) {
    case types.UPDATE_EXPERT_SETTINGS_REQUEST:
      return true;
    case types.UPDATE_EXPERT_SETTINGS_SUCCESS:
    case types.UPDATE_EXPERT_SETTINGS_FAIL:
      return false;
    default:
      return state;
  }
};

const isLogoUploading = (state = false, action) => {
  switch (action.type) {
    case types.UPLOAD_LOGO_REQUEST:
      return true;
    case types.UPLOAD_LOGO_SUCCESS:
    case types.UPLOAD_LOGO_FAIL:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  isUpdatingExpertSettings,
  isLogoUploading
});
