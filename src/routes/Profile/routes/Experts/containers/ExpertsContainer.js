import {reduxForm} from 'redux-form';
import Experts from '../components/ExpertsView';
import {getExpertData, getAddress, getProfile} from 'modules/profile/selectors';
import placeholder from 'static/expert.svg';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';
import {createValidator} from 'lib/validator';

const mapStateToProps = state => {
  const expertData = getExpertData(state);
  const profile = getProfile(state);
  const contactInfo = getAddress(state);
  const expertLogoUrl = expertData.logo
    ? `https://${APP_CONFIG.expertLogosBucket}.s3.amazonaws.com/${profile.id}/${expertData.logo}`
    : placeholder;
  const defaultExpertData = {
    email: profile.email,
    ...contactInfo,
    ...expertData
  };
  return {
    isUpdatingExpertSettings: selectors.getIsUpdatingExpertSettings(state),
    initialValues: defaultExpertData,
    expertLogoUrl,
    expertData: {
      description: `The easiest way to send Email Marketing Newsletters! 
      Create, design and analyze your Email Marketing campaigns in a minute using MoonMail, 
      the top Email Marketing Software Platform.`,
      company: 'microapps S.L',
      country: 'Spain',
      websiteUrl: 'http://microapps.com/',
      ...defaultExpertData
    },
    isLogoUploading: selectors.getIsLogoUploading(state)
  };
};

export default reduxForm(
  {
    form: 'expert',
    fields: ['isExpert', 'description', 'type', 'email', 'company', 'websiteUrl', 'country'],
    validate: createValidator({
      description: 'required|min:50|max:300',
      type: 'required',
      email: 'email',
      company: 'required',
      websiteUrl: 'required|url',
      country: 'required'
    })
  },
  mapStateToProps,
  actions
)(Experts);
