import React from 'react';
import PropTypes from 'prop-types';
import CheckBoxInput from 'components/CheckBoxInput';
import Input from 'components/Input';
import Button from 'components/Button';
import FileButton from 'components/FileButton';
import {EXPERT_TYPES, NO_LOCALIZE} from 'lib/constants';
import Select, {SelectItem} from 'components/Select';
import countries from 'lib/countries';
import classNames from './ExpertsView.scss';
import cx from 'classnames';

const ExpertsView = ({
  handleSubmit,
  untouchAll,
  fields: {isExpert, description, type, email, company, websiteUrl, country},
  expertData,
  values,
  invalid,
  expertLogoUrl,
  updateExpertSettings,
  uploadLogo,
  isLogoUploading,
  isUpdatingExpertSettings
}) => {
  const saveExpertSettings = formProps => {
    updateExpertSettings(formProps);
  };

  const toggleExpert = e => {
    const isEnabled = e.target.value !== 'true';
    if (isEnabled !== isExpert.initialValue) {
      updateExpertSettings({isExpert: isEnabled}, true);
    }
    untouchAll();
    isExpert.onChange(e);
  };

  const onFileChange = e => {
    const {files} = e.target;
    if (files.length > 0) {
      uploadLogo(files[0]);
    }
  };

  return (
    <section className="ui vertical segment">
      <p>
        Are you a digital marketing, graphic design or software development expert? <br />
        Apply to become a MoonMail certified expert and have your agency advertised to thousands of
        potential users, looking to hire an expert like you.
      </p>
      <div className="ui message">
        <div className="content">
          <div className="header">Benefits of MoonMail expert</div>
          <ul className="ui relaxed list">
            <li className="item">
              Be featured in the{' '}
              <a
                href="https://moonmail.io/email-marketing-experts"
                target="_blank"
                rel="noopener noreferrer">
                Experts Directory
              </a>
            </li>
            <li className="item">Promote your business and expand your brand</li>
            <li className="item">Get tons of new clients</li>
            <li className="item">
              Get exclusive support and high level engagement with the core MoonMail team
            </li>
            <li className="item">Be the first to find out about new releases and features</li>
          </ul>
        </div>
      </div>
      <CheckBoxInput
        {...isExpert}
        onChange={toggleExpert}
        disabled={invalid}
        toggle
        label="List me as a MoonMail Expert"
      />
      <h4>
        Preview of the{' '}
        <a
          href="https://moonmail.io/email-marketing-experts"
          target="_blank"
          rel="noopener noreferrer">
          Experts Directory
        </a>:
      </h4>
      <div
        className={cx(classNames.preview, NO_LOCALIZE, {
          [classNames.previewActive]: expertData.isExpert
        })}>
        <div className={classNames.logo}>
          <img src={expertLogoUrl} />
        </div>
        <div className={classNames.content}>
          <h4>
            {expertData.company} <span className="text grey">- {expertData.country}</span>
          </h4>
          <p>{expertData.description}</p>
          <p>
            <a href={expertData.websiteUrl}>{expertData.websiteUrl}</a>
          </p>
        </div>
        <div>
          <a
            className={cx('ui button teal', {disabled: !expertData.isExpert})}
            href={`mailto:${values.email}`}>
            Contact
          </a>
        </div>
      </div>
      <br />
      <div className="ui stackable grid">
        <form className="ui form nine wide column" onSubmit={handleSubmit(saveExpertSettings)}>
          <div className="field">
            <FileButton
              type="button"
              accept="image/*"
              loading={isLogoUploading}
              onChange={onFileChange}>
              <i className="icon cloud upload" />
              {expertData.logo ? 'Change' : 'Upload'} logo
            </FileButton>
            <p className="text grey">We accept png, jpg and svg images less than 250 KiB</p>
          </div>
          <Select localize label="Which type of expert are you?" {...type}>
            {Object.keys(EXPERT_TYPES).map((key, i) => (
              <SelectItem localize key={i} value={key}>
                {EXPERT_TYPES[key]}
              </SelectItem>
            ))}
          </Select>
          <Input label="Company / Organization" {...company} />
          <Select label="Country" search {...country}>
            {countries.map(country => (
              <SelectItem key={country.code} value={country.name}>
                <i className={cx(country.code.toLowerCase(), 'flag')} />
                {country.name}
              </SelectItem>
            ))}
          </Select>
          <Input {...description} rows={3} component="textarea" label="Expert Description" />
          <Input label="Website URL" {...websiteUrl} />
          <Input {...email} label="Your contact email" />
          <Button primary type="submit" disabled={invalid} loading={isUpdatingExpertSettings}>
            Save
          </Button>
        </form>
      </div>
    </section>
  );
};

ExpertsView.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  updateExpertSettings: PropTypes.func.isRequired,
  untouchAll: PropTypes.func.isRequired,
  values: PropTypes.object.isRequired,
  isUpdatingExpertSettings: PropTypes.bool,
  invalid: PropTypes.bool.isRequired,
  uploadLogo: PropTypes.func.isRequired,
  isLogoUploading: PropTypes.bool.isRequired,
  expertData: PropTypes.object.isRequired,
  expertLogoUrl: PropTypes.string.isRequired,
  fields: PropTypes.shape({
    isExpert: PropTypes.object,
    description: PropTypes.object
  }).isRequired
};

export default ExpertsView;
