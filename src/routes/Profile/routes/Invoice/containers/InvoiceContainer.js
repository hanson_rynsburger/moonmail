import React, {Component} from 'react';
import {withRouter} from 'react-router';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import InvoiceView from '../components/InvoiceView';
import * as profileSelectors from 'modules/profile/selectors';
import * as chargesSelectors from 'routes/Profile/routes/BillingHistory/modules/selectors';

class Invoice extends Component {
  static propTypes = {
    charge: PropTypes.object,
    router: PropTypes.object
  };

  componentWillMount() {
    if (!this.props.charge) {
      this.props.router.replace('/profile/billing-history');
    }
  }

  render() {
    if (!this.props.charge) return null;
    return <InvoiceView {...this.props} />;
  }
}

const mapStateToProps = (state, props) => {
  const charges = chargesSelectors.getCharges(state);
  const charge = charges.find(c => c.id === props.params.chargeId);
  return {
    charge,
    address: profileSelectors.getAddress(state),
    profile: profileSelectors.getProfile(state)
  };
};

export default connect(mapStateToProps)(withRouter(Invoice));
