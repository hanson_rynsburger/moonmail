import React from 'react';
import PropTypes from 'prop-types';
import {formatDate, popupWindow} from 'lib/utils';
import {NO_LOCALIZE} from 'lib/constants';
import NoLocalize from 'components/NoLocalize';
import Button from 'components/Button';

const printInvoice = () => {
  const invoiceHTML = document.getElementById('invoice').innerHTML;
  const popupWin = popupWindow('', 'to_print', 800, 400);
  popupWin.document.open();
  popupWin.document.write(`
      <html>
        <head>
          <link rel="stylesheet" type="text/css"
                href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css" />
        </head>
        <body >
          ${invoiceHTML}
          <script type="text/javascript">
            setTimeout(window.print, 500);
          </script>
        </body>
        
      </html>
    `);
  popupWin.document.close();
};

export const InvoiceView = ({charge, address, profile}) => {
  const currency = (charge.currency || '').toUpperCase();
  const amount = (charge.refunded ? -charge.amount : charge.amount) / 100;
  return (
    <sections>
      <section className="ui very padded segment" id="invoice">
        <table style={{width: '100%'}}>
          <tbody>
            <tr>
              <td>
                <h2 className={NO_LOCALIZE} style={{marginBottom: '.25em', marginTop: '.25em'}}>
                  microapps S.L.
                </h2>
                <h3 className={NO_LOCALIZE} style={{marginBottom: '.25em', marginTop: '.25em'}}>
                  ESB93160414
                </h3>
                <address className={NO_LOCALIZE}>
                  Severo Ochoa, 25-27<br />
                  Parque Tecnológico de Málaga<br />
                  29590 - Spain<br />
                </address>

                <h5 style={{marginBottom: '.25em'}}>Bill to:</h5>
                <h4 className={NO_LOCALIZE} style={{marginBottom: '.25em', marginTop: '.25em'}}>
                  {address.company}
                </h4>
                <address className={NO_LOCALIZE}>
                  {address.address}
                  <br />
                  {address.city} {address.state && `, ${address.state}`}
                  <br />
                  {profile.vat}
                </address>
              </td>
              <td
                style={{
                  verticalAlign: 'bottom',
                  textAlign: 'right'
                }}>
                <p>
                  Invoice ID:<br />
                  <NoLocalize>{charge.id}</NoLocalize>
                </p>
                <p>
                  Date:<br />
                  <NoLocalize>
                    {formatDate(charge.created, {format: 'MMMM D, YYYY', showTime: true})}
                  </NoLocalize>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
        <table className="ui very basic large table" style={{marginTop: '2.25rem'}}>
          <thead>
            <tr>
              <th>Description</th>
              <th>{charge.refunded ? 'Refund amount' : 'Amount'}</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className={NO_LOCALIZE}>{charge.statement_descriptor || charge.description}</td>
              <td>
                <NoLocalize>
                  {amount} {currency}
                </NoLocalize>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th style={{textAlign: 'right'}}>Total:</th>
              <th>
                <NoLocalize el="b">
                  {amount} {currency}
                </NoLocalize>
              </th>
            </tr>
          </tfoot>
        </table>
      </section>
      <Button primary onClick={printInvoice} className="right floated">
        <i className="icon print" />
        Print invoice
      </Button>
    </sections>
  );
};

InvoiceView.propTypes = {
  charge: PropTypes.object,
  profile: PropTypes.object,
  address: PropTypes.object
};

export default InvoiceView;
