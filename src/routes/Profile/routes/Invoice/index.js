import Invoice from './containers/InvoiceContainer';

export default {
  path: 'invoice/:chargeId',
  component: Invoice
};
