import NewSender from './containers/NewSenderContainer';
import RequireSenders from 'containers/RequireSenders';

export default {
  path: 'senders/new',
  component: RequireSenders(NewSender)
};
