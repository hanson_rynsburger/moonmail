import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Button from 'components/Button';

const NewSenderView = ({fields, handleSubmit, invalid, isCreating, createSender}) => {
  const submit = formProps => {
    createSender(formProps);
  };
  return (
    <section className="ui vertical segment">
      <h2 className="ui header">Create Sender</h2>
      <div className="ui stackable vertically padded grid">
        <form className="ui form nine wide column" onSubmit={handleSubmit(submit)}>
          {Object.keys(fields).map(key => <Input key={key} type="text" {...fields[key]} />)}
          <Button primary loading={isCreating} type="submit" disabled={invalid || isCreating}>
            Create
          </Button>
        </form>
      </div>
    </section>
  );
};

NewSenderView.propTypes = {
  isCreating: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  createSender: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  fields: PropTypes.object.isRequired
};

export default NewSenderView;
