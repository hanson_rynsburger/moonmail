import {reduxForm} from 'redux-form';
import NewSenderView from '../components/NewSenderView';
import Validator, {createValidator} from 'lib/validator';
import selectors from 'modules/senders/selectors';
import actions from 'modules/senders/actions';
import {BLACKLISTED_SENDER_DOMAINS} from 'modules/senders/constants';

const mapStateToProps = state => ({
  isCreating: selectors.getIsCreating(state)
});

Validator.register(
  'senderEmail',
  value => !new RegExp(`@(${BLACKLISTED_SENDER_DOMAINS.join('|')}).[a-zA-Z]{2,}$`).test(value),
  'This domain is not allowed. Please use you custom domain.'
);

export default reduxForm(
  {
    form: 'newSender',
    fields: ['fromName', 'email'],
    validate: createValidator({
      email: 'required|email|senderEmail',
      fromName: 'fromName'
    })
  },
  mapStateToProps,
  actions
)(NewSenderView);
