import {reduxForm} from 'redux-form';
import PhoneConfirm from '../components/PhoneConfirm';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';
import {createValidator} from 'lib/validator';

const mapStateToProps = state => ({
  isCheckingCode: selectors.getIsCheckingCode(state),
  currentPhoneNumber: selectors.getPhoneNumber(state)
});

export default reduxForm(
  {
    form: 'phoneConfirm',
    fields: ['verificationCode'],
    validate: createValidator({
      verificationCode: 'required|size:4'
    })
  },
  mapStateToProps,
  actions
)(PhoneConfirm);
