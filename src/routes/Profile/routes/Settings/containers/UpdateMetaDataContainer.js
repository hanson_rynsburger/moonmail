import {reduxForm} from 'redux-form';
import UpdateMetaData from '../components/UpdateMetaData';
import {getProfile} from 'modules/profile/selectors';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';
import {fetchHints} from 'modules/hints/actions';

const mapStateToProps = state => {
  const profile = getProfile(state);
  return {
    isUpdatingMetaData: selectors.getIsUpdatingMetaData(state),
    initialValues: {isAutoUpgradeEnabled: true, ...profile.user_metadata}
  };
};

export default reduxForm(
  {
    form: 'updateMetaData',
    fields: ['isHintsDisabled', 'isAutoUpgradeEnabled']
  },
  mapStateToProps,
  {...actions, fetchHints}
)(UpdateMetaData);
