import {reduxForm} from 'redux-form';
import UpdateCreds from '../components/UpdateCreds';
import {getProfile, getIsSesVerified} from 'modules/profile/selectors';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';
import {createValidator} from 'lib/validator';

const mapStateToProps = state => {
  const profile = getProfile(state);
  const ses = profile.ses || {};
  return {
    isUpdatingCreds: selectors.getIsUpdatingCreds(state),
    isSesVerified: getIsSesVerified(state),
    initialValues: {region: 'us-east-1', ...ses}
  };
};

export default reduxForm(
  {
    form: 'updateCreds',
    fields: ['apiKey', 'apiSecret', 'region'],
    validate: createValidator({
      apiKey: 'required',
      apiSecret: 'required',
      region: 'required'
    })
  },
  mapStateToProps,
  actions
)(UpdateCreds);
