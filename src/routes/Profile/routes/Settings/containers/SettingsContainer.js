import {connect} from 'react-redux';
import SettingsView from '../components/SettingsView';
import {getProfile, getIsSesUser, getIsPhoneVerified} from 'modules/profile/selectors';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';

const mapStateToProps = state => {
  const profile = getProfile(state);
  const isPhoneVerified = getIsPhoneVerified(state);
  return {
    isPhoneVerified,
    isSesUser: getIsSesUser(state),
    phoneNumber: profile.phoneNumber,
    isVerificationStarted: selectors.getIsPhoneVerificationStarted(state),
    isEditingPhone: selectors.getIsEditingPhone(state) || !isPhoneVerified
  };
};

export default connect(mapStateToProps, actions)(SettingsView);
