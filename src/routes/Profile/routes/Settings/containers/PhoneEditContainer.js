import {reduxForm} from 'redux-form';
import PhoneEdit from '../components/PhoneEdit';
import {getProfile} from 'modules/profile/selectors';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';
import countries, {findByCode, findByPhone} from 'lib/countries';
import {createValidator} from 'lib/validator';

const getCountry = profile => {
  if (profile.phoneVerified) {
    return findByPhone(profile.phoneNumber);
  } else if (profile.geoip) {
    return findByCode(profile.geoip.country_code);
  }
  return {};
};

const mapStateToProps = state => {
  const profile = getProfile(state);
  const country = getCountry(profile);
  return {
    countries,
    isVerifying: selectors.getIsVerifying(state),
    isPhoneVerified: profile.phoneVerified,
    currentPhoneNumber: profile.phoneNumber,
    initialValues: {
      countryCode: country.phoneCode,
      phoneNumber: profile.phoneNumber
    }
  };
};

export default reduxForm(
  {
    form: 'phoneEdit',
    fields: ['phoneNumber', 'countryCode'],
    validate: createValidator({
      phoneNumber: 'phoneNumber'
    })
  },
  mapStateToProps,
  actions
)(PhoneEdit);
