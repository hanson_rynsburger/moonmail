import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Button from 'components/Button';
import NoLocalize from 'components/NoLocalize';

const SettingsForm = ({
  fields: {verificationCode},
  handleSubmit,
  invalid,
  phoneVerificationCheck,
  phoneVerificationCancel,
  isCheckingCode,
  currentPhoneNumber
}) => {
  const submit = ({verificationCode}) => {
    phoneVerificationCheck({
      verificationCode,
      phoneNumber: currentPhoneNumber
    });
  };
  const cancel = e => {
    e.preventDefault();
    phoneVerificationCancel();
  };
  return (
    <div>
      <div className="ui info message">
        <p>
          We've sent you an SMS on number
          <NoLocalize el="b"> {currentPhoneNumber} </NoLocalize>
          with a code. Please enter it below.
        </p>
      </div>
      <form className="ui form" onSubmit={handleSubmit(submit)}>
        <div className="three fields">
          <Input {...verificationCode} mask="9999" maskChar="" type="text" />
        </div>
        <Button onClick={cancel}>Cancel</Button>
        <Button primary type="submit" loading={isCheckingCode} disabled={invalid || isCheckingCode}>
          Confirm
        </Button>
      </form>
    </div>
  );
};

SettingsForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  phoneVerificationCheck: PropTypes.func.isRequired,
  phoneVerificationCancel: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isCheckingCode: PropTypes.bool.isRequired,
  fields: PropTypes.object.isRequired,
  currentPhoneNumber: PropTypes.string
};

export default SettingsForm;
