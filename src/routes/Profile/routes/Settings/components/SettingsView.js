import React from 'react';
import PropTypes from 'prop-types';
import PhoneEdit from '../containers/PhoneEditContainer';
import PhoneConfirm from '../containers/PhoneConfirmContainer';
import PhoneView from './PhoneView';
import UpdateCreds from '../containers/UpdateCredsContainer';
import UpdateMetaData from '../containers/UpdateMetaDataContainer';
import DotHint from 'components/DotHint';
import Link from 'react-router/lib/Link';

const SettingsView = ({
  isPhoneVerified,
  isSesUser,
  phoneNumber,
  isVerificationStarted,
  isEditingPhone,
  phoneEditStart
}) => (
  <section className="ui vertical segment">
    <h3>Phone settings</h3>
    <div className="ui stackable grid">
      <div className="nine wide column">
        {!isEditingPhone && (
          <div>
            <PhoneView
              phoneNumber={phoneNumber}
              onEdit={phoneEditStart}
              isPhoneVerified={isPhoneVerified}
            />
          </div>
        )}
        {isEditingPhone && (
          <div>
            {!isVerificationStarted && <PhoneEdit />}
            {isVerificationStarted && <PhoneConfirm />}
          </div>
        )}
      </div>
    </div>
    <h3>Application settings</h3>
    <div className="ui stackable grid">
      <div className="nine wide column">
        <UpdateMetaData />
      </div>
    </div>
    {isSesUser && <h3>AWS IAM Credentials</h3>}
    {isSesUser && (
      <div className="ui stackable grid">
        <div className="nine wide column">
          <UpdateCreds />
        </div>
      </div>
    )}
    <h3>
      <DotHint id="profile.settings.delete_account">Danger Zone</DotHint>
    </h3>
    <Link to="/profile/delete" className="ui button negative">
      Delete Account
    </Link>
  </section>
);

SettingsView.propTypes = {
  isPhoneVerified: PropTypes.bool,
  isSesUser: PropTypes.bool,
  phoneNumber: PropTypes.string,
  isVerificationStarted: PropTypes.bool,
  isEditingPhone: PropTypes.bool,
  phoneEditStart: PropTypes.func.isRequired
};

export default SettingsView;
