import React from 'react';
import PropTypes from 'prop-types';
import CheckBox from 'components/CheckBox';
import Button from 'components/Button';
import DotHint from 'components/DotHint';

const UpdateMetaData = ({
  fields: {isHintsDisabled, isAutoUpgradeEnabled},
  handleSubmit,
  updateUserMetaData,
  isUpdatingMetaData,
  fetchHints,
  invalid
}) => {
  const disableHints = e => {
    const value = e.target.value !== 'true';
    updateUserMetaData({isHintsDisabled: value});
    if (!value) fetchHints(true);
    isHintsDisabled.onChange(e);
  };
  const disableAutoUpgrade = e => {
    updateUserMetaData({isAutoUpgradeEnabled: e.target.value !== 'true'});
    isAutoUpgradeEnabled.onChange(e);
  };
  return (
    <form className="ui form" onSubmit={handleSubmit(updateUserMetaData)}>
      <div className="field">
        <CheckBox
          {...isHintsDisabled}
          onChange={disableHints}
          toggle
          label={`${isHintsDisabled.value ? 'Enable' : 'Disable'} application hints`}
        />
        <DotHint id="profile.settings.example_hint" />
      </div>
      <div className="field">
        <CheckBox
          {...isAutoUpgradeEnabled}
          onChange={disableAutoUpgrade}
          toggle
          label="Auto upgrade"
        />
        <DotHint id="profile.settings.auto_upgrade" />
      </div>
      {false && (
        <Button
          primary
          type="submit"
          loading={isUpdatingMetaData}
          disabled={invalid || isUpdatingMetaData}>
          Update
        </Button>
      )}
    </form>
  );
};

UpdateMetaData.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  updateUserMetaData: PropTypes.func.isRequired,
  fields: PropTypes.object.isRequired,
  isUpdatingMetaData: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  fetchHints: PropTypes.func.isRequired
};

export default UpdateMetaData;
