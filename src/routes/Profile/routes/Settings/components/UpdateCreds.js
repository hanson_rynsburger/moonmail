import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import Button from 'components/Button';
import Select, {SelectItem} from 'components/Select';
import {SES_REGIONS} from 'lib/constants';

const UpdateCreds = ({
  fields: {apiKey, apiSecret, region},
  handleSubmit,
  invalid,
  isSesVerified,
  isUpdatingCreds,
  updateSesCredentials
}) => {
  return (
    <form className="ui form" onSubmit={handleSubmit(updateSesCredentials)}>
      <Input {...apiKey} disabled={isSesVerified} label="Access Key ID" />
      <Input
        {...apiSecret}
        disabled={isSesVerified}
        label="Secret Access Key"
        placeholder="••••••••••••••••••••••••••"
        type="password"
      />
      <Select {...region} disabled={isSesVerified}>
        {SES_REGIONS.map(({value, name}, i) => (
          <SelectItem key={i} value={value}>
            {name}
          </SelectItem>
        ))}
      </Select>
      {!isSesVerified && (
        <Button
          primary
          type="submit"
          loading={isUpdatingCreds}
          disabled={invalid || isUpdatingCreds}>
          {isSesVerified ? 'Update' : 'Save'}
        </Button>
      )}
    </form>
  );
};

UpdateCreds.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  updateSesCredentials: PropTypes.func.isRequired,
  isUpdatingCreds: PropTypes.bool.isRequired,
  isSesVerified: PropTypes.bool,
  invalid: PropTypes.bool.isRequired,
  fields: PropTypes.object.isRequired
};

export default UpdateCreds;
