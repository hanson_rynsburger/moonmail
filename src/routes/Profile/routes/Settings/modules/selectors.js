import {createSelector} from 'reselect';
import {stateKey} from './reducer';

const settingsSelector = state => state[stateKey];

export const getIsPhoneVerificationStarted = createSelector(
  settingsSelector,
  settings => settings.isPhoneVerificationStarted
);

export const getIsVerifying = createSelector(settingsSelector, settings => settings.isVerifying);

export const getIsCheckingCode = createSelector(
  settingsSelector,
  settings => settings.isCheckingCode
);

export const getIsEditingPhone = createSelector(
  settingsSelector,
  settings => settings.isEditingPhone
);

export const getIsUpdatingCreds = createSelector(
  settingsSelector,
  settings => settings.isUpdatingCreds
);

export const getIsUpdatingMetaData = createSelector(
  settingsSelector,
  settings => settings.isUpdatingMetaData
);

export const getPhoneNumber = createSelector(settingsSelector, settings => settings.phoneNumber);
