import * as types from './types';
import {combineReducers} from 'redux';

export const stateKey = 'profileSettings';

const phoneNumber = (state = null, action) => {
  switch (action.type) {
    case types.PHONE_VERIFICATION_START_SUCCESS:
      return action.phoneNumber;
    default:
      return state;
  }
};

const isPhoneVerificationStarted = (state = false, action) => {
  switch (action.type) {
    case types.PHONE_VERIFICATION_START_SUCCESS:
      return true;
    case types.PHONE_VERIFICATION_CHECK_CANCEL:
    case types.PHONE_VERIFICATION_CHECK_SUCCESS:
      return false;
    default:
      return state;
  }
};

const isEditingPhone = (state = false, action) => {
  switch (action.type) {
    case types.PHONE_EDIT_START:
      return true;
    case types.PHONE_EDIT_CANCEL:
    case types.PHONE_VERIFICATION_CHECK_SUCCESS:
      return false;
    default:
      return state;
  }
};

const isVerifying = (state = false, action) => {
  switch (action.type) {
    case types.PHONE_VERIFICATION_START_REQUEST:
      return true;
    case types.PHONE_VERIFICATION_START_SUCCESS:
    case types.PHONE_VERIFICATION_START_FAIL:
      return false;
    default:
      return state;
  }
};

const isUpdatingCreds = (state = false, action) => {
  switch (action.type) {
    case types.UPDATE_SES_CREDENTIALS_REQUEST:
      return true;
    case types.UPDATE_SES_CREDENTIALS_SUCCESS:
    case types.UPDATE_SES_CREDENTIALS_FAIL:
      return false;
    default:
      return state;
  }
};

const isUpdatingMetaData = (state = false, action) => {
  switch (action.type) {
    case types.UPDATE_USER_METADATA_REQUEST:
      return true;
    case types.UPDATE_USER_METADATA_SUCCESS:
    case types.UPDATE_USER_METADATA_FAIL:
      return false;
    default:
      return state;
  }
};

const isCheckingCode = (state = false, action) => {
  switch (action.type) {
    case types.PHONE_VERIFICATION_CHECK_REQUEST:
      return true;
    case types.PHONE_VERIFICATION_CHECK_SUCCESS:
    case types.PHONE_VERIFICATION_CHECK_FAIL:
    case types.PHONE_VERIFICATION_CHECK_CANCEL:
      return false;
    default:
      return state;
  }
};

export default combineReducers({
  isPhoneVerificationStarted,
  isVerifying,
  isCheckingCode,
  isEditingPhone,
  isUpdatingCreds,
  isUpdatingMetaData,
  phoneNumber
});
