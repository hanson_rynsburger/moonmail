import * as api from 'lib/api';
import * as types from './types';
import {getPhoneNumber} from './selectors';
import {getUserId} from 'modules/profile/selectors';
import {addMessage} from 'modules/messages/actions';
import {updateIntercom} from 'lib/remoteUtils';
import {updateProfileLocally} from 'modules/profile/actions';

export const phoneEditStart = () => ({
  type: types.PHONE_EDIT_START
});

export const phoneEditCancel = () => ({
  type: types.PHONE_EDIT_CANCEL
});

export const phoneVerificationStart = ({phoneNumber}) => {
  return async dispatch => {
    dispatch({
      type: types.PHONE_VERIFICATION_START_REQUEST
    });
    try {
      await api.phoneVerificationStart({phoneNumber});
      dispatch({
        type: types.PHONE_VERIFICATION_START_SUCCESS,
        phoneNumber
      });
      dispatch(
        addMessage({
          style: 'success',
          text: 'You should receive SMS with verification code'
        })
      );
    } catch (error) {
      dispatch({
        type: types.PHONE_VERIFICATION_START_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const phoneVerificationCancel = () => ({
  type: types.PHONE_VERIFICATION_CHECK_CANCEL
});

export const phoneVerificationCheck = ({verificationCode}) => {
  return async (dispatch, getState) => {
    const phoneNumber = getPhoneNumber(getState());
    dispatch({
      type: types.PHONE_VERIFICATION_CHECK_REQUEST
    });
    try {
      const result = await api.phoneVerificationCheck({
        phoneNumber,
        verificationCode
      });
      dispatch(updateProfileLocally(result));
      dispatch({
        type: types.PHONE_VERIFICATION_CHECK_SUCCESS
      });
      dispatch(
        addMessage({
          style: 'success',
          text: 'Your phone has been successfully verified'
        })
      );
    } catch (error) {
      dispatch({
        type: types.PHONE_VERIFICATION_CHECK_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const updateSesCredentials = ({apiKey, apiSecret, region}) => {
  return async dispatch => {
    dispatch({
      type: types.UPDATE_SES_CREDENTIALS_REQUEST
    });
    try {
      const user = await api.updateSesCredentials({apiKey, apiSecret, region});
      dispatch(updateProfileLocally(user));
      dispatch({
        type: types.UPDATE_SES_CREDENTIALS_SUCCESS
      });
      dispatch(
        addMessage({
          style: 'success',
          text: 'Your AWS Api keys have been successfully updated'
        })
      );
      dispatch(updateIntercom({has_amazon_api_key: true}));
    } catch (error) {
      dispatch({
        type: types.UPDATE_SES_CREDENTIALS_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const updateUserMetaData = (data, isSilent = false) => {
  return async (dispatch, getState) => {
    const userId = getUserId(getState());
    dispatch({
      type: types.UPDATE_USER_METADATA_REQUEST
    });
    try {
      const user = await api.updateUserMetaData(userId, data);
      dispatch(updateProfileLocally(user));
      dispatch({
        type: types.UPDATE_USER_METADATA_SUCCESS
      });
      !isSilent &&
        dispatch(
          addMessage({
            style: 'success',
            text: 'Your settings have been successfully updated'
          })
        );
    } catch (error) {
      dispatch({
        type: types.UPDATE_USER_METADATA_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};
