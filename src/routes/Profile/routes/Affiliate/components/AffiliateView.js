import React from 'react';
import PropTypes from 'prop-types';
import {
  FacebookShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  VKShareButton
} from 'react-share';
import Input from 'components/Input';
import Button from 'components/Button';
import KeyInput from 'components/KeyInput';

export const AffiliateView = ({
  profile,
  copyToClipboard,
  fields: {payPalEmail},
  updateProfile,
  invalid,
  isUpdating,
  handleSubmit
}) => {
  const submit = formProps => {
    updateProfile(formProps, 'Your PayPal email was updated.');
  };
  return (
    <section className="ui vertical segment">
      <p>
        Invite your friends to register in MoonMail using the link below and get <b>25%</b> of all
        their spendings.
      </p>
      <div className="ui stackable grid">
        <div className="ui form nine wide column">
          <div className="field">
            <label htmlFor="referralLink">Your referral link</label>
            <KeyInput
              id="referralLink"
              value={profile.referralLink}
              onCopy={() => copyToClipboard(profile.referralLink, 'Referral link')}
            />
          </div>
          <div className="field">
            <label>You can share this link in</label>
            <div className="ui input">
              <div>
                <FacebookShareButton
                  url={profile.referralLink}
                  className="ui circular facebook icon button">
                  <i className="facebook f icon" />
                </FacebookShareButton>
                <VKShareButton url={profile.referralLink} className="ui circular vk icon button">
                  <i className=" vk icon" />
                </VKShareButton>
                <TwitterShareButton
                  url={profile.referralLink}
                  className="ui circular twitter icon button">
                  <i className="twitter icon" />
                </TwitterShareButton>
                <LinkedinShareButton
                  url={profile.referralLink}
                  className="ui circular linkedin icon button">
                  <i className="linkedin icon" />
                </LinkedinShareButton>
                <GooglePlusShareButton
                  url={profile.referralLink}
                  className="ui circular google plus icon button">
                  <i className="google plus icon" />
                </GooglePlusShareButton>
              </div>
            </div>
          </div>
        </div>
        <form className="ui form nine wide column" onSubmit={handleSubmit(submit)}>
          <p>
            Please provide your{' '}
            <a href="https://www.paypal.com" target="_blank" rel="noopener noreferrer">
              PayPal
            </a>{' '}
            email. We're releasing payments once the balance is more than $25
          </p>
          <Input
            label={false}
            leftLabelClass="ui basic icon button"
            leftLabel={<i className="paypal blue icon" />}
            placeholder="email@example.com"
            {...payPalEmail}
          />
          <Button primary type="submit" loading={isUpdating} disabled={invalid}>
            Save email address
          </Button>
        </form>
      </div>
    </section>
  );
};

AffiliateView.propTypes = {
  profile: PropTypes.shape({
    name: PropTypes.string
  }).isRequired,
  copyToClipboard: PropTypes.func.isRequired,
  fields: PropTypes.shape({
    payPalEmail: PropTypes.object
  }).isRequired,
  updateProfile: PropTypes.func,
  handleSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isUpdating: PropTypes.bool
};

export default AffiliateView;
