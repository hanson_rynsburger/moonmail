import {reduxForm} from 'redux-form';
import {createValidator} from 'lib/validator';
import AffiliateView from '../components/AffiliateView';
import * as selectors from 'modules/profile/selectors';
import * as actions from 'modules/profile/actions';
import {copyToClipboard} from 'modules/messages/actions';

const mapStateToProps = state => {
  const profile = selectors.getProfile(state);
  return {
    profile,
    initialValues: profile,
    isUpdating: selectors.getIsUpdating(state)
  };
};

export default reduxForm(
  {
    form: 'contactInfo',
    fields: ['payPalEmail'],
    validate: createValidator({
      payPalEmail: 'required|email'
    })
  },
  mapStateToProps,
  {...actions, copyToClipboard}
)(AffiliateView);
