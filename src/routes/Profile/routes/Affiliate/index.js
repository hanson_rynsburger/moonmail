import Affiliate from './containers/AffiliateContainer';

// Sync route definition
export default {
  path: 'affiliate',
  component: Affiliate
};
