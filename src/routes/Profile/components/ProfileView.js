import React from 'react';
import PropTypes from 'prop-types';
import ProfileHeader from './ProfileHeader';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';

export const ProfileView = props => (
  <section className="ui basic segment padded-bottom">
    <h1 className={cx('ui header', NO_LOCALIZE)}>{props.profile.name}</h1>
    <ProfileHeader {...props} />
    {props.children}
  </section>
);

ProfileView.propTypes = {
  children: PropTypes.element.isRequired,
  profile: PropTypes.shape({
    name: PropTypes.string
  }).isRequired
};

export default ProfileView;
