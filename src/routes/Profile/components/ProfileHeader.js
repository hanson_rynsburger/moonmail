import React from 'react';
import PropTypes from 'prop-types';
import DropDownMenu from 'components/DropDownMenu';
import {Mobile, Desktop} from 'components/MediaQueries';
import {getRelativeItemLink} from 'components/Links';
import {Aux} from 'lib/utils';

const Link = getRelativeItemLink('/profile/');

const ProfileHeader = ({paymentMethod, isExpert, isAmazonUser}) => {
  const links = {};
  links.index = <Link index>Overview</Link>;
  links.billingInfo = <Link to="billing-info">Billing information</Link>;
  links.billingHistory = <Link to="billing-history">Billing history</Link>;
  links.senders = <Link to="senders">Senders</Link>;
  links.authorizations = <Link to="authorizations">Authorized users</Link>;
  links.experts = (
    <Link to="experts">{isExpert ? 'MoonMail Expert' : 'Become a MoonMail Expert'}</Link>
  );
  links.affiliateConversions = <Link to="affiliate-conversions">Your conversions</Link>;
  links.affiliatePayouts = <Link to="affiliate-payouts">Your payouts</Link>;
  links.contactInfo = <Link to="contact-info">Contact Information</Link>;
  links.settings = <Link to="settings">Settings</Link>;
  if (!isAmazonUser) {
    links.plan = <Link to="plan">Your Plan</Link>;
  }
  return (
    <div className="ui secondary pointing large menu no-padding">
      <Desktop>
        {links.index}
        {links.plan}
        {!isAmazonUser &&
          paymentMethod && (
            <DropDownMenu className="item">
              Billing
              <i className="dropdown icon" />
              <div className="menu">
                {links.billingInfo}
                {links.billingHistory}
              </div>
            </DropDownMenu>
          )}
        {links.senders}
        {links.authorizations}
        {links.experts}
        <DropDownMenu className="item">
          Affiliate program
          <i className="dropdown icon" />
          <div className="menu">
            <Link to="affiliate">Overview</Link>
            {links.affiliateConversions}
            {links.affiliatePayouts}
          </div>
        </DropDownMenu>
        <DropDownMenu className="item">
          More
          <i className="dropdown icon" />
          <div className="menu">
            {links.contactInfo}
            {links.settings}
          </div>
        </DropDownMenu>
      </Desktop>
      <Mobile>
        {links.index}
        {links.plan}
        <DropDownMenu className="item">
          More
          <i className="dropdown icon" />
          <div className="left menu transition hidden">
            {!isAmazonUser &&
              paymentMethod && (
                <Aux>
                  {links.billingInfo}
                  {links.billingHistory}
                </Aux>
              )}
            {links.senders}
            {links.authorizations}
            {links.experts}
            <Link to="affiliate">Affiliate program</Link>
            {links.affiliateConversions}
            {links.affiliatePayouts}
            {links.contactInfo}
            {links.settings}
          </div>
        </DropDownMenu>
      </Mobile>
    </div>
  );
};

ProfileHeader.propTypes = {
  isExpert: PropTypes.bool,
  isAmazonUser: PropTypes.bool,
  paymentMethod: PropTypes.object
};

export default ProfileHeader;
