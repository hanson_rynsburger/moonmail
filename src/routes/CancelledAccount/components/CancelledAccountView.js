import React from 'react';
import classNames from './CancelledAccount.scss';
import Button from 'components/Button';
import Footer from 'components/Footer';
import {Aux} from 'lib/utils';

const CancelledAccountView = () => (
  <Aux>
    <section className={classNames.container}>
      <section className="ui basic segment">
        <h1 className="ui center aligned icon header">
          <i className="ban red icon" />
          <div className="content">
            Account had been cancelled.
            <div className="sub header">
              <p>If you cancelled the account by mistake, please contact us.</p>
              <p>
                <Button to="/signin">Login to another account</Button>
                <a className="ui button primary" href="mailto:hi@moonmail.io">
                  Contact us
                </a>
              </p>
            </div>
          </div>
        </h1>
      </section>
    </section>
    <Footer />
  </Aux>
);

export default CancelledAccountView;
