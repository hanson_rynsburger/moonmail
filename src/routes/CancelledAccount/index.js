import CancelledAccountContainer from './containers/CancelledAccountContainer';

// Sync route definition
export default {
  path: '/cancelled-account',
  component: CancelledAccountContainer,
  isPlain: true
};
