import React from 'react';
import CancelledAccountView from '../components/CancelledAccountView';
import {connect} from 'react-redux';
import {getIsCancelled} from 'modules/profile/selectors';
import NotFound from 'routes/NotFound/components/NotFoundView';

const CancelledAccount = ({isCancelled}) => (isCancelled ? <CancelledAccountView /> : <NotFound />);

const mapStateToProps = state => ({
  isCancelled: getIsCancelled(state)
});

export default connect(mapStateToProps)(CancelledAccount);
