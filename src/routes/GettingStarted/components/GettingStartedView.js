import React from 'react';
import PropTypes from 'prop-types';
import UpdateCreds from 'routes/Profile/routes/Settings/containers/UpdateCredsContainer';
import classNames from './GettingStartedView.scss';
import cx from 'classnames';
import YouTube from 'react-youtube';

const GettingStartedView = ({toggleVideo, isVideoVisible, setupLater}) => (
  <section className="ui basic segment padded-bottom">
    <section className="ui very padded teal segment">
      <div className="ui large dividing header">
        Ready to connect your AWS-SES account with MoonMail?
        <div className="sub header">Follow the next steps:</div>
      </div>
      <div className="ui large relaxed list">
        <div className="item">
          1.{' '}
          <a
            href="https://portal.aws.amazon.com/billing/signup"
            target="_blank"
            rel="noopener noreferrer">
            Create a free AWS account
          </a>{' '}
          if you don't have one.
        </div>
        <div className="item">
          2. Create a new user and attach to him the <b>AmazonSESFullAccess</b> policy.
        </div>
        <div className="item">
          <a href="" onClick={toggleVideo}>
            {isVideoVisible ? 'Hide the video instructions' : 'Show me the video instructions'}
          </a>{' '}
          or{' '}
          <a
            href={
              'http://support.moonmail.io/use-your-own-aws-ses-account/' +
              'how-do-i-connect-my-amazon-ses-account-with-moonmail'
            }
            target="_blank"
            rel="noopener noreferrer">
            click here
          </a>{' '}
          to read the Installation instructions
        </div>
        {isVideoVisible && (
          <div>
            <div className={cx('ui embed', classNames.video)}>
              <YouTube videoId="AC_eLB8ghZI" />
            </div>
            <br />
          </div>
        )}
        <div className="item">
          3. Paste <b>User Security Credentials</b> into the form below.
        </div>
        <div className="item">
          <div className="ui stackable vertically padded grid">
            <div className="nine wide column">
              <UpdateCreds />
            </div>
          </div>
        </div>
        <div className="item">
          <a href="#" onClick={setupLater}>
            I will add my AWS credentials later
          </a>
        </div>
      </div>
    </section>
  </section>
);

GettingStartedView.propTypes = {
  toggleVideo: PropTypes.func.isRequired,
  isVideoVisible: PropTypes.bool.isRequired,
  setupLater: PropTypes.func.isRequired
};

export default GettingStartedView;
