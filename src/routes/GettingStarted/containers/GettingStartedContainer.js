import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getIsSesVerified, getIsSesUser} from 'modules/profile/selectors';
import GettingStartedView from '../components/GettingStartedView';

class GettingStarted extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVideoVisible: false
    };
  }

  static propTypes = {
    isSesVerified: PropTypes.bool.isRequired,
    isSesUser: PropTypes.bool.isRequired,
    router: PropTypes.object,
    pathname: PropTypes.string
  };

  componentWillMount() {
    const {isSesVerified, isSesUser, pathname, router} = this.props;
    if (!isSesUser || isSesVerified) {
      router.replace(pathname);
    }
  }

  componentWillUpdate(nextProps) {
    const {isSesVerified, isSesUser, pathname, router} = nextProps;
    if (!isSesUser || isSesVerified) {
      router.replace(pathname);
    }
  }

  toggleVideo = e => {
    e.preventDefault();
    this.setState({
      isVideoVisible: !this.state.isVideoVisible
    });
  };

  setupLater = e => {
    e.preventDefault();
    this.props.router.replace(this.props.pathname);
  };

  render() {
    return (
      <GettingStartedView
        setupLater={this.setupLater}
        toggleVideo={this.toggleVideo}
        isVideoVisible={this.state.isVideoVisible}
        {...this.props}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    isSesVerified: getIsSesVerified(state),
    isSesUser: getIsSesUser(state),
    pathname: '/'
  };
};

export default connect(mapStateToProps)(GettingStarted);
