import GettingStarted from './containers/GettingStartedContainer';

// Sync route definition
export default {
  path: 'getting-started-aws-ses',
  hideAlerts: true,
  component: GettingStarted
};
