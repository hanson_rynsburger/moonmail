import {injectReducer} from 'store/reducers';
import Integrations from './containers/IntegrationsContainer';
import reducer, {stateKey} from './modules/reducer';

export default store => {
  injectReducer(store, {key: stateKey, reducer});
  return {
    path: 'integrations',
    component: Integrations
  };
};
