import PropTypes from 'prop-types';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as selectors from '../modules/selectors';
import * as actions from '../modules/actions';
import zapierTemplateActions from 'modules/zapierTemplates/actions';
import zapierTemplateSelectors from 'modules/zapierTemplates/selectors';
import {getApiKey} from 'modules/profile/selectors';
import {copyToClipboard} from 'modules/messages/actions';
import IntegrationsView from '../components/IntegrationsView';

class Integrations extends Component {
  static propTypes = {
    fetchZapierTemplates: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchZapierTemplates({limit: 100});
  }

  render() {
    return <IntegrationsView {...this.props} />;
  }
}

const mapStateToProps = state => ({
  isGenerating: selectors.getIsGenerating(state),
  isRevoking: selectors.getIsRevoking(state),
  isRevokeStarted: selectors.getIsRevokeStarted(state),
  apiKey: getApiKey(state),
  zapierTemplates: zapierTemplateSelectors.getResources(state),
  isFetchingAll: zapierTemplateSelectors.getIsFetchingAll(state)
});

export default connect(mapStateToProps, {
  ...actions,
  copyToClipboard,
  fetchZapierTemplates: zapierTemplateActions.fetchZapierTemplates
})(Integrations);
