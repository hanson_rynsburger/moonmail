import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import Confirm from 'components/Modal/Confirm';
import KeyInput from 'components/KeyInput';
import IntegrationItem from './IntegrationItem';
import ResourceList from 'components/ResourceList';
import cx from 'classnames';
import classNames from './IntegrationsView.scss';

class IntegrationsView extends Component {
  static propTypes = {
    apiKey: PropTypes.string,
    isGenerating: PropTypes.bool.isRequired,
    isRevoking: PropTypes.bool.isRequired,
    isRevokeStarted: PropTypes.bool.isRequired,
    generateApiKey: PropTypes.func.isRequired,
    revokeApiKey: PropTypes.func.isRequired,
    revokeApiKeyStart: PropTypes.func.isRequired,
    revokeApiKeyCancel: PropTypes.func.isRequired,
    copyToClipboard: PropTypes.func.isRequired,
    zapierTemplates: PropTypes.array,
    isFetchingAll: PropTypes.bool.isRequired
  };

  render() {
    const {
      apiKey,
      isGenerating,
      isRevoking,
      isRevokeStarted,
      generateApiKey,
      revokeApiKey,
      revokeApiKeyStart,
      revokeApiKeyCancel,
      copyToClipboard,
      zapierTemplates,
      isFetchingAll
    } = this.props;
    return (
      <section className="ui basic segment padded-bottom">
        <section className="ui very padded teal segment padded-bottom">
          <div className="ui stackable grid">
            <div className="ui form nine wide column">
              <h2 className="ui header">MoonMail API</h2>
              <p>
                MoonMail Recipients API lets you easily inject, repair and burn off recipients and
                metafields from your space. All requests are authenticated using an api-key.
              </p>
              <p>
                Check out our{' '}
                <a href="http://docs.moonmail.io/" target="_blank" rel="noopener noreferrer">
                  API documentation
                </a>
              </p>
              {apiKey && (
                <div className="field">
                  <label htmlFor="apiKey">API key</label>
                  <KeyInput onCopy={() => copyToClipboard(apiKey, 'API Key')} value={apiKey} />
                </div>
              )}
              {apiKey ? (
                <Button onClick={revokeApiKeyStart}>Revoke API key</Button>
              ) : (
                <Button primary onClick={generateApiKey} loading={isGenerating}>
                  Generate API key
                </Button>
              )}
            </div>
          </div>
          <Confirm
            isOpen={isRevokeStarted}
            isLoading={isRevoking}
            onCancel={revokeApiKeyCancel}
            onConfirm={revokeApiKey}
            confirmText="Revoke API key"
            confirmClass="negative">
            <p>
              This action will revoke API key. All MoonMail Zapier Integrations and other apps using
              this API key will stop working.
            </p>
            <p>
              <b>Careful, you can not undo this operation!</b>
            </p>
          </Confirm>

          <h2 className="ui header">Zapier Integrations</h2>
          <p>
            You can use{' '}
            <a
              href="https://zapier.com/apps/moonmail/integrations"
              target="_blank"
              rel="noopener noreferrer">
              <b>MoonMail Zapier</b>
            </a>{' '}
            Integration to automatically subscribe people (email addresses) from other apps into
            your lists. <br />
            Use your <b>API Key</b> to connect MonnMail with Zapier. Check out popular integrations
            listed below or{' '}
            <a
              href="https://zapier.com/apps/moonmail/integrations"
              target="_blank"
              rel="noopener noreferrer">
              <b>create your own</b>
            </a>!
          </p>
          <ResourceList
            isFetching={isFetchingAll}
            resources={zapierTemplates}
            dimmerClass={classNames.dimmer}
            pageSize={20}
            className={cx('ui cards', classNames.list)}
            resourceName="Zapier templates">
            {(zapierTemplate, i) => <IntegrationItem key={i} zapierTemplate={zapierTemplate} />}
          </ResourceList>
        </section>
      </section>
    );
  }
}

export default IntegrationsView;
