import React from 'react';
import PropTypes from 'prop-types';
import classNames from './IntegrationsView.scss';
import cx from 'classnames';
import Button from 'components/Button';
import {popupWindow} from 'lib/utils';

const openZapEditor = zapierTemplate => {
  const url = `https://zapier.com/app/editor/template/${zapierTemplate.id}`;
  popupWindow(url, zapierTemplate.title, 1000, 700);
};

const IntegrationItem = ({zapierTemplate = {}}) => {
  const summary = zapierTemplate.description_plain.split('\n')[0];
  const services = [];
  zapierTemplate.steps.forEach((step, i) => {
    if (i > 0) {
      services.push(
        <i key={`zap-arrow-${i}`} className={cx(classNames.arrow, 'icon large grey caret right')} />
      );
    }
    services.push(
      <div key={`zap-service-${i}`} className={classNames.zapService}>
        <img src={step.images['url_128x128']} />
      </div>
    );
  });
  return (
    <div className={cx('ui card', classNames.item)}>
      <div className={classNames.zapServices}>{services}</div>
      <div className="content">
        <div className="header">{zapierTemplate.title}</div>
        <div className="description">
          <p>{summary}</p>
        </div>
      </div>
      <Button className="bottom attached" primary onClick={() => openZapEditor(zapierTemplate)}>
        Use this Zap
      </Button>
    </div>
  );
};

IntegrationItem.propTypes = {
  zapierTemplate: PropTypes.object.isRequired
};

export default IntegrationItem;
