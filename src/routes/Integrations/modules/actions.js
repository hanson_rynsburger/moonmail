import * as api from 'lib/api';
import * as types from './types';
import {addMessage} from 'modules/messages/actions';
import {updateProfileLocally} from 'modules/profile/actions';

export const generateApiKey = () => {
  return async dispatch => {
    dispatch({type: types.GENERATE_API_KEY_REQUEST});
    try {
      const data = await api.generateApiKey();
      dispatch(updateProfileLocally(data));
      dispatch({type: types.GENERATE_API_KEY_SUCCESS});
    } catch (error) {
      dispatch({
        type: types.GENERATE_API_KEY_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const revokeApiKeyStart = () => ({
  type: types.REVOKE_API_KEY_START
});

export const revokeApiKeyCancel = () => ({
  type: types.REVOKE_API_KEY_CANCEL
});

export const revokeApiKey = () => {
  return async dispatch => {
    dispatch({type: types.REVOKE_API_KEY_REQUEST});
    try {
      await api.revokeApiKey();
      dispatch(updateProfileLocally({apiKey: null}));
      dispatch({type: types.REVOKE_API_KEY_SUCCESS});
    } catch (error) {
      dispatch({
        type: types.REVOKE_API_KEY_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};
