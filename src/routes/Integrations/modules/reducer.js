import * as types from './types';
import {combineReducers} from 'redux';
import {createAsyncFlagReducer, createStartFlagReducer} from 'lib/reduxUtils';

export const stateKey = 'apiKey';
const isGenerating = createAsyncFlagReducer(types, 'generateApiKey');
const isRevoking = createAsyncFlagReducer(types, 'revokeApiKey');
const isRevokeStarted = createStartFlagReducer(types, 'revokeApiKey');

export default combineReducers({isGenerating, isRevoking, isRevokeStarted});
