import {createStateSelector, keySelectorFactory} from 'lib/reduxUtils';
import {stateKey} from './reducer';

const stateSelector = createStateSelector(stateKey);
const keySelector = keySelectorFactory(stateSelector);
export const getIsGenerating = keySelector('isGenerating');
export const getIsRevoking = keySelector('isRevoking');
export const getIsRevokeStarted = keySelector('isRevokeStarted');
