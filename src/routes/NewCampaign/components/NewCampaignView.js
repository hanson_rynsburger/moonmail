import React from 'react';
import PropTypes from 'prop-types';
import SettingsForm from 'routes/Campaign/routes/Settings/containers/SettingsFromContainer';

const NewCampaignView = ({createCampaign, isCreating, ...formProps}) => {
  const submit = formProps => {
    createCampaign(formProps);
  };
  return (
    <section className="ui basic padded-bottom segment">
      <h1 className="ui header">Create Campaign</h1>
      <div className="ui stackable vertically padded grid">
        <SettingsForm
          {...formProps}
          onFormSubmit={submit}
          isLoading={isCreating}
          buttonText="Create"
        />
      </div>
    </section>
  );
};

NewCampaignView.propTypes = {
  isCreating: PropTypes.bool.isRequired,
  createCampaign: PropTypes.func.isRequired
};

export default NewCampaignView;
