import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import NewCampaignView from '../components/NewCampaignView';
import listActions from 'modules/lists/actions';
import actions from 'modules/campaigns/actions';
import selectors from 'modules/campaigns/selectors';

class NewCampaign extends Component {
  static propTypes = {
    fetchLists: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchLists();
  }

  render() {
    return <NewCampaignView {...this.props} />;
  }
}

const mapStateToProps = state => ({
  isCreating: selectors.getIsCreating(state)
});

export default connect(mapStateToProps, {
  ...actions,
  fetchLists: listActions.fetchLists
})(NewCampaign);
