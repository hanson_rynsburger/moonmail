import NewCampaign from './containers/NewCampaignContainer';

export default {
  path: '/campaigns/new',
  component: NewCampaign
};
