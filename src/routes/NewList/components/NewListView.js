import React from 'react';
import PropTypes from 'prop-types';
import SettingsForm from 'routes/List/routes/Settings/containers/SettingsFormContainer';

const NewListView = ({isCreating, createList}) => (
  <section className="ui basic padded-bottom segment">
    <h1 className="ui header">Create List</h1>
    <SettingsForm
      onFormSubmit={createList}
      isLoading={isCreating}
      initialValues={{sendEmailVerificationOnSubscribe: true}}
      buttonText="Create"
    />
  </section>
);

NewListView.propTypes = {
  isCreating: PropTypes.bool.isRequired,
  createList: PropTypes.func.isRequired
};

export default NewListView;
