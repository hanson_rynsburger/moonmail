import NewList from './containers/NewListContainer';

export default {
  path: 'lists/new',
  component: NewList
};
