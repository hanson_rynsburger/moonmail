import {connect} from 'react-redux';
import NewListView from '../components/NewListView';
import actions from 'modules/lists/actions';
import selectors from 'modules/lists/selectors';

const mapStateToProps = state => ({
  isCreating: selectors.getIsCreating(state)
});

export default connect(mapStateToProps, actions)(NewListView);
