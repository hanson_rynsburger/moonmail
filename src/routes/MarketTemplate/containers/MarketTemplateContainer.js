import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import selectors from 'modules/marketTemplates/selectors';
import actions from 'modules/marketTemplates/actions';
import MarketTemplateView from '../components/MarketTemplateView';
import {getIsTemplateManager} from 'modules/profile/selectors';

class MarketTemplateContainer extends Component {
  static propTypes = {
    fetchMarketTemplate: PropTypes.func.isRequired,
    templateId: PropTypes.string.isRequired
  };

  componentDidMount() {
    const {fetchMarketTemplate, templateId} = this.props;
    fetchMarketTemplate(templateId);
  }

  render() {
    return <MarketTemplateView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  templateId: ownProps.params.templateId,
  template: selectors.getResourceById(state, ownProps.params.templateId),
  isTemplateManager: getIsTemplateManager(state),
  isFetching: selectors.getIsFetching(state)
});

export default connect(mapStateToProps, actions)(MarketTemplateContainer);
