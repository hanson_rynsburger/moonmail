import {connect} from 'react-redux';
import OverviewView from '../components/OverviewView';
import selectors from 'modules/marketTemplates/selectors';

const mapStateToProps = (state, ownProps) => ({
  template: selectors.getResourceById(state, ownProps.params.templateId)
});

export default connect(mapStateToProps)(OverviewView);
