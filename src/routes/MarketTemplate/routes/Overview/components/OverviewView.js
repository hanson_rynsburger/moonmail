import React from 'react';
import PropTypes from 'prop-types';
import {humanize, joinComponents} from 'lib/utils';
import Button from 'components/Button';
import EmailLink from 'components/EmailLink';

const OverviewView = ({
  template: {
    images = [],
    designer = {},
    price = 0,
    tags = [],
    categories = [],
    description,
    thumbnail
  }
}) => {
  if (images.length === 0) images.push(thumbnail);
  return (
    <section>
      <div className="ui stackable grid">
        <div className="ten wide column">
          {images.map((img, i) => (
            <img key={i} src={img} className="ui bordered rounded fluid image" />
          ))}
        </div>
        <div className="six wide column">
          <div className="ui segment">
            {description && <p>{description}</p>}
            <div className="ui list">
              <div className="item">
                <b>Categories:</b> {joinComponents(categories, c => <a href="#">{humanize(c)}</a>)}
              </div>
              <div className="item">
                <b>Tags:</b> {joinComponents(tags, t => <a href="#">{humanize(t)}</a>)}
              </div>
            </div>
            {price === 0 ? (
              <Button primary fluid>
                Select this template
              </Button>
            ) : (
              <Button primary fluid>
                Buy for ${price}
              </Button>
            )}
          </div>
          <div className="ui segment">
            <h4>Designed by</h4>
            <div className="ui list">
              <div className="item">
                <a href={designer.url} target="_blank">
                  {designer.name}
                </a>
              </div>
              <div>
                <EmailLink to={designer.email} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

OverviewView.propTypes = {
  template: PropTypes.object.isRequired
};

export default OverviewView;
