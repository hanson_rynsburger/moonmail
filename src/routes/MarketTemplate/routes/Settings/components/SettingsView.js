import React from 'react';
import PropTypes from 'prop-types';
import PublishForm from 'routes/PublishTemplate/containers/PublishTemplateFormContainer';

const SettingsView = ({template, isUpdating, updateMarketTemplate}) => (
  <section>
    <PublishForm
      initialValues={template}
      isPublishing={isUpdating}
      buttonText="Save"
      onPublish={data => updateMarketTemplate(template.id, data)}
    />
  </section>
);

SettingsView.propTypes = {
  template: PropTypes.object.isRequired,
  isUpdating: PropTypes.bool.isRequired,
  updateMarketTemplate: PropTypes.func.isRequired
};

export default SettingsView;
