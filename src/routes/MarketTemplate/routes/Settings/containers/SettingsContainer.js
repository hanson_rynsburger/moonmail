import React from 'react';
import {connect} from 'react-redux';
import SettingsView from '../components/SettingsView';
import selectors from 'modules/marketTemplates/selectors';
import actions from 'modules/marketTemplates/actions';
import {getIsTemplateManager} from 'modules/profile/selectors';
import NotFound from 'routes/NotFound/components/NotFoundView';

const SettingsContainer = props => {
  if (!props.isTemplateManager) return <NotFound />;
  return <SettingsView {...props} />;
};

const mapStateToProps = (state, ownProps) => ({
  template: selectors.getResourceById(state, ownProps.params.templateId),
  isTemplateManager: getIsTemplateManager(state),
  isUpdating: selectors.getIsUpdating(state)
});

export default connect(mapStateToProps, actions)(SettingsContainer);
