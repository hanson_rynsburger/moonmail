import MarketTemplate from './containers/MarketTemplateContainer';
import Overview from './routes/Overview';
import Settings from './routes/Settings';

export default {
  path: '/market-templates/:templateId',
  indexRoute: Overview,
  component: MarketTemplate,
  hideFooter: true,
  childRoutes: [Settings]
};
