import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/Loader';
import Header from './MarketTemplateHeader';

const MarketTemplateView = ({template, children, isTemplateManager}) => {
  if (!template.id) {
    return (
      <section className="ui basic segment">
        <Loader active dimmer />
      </section>
    );
  }
  return (
    <section className="ui basic segment padded-bottom">
      <h1>{template.name}</h1>
      {isTemplateManager && <Header templateId={template.id} />}
      {children}
    </section>
  );
};

MarketTemplateView.propTypes = {
  template: PropTypes.object.isRequired,
  children: PropTypes.element.isRequired,
  isTemplateManager: PropTypes.bool.isRequired
};

export default MarketTemplateView;
