import React from 'react';
import PropTypes from 'prop-types';
import {getRelativeItemLink} from 'components/Links/index';

const MarketTemplateHeader = ({templateId}) => {
  const Link = getRelativeItemLink(`/market-templates/${templateId}/`);
  return (
    <div className="ui secondary pointing large menu no-padding">
      <Link index>Overview</Link>
      <Link to="settings">Settings</Link>
    </div>
  );
};

MarketTemplateHeader.propTypes = {
  templateId: PropTypes.string.isRequired
};

export default MarketTemplateHeader;
