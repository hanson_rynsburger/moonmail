import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import NewAutomationView from '../components/NewAutomationView';
import actions from 'modules/automations/actions';
import selectors from 'modules/automations/selectors';
import listActions from 'modules/lists/actions';

class NewAutomation extends Component {
  static propTypes = {
    fetchLists: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.fetchLists();
  }

  render() {
    return <NewAutomationView {...this.props} />;
  }
}

const mapStateToProps = state => ({
  isCreating: selectors.getIsCreating(state)
});

export default connect(mapStateToProps, {
  ...actions,
  fetchLists: listActions.fetchLists
})(NewAutomation);
