import NewAutomation from './containers/NewAutomationContainer';

export default {
  path: 'automations/new',
  component: NewAutomation
};
