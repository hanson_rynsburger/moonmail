import React from 'react';
import PropTypes from 'prop-types';
import AutomationForm from 'routes/Automation/routes/Settings/containers/SettingsFormContainer';

const NewAutomationView = ({createResource, isCreating, router, ...formProps}) => {
  const submit = formProps => {
    createResource(formProps, true).then(res => {
      router.push(`/automations/${res.id}`);
    });
  };
  return (
    <section className="ui basic padded-bottom segment">
      <h1 className="ui header">Create Automation</h1>
      <div className="ui stackable vertically padded grid">
        <AutomationForm
          {...formProps}
          onFormSubmit={submit}
          isLoading={isCreating}
          buttonText="Create"
        />
      </div>
    </section>
  );
};

NewAutomationView.propTypes = {
  isCreating: PropTypes.bool.isRequired,
  createResource: PropTypes.func.isRequired,
  router: PropTypes.object.isRequired
};

export default NewAutomationView;
