import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import DropDownMenu from 'components/DropDownMenu';
import placeholder from 'static/blank-template.png';
import {NO_LOCALIZE} from 'lib/constants';
import classNames from './MarketTemplates.scss';
import Loader from 'components/Loader';
import {ItemLink} from 'components/Links';
import cx from 'classnames';
import path from 'object-path';

const TemplateItem = ({
  template,
  onSelect,
  onDelete,
  isLoading,
  isTemplateManager,
  isDisabled,
  isPurchased
}) => {
  const thumbUrl = path.get(template, 'thumbnail', placeholder);
  const getButtonText = () => {
    if (isDisabled) return 'Complete verification';
    if (isTemplateManager) return 'Get';
    if (isPurchased) return 'Select';
    if (template.price) return `Get for $${template.price}`;
    return 'Get for free';
  };
  return (
    <div className={cx('ui card', classNames.item)}>
      <Loader active={isLoading} inline={false} />
      {isPurchased && (
        <label className="ui green right corner label">
          <i className="check icon" />
        </label>
      )}
      <div className={cx('image', classNames.image)}>
        <img
          src={thumbUrl}
          className={cx({clickable: !isDisabled})}
          onClick={() => !isDisabled && onSelect(template.id)}
        />
      </div>
      <div className="content">
        <div className="description">
          <h4 className={cx('truncate', NO_LOCALIZE)}>{template.name}</h4>
        </div>
      </div>
      <div className="ui bottom attached buttons">
        <Button primary onClick={() => onSelect(template.id)} disabled={isDisabled}>
          {getButtonText()}
        </Button>
        {isTemplateManager && (
          <DropDownMenu className="button primary right labeled icon floating">
            Actions
            <i className="dropdown icon" />
            <div className="menu">
              <ItemLink to={`/market-templates/${template.id}/settings`}>Settings</ItemLink>
              <a className="item" onClick={() => onDelete(template.id)}>
                Delete
              </a>
            </div>
          </DropDownMenu>
        )}
      </div>
    </div>
  );
};

TemplateItem.propTypes = {
  template: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }),
  onSelect: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  isTemplateManager: PropTypes.bool,
  isDisabled: PropTypes.bool,
  isPurchased: PropTypes.bool
};

export default TemplateItem;
