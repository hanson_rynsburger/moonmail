import React from 'react';
import PropTypes from 'prop-types';
import MarketTemplateItem from './MarketTemplateItem';
import {PaginatedList} from 'components/ResourceList';
import Confirm from 'components/Modal/Confirm';
import NoLocalize from 'components/NoLocalize';
import classNames from './MarketTemplates.scss';
import cx from 'classnames';
import Payment from 'components/Payment';
import MarketTemplatePreview from './MarketTemplatePreview';
import DropDownMenu from 'components/DropDownMenu';
import CATEGORIES from 'modules/marketTemplates/categories';
import humanize from 'humanize-string';
import {Aux} from 'lib/utils';

const MarketTemplatesView = ({
  templates,
  isFetchingAll,
  selectedTemplate,
  isDeleteStarted,
  isDeleting,
  deleteMarketTemplate,
  deleteMarketTemplateCancel,
  deleteMarketTemplateStart,
  isTemplateManager,
  isPaymentOpen,
  buyMarketTemplate,
  buyMarketTemplateStart,
  buyMarketTemplateCancel,
  isBuying,
  isVerified,
  changePage,
  pagination,
  category,
  handleCategoryChange,
  purchasedTemplates
}) => {
  const id = selectedTemplate.id;
  const isPurchased = purchasedTemplates.includes(id);
  return (
    <div className="ui basic segment padded-bottom">
      <div className="header-with-actions">
        <h2 className="ui header">Email Templates Marketplace</h2>
        {__STAGE__ === 'development' && (
          <DropDownMenu className={cx('floating labeled icon button', {teal: category})}>
            <i className="filter icon" />
            <span className="text">{humanize(status || 'any category')}</span>
            <div className="menu">
              <div className="scrolling menu">
                <div className="item" onClick={() => handleCategoryChange()}>
                  Any category
                </div>
                {Object.keys(CATEGORIES).map(key => (
                  <Aux key={key}>
                    <div className="header">{humanize(key)}</div>
                    {CATEGORIES[key].map(sub => (
                      <div
                        key={sub}
                        className={cx('item', {'active selected': sub === category})}
                        onClick={() => handleCategoryChange(sub)}>
                        {humanize(sub)}
                      </div>
                    ))}
                  </Aux>
                ))}
              </div>
            </div>
          </DropDownMenu>
        )}
      </div>
      <PaginatedList
        isFetching={isFetchingAll}
        resources={templates}
        className={cx('ui cards', classNames.list)}
        changePage={changePage}
        resourceName="free templates"
        {...pagination}>
        {(template, i) => (
          <MarketTemplateItem
            key={i}
            onSelect={buyMarketTemplateStart}
            onDelete={deleteMarketTemplateStart}
            isTemplateManager={isTemplateManager}
            isPurchased={purchasedTemplates.includes(template.id)}
            isDisabled={!isVerified}
            template={template}
          />
        )}
      </PaginatedList>
      <Confirm
        isOpen={isDeleteStarted}
        isLoading={isDeleting}
        onCancel={deleteMarketTemplateCancel}
        onConfirm={() => deleteMarketTemplate(selectedTemplate.id)}
        confirmText="Delete template"
        confirmClass="negative">
        <p>
          This action will completely delete{' '}
          <NoLocalize el="b">"{selectedTemplate.name}"</NoLocalize> from market
        </p>
      </Confirm>
      <Payment
        isOpen={isPaymentOpen}
        isProcessing={isBuying}
        process={() => buyMarketTemplate(selectedTemplate)}
        processCancel={buyMarketTemplateCancel}
        confirmText={isPurchased && 'Select'}
        price={selectedTemplate.price}
        headerText={selectedTemplate.name}
        contentClass={classNames.preview}
        hasSeparatePreview>
        <MarketTemplatePreview template={selectedTemplate} />
      </Payment>
    </div>
  );
};

MarketTemplatesView.propTypes = {
  templates: PropTypes.array.isRequired,
  isFetchingAll: PropTypes.bool.isRequired,
  selectedTemplate: PropTypes.object.isRequired,
  deleteMarketTemplateStart: PropTypes.func.isRequired,
  isDeleteStarted: PropTypes.bool.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  deleteMarketTemplate: PropTypes.func.isRequired,
  deleteMarketTemplateCancel: PropTypes.func.isRequired,
  isTemplateManager: PropTypes.bool,
  isVerified: PropTypes.bool.isRequired,
  selectMarketTemplate: PropTypes.func.isRequired,
  clearSelectedMarketTemplate: PropTypes.func.isRequired,
  isPaymentOpen: PropTypes.bool.isRequired,
  buyMarketTemplate: PropTypes.func.isRequired,
  buyMarketTemplateStart: PropTypes.func.isRequired,
  buyMarketTemplateCancel: PropTypes.func.isRequired,
  isBuying: PropTypes.bool.isRequired,
  changePage: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired,
  category: PropTypes.string,
  handleCategoryChange: PropTypes.func.isRequired,
  purchasedTemplates: PropTypes.array.isRequired
};

export default MarketTemplatesView;
