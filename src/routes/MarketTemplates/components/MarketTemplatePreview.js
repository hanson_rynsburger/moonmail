import React from 'react';
import PropTypes from 'prop-types';
import {humanize, joinComponents} from 'lib/utils';
import EmailLink from 'components/EmailLink';
import classNames from './MarketTemplates.scss';

const MarketTemplatePreview = ({
  template: {images = [], designer = {}, tags = [], categories = [], description, thumbnail}
}) => {
  if (images.length === 0) images.push(thumbnail);
  return (
    <section>
      <div className={classNames.previewImages}>
        {images.map((img, i) => <img key={i} src={img} className={classNames.previewImage} />)}
      </div>
      <section className="ui basic segment">
        <div className="ui list">
          {description && <div className="item">{description}</div>}
          {categories.length > 0 && (
            <div className="item">
              <b>Categories:</b>{' '}
              {joinComponents(categories, (c, i) => (
                <a key={i} href="#">
                  {humanize(c)}
                </a>
              ))}
            </div>
          )}
          {tags.length > 0 && (
            <div className="item">
              <b>Tags:</b>{' '}
              {joinComponents(tags, (t, i) => (
                <a key={i} href="#">
                  {humanize(t)}
                </a>
              ))}
            </div>
          )}
          <div className="item">
            <b>Designed by</b>{' '}
            <a href={designer.url} target="_blank">
              {designer.name}
            </a>{' '}
            {designer.email && <EmailLink to={designer.email} />}
          </div>
        </div>
      </section>
    </section>
  );
};

MarketTemplatePreview.propTypes = {
  template: PropTypes.object.isRequired
};

export default MarketTemplatePreview;
