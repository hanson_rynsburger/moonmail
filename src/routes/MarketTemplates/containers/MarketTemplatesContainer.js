import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/marketTemplates/actions';
import selectors from 'modules/marketTemplates/selectors';
import MarketTemplatesView from '../components/MarketTemplatesView';
import {getIsTemplateManager, getIsVerified, getTemplates} from 'modules/profile/selectors';

class MarketTemplates extends Component {
  static propTypes = {
    fetchMarketTemplates: PropTypes.func.isRequired,
    clearMarketTemplates: PropTypes.func.isRequired,
    router: PropTypes.object.isRequired,
    pagination: PropTypes.object.isRequired,
    category: PropTypes.string
  };

  updateQuery({category}) {
    const {router} = this.props;
    router.push({
      pathname: '/market-templates',
      query: {category}
    });
  }

  componentWillUnmount() {
    const {category, clearMarketTemplates, pagination} = this.props;
    if (category || pagination.from) {
      clearMarketTemplates();
    }
  }

  handleCategoryChange = category => {
    this.updateQuery({category});
    this.props.fetchMarketTemplates({category}, true);
  };

  changePage = ({from, size}) => {
    this.props.fetchMarketTemplates({from, size}, true);
  };

  componentDidMount() {
    const {fetchMarketTemplates, category} = this.props;
    fetchMarketTemplates({category, size: 50});
  }

  render() {
    return (
      <MarketTemplatesView
        {...this.props}
        changePage={this.changePage}
        handleCategoryChange={this.handleCategoryChange}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const {category} = ownProps.location.query || {};
  const isDeleteStarted = selectors.getIsDeleteStarted(state);
  return {
    templates: selectors.getResources(state),
    isFetchingAll: selectors.getIsFetchingAll(state),
    isDeleting: selectors.getIsDeleting(state),
    isDeleteStarted,
    selectedTemplate: selectors.getSelectedResource(state),
    isPaymentOpen: selectors.getIsBuyStarted(state),
    isBuying: selectors.getIsBuying(state),
    isTemplateManager: getIsTemplateManager(state),
    isVerified: getIsVerified(state),
    pagination: selectors.getParams(state),
    purchasedTemplates: getTemplates(state),
    isFetching: selectors.getIsFetching(state),
    category
  };
};

export default connect(mapStateToProps, actions)(MarketTemplates);
