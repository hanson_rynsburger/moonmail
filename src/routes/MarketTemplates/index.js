import MarketTemplates from './containers/MarketTemplatesContainer';

export default {
  path: '/market-templates',
  component: MarketTemplates
};
