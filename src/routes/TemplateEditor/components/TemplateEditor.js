import PropTypes from 'prop-types';
import React, {Component} from 'react';
import Loader from 'components/Loader';
import classNames from './TemplateEditor.scss';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';
import {CAMPAIGN_DEFAULT_METAFIELDS} from 'modules/campaigns/constants';
import {getLangTag} from 'lib/remoteUtils';

// TODO: remove this when fixed by beefree team
const fixTemplateLinks = html =>
  html.replace(/href="([^'"]+)"/g, href => href.replace(/&amp;/g, '&'));

class TemplateEditor extends Component {
  constructor(props) {
    super(props);
    this.defaultOptions = {
      container: 'editorContainer',
      autosave: 60,
      language: getLangTag(),
      mergeTags: CAMPAIGN_DEFAULT_METAFIELDS.map(field => ({
        name: field,
        value: `{{${field}}}`
      })),
      specialLinks: [
        {
          type: 'Manage subscription',
          label: 'unsubscribe',
          link: '{{unsubscribe_url}}'
        }
      ],
      onSave: this.onSave,
      onSaveAsTemplate: props.onSaveAsTemplate,
      onAutoSave: props.onAutoSave,
      onSend: props.onSend,
      onError: this.onError
    };
    this.state = {
      isStarted: false
    };
  }

  static propTypes = {
    uid: PropTypes.string,
    token: PropTypes.object,
    options: PropTypes.object,
    template: PropTypes.string,
    onSave: PropTypes.func,
    onSaveAsTemplate: PropTypes.func,
    onAutoSave: PropTypes.func,
    onSend: PropTypes.func,
    onError: PropTypes.func,
    addMessage: PropTypes.func.isRequired,
    cleanEditorToken: PropTypes.func.isRequired,
    isFetchingToken: PropTypes.bool.isRequired
  };

  onSave = (template, body) => {
    this.props.onSave && this.props.onSave(template, fixTemplateLinks(body));
  };

  onError = error => {
    const {addMessage, onError} = this.props;
    addMessage({text: error});
    onError && onError(error);
  };

  init() {
    this.initialized = true;
    const {options, uid} = this.props;
    window.BeePlugin.create(
      this.props.token,
      {...this.defaultOptions, ...options, uid},
      beePluginInstance => {
        this.editor = beePluginInstance;
        this.editor.start(JSON.parse(this.props.template));
        setTimeout(() => {
          this.setState({isStarted: true});
          this.props.cleanEditorToken();
        }, 100);
      }
    );
  }

  componentDidUpdate() {
    const {isFetchingToken, token, uid} = this.props;
    if (!this.initialized && !isFetchingToken && token && uid) {
      this.init();
    }
  }

  render() {
    return (
      <div>
        <Loader
          inline={false}
          dimmer
          text="Loading template"
          active={!this.state.isStarted}
          dimmerClassName={classNames.dimmer}
        />
        <div id="editorContainer" className={cx(NO_LOCALIZE, classNames.container)} />
      </div>
    );
  }
}

export default TemplateEditor;
