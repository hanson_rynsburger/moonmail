import React from 'react';
import Link from 'react-router/lib/Link';
import classNames from './TemplateHeader.scss';

const TemplateHeader = () => (
  <header className={classNames.container}>
    <Link to="/templates" className={classNames.link}>
      <i className="list icon" /> Your templates
    </Link>
  </header>
);

export default TemplateHeader;
