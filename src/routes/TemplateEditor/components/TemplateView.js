import PropTypes from 'prop-types';
import React, {Component} from 'react';
import fs from 'browser-filesaver';
import {slugify} from 'lib/utils';
import TemplateEditor from '../containers/TemplateEditorContainer';
import RenameTemplate from 'routes/Templates/containers/RenameTemplateContainer';
import TemplateHeader from './TemplateHeader';

class TemplateView extends Component {
  static propTypes = {
    updateTemplate: PropTypes.func.isRequired,
    addMessage: PropTypes.func.isRequired,
    updateTemplateStart: PropTypes.func.isRequired,
    isUpdateStarted: PropTypes.bool,
    isUpdating: PropTypes.bool,
    updateTemplateCancel: PropTypes.func.isRequired,
    template: PropTypes.shape({
      body: PropTypes.string,
      name: PropTypes.string
    }).isRequired
  };

  onSave = (body, html) => {
    const {updateTemplate, updateTemplateStart, template} = this.props;
    if (template.name) {
      updateTemplate(template.id, {body, html}, false);
    } else {
      this.setState({html, body});
      updateTemplateStart(template.id);
    }
  };

  onSaveAsTemplate = json => {
    fs.saveAs(
      new Blob([json], {type: 'application/json;charset=utf-8'}),
      slugify(this.props.template.name) + '.json'
    );
  };

  onAutoSave = body => {
    const {updateTemplate, template} = this.props;
    updateTemplate(template.id, {body}, true);
  };

  onSend = () => {
    this.props.addMessage({
      text: 'You can send test emails only if template is attached to an existing campaign'
    });
  };

  render() {
    const {
      template,
      isUpdateStarted,
      isUpdating,
      updateTemplateCancel,
      updateTemplate
    } = this.props;
    return (
      <section>
        <TemplateHeader />
        <TemplateEditor
          template={template.body}
          onSave={this.onSave}
          onSaveAsTemplate={this.onSaveAsTemplate}
          onAutoSave={this.onAutoSave}
          onSend={this.onSend}
        />
        <RenameTemplate
          initialValues={template}
          isOpen={isUpdateStarted}
          isSaving={isUpdating}
          onCancel={updateTemplateCancel}
          onSave={data => updateTemplate(template.id, {...data, ...this.state})}
        />
      </section>
    );
  }
}

export default TemplateView;
