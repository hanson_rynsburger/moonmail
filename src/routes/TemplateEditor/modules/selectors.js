import {createSelector} from 'reselect';
import {stateKey} from './reducer';

const editorSelector = state => state[stateKey];

export const getIsEditorLoading = createSelector(editorSelector, editor => editor.isEditorLoading);

export const getIsFetchingToken = createSelector(editorSelector, editor => editor.isFetchingToken);

export const getToken = createSelector(editorSelector, editor => editor.token);
