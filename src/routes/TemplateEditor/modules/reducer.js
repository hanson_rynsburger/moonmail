import * as types from './types';
import {combineReducers} from 'redux';

export const stateKey = 'templateEditor';

const isEditorLoading = (state = false, action) => {
  switch (action.type) {
    case types.LOAD_EDITOR_REQUEST:
      return true;
    case types.LOAD_EDITOR_SUCCESS:
    case types.LOAD_EDITOR_FAIL:
      return false;
    default:
      return state;
  }
};

const isFetchingToken = (state = false, action) => {
  switch (action.type) {
    case types.EDITOR_TOKEN_REQUEST:
      return true;
    case types.EDITOR_TOKEN_SUCCESS:
    case types.EDITOR_TOKEN_FAIL:
      return false;
    default:
      return state;
  }
};

const token = (state = null, action) => {
  switch (action.type) {
    case types.EDITOR_TOKEN_SUCCESS:
      return action.token;
    case types.CLEAN_EDITOR_TOKEN:
      return null;
    default:
      return state;
  }
};

export default combineReducers({
  isEditorLoading,
  isFetchingToken,
  token
});
