import * as types from './types';
import * as api from 'lib/api';
import load from 'loadjs';
import {addMessage} from 'modules/messages/actions';

export const loadEditorScript = () => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      if (window.BeePlugin) resolve();
      dispatch({
        type: types.LOAD_EDITOR_REQUEST
      });
      load('https://app-rsrc.getbee.io/plugin/BeePlugin.js', {
        success() {
          dispatch({
            type: types.LOAD_EDITOR_SUCCESS
          });
          resolve();
        },
        fail() {
          dispatch(
            addMessage({
              text: 'Error loading editor'
            })
          );
          dispatch({
            type: types.LOAD_EDITOR_FAIL
          });
          reject();
        }
      });
    });
  };
};

export const cleanEditorToken = () => ({
  type: types.CLEAN_EDITOR_TOKEN
});

export const fetchEditorToken = () => {
  return async dispatch => {
    dispatch({
      type: types.EDITOR_TOKEN_REQUEST
    });
    try {
      const token = await api.fetchEditorToken();
      dispatch({
        type: types.EDITOR_TOKEN_SUCCESS,
        token
      });
    } catch (error) {
      dispatch({
        type: types.EDITOR_TOKEN_FAIL
      });
      dispatch(addMessage({text: error}));
    }
  };
};

export const loadEditor = () => {
  return async dispatch => {
    await dispatch(loadEditorScript());
    await dispatch(fetchEditorToken());
  };
};
