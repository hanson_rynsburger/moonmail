import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/templates/actions';
import selectors from 'modules/templates/selectors';
import {getIsVerified} from 'modules/profile/selectors';
import {addMessage} from 'modules/messages/actions';
import TemplateView from '../components/TemplateView';

class Template extends Component {
  static propTypes = {
    fetchTemplate: PropTypes.func.isRequired,
    templateId: PropTypes.string.isRequired,
    isFetching: PropTypes.bool.isRequired,
    isVerified: PropTypes.bool,
    router: PropTypes.object,
    template: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      createdAt: PropTypes.number
    })
  };

  redirect() {
    this.props.router.replace('/templates');
  }

  componentDidMount() {
    const {fetchTemplate, templateId, template, isVerified} = this.props;

    // handleRedirect user if he is not verified
    if (!isVerified) this.redirect();

    fetchTemplate(templateId);
    if (template._isUpToDate && !template.body) this.redirect();
  }

  componentWillReceiveProps({template}) {
    if (template._isUpToDate && !template.body) this.redirect();
  }

  render() {
    return <TemplateView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  template: selectors.getActiveResource(state),
  isFetching: selectors.getIsFetching(state),
  isUpdating: selectors.getIsUpdating(state),
  isUpdateStarted: selectors.getIsUpdateStarted(state),
  templateId: ownProps.params.templateId,
  isVerified: getIsVerified(state)
});

export default connect(mapStateToProps, {...actions, addMessage})(Template);
