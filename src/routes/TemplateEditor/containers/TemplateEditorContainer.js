import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as actions from '../modules/actions';
import {addMessage} from 'modules/messages/actions';
import * as selectors from '../modules/selectors';
import {getUserIdBase64} from 'modules/profile/selectors';
import TemplateEditor from '../components/TemplateEditor';

class Template extends Component {
  static propTypes = {
    loadEditor: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.loadEditor();
  }

  render() {
    return <TemplateEditor {...this.props} />;
  }
}

const mapStateToProps = state => ({
  isEditorLoading: selectors.getIsEditorLoading(state),
  isFetchingToken: selectors.getIsFetchingToken(state),
  token: selectors.getToken(state),
  uid: getUserIdBase64(state)
});

export default connect(mapStateToProps, {...actions, addMessage})(Template);
