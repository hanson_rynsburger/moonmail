import Template from './containers/TemplateContainer';
import {injectReducer} from 'store/reducers';
import reducer, {stateKey} from './modules/reducer';

export default store => {
  injectReducer(store, {key: stateKey, reducer});
  return {
    path: '/templates/:templateId',
    component: Template,
    isPlain: true
  };
};
