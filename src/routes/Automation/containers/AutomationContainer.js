import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/automations/actions';
import selectors from 'modules/automations/selectors';
import actionSelectors from 'modules/automationActions/selectors';
import {getIsAllowedToSend, getIsFreeUser} from 'modules/profile/selectors';
import actionActions from 'modules/automationActions/actions';
import AutomationView from '../components/AutomationView';
import listsActions from 'modules/lists/actions';

class Automation extends Component {
  static propTypes = {
    fetchAutomation: PropTypes.func.isRequired,
    fetchLists: PropTypes.func.isRequired,
    clearActions: PropTypes.func.isRequired,
    automationId: PropTypes.string.isRequired
  };

  componentDidMount() {
    const {fetchAutomation, fetchLists, automationId} = this.props;
    fetchAutomation(automationId);
    fetchLists();
  }

  componentWillUnmount() {
    this.props.clearActions();
  }

  render() {
    return <AutomationView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => {
  const isFreeUser = getIsFreeUser(state);
  return {
    automation: selectors.getActiveResource(state),
    isFetching: selectors.getIsFetching(state),
    isActivating: selectors.getIsActivating(state),
    isActivateStarted: selectors.getIsActivateStarted(state),
    isPausing: selectors.getIsPausing(state),
    isActive: selectors.getIsActive(state),
    automationId: ownProps.params.automationId,
    actionId: ownProps.params.automationActionId,
    isFreeUser,
    isAllowedToActivate:
      !isFreeUser && getIsAllowedToSend(state) && actionSelectors.getIds(state).length > 0
  };
};

export default connect(mapStateToProps, {
  ...actions,
  clearActions: actionActions.clearResources,
  fetchLists: listsActions.fetchLists
})(Automation);
