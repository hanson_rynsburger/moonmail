import Automation from './containers/AutomationContainer';
import Settings from './routes/Settings';
import Actions from './routes/Actions';
import Action from './routes/Action';
import NewAction from './routes/NewAction';

export default {
  path: 'automations/:automationId',
  indexRoute: Actions,
  component: Automation,
  hideFooter: true,
  childRoutes: [Settings, NewAction, Action]
};
