import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/Loader';
import Button from 'components/Button';
import Confirm from 'components/Modal/Confirm';
import {NO_LOCALIZE} from 'lib/constants';
import {Aux} from 'lib/utils';
import PopUp from 'components/PopUp';
import cx from 'classnames';
import {getRelativeItemLink} from 'components/Links';

const AutomationView = ({
  automation,
  isFetching,
  isActivating,
  isActivateStarted,
  isActive,
  activateAutomationStart,
  activateAutomation,
  activateAutomationCancel,
  pauseAutomation,
  isPausing,
  isAllowedToActivate,
  isFreeUser,
  children,
  actionId
}) => {
  if (actionId) return children;
  if (isFetching || !automation.id) {
    return (
      <section className="ui basic segment">
        <Loader active inline />
      </section>
    );
  }
  const Link = getRelativeItemLink(`/automations/${automation.id}/`);
  return (
    <section className="ui basic segment padded-bottom">
      <div className="header-with-actions">
        <h1 className={cx('ui header', NO_LOCALIZE)}>{automation.name}</h1>
        {isActive ? (
          <Button loading={isPausing} onClick={() => pauseAutomation(automation.id)}>
            <i className="pause icon" />
            Pause
          </Button>
        ) : (
          <PopUp
            on="click"
            showDelay={500}
            closable={false}
            variation="small"
            disabled={!isFreeUser}
            content={
              <Aux>
                <Link to="/profile/plan">Upgrade</Link> to activate automation
              </Aux>
            }>
            <Button positive onClick={activateAutomationStart} disabled={!isAllowedToActivate}>
              <i className="play icon" />
              Activate
            </Button>
          </PopUp>
        )}
      </div>
      <div className="ui secondary pointing large menu no-padding">
        <Link onlyActiveOnIndex>Actions</Link>
        <Link to="settings">Settings</Link>
      </div>
      {children}
      <Confirm
        isOpen={isActivateStarted}
        isLoading={isActivating}
        onCancel={activateAutomationCancel}
        onConfirm={() => activateAutomation(automation.id)}
        headerText="You’re all set to activate!"
        confirmText="Activate automation"
        confirmClass="positive">
        You will activate this automation with all subsequent actions. Are you sure?
      </Confirm>
    </section>
  );
};

AutomationView.propTypes = {
  automation: PropTypes.shape({
    id: PropTypes.string,
    status: PropTypes.string
  }),
  actionId: PropTypes.string,
  isFetching: PropTypes.bool.isRequired,
  isActive: PropTypes.bool.isRequired,
  isFreeUser: PropTypes.bool.isRequired,
  isPausing: PropTypes.bool.isRequired,
  isActivating: PropTypes.bool.isRequired,
  isActivateStarted: PropTypes.bool.isRequired,
  activateAutomation: PropTypes.func.isRequired,
  activateAutomationCancel: PropTypes.func.isRequired,
  isAllowedToActivate: PropTypes.bool.isRequired,
  activateAutomationStart: PropTypes.func.isRequired,
  pauseAutomation: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired
};

export default AutomationView;
