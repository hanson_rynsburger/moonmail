import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import RequirePermission from '../../../containers/RequirePermission';
import TemplateSelector from 'routes/Templates/containers/TemplateSelectorContainer';
import selectors from 'modules/automationActions/selectors';
import actions from 'modules/automationActions/actions';

class Template extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  static propTypes = {
    actionId: PropTypes.string.isRequired,
    updateResourceLocally: PropTypes.func.isRequired
  };

  onSelect = async template => {
    const {updateResourceLocally, actionId, automationId} = this.props;
    updateResourceLocally(actionId, {campaign: {template}});
    this.context.router.push(`/automations/${automationId}/actions/${actionId}/editor`);
  };

  render() {
    return <TemplateSelector {...this.props} onSelect={this.onSelect} />;
  }
}

const mapStateToProps = (state, ownProps) => {
  const isEditable = selectors.getIsEditable(state);
  return {
    actionId: ownProps.params.automationActionId,
    automationId: ownProps.params.automationId,
    action: selectors.getActiveResource(state),
    isEditable,
    isPermitted: isEditable
  };
};

export default connect(mapStateToProps, actions)(RequirePermission(Template));
