import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/automationActions/actions';
import selectors from 'modules/automationActions/selectors';

class Index extends Component {
  constructor(props) {
    super(props);
    this.urlBase = `/automations/${props.automationId}/actions/${props.actionId}`;
  }

  static contextTypes = {
    router: PropTypes.object
  };

  static propTypes = {
    automationId: PropTypes.string.isRequired,
    actionId: PropTypes.string.isRequired
  };

  handleRedirect({isEditable}) {
    if (isEditable) {
      return this.context.router.replace(`${this.urlBase}/html`);
    }
    this.context.router.replace(`${this.urlBase}/preview`);
  }

  componentWillReceiveProps(nextProps) {
    this.handleRedirect(nextProps);
  }

  componentWillMount() {
    this.handleRedirect(this.props);
  }

  render() {
    return <div />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  actionId: ownProps.params.automationActionId,
  isEditable: selectors.getIsEditable(state),
  isActive: selectors.getIsActive(state),
  automationId: ownProps.params.automationId
});

export default connect(mapStateToProps, actions)(Index);
