import React from 'react';
import PropTypes from 'prop-types';
import CodeEditor from 'components/CodeEditor';
import classNames from './RawView.scss';
import cx from 'classnames';

const RawView = ({action: {campaign = {}}}) => {
  const options = {
    mode: 'text/html',
    lineNumbers: true,
    readOnly: true
  };
  return (
    <section className={cx('ui vertical segment full height', classNames.section)}>
      <form className={cx('ui form', classNames.form)}>
        <CodeEditor
          name="body"
          label={false}
          value={campaign.body}
          options={options}
          className={classNames.editorField}
          editorClass={classNames.editor}
        />
      </form>
    </section>
  );
};

RawView.propTypes = {
  action: PropTypes.shape({
    campaign: PropTypes.object
  }).isRequired
};

export default RawView;
