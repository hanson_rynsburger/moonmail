import Raw from './containers/RawContainer';

export default {
  path: 'raw',
  component: Raw
};
