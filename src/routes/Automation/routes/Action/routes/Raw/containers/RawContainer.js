import {connect} from 'react-redux';
import RawView from '../components/RawView';
import selectors from 'modules/automationActions/selectors';
import RequirePermission from '../../../containers/RequirePermission';

const mapStateToProps = (state, ownProps) => ({
  actionId: ownProps.params.automationActionId,
  action: selectors.getActiveResource(state),
  isPermitted: selectors.getIsActive(state)
});

export default connect(mapStateToProps)(RequirePermission(RawView));
