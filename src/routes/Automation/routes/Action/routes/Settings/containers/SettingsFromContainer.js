import {reduxForm} from 'redux-form';
import SettingsForm from '../components/SettingsForm';
import {createValidator} from 'lib/validator';
import actions from 'modules/automationActions/actions';
import selectors from 'modules/automationActions/selectors';

const rules = {
  name: 'required|max:140',
  type: 'required',
  campaign: {
    subject: 'required'
  }
};

const mapStateToProps = state => ({
  isEditable: selectors.getIsEditable(state),
  initialValues: {
    delay: 0,
    type: 'list.recipient.subscribe',
    ...selectors.getActiveResource(state)
  }
});

export default reduxForm(
  {
    form: 'actionSettings',
    fields: ['name', 'type', 'campaign.subject', 'delay'],
    validate: createValidator(rules)
  },
  mapStateToProps,
  {...actions}
)(SettingsForm);
