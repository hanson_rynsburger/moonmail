import {connect} from 'react-redux';
import SettingsView from '../components/SettingsView';
import actions from 'modules/automationActions/actions';
import selectors from 'modules/automationActions/selectors';

const mapStateToProps = (state, ownProps) => {
  return {
    actionId: ownProps.params.automationActionId,
    isUpdating: selectors.getIsUpdating(state),
    automationId: ownProps.params.automationId
  };
};

export default connect(mapStateToProps, actions)(SettingsView);
