import React from 'react';
import PropTypes from 'prop-types';
import SettingsForm from '../containers/SettingsFromContainer';

const Settings = ({automationId, actionId, updateResource, isUpdating, ...formProps}) => {
  const submit = formProps => {
    updateResource(actionId, automationId, formProps);
  };
  return (
    <section className="ui vertical segment">
      <div className="ui stackable grid">
        <SettingsForm {...formProps} onFormSubmit={submit} isLoading={isUpdating} />
      </div>
    </section>
  );
};

Settings.propTypes = {
  isUpdating: PropTypes.bool.isRequired,
  updateResource: PropTypes.func.isRequired,
  automationId: PropTypes.string.isRequired,
  actionId: PropTypes.string.isRequired
};

export default Settings;
