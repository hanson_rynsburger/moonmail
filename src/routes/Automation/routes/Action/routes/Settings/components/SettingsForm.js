import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import EmojiInput from 'components/EmojiInput';
import DelayInput from 'components/DelayInput';
import Select, {SelectItem} from 'components/Select';
import Button from 'components/Button';
import DotHint from 'components/DotHint';
import {ACTION_TYPES} from 'modules/automationActions/constants';

const SettingsForm = ({
  fields: {name, type, delay, campaign: {subject}},
  handleSubmit,
  invalid,
  isLoading,
  isEditable,
  onFormSubmit,
  buttonText
}) => {
  return (
    <form className="ui form eight wide column" onSubmit={handleSubmit(onFormSubmit)}>
      <Input type="text" {...name} />
      <EmojiInput
        type="text"
        {...subject}
        disabled={!isEditable}
        label={<DotHint id="action.settings.subject">Subject</DotHint>}
      />
      <Select
        disabled={!isEditable}
        label={<DotHint id="action.settings.action_type">Action type</DotHint>}
        {...type}>
        {Object.keys(ACTION_TYPES).map((key, i) => (
          <SelectItem key={i} value={key}>
            {ACTION_TYPES[key]}
          </SelectItem>
        ))}
      </Select>
      <DelayInput
        {...delay}
        label={<DotHint id="action.settings.delay">Delay</DotHint>}
        disabled={!isEditable}
      />
      <Button primary loading={isLoading} type="submit" disabled={invalid || isLoading}>
        {buttonText || 'Update'}
      </Button>
    </form>
  );
};

SettingsForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isEditable: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  fields: PropTypes.shape({
    name: PropTypes.object.isRequired,
    type: PropTypes.object.isRequired
  }).isRequired,
  buttonText: PropTypes.string
};

export default SettingsForm;
