import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import CodeEditor from 'components/CodeEditor';
import cx from 'classnames';
import classNames from './HtmlView.scss';
import Link from 'react-router/lib/Link';
import FileAttachment from '../containers/FileAttachmentContainer';
import DotHint from 'components/DotHint';

const HtmlView = ({
  automationId,
  fields: {campaign: {body}},
  handleSubmit,
  invalid,
  updateResource,
  updateResourceLocally,
  isUpdating,
  actionId,
  metaData
}) => {
  const submit = formProps => {
    updateResource(actionId, automationId, formProps).then(() => {
      updateResourceLocally(actionId, {_isPendingUpdate: false});
    });
  };

  const handleChange = html => {
    body.onChange(html);
    updateResourceLocally(actionId, {_isPendingUpdate: true, campaign: {body: html}});
  };

  const tags = metaData.map(tag => `{{${tag}}}`).join('</br>');
  return (
    <section className={cx('ui vertical segment full height', classNames.section)}>
      <form className={cx('ui form', classNames.form)} onSubmit={handleSubmit(submit)}>
        <CodeEditor
          label={
            <DotHint id="action.html.body" variables={{tags}}>
              Email body (paste html or drag your html file here)
            </DotHint>
          }
          {...body}
          onChange={handleChange}
          className={classNames.editorField}
          editorClass={classNames.editor}
        />
        <div className="page-actions">
          <Button loading={isUpdating} primary type="submit" disabled={invalid || isUpdating}>
            Save draft
          </Button>
          <FileAttachment {...{actionId, automationId}} />
          <div className="spacer" />
          <Link
            to={`/automations/${automationId}/actions/${actionId}/preview`}
            className="ui primary button">
            Save & Preview
          </Link>
        </div>
      </form>
    </section>
  );
};

HtmlView.propTypes = {
  automationId: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  updateResource: PropTypes.func.isRequired,
  testActionStart: PropTypes.func.isRequired,
  updateResourceLocally: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isUpdating: PropTypes.bool.isRequired,
  actionId: PropTypes.string.isRequired,
  fields: PropTypes.object.isRequired,
  metaData: PropTypes.array.isRequired
};

export default HtmlView;
