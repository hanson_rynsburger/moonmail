import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {reduxForm} from 'redux-form';
import HtmlView from '../components/HtmlView';
import {createValidator} from 'lib/validator';
import actions from 'modules/automationActions/actions';
import selectors from 'modules/automationActions/selectors';
import listsSelectors from 'modules/lists/selectors';
import {parse} from 'lib/parser';
import {getIsFreeUser} from 'modules/profile/selectors';
import RequirePermission from '../../../containers/RequirePermission';
import {compose} from 'recompose';

class Html extends Component {
  static propTypes = {
    updateResourceLocally: PropTypes.func.isRequired,
    updateResource: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
    invalid: PropTypes.bool.isRequired,
    isEditable: PropTypes.bool,
    actionId: PropTypes.string.isRequired,
    action: PropTypes.object.isRequired,
    automationId: PropTypes.string.isRequired
  };

  beforeUnload = e => {
    const {action, isEditable} = this.props;
    if (action._isPendingUpdate && isEditable) {
      const s = 'You have unsaved changes.';
      e.returnValue = s;
      return s;
    }
  };

  componentDidMount() {
    window.onbeforeunload = this.beforeUnload;
  }

  componentWillUnmount() {
    window.onbeforeunload = null;
    const {
      updateResource,
      values,
      invalid,
      action,
      actionId,
      automationId,
      isEditable
    } = this.props;
    if (isEditable && !invalid && action._isPendingUpdate) {
      updateResource(actionId, automationId, values, true);
    }
  }

  render() {
    return <HtmlView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => {
  const action = selectors.getActiveResource(state);
  const isFreeUser = getIsFreeUser(state);
  const isEditable = selectors.getIsEditable(state);
  const asyncValidate = values => {
    return new Promise((resolve, reject) => {
      parse(values.campaign.body, !isFreeUser).then(
        () => {
          resolve({});
        },
        error => {
          reject({campaign: {body: error}});
        }
      );
    });
  };

  return {
    automationId: ownProps.params.automationId,
    actionId: ownProps.params.automationActionId,
    isUpdating: selectors.getIsUpdating(state),
    metaData: listsSelectors.getActiveActionMetaData(state),
    isPermitted: isEditable,
    isEditable,
    action,
    asyncValidate,
    initialValues: action
  };
};

export default compose(
  reduxForm(
    {
      form: 'automationHtml',
      fields: ['campaign.body'],
      asyncBlurFields: ['campaign.body'],
      validate: createValidator({
        campaign: {
          body: 'required'
        }
      })
    },
    mapStateToProps,
    actions
  ),
  RequirePermission
)(Html);
