import {connect} from 'react-redux';
import * as profileSelectors from 'modules/profile/selectors';
import actions from 'modules/automationActions/actions';
import selectors from 'modules/automationActions/selectors';
import FileAttachment from '../components/FileAttachment';

const {attachFile, detachFile, openFile} = actions;

const EXTENSION_ID = 'file-attachment';

export default connect(
  state => ({
    isSupported: profileSelectors.getExtensions(state).includes(EXTENSION_ID),
    attachments: selectors.getAttachments(state),
    isAttachingFile: selectors.getIsAttachingFile(state),
    isDetachingFile: selectors.getIsDetachingFile(state),
    detachingFileName: selectors.getDetachingFileName(state)
  }),
  {
    attachFile,
    detachFile,
    openFile
  }
)(FileAttachment);
