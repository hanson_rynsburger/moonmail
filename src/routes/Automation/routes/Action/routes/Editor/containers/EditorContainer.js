import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as actions from 'routes/TemplateEditor/modules/actions';
import {addMessage} from 'modules/messages/actions';
import * as selectors from 'routes/TemplateEditor/modules/selectors';
import actionActions from 'modules/automationActions/actions';
import actionSelectors from 'modules/automationActions/selectors';
import listsSelectors from 'modules/lists/selectors';
import {getUserIdBase64, getIsFreeUser} from 'modules/profile/selectors';
import EditorView from '../components/EditorView';

class Editor extends Component {
  static propTypes = {
    loadEditor: PropTypes.func.isRequired,
    action: PropTypes.object,
    actionId: PropTypes.string.isRequired,
    automationId: PropTypes.string.isRequired,
    router: PropTypes.object
  };

  componentWillMount() {
    const {action: {id, campaign = {}}, actionId, automationId, router} = this.props;
    if (id && !campaign.template) {
      router.push(`/automations/${automationId}/actions/${actionId}/templates`);
    }
  }

  componentDidMount() {
    this.props.loadEditor();
  }

  render() {
    return <EditorView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  isEditorLoading: selectors.getIsEditorLoading(state),
  isFetchingToken: selectors.getIsFetchingToken(state),
  isFreeUser: getIsFreeUser(state),
  action: actionSelectors.getActiveResource(state),
  actionId: ownProps.params.automationActionId,
  automationId: ownProps.params.automationId,
  token: selectors.getToken(state),
  uid: getUserIdBase64(state),
  metaData: listsSelectors.getActiveActionMetaData(state)
});

export default connect(mapStateToProps, {
  ...actions,
  ...actionActions,
  addMessage
})(Editor);
