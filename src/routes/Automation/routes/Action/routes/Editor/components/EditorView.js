import PropTypes from 'prop-types';
import React, {Component} from 'react';
import fs from 'browser-filesaver';
import TemplateEditor from 'routes/TemplateEditor/containers/TemplateEditorContainer';
import TemplateHeader from './TemplateHeader';
import TestModal from '../../../containers/TestActionContainer';
import {parse} from 'lib/parser';

class EditorView extends Component {
  static propTypes = {
    updateResource: PropTypes.func.isRequired,
    updateResourceLocally: PropTypes.func.isRequired,
    addMessage: PropTypes.func.isRequired,
    testActionStart: PropTypes.func.isRequired,
    actionId: PropTypes.string.isRequired,
    automationId: PropTypes.string.isRequired,
    metaData: PropTypes.array.isRequired,
    action: PropTypes.shape({
      name: PropTypes.string,
      campaign: PropTypes.object
    }),
    isFreeUser: PropTypes.bool.isRequired,
    router: PropTypes.object
  };

  onSave = (template, body) => {
    this.setState({isSaving: true});
    const {
      updateResource,
      updateResourceLocally,
      action,
      actionId,
      automationId,
      isFreeUser,
      addMessage,
      router
    } = this.props;
    const campaign = {template, body};
    updateResourceLocally(action.id, {campaign});
    parse(body, !isFreeUser)
      .then(() => {
        router.push(`/automations/${automationId}/actions/${actionId}/preview`);
        updateResource(actionId, automationId, {campaign});
      })
      .catch(error => {
        addMessage({text: error});
      });
  };

  onSaveAsTemplate = jsonFile => {
    fs.saveAs(
      new Blob([jsonFile], {type: 'application/json;charset=utf-8'}),
      this.props.action.name.replace(' ', '_') + '.json'
    );
  };

  onAutoSave = template => {
    const {updateResource, actionId, automationId} = this.props;
    const campaign = {template};
    updateResource(actionId, automationId, {campaign}, true);
  };

  onSend = body => {
    const {updateResourceLocally, testActionStart, actionId} = this.props;
    const campaign = {body};
    updateResourceLocally(actionId, {campaign});
    testActionStart();
  };

  render() {
    const options = {
      mergeTags: this.props.metaData.map(field => ({
        name: field,
        value: `{{${field}}}`
      }))
    };
    const {action: {campaign = {}}, actionId, automationId} = this.props;
    return (
      <div>
        <TemplateHeader actionId={actionId} automationId={automationId} />
        <TemplateEditor
          template={campaign.template}
          options={options}
          onSave={this.onSave}
          onSaveAsTemplate={this.onSaveAsTemplate}
          onAutoSave={this.onAutoSave}
          onSend={this.onSend}
        />
        <TestModal actionId={actionId} />
      </div>
    );
  }
}

export default EditorView;
