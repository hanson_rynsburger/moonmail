import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router/lib/Link';
import classNames from 'routes/TemplateEditor/components/TemplateHeader.scss';

const TemplateHeader = ({actionId, automationId}) => (
  <header className={classNames.container}>
    <Link to={`/automations/${automationId}/actions/${actionId}/html`} className={classNames.link}>
      Edit code
    </Link>
    <Link
      to={`/automations/${automationId}/actions/${actionId}/preview`}
      className={classNames.link}>
      Preview and activate
    </Link>
    <Link
      to={`/automations/${automationId}/actions/${actionId}/settings`}
      className={classNames.link}>
      Settings
    </Link>
  </header>
);

TemplateHeader.propTypes = {
  actionId: PropTypes.string.isRequired,
  automationId: PropTypes.string.isRequired
};

export default TemplateHeader;
