import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import PreviewView from '../components/PreviewView';
import selectors from 'modules/automationActions/selectors';
import actions from 'modules/automationActions/actions';
import * as profileSelectors from 'modules/profile/selectors';
import listsSelectors from 'modules/lists/selectors';
import campaignsActions from 'modules/campaigns/actions';
import campaignsSelectors from 'modules/campaigns/selectors';
import sendersSelectors from 'modules/senders/selectors';

class Preview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: false
    };
  }

  static propTypes = {
    fetchActionLinks: PropTypes.func.isRequired,
    validateAndParse: PropTypes.func.isRequired,
    action: PropTypes.object.isRequired,
    actionId: PropTypes.string.isRequired,
    automationId: PropTypes.string.isRequired,
    metaData: PropTypes.array.isRequired,
    isFreeUser: PropTypes.bool.isRequired,
    isEditable: PropTypes.bool.isRequired,
    profile: PropTypes.object.isRequired,
    sender: PropTypes.object,
    sendersUpToDate: PropTypes.bool.isRequired
  };

  componentDidMount() {
    const {
      action: {id, campaign = {}, senderId},
      metaData,
      isEditable,
      validateAndParse,
      sendersUpToDate
    } = this.props;
    if (sendersUpToDate && id) {
      validateAndParse({body: campaign.body, senderId}, metaData, isEditable);
    }
  }

  componentWillReceiveProps({
    sendersUpToDate,
    action: {id, campaign = {}, senderId},
    metaData,
    isEditable
  }) {
    if (sendersUpToDate && id) {
      this.props.validateAndParse({body: campaign.body, senderId}, metaData, isEditable);
    }
  }

  switchMode = () => {
    this.setState({
      isMobile: !this.state.isMobile
    });
  };

  render() {
    return (
      <PreviewView
        html={this.state.html}
        switchMode={this.switchMode}
        isMobile={this.state.isMobile}
        invalid={this.state.invalid}
        {...this.props}
      />
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const action = selectors.getActiveResource(state);
  const isActive = selectors.getIsActive(state);
  return {
    actionId: ownProps.params.automationActionId,
    automationId: ownProps.params.automationId,
    action,
    sender: sendersSelectors.getResourceById(state, action.senderId),
    links: selectors.getActionLinks(state),
    isActive,
    isValid: campaignsSelectors.getIsValid(state),
    campaignBody: campaignsSelectors.getCampaignBody(state),
    isPermitted: isActive,
    isEditable: selectors.getIsEditable(state),
    isUpdating: selectors.getIsUpdating(state),
    metaData: listsSelectors.getActiveActionMetaData(state),
    isFreeUser: profileSelectors.getIsFreeUser(state),
    profile: profileSelectors.getProfile(state),
    hasSenders: profileSelectors.getHasSenders(state),
    sendersUpToDate: sendersSelectors.getIsUpToDate(state),
    isAllowedToSend: profileSelectors.getIsAllowedToSend(state)
  };
};

export default connect(mapStateToProps, {
  ...actions,
  validateAndParse: campaignsActions.validateAndParseCampaign
})(Preview);
