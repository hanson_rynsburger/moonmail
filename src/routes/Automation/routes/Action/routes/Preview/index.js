import preview from './containers/PreviewContainer';

export default {
  path: 'preview',
  component: preview
};
