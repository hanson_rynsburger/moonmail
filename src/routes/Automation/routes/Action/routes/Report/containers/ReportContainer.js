import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import ReportView from '../components/ReportView';
import actions from 'modules/automationActions/actions';
import selectors from 'modules/automationActions/selectors';
import RequirePermission from '../../../containers/RequirePermission';

class Report extends Component {
  static propTypes = {
    fetchActionReport: PropTypes.func.isRequired,
    automationId: PropTypes.string.isRequired,
    actionId: PropTypes.string.isRequired
  };

  componentDidMount() {
    const {fetchActionReport, automationId, actionId} = this.props;
    fetchActionReport(actionId, automationId);
  }

  render() {
    return <ReportView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  automationId: ownProps.params.automationId,
  actionId: ownProps.params.automationActionId,
  action: selectors.getActiveResource(state),
  isPermitted: selectors.getIsActive(state)
});

export default connect(mapStateToProps, actions)(RequirePermission(Report));
