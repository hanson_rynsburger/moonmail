import React from 'react';
import PropTypes from 'prop-types';
import Progress from 'components/Progress';
import DotHint from 'components/DotHint';
import NL from 'components/NoLocalize';
import {formatDate, deCapitalize, formatRate, Aux} from 'lib/utils';
import {getActionTrigger} from 'modules/automationActions/lib';
import {RATE_LIMITS} from 'lib/constants';

const ReportView = ({
  action: {
    opensCount = 0,
    sentCount = 0,
    unsubscribeCount = 0,
    bouncesCount = 0,
    complaintsCount = 0,
    clicksCount = 0,
    status,
    createdAt,
    ...action
  }
}) => {
  const openRate = formatRate(opensCount / sentCount);
  const unsubscribeRate = formatRate(unsubscribeCount / sentCount);
  const bounceRate = formatRate(bouncesCount / sentCount);
  const bouncePercent = formatRate(bounceRate / RATE_LIMITS.bounceRate);
  const complaintRate = formatRate(complaintsCount / sentCount, 3);
  const complaintPercent = formatRate(complaintRate / RATE_LIMITS.complaintRate, 3);
  const deliveryBase = sentCount - bouncesCount - complaintsCount;
  const emailDeliveryRate = formatRate(deliveryBase / sentCount);
  const clickOpenRate = formatRate(clicksCount / opensCount);

  if (deCapitalize(status) === 'badReputation') {
    return (
      <div className="ui icon error message">
        <i className="ban icon" />
        <div className="content">
          <div className="header">
            This action was blocked because of the bad reputation (too high bounce or complaint
            rate)
          </div>
          <p>
            You can not activate any more actions. Please contact support if you think its a
            mistake.
          </p>
        </div>
      </div>
    );
  }

  const readMore = (
    <a
      target="_blank"
      rel="noopener noreferrer"
      href="http://support.moonmail.io/faq/validation-and-reputation/what-are-the-acceptable-complaint-and-bounce-rates">
      Read More
    </a>
  );

  return (
    <section className="ui vertical segment padded-bottom">
      <div className="ui big list">
        <div className="item">
          <b>Created:</b>
          <NL> {formatDate(createdAt, {fromNow: true})}</NL>
        </div>
        <div className="item">
          <b>Trigger:</b> {getActionTrigger(action)}
        </div>
      </div>
      <div className="ui vertical segment">
        <div className="ui small statistics">
          <div className="grey statistic">
            <div className="value">{sentCount}</div>
            <div className="label">Sent emails</div>
          </div>
          <div className="teal statistic">
            <div className="value">{opensCount}</div>
            <div className="label">Opened</div>
          </div>
          <div className="teal statistic">
            <div className="value">{clicksCount}</div>
            <div className="label">Clicked</div>
          </div>
          <div className="red statistic">
            <div className="value">{unsubscribeCount}</div>
            <div className="label">Unsubscribed</div>
          </div>
          <div className="red statistic">
            <div className="value">{bouncesCount}</div>
            <div className="label">Bounced</div>
          </div>
          <div className="red statistic">
            <div className="value">{complaintsCount}</div>
            <div className="label">Complained</div>
          </div>
        </div>
      </div>
      <div className="ui vertical segment">
        <h3 className="ui header">
          Email delivery rate
          <DotHint id="action.report.delivery_rate" />
        </h3>
        <Progress percent={emailDeliveryRate} className="teal" />
        <h3 className="ui header">
          Open rate
          <DotHint id="action.report.open_rate" />
        </h3>
        <Progress percent={openRate} className="teal" />
        <h3 className="ui header">
          Click open rate
          <DotHint id="action.report.click_rate" />
        </h3>
        <Progress percent={clickOpenRate} className="teal" />
        <h3 className="ui header">
          Unsubscribe rate
          <DotHint id="action.report.unsubscribe_rate" />
        </h3>
        <Progress className="error" percent={unsubscribeRate} indicating negative />
        <h3 className="ui header">
          Bounce rate <NL>{bounceRate}%</NL> out of <NL>{RATE_LIMITS.bounceRate.toFixed(2)}%</NL>.
          <DotHint id="action.report.bounce_rate" />
        </h3>
        <Progress
          percent={bouncePercent}
          label={<Aux>{readMore} about bounce rate restrictions.</Aux>}
          value={false}
          indicating
          negative
        />
        <h3 className="ui header">
          Complaint rate <NL>{complaintRate}%</NL> out of <NL>{RATE_LIMITS.complaintRate}%</NL>.
          <DotHint id="action.report.complaint_rate" />
        </h3>
        <Progress
          percent={complaintPercent}
          label={<Aux>{readMore} about complaint rate restrictions.</Aux>}
          value={false}
          indicating
          negative
        />
      </div>
    </section>
  );
};

ReportView.propTypes = {
  action: PropTypes.object.isRequired
};

export default ReportView;
