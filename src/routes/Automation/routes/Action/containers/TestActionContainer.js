import {reduxForm} from 'redux-form';
import actions from 'modules/automationActions/actions';
import selectors from 'modules/automationActions/selectors';
import campaignsSelectors from 'modules/campaigns/selectors';
import {stringToArray} from 'lib/utils';
import Validator from 'lib/validator';
import TestAction from '../components/TestAction';

const mapStateToProps = state => ({
  isTesting: selectors.getIsTesting(state),
  isOpen: selectors.getIsTestStarted(state),
  action: selectors.getActiveResource(state),
  body: campaignsSelectors.getCampaignBody(state)
});

const rules = {
  email: 'required|email'
};

const validate = values => {
  let errors = {};
  stringToArray(values.email).forEach(email => {
    const validator = new Validator({email}, rules);
    validator.passes();
    errors = validator.errors.all();
  });
  return errors;
};

export default reduxForm(
  {
    form: 'testAction',
    fields: ['email'],
    validate
  },
  mapStateToProps,
  actions
)(TestAction);
