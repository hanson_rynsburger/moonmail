import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from 'modules/automationActions/actions';
import selectors from 'modules/automationActions/selectors';
import automationSelectors from 'modules/automations/selectors';
import ActionView from '../components/ActionView';
import {getIsVerified} from 'modules/profile/selectors';

class Action extends Component {
  static propTypes = {
    fetchAutomationAction: PropTypes.func.isRequired,
    automationId: PropTypes.string.isRequired,
    actionId: PropTypes.string.isRequired,
    action: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {fetchAutomationAction, automationId, actionId} = this.props;
    fetchAutomationAction(actionId, automationId);
  }

  render() {
    return <ActionView {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  action: selectors.getActiveResource(state),
  automation: automationSelectors.getActiveResource(state),
  isEditable: selectors.getIsEditable(state),
  isFetching: selectors.getIsFetching(state),
  isActive: selectors.getIsActive(state),
  actionId: ownProps.params.automationActionId,
  automationId: ownProps.params.automationId,
  isVerified: getIsVerified(state)
});

export default connect(mapStateToProps, actions)(Action);
