import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import selectors from 'modules/automationActions/selectors';

export default function(ComposedComponent) {
  class RequirePermission extends Component {
    static propTypes = {
      isPermitted: PropTypes.bool.isRequired,
      automationId: PropTypes.string.isRequired,
      router: PropTypes.object.isRequired
    };

    handleRedirect({isPermitted, automationId, action}) {
      if (action.id && !isPermitted) {
        this.props.router.replace(`/automations/${automationId}`);
      }
    }

    componentWillReceiveProps(nextProps) {
      this.handleRedirect(nextProps);
    }

    componentWillMount() {
      this.handleRedirect(this.props);
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  const mapStateToProps = (state, props) => ({
    action: selectors.getActiveResource(state),
    automationId: props.params.automationId
  });

  return connect(mapStateToProps)(withRouter(RequirePermission));
}
