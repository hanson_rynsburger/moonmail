import React from 'react';
import PropTypes from 'prop-types';
import {DEFAULT_SENDER_EMAIL} from 'modules/senders/constants';
import {ACTION_TYPES} from 'modules/automationActions/constants';
import NL from 'components/NoLocalize';
import pluralize from 'pluralize';
import {joinComponents} from 'lib/utils';

const ActionSummary = ({action, sender, lists}) => {
  const listNames = lists.map(l => l && l.name).join(', ');
  const formatSender = ({name, emailAddress}) => {
    if (!name) return emailAddress;
    return `${name} <${emailAddress}>`;
  };
  return (
    <div className="ui list">
      <div className="item">
        Subject: <NL el="b">{action.subject}</NL>
      </div>
      <div className="item">
        Sender:{' '}
        <b>
          {action.senderId ? (
            <NL>{formatSender(sender)}</NL> || 'Loading...'
          ) : (
            <NL>{DEFAULT_SENDER_EMAIL}</NL>
          )}
        </b>
      </div>
      <div className="item">
        Type: <NL el="b">{ACTION_TYPES[action.type]}</NL>
      </div>
      <div className="item">
        Lists: <NL el="b">{listNames}</NL>
      </div>
      {action.attachments && (
        <div className="item">
          {pluralize('Attachment', action.attachments.length)}:{' '}
          <NL el="b">
            {joinComponents(action.attachments, (file, i) => (
              <a key={i} target="_blank" rel="noopener noreferrer" href={file.url}>
                {file.name}
              </a>
            ))}
          </NL>
        </div>
      )}
    </div>
  );
};

ActionSummary.propTypes = {
  action: PropTypes.shape({
    subject: PropTypes.string
  }).isRequired,
  sender: PropTypes.shape({
    emailAddress: PropTypes.string
  }).isRequired,
  lists: PropTypes.array.isRequired
};

export default ActionSummary;
