import React from 'react';
import PropTypes from 'prop-types';
import Input from 'components/Input';
import {stringToArray} from 'lib/utils';
import Confirm from 'components/Modal/Confirm';

const TestAction = ({
  isOpen,
  isTesting,
  fields: {email},
  handleSubmit,
  invalid,
  action,
  body,
  testAction,
  actionId,
  resetForm,
  testActionCancel
}) => {
  const onSubmit = async formProps => {
    const emails = stringToArray(formProps.email);
    const {campaign: {subject} = {}, senderId} = action;
    await testAction(actionId, {subject, body, senderId, emails});
    resetForm();
  };
  const onCancel = () => {
    testActionCancel();
    resetForm();
  };
  return (
    <Confirm
      isOpen={isOpen}
      isLoading={isTesting}
      isDisabled={invalid}
      onCancel={onCancel}
      onConfirm={handleSubmit(onSubmit)}
      headerText="Send test email"
      confirmText="Send test email"
      confirmClass="primary">
      <form className="ui form" onSubmit={handleSubmit(onSubmit)}>
        <Input type="text" label="Emails separated by comma" {...email} />
      </form>
    </Confirm>
  );
};

TestAction.propTypes = {
  actionId: PropTypes.string.isRequired,
  action: PropTypes.shape({
    subject: PropTypes.string
  }).isRequired,
  fields: PropTypes.object.isRequired,
  testAction: PropTypes.func.isRequired,
  testActionCancel: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isOpen: PropTypes.bool.isRequired,
  isTesting: PropTypes.bool.isRequired,
  body: PropTypes.string.isRequired
};

export default TestAction;
