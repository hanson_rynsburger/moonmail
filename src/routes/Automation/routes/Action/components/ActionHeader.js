import React from 'react';
import PropTypes from 'prop-types';
import {getRelativeItemLink} from 'components/Links';
import DropDownMenu from 'components/DropDownMenu';
import {Desktop, Mobile} from 'components/MediaQueries';

const ActionHeader = ({action, isEditable, isActive, isVerified, automationId}) => {
  const Link = getRelativeItemLink(`/automations/${automationId}/actions/${action.id}/`);
  const campaign = action.campaign || {};
  const links = {};

  links.settings = <Link to="settings">Settings</Link>;
  links.preview = <Link to="preview">Preview</Link>;

  if (isActive) {
    links.report = <Link to="report">Report</Link>;
    links.raw = <Link to="raw">HTML</Link>;
  }

  if (isEditable) {
    links.template = (
      <Link to="template" disabled={!isVerified}>
        Select template
      </Link>
    );
    links.html = <Link to="html">Edit code</Link>;
    if (campaign.template) {
      links.editor = (
        <Link to="editor" disabled={!isVerified}>
          Edit template
        </Link>
      );
    }
  }

  return (
    <div className="ui secondary pointing large menu no-padding">
      <Desktop>
        {links.report}
        {links.template}
        {links.editor}
        {links.html}
        {links.settings}
        {links.raw}
        {links.preview}
      </Desktop>
      <Mobile>
        {links.html}
        {links.preview}
        <DropDownMenu className="item">
          Other
          <i className="dropdown icon" />
          <div className="left menu transition hidden">
            {links.report}
            {links.template}
            {links.editor}
            {links.settings}
            {links.raw}
          </div>
        </DropDownMenu>
      </Mobile>
    </div>
  );
};

ActionHeader.propTypes = {
  action: PropTypes.object.isRequired,
  isEditable: PropTypes.bool.isRequired,
  isActive: PropTypes.bool.isRequired,
  automationId: PropTypes.string.isRequired,
  isVerified: PropTypes.bool
};

export default ActionHeader;
