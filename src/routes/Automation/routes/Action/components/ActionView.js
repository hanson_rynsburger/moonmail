import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/Loader';
import ActionHeader from './ActionHeader';
import {NO_LOCALIZE} from 'lib/constants';
import Link from 'react-router/lib/Link';
import cx from 'classnames';

const ActionView = ({
  automation,
  automationId,
  action,
  isEditable,
  isFetching,
  isActive,
  isVerified,
  children
}) => {
  if (isFetching || !action.id) {
    return (
      <section className="ui basic segment">
        <Loader active dimmer />
      </section>
    );
  }
  return (
    <section className="ui basic segment" style={{height: '100%', marginBottom: 0}}>
      <div className={cx('ui massive breadcrumb', NO_LOCALIZE)}>
        <Link to={`/automations/${automationId}`} className="section">
          {automation.name}
        </Link>
        <i className="right chevron icon divider" />
        <span className="active section">{action.name}</span>
      </div>
      <ActionHeader {...{automationId, action, isEditable, isVerified, isActive}} />
      {children}
    </section>
  );
};

ActionView.propTypes = {
  automation: PropTypes.object.isRequired,
  automationId: PropTypes.string.isRequired,
  action: PropTypes.shape({
    id: PropTypes.string,
    status: PropTypes.string
  }),
  isEditable: PropTypes.bool.isRequired,
  isVerified: PropTypes.bool,
  isActive: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  children: PropTypes.element.isRequired
};

export default ActionView;
