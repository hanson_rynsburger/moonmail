import Action from './containers/ActionContainer';
import Index from './routes/Index';
import Settings from './routes/Settings';
import Editor from './routes/Editor';
import Html from './routes/Html';
import Preview from './routes/Preview';
import Raw from './routes/Raw';
import Report from './routes/Report';
import Template from './routes/Template';

export default {
  path: 'actions/:automationActionId',
  indexRoute: Index,
  component: Action,
  childRoutes: [Settings, Html, Editor, Preview, Raw, Report, Template]
};
