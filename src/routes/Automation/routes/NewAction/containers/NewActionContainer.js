import {connect} from 'react-redux';
import NewActionView from '../components/NewActionView';
import actions from 'modules/automationActions/actions';
import selectors from 'modules/automationActions/selectors';

const mapStateToProps = (state, ownProps) => ({
  automationId: ownProps.params.automationId,
  isCreating: selectors.getIsCreating(state)
});

export default connect(mapStateToProps, actions)(NewActionView);
