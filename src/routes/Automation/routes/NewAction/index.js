import NewAction from './containers/NewActionContainer';

export default {
  path: 'actions/new',
  component: NewAction
};
