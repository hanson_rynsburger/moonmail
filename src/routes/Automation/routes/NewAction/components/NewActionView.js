import React from 'react';
import PropTypes from 'prop-types';
import SettingsForm from '../../Action/routes/Settings/containers/SettingsFromContainer';

const NewActionView = ({createResource, isCreating, automationId, router, ...formProps}) => {
  const submit = formProps => {
    createResource(automationId, formProps, true).then(data => {
      router.push(`/automations/${automationId}/actions/${data.id}`);
    });
  };
  return (
    <section className="ui vertical segment">
      <h2 className="ui header">New action</h2>
      <div className="ui stackable vertically padded grid">
        <SettingsForm
          {...formProps}
          onFormSubmit={submit}
          isLoading={isCreating}
          buttonText="Create"
        />
      </div>
    </section>
  );
};

NewActionView.propTypes = {
  isCreating: PropTypes.bool.isRequired,
  createResource: PropTypes.func.isRequired,
  automationId: PropTypes.string.isRequired,
  router: PropTypes.object.isRequired
};

export default NewActionView;
