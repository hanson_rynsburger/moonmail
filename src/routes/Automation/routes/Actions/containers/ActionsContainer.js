import React, {Component} from 'react';
import {EMPTY_OBJECT} from 'lib/constants';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import automationSelectors from 'modules/automations/selectors';
import listsSelectors from 'modules/lists/selectors';
import selectors from 'modules/automationActions/selectors';
import actions from 'modules/automationActions/actions';
import AutomationActionsView from '../components/ActionsView';
import {getIsFreeUser} from 'modules/profile/selectors';

class AutomationActions extends Component {
  static propTypes = {
    fetchResources: PropTypes.func.isRequired,
    automationId: PropTypes.string.isRequired
  };

  componentDidMount() {
    const {fetchResources, automationId} = this.props;
    fetchResources(automationId, {
      limit: 9999,
      fields: ['campaign', 'footprint', 'userId'].join(','),
      include_fields: false
    });
  }

  render() {
    return <AutomationActionsView {...this.props} />;
  }
}

const mapStateToProps = (state, props) => ({
  automationId: props.params.automationId,
  list: listsSelectors.getActiveAutomationLists(state)[0] || EMPTY_OBJECT,
  automationActions: selectors.getReverseSortedResources(state),
  isDeleteStarted: selectors.getIsDeleteStarted(state),
  isDeleting: selectors.getIsDeleting(state),
  isFetching: selectors.getIsFetchingAll(state),
  isActive: automationSelectors.getIsActive(state),
  selectedAction: selectors.getSelectedResource(state),
  isFreeUser: getIsFreeUser(state)
});

export default connect(mapStateToProps, actions)(AutomationActions);
