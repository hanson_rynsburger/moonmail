import React from 'react';
import PropTypes from 'prop-types';
import ResourceList from 'components/ResourceList';
import NoLocalize from 'components/NoLocalize';
import ActionItem from './ActionItem';
import Confirm from 'components/Modal/Confirm';
import Button from 'components/Button';
import DotHint from 'components/DotHint';

const AutomationActionsView = ({
  isFetching,
  automationActions,
  isDeleteStarted,
  isDeleting,
  deleteResourceStart,
  deleteResourceCancel,
  deleteResource,
  selectedAction,
  automationId,
  isActive,
  isFreeUser,
  list
}) => {
  return (
    <section>
      <ResourceList isFetching={isFetching} resources={automationActions} resourceName="actions">
        {(action, i) => (
          <ActionItem
            key={i}
            action={action}
            listName={list.name}
            isFreeUser={isFreeUser}
            onDelete={deleteResourceStart}
          />
        )}
      </ResourceList>
      <br />
      <Button
        to={`/automations/${automationId}/actions/new`}
        primary
        disabled={isActive || isFreeUser}>
        New action
      </Button>
      <DotHint id="actions.list.new_action" />
      <Confirm
        isOpen={isDeleteStarted}
        isLoading={isDeleting}
        onCancel={deleteResourceCancel}
        onConfirm={() => deleteResource(selectedAction.id, automationId)}
        confirmText="Delete action"
        confirmClass="negative">
        <p>
          This action will completely delete <NoLocalize el="b">"{selectedAction.name}"</NoLocalize>
        </p>
      </Confirm>
    </section>
  );
};

AutomationActionsView.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  automationActions: PropTypes.array.isRequired,
  isDeleteStarted: PropTypes.bool.isRequired,
  isDeleting: PropTypes.bool.isRequired,
  isActive: PropTypes.bool.isRequired,
  isFreeUser: PropTypes.bool.isRequired,
  deleteResourceStart: PropTypes.func.isRequired,
  deleteResourceCancel: PropTypes.func.isRequired,
  deleteResource: PropTypes.func.isRequired,
  selectedAction: PropTypes.object,
  automationId: PropTypes.string.isRequired,
  list: PropTypes.object
};

export default AutomationActionsView;
