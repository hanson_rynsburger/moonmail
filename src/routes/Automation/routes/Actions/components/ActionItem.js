import React from 'react';
import PropTypes from 'prop-types';
import classNames from './ActionItem.scss';
import Link from 'react-router/lib/Link';
import DropDownMenu from 'components/DropDownMenu';
import {NO_LOCALIZE} from 'lib/constants';
import cx from 'classnames';
import {getActionTrigger} from 'modules/automationActions/lib';

const ActionItem = ({action, onDelete, isFreeUser, listName}) => {
  const isActive = action.status === 'active';
  const iconClass = cx('icon', {
    'grey edit': !isActive,
    'green play': isActive
  });
  const baseUrl = `/automations/${action.automationId}/actions/${action.id}`;
  return (
    <div className={classNames.item}>
      <div className={cx('ui segment', classNames.itemInner, {secondary: isActive})}>
        <h3 className="ui header">
          <i className={iconClass} />
          <div className={cx('content', NO_LOCALIZE)}>
            <Link to={baseUrl}>{action.name}</Link>
            <div className="sub header">
              <b>Trigger:</b> {getActionTrigger(action, listName)}
            </div>
          </div>
        </h3>
        <div className={classNames.actions}>
          <div className="ui buttons">
            {isActive && (
              <Link to={`${baseUrl}/report`} className="ui button">
                View report
              </Link>
            )}
            {!isActive && (
              <Link to={`${baseUrl}/html`} className="ui button">
                Edit
              </Link>
            )}
            <DropDownMenu action="hide" className="button icon floating">
              <i className="dropdown icon" />
              <div className="menu">
                <Link to={`${baseUrl}/preview`} className="item">
                  Preview
                </Link>
                <Link to={`${baseUrl}/settings`} className="item">
                  Settings
                </Link>
                {((!isActive && !isFreeUser) || __DEV__) && (
                  <a className="item" onClick={() => onDelete(action.id)}>
                    Delete
                  </a>
                )}
              </div>
            </DropDownMenu>
          </div>
        </div>
      </div>
    </div>
  );
};

ActionItem.propTypes = {
  action: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    createdAt: PropTypes.number
  }),
  onDelete: PropTypes.func.isRequired,
  isFreeUser: PropTypes.bool,
  listName: PropTypes.string
};

export default ActionItem;
