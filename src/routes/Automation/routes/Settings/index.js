import Edit from './containers/SettingsContainer';

export default {
  path: 'settings',
  component: Edit
};
