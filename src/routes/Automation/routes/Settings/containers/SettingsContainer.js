import {connect} from 'react-redux';
import EditView from '../components/SettingsView';
import actions from 'modules/automations/actions';
import selectors from 'modules/automations/selectors';

const mapStateToProps = (state, ownProps) => {
  return {
    automationId: ownProps.params.automationId,
    isUpdating: selectors.getIsUpdating(state)
  };
};

export default connect(mapStateToProps, actions)(EditView);
