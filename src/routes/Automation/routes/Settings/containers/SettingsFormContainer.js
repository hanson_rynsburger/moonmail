import React, {Component} from 'react';
import {reduxForm} from 'redux-form';
import AutomationForm from '../components/SettingsForm';
import {createValidator} from 'lib/validator';
import actions from 'modules/automations/actions';
import selectors from 'modules/automations/selectors';
import * as profileSelectors from 'modules/profile/selectors';
import listSelectors from 'modules/lists/selectors';
import sendersSelectors from 'modules/senders/selectors';

class AutomationFromContainer extends Component {
  static propTypes = {};

  componentDidMount() {}

  render() {
    return <AutomationForm {...this.props} />;
  }
}

const mapStateToProps = state => {
  const automation = selectors.getActiveResource(state);
  const hasSenders = profileSelectors.getHasSenders(state);

  const rules = {
    name: 'required|max:140',
    listId: 'required',
    senderId: 'required'
  };

  return {
    isFetchingLists: listSelectors.getIsFetchingAll(state),
    isEditable: !automation.id,
    lists: listSelectors.getAvailableLists(state),
    limits: profileSelectors.getLimits(state),
    senders: sendersSelectors.getVerifiedSenders(state),
    isFetchingSenders: sendersSelectors.getIsFetchingAll(state),
    hasSenders,
    automationId: automation.id,
    initialValues: {
      senderId: sendersSelectors.getDefaultResourceId(state),
      ...automation
    },
    validate: createValidator(rules)
  };
};

export default reduxForm(
  {
    form: 'automationForm',
    fields: ['name', 'listId', 'senderId']
  },
  mapStateToProps,
  actions
)(AutomationFromContainer);
