import React from 'react';
import PropTypes from 'prop-types';
import Form from '../containers/SettingsFormContainer';

const EditView = ({automationId, updateResource, isUpdating, ...formProps}) => {
  const submit = formProps => {
    updateResource(automationId, formProps);
  };
  return (
    <section className="ui vertical segment">
      <div className="ui stackable grid">
        <Form {...formProps} onFormSubmit={submit} isLoading={isUpdating} />
      </div>
    </section>
  );
};

EditView.propTypes = {
  isUpdating: PropTypes.bool.isRequired,
  updateResource: PropTypes.func.isRequired,
  automationId: PropTypes.string.isRequired
};

export default EditView;
