import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router/lib/Link';
import Input from 'components/Input';
import Select, {SelectItem} from 'components/Select';
import Button from 'components/Button';
import DotHint from 'components/DotHint';
import SenderHint from 'components/SenderHint';

const SettingsForm = ({
  fields: {name, listId, senderId},
  lists,
  senders,
  handleSubmit,
  invalid,
  isLoading,
  isFetchingLists,
  isFetchingSenders,
  hasSenders,
  isEditable,
  onFormSubmit,
  buttonText,
  limits: {recipientsPerCampaign}
}) => {
  const isCreateListHint = isEditable && lists.length === 0 && !isFetchingLists;

  return (
    <form className="ui form nine wide column" onSubmit={handleSubmit(onFormSubmit)}>
      <Input type="text" {...name} />
      <Select
        label="Sender"
        search
        loading={isFetchingSenders}
        disabled={!isEditable}
        {...senderId}>
        {senders.map((sender, i) => (
          <SelectItem key={i} disabled={!hasSenders && !sender.default} value={sender.id}>
            {sender.fromName ? (
              <span>
                <b>{sender.fromName}</b> {`<${sender.emailAddress}>`}
              </span>
            ) : (
              sender.emailAddress
            )}
          </SelectItem>
        ))}
      </Select>
      {isEditable && <SenderHint resource="an automation" />}
      <Select
        search
        loading={isFetchingLists}
        disabled={!isEditable}
        label={<DotHint id="automation.settings.list">List</DotHint>}
        {...listId}>
        {lists.map((list, i) => (
          <SelectItem
            key={i}
            value={list.id}
            disabled={
              list.isProcessing ||
              (recipientsPerCampaign && list.subscribedCount > recipientsPerCampaign)
            }>
            {list.name} <span className="text grey">({list.subscribedCount || 0})</span>
          </SelectItem>
        ))}
      </Select>
      {isCreateListHint && (
        <div className="ui info small message">
          To create a automation you need a list. <Link to="/lists/new">Create list</Link>
        </div>
      )}
      <Button primary loading={isLoading} type="submit" disabled={invalid || isLoading}>
        {buttonText || 'Update'}
      </Button>
    </form>
  );
};

SettingsForm.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onFormSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isFetchingLists: PropTypes.bool.isRequired,
  isEditable: PropTypes.bool.isRequired,
  isFetchingSenders: PropTypes.bool.isRequired,
  hasSenders: PropTypes.bool.isRequired,
  fields: PropTypes.shape({
    name: PropTypes.object.isRequired,
    listId: PropTypes.object.isRequired
  }).isRequired,
  lists: PropTypes.arrayOf(PropTypes.object).isRequired,
  senders: PropTypes.arrayOf(PropTypes.object).isRequired,
  buttonText: PropTypes.string,
  limits: PropTypes.object
};

export default SettingsForm;
