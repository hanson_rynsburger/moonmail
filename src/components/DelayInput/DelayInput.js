import React, {Component} from 'react';
import Input from 'components/Input';
import Select, {SelectItem} from 'components/Select';
import PropTypes from 'prop-types';
import cx from 'classnames';
import classNames from './DelayInput.scss';

const DIVIDERS = {
  second: 1,
  minute: 60,
  hour: 3600,
  day: 86400,
  week: 604800
};

class DelayInput extends Component {
  static propTypes = {
    value: PropTypes.any,
    onChange: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.element])
  };

  constructor(props) {
    super(props);
    this.state = {
      divider: this.getDivider(props.value)
    };
  }

  getDivider(value = 0) {
    let divider = 1;
    Object.keys(DIVIDERS).forEach(key => {
      if (value / DIVIDERS[key] >= 1) divider = DIVIDERS[key];
    });
    return divider;
  }

  onChange = e => {
    if (e.target.value > 0) {
      this.props.onChange(e.target.value * this.state.divider);
    } else {
      this.props.onChange(e.target.value);
    }
  };

  onDividerChange = value => {
    this.setState({divider: value});
  };

  render() {
    const {value, name, label, disabled} = this.props;
    const inputValue = value > 0 ? Math.round(value / this.state.divider) : value;
    return (
      <div className={cx(classNames.fields, 'two fields')}>
        <Input
          name="delay"
          type="number"
          onChange={this.onChange}
          id={name}
          label={label}
          disabled={disabled}
          value={inputValue}
        />
        <Select
          label={false}
          name="delay divider"
          disabled={disabled}
          onChange={this.onDividerChange}
          value={this.state.divider}>
          {Object.keys(DIVIDERS).map((key, i) => (
            <SelectItem key={i} value={DIVIDERS[key]}>
              {key}(s)
            </SelectItem>
          ))}
        </Select>
      </div>
    );
  }
}

export default DelayInput;
