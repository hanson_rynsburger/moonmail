import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Progress = ({
  className,
  label,
  percent = 0,
  indicating,
  error,
  success,
  warning,
  active,
  negative,
  value
}) => {
  const style = {
    transitionDuration: '300ms',
    width: `${percent}%`,
    minWidth: value === false ? undefined : 50
  };
  return (
    <div
      className={cx('ui progress', className, {
        indicating,
        error,
        success,
        warning,
        active,
        negative
      })}
      data-percent={Math.round(percent)}>
      <div className="bar" style={style}>
        <div className="progress">{value === false ? '' : value || `${percent}%`}</div>
      </div>
      {label && <div className="label">{label}</div>}
    </div>
  );
};

Progress.propTypes = {
  className: PropTypes.string,
  label: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  percent: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.bool]),
  error: PropTypes.bool,
  success: PropTypes.bool,
  warning: PropTypes.bool,
  indicating: PropTypes.bool,
  negative: PropTypes.bool,
  active: PropTypes.bool
};

export default Progress;
