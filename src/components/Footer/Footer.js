import React from 'react';
import classNames from './Footer.scss';
import cx from 'classnames';

const Footer = () => (
  <footer className={cx('ui secondary vertical segment', classNames.footer)}>
    <div className="ui container">
      <div className={classNames.content}>
        <div>
          ©{new Date().getFullYear()}{' '}
          <a href="http://microapps.com/" target="_blank" rel="noopener noreferrer">
            microapps.com
          </a>
        </div>
        <div>
          <iframe
            src="https://ghbtns.com/github-btn.html?user=microapps&repo=MoonMail&type=star&count=true"
            frameBorder="0"
            scrolling="0"
            width="110px"
            height="20px"
          />
        </div>
        <div className="ui horizontal list">
          <a
            className="item"
            href="https://stackoverflow.com/questions/tagged/moonmail"
            target="_blank"
            rel="nofollow noopener noreferrer">
            Let the community help you out!
          </a>
          <a
            className="item"
            href="http://docs.moonmail.io/"
            target="_blank"
            rel="nofollow noopener noreferrer">
            API
          </a>
          <a
            className="item"
            href="http://support.moonmail.io/"
            target="_blank"
            rel="nofollow noopener noreferrer">
            Support
          </a>
        </div>
      </div>
    </div>
  </footer>
);

export default Footer;
