import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';

const SelectItem = ({value, children, disabled, localize = false}) => (
  <div className={cx('item', {disabled}, {[NO_LOCALIZE]: !localize})} data-value={value}>
    {children}
  </div>
);

SelectItem.propTypes = {
  value: PropTypes.any,
  children: PropTypes.any,
  disabled: PropTypes.bool,
  localize: PropTypes.bool
};

export default SelectItem;
