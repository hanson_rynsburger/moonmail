import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import {stringToArray, humanize} from 'lib/utils';
import {NO_LOCALIZE} from 'lib/constants';

class Select extends Component {
  static propTypes = {
    onBlur: PropTypes.func,
    onChange: PropTypes.func.isRequired,
    multiple: PropTypes.bool,
    allowAdditions: PropTypes.bool,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.array]),
    initDelay: PropTypes.number
  };

  componentDidMount() {
    this.element.dropdown({
      onHide: this.props.onBlur,
      onChange: this.onChange,
      allowAdditions: this.props.allowAdditions
    });
    this.selectDefaultValue();
  }

  selectDefaultValue() {
    if (this.props.value) {
      return setTimeout(() => {
        this.element.dropdown('set selected', this.props.value);
      }, this.props.initDelay);
    }
    this.element.dropdown('clear');
  }

  onChange = value => {
    if (this.props.multiple && typeof value === 'string') {
      value = stringToArray(value);
    }
    this.props.onChange(value);
  };

  componentDidUpdate() {
    this.selectDefaultValue();
  }

  render() {
    const props = this.props;
    const {localize = false} = props;
    const labelText = props.label || humanize(props.name);
    const errorText = Array.isArray(props.error) ? props.error[0] : props.error;
    const dropDownClass = cx('ui dropdown selection', {
      fluid: props.fluid,
      multiple: props.multiple,
      loading: props.loading,
      disabled: props.disabled,
      search: props.search | props.allowAdditions
    });
    return (
      <div className={cx('field', props.fieldClass, {error: props.touched && props.invalid})}>
        {props.label !== false && <label htmlFor={props.name}>{labelText}</label>}
        <div
          className={cx(dropDownClass, {[NO_LOCALIZE]: !localize})}
          ref={s => {
            this.element = $(s);
          }}>
          <input name={props.name} id={props.name} type="hidden" />
          <i className="dropdown icon" />
          <div className="default text">{props.placeholder || ''}</div>
          <div className="menu">{props.children}</div>
        </div>
        {props.hint &&
          !(props.touched && props.invalid) && <div className="hint">{props.hint}</div>}
        {props.touched &&
          props.invalid && <div className="ui basic red pointing prompt label">{errorText}</div>}
      </div>
    );
  }
}

export default Select;
