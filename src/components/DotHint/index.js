import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import DotHint from './DotHint';
import {getIsHintDisabled} from 'modules/profile/selectors';
import {getHintById} from 'modules/hints/selectors';
import {withRouter} from 'react-router';
import * as actions from 'modules/hints/actions';

class Hint extends Component {
  static propTypes = {
    isHintsDisabled: PropTypes.bool,
    children: PropTypes.any,
    hint: PropTypes.object
  };

  render() {
    if (this.props.isHintsDisabled || !this.props.hint.content) {
      return <span>{this.props.children}</span>;
    }
    return <DotHint {...this.props} />;
  }
}

const mapStateToProps = (state, ownProps) => ({
  isHintsDisabled: getIsHintDisabled(state),
  hint: getHintById(state)(ownProps.id)
});

export default connect(mapStateToProps, actions)(withRouter(Hint));
