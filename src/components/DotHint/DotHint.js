import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from './DotHint.scss';
import cx from 'classnames';
import _debug from 'debug';

const info = _debug('app:info:hint');

class DotHint extends Component {
  static propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
    hint: PropTypes.object,
    id: PropTypes.string.isRequired,
    router: PropTypes.object.isRequired,
    variables: PropTypes.object
  };

  componentDidMount() {
    const {id, router, hint} = this.props;
    this.element.popup({
      popup: this.popup,
      preserve: true,
      inline: true,
      lastResort: true,
      on: 'click',
      variation: hint.variation,
      position: hint.position || 'right center',
      onShow() {
        info(id);
        if (!this.binded) {
          this.find("a[href^='/']").on('click', function(e) {
            e.preventDefault();
            router.push($(this).attr('href'));
          });
          this.binded = true;
        }
      }
    });
  }

  componentWillUnmount() {
    this.element.popup('destroy');
  }

  render() {
    const {children, className, hint, variables} = this.props;
    let content;
    if (variables) {
      const regexp = new RegExp(
        Object.keys(variables)
          .map(v => `{${v}}`)
          .join('|'),
        'gi'
      );
      content = hint.content.replace(regexp, match => variables[match.slice(1, -1)]);
    } else {
      content = hint.content;
    }
    return (
      <span className={cx(classNames.wrapper, className)}>
        {children}
        <span className={cx(classNames.hint)} ref={n => (this.element = $(n))}>
          <span className={classNames.pulse} />
          <span className={classNames.dot} />
          <div
            className={cx('ui popup transition hidden', {flowing: hint.flowing})}
            dangerouslySetInnerHTML={{__html: content}}
            ref={n => (this.popup = $(n))}
          />
        </span>
      </span>
    );
  }
}

export default DotHint;
