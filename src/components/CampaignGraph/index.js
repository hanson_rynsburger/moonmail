import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import CampaignGraph from './CampaignGraph';
import selectors from 'modules/campaigns/selectors';
import actions from 'modules/campaigns/actions';
import {DEMO_GRAPH_DATA} from 'modules/campaigns/constants';
import {propsChanged} from 'lib/utils';

class CampaignGraphContainer extends PureComponent {
  static propTypes = {
    params: PropTypes.shape({
      campaignId: PropTypes.string,
      campaignName: PropTypes.string,
      start: PropTypes.number,
      end: PropTypes.number
    }).isRequired,
    data: PropTypes.shape({
      opens: PropTypes.array,
      clicks: PropTypes.array,
      timestamps: PropTypes.array
    }).isRequired,
    isLoading: PropTypes.bool,
    isFetching: PropTypes.bool,
    isDemo: PropTypes.bool,
    fetchCampaignGraphData: PropTypes.func.isRequired
  };

  componentDidMount() {
    if (this.props.params.campaignId) {
      this.props.fetchCampaignGraphData(this.props.params);
    }
  }

  componentWillReceiveProps({params}) {
    if (propsChanged(params, this.props.params)) {
      this.props.fetchCampaignGraphData(params);
    }
  }

  render() {
    let {data, params, isLoading, isFetching, isDemo} = this.props;
    if (isDemo) {
      data = DEMO_GRAPH_DATA.data;
      params = DEMO_GRAPH_DATA.params;
    }
    return <CampaignGraph isLoading={isLoading || isFetching} data={data} params={params} />;
  }
}

const mapStateToProps = state => ({
  isFetching: selectors.getIsFetchingGraphData(state),
  data: selectors.getGraphData(state)
});

export default connect(mapStateToProps, actions)(CampaignGraphContainer);
