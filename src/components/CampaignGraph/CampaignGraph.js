import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {LineChart} from 'components/Charts';
import classNames from './CampaignGraph.scss';
import {formatDate} from 'lib/utils';
import cx from 'classnames';
import Loader from 'components/Loader';
import Button from 'components/Button';

const dataSetConfig = {
  lineTension: 0.1,
  pointRadius: 4,
  pointBorderColor: '#FFFFFF',
  pointHoverBackgroundColor: '#FFFFFF',
  pointHoverRadius: 5
};

const timeFormat = {
  showTime: true,
  format: 'MMM D,'
};

class CampaignGraphView extends Component {
  static propTypes = {
    params: PropTypes.shape({
      campaignId: PropTypes.string,
      campaignName: PropTypes.string,
      start: PropTypes.number,
      end: PropTypes.number
    }).isRequired,
    data: PropTypes.shape({
      opens: PropTypes.array,
      clicks: PropTypes.array,
      timestamps: PropTypes.array
    }).isRequired,
    isLoading: PropTypes.bool
  };

  render() {
    const {
      data: {opens = [], clicks = [], timestamps = []},
      params: {campaignId, campaignName, start, end},
      isLoading
    } = this.props;
    const getTitle = () => {
      if (!campaignId) return '';
      return `${campaignName} ${formatDate(start, timeFormat)} - ${formatDate(end, timeFormat)}`;
    };
    return (
      <div className={cx('ui segment', classNames.chart)}>
        {isLoading ? (
          <Loader active inline={false} />
        ) : (
          <div>
            <LineChart
              labels={timestamps}
              datasets={[
                {
                  data: clicks,
                  label: 'Clicks',
                  borderColor: '#21BA45',
                  spanGaps: true,
                  pointBackgroundColor: '#229b44',
                  pointHoverBorderColor: '#229b44',
                  backgroundColor: 'rgba(34,155,68,0.2)',
                  ...dataSetConfig
                },
                {
                  data: opens,
                  label: 'Opens',
                  borderColor: '#1c879b',
                  spanGaps: true,
                  pointBackgroundColor: '#1C9AAE',
                  pointHoverBorderColor: '#1C9AAE',
                  backgroundColor: 'rgba(28,154,174,0.2)',
                  ...dataSetConfig
                }
              ]}
              options={{
                maintainAspectRatio: false,
                title: {
                  display: true,
                  fontSize: 16,
                  lineHeight: 1.6,
                  text: getTitle()
                },
                animation: {
                  animateScale: true
                },
                legend: {
                  position: 'bottom',
                  labels: {
                    fontSize: 14
                  }
                },
                tooltips: {
                  mode: 'x',
                  position: 'nearest',
                  xPadding: 10,
                  yPadding: 10,
                  titleFontFamily: 'Helvetica, sans-serif',
                  bodyFontFamily: 'Helvetica, sans-serif',
                  displayColors: false
                },
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        beginAtZero: true,
                        min: 0,
                        callback(value) {
                          if (Number.isInteger(value)) {
                            return value;
                          }
                        }
                      }
                    }
                  ],
                  xAxes: [
                    {
                      type: 'time',
                      autoSkipPadding: 10,
                      time: {
                        isoWeekday: true,
                        tooltipFormat: 'MMM D, YYYY H:mm',
                        displayFormats: {
                          minute: 'H:mm',
                          hour: 'H:mm'
                        }
                      }
                    }
                  ]
                }
              }}
            />
            {campaignId === 'demo' && (
              <div className="ui inverted dimmer transparent active">
                <div className="content">
                  <div className="center">
                    <h3 className="ui header">Your campaign can look like this!</h3>
                    <Button to="campaigns/new" primary>
                      Create Campaign
                    </Button>
                  </div>
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default CampaignGraphView;
