import React from 'react';
import MediaQuery from 'react-responsive';

export const MOBILE_BREAKPOINT = 768;
export const Mobile = props => <MediaQuery maxWidth={MOBILE_BREAKPOINT} {...props} />;
export const Desktop = props => <MediaQuery minWidth={MOBILE_BREAKPOINT + 1} {...props} />;
export const MobileMatch = props => <MediaQuery maxWidth={MOBILE_BREAKPOINT} {...props} />;
