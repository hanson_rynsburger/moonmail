import PaymentComponent from './Payment';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter} from 'react-router';
import {BillingFormContainer} from 'components/BillingForm';
import * as actions from 'modules/billingInfo/actions';
import * as selectors from 'modules/billingInfo/selectors';
import * as profileSelectors from 'modules/profile/selectors';
import {compose} from 'recompose';

export const PaymentContainer = ComposedComponent => {
  class Payment extends Component {
    static propTypes = {
      loadStripe: PropTypes.func.isRequired
    };

    componentDidMount() {
      this.props.loadStripe();
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  const mapStateToProps = state => ({
    paymentMethod: profileSelectors.getPaymentMethod(state),
    isStripeLoading: selectors.getIsStripeLoading(state),
    isTokenCreating: selectors.getIsTokenCreating(state),
    isUpdating: selectors.getIsUpdating(state)
  });

  return connect(mapStateToProps, actions)(withRouter(Payment));
};

export default compose(PaymentContainer, BillingFormContainer)(PaymentComponent);
