import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {capitalize} from 'lib/utils';
import Button from 'components/Button';
import Modal from 'components/Modal';
import {BillingForm} from 'components/BillingForm';
import confirmClassNames from 'components/Modal/Confirm.scss';
import classNames from './Payment.scss';
import cx from 'classnames';

const Aux = props => props.children;

const STEP_PREVIEW = 'STEP_PREVIEW';
const STEP_PAYMENT_FORM = 'STEP_PAYMENT_FORM';

class Payment extends Component {
  static propTypes = {
    /*
     Actions and state.
     */
    isOpen: PropTypes.bool.isRequired,
    isProcessing: PropTypes.bool.isRequired,
    process: PropTypes.func.isRequired,
    processCancel: PropTypes.func.isRequired,

    /*
     Configuration.
     */
    price: PropTypes.number,
    subscriptions: PropTypes.array,
    headerText: PropTypes.any,
    confirmText: PropTypes.any,
    hasSeparatePreview: PropTypes.bool,
    isHandledBySupport: PropTypes.bool,
    supportMessage: PropTypes.string,
    isContactInfoNeeded: PropTypes.bool,
    children: PropTypes.any,
    contentClass: PropTypes.string,

    /*
     Payment form flow.
     */
    paymentMethod: PropTypes.object,
    createToken: PropTypes.func.isRequired,
    isTokenCreating: PropTypes.bool.isRequired,
    resetForm: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,

    router: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      step: STEP_PREVIEW
    };
  }

  shouldComponentUpdate(nextProps) {
    return !(!this.props.paymentMethod && nextProps.paymentMethod);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isOpen !== this.props.isOpen) {
      const state = {step: STEP_PREVIEW};
      if (nextProps.subscriptions) {
        state.type = nextProps.subscriptions.find(s => s.primary).type;
      }
      this.setState(state);
    }
  }

  showPaymentForm = () => {
    this.setState({step: STEP_PAYMENT_FORM});
  };

  processThenClose = (subscriptionType = null, token = null) => {
    this.props.process({subscriptionType, token}).then(() => this.close());
  };

  onPaymentFormSubmit = async (formProps, subscriptionType = null) => {
    const {price, paymentMethod, createToken} = this.props;

    let token = null;

    if (price && !paymentMethod) {
      token = await createToken(formProps);
    }

    this.processThenClose(subscriptionType, token);
  };

  close = () => {
    this.props.processCancel();
    this.props.resetForm();
  };

  buildButtons = () => {
    const {
      price,
      subscriptions,
      confirmText,
      isProcessing,
      isTokenCreating,
      isHandledBySupport,
      supportMessage = 'Hello',
      isContactInfoNeeded,
      handleSubmit
    } = this.props;
    const neededSteps = this.getNeededSteps();
    const closeButton = (
      <Button key="-1" onClick={() => this.close()}>
        Cancel
      </Button>
    );
    const actionButtons = [closeButton];

    const addActionButton = (content, onClick, primary = true) => {
      actionButtons.push(
        <Button
          key={actionButtons.length}
          loading={isProcessing || isTokenCreating}
          disabled={isProcessing || isTokenCreating}
          onClick={onClick}
          primary={primary}>
          {content}
        </Button>
      );
    };

    if (isHandledBySupport) {
      addActionButton('Contact support', () => Intercom('showNewMessage', supportMessage));
    } else if (isContactInfoNeeded) {
      addActionButton('Provide your contact information', () => {
        this.close();
        this.props.router.push('/profile/contact-info');
      });
    } else if (price && subscriptions) {
      const sub = subscriptions.find(s => s.type === this.state.type);
      const onClick = neededSteps.paymentForm
        ? handleSubmit(formProps => this.onPaymentFormSubmit(formProps, sub.type))
        : () => this.processThenClose(sub.type);

      addActionButton('Change plan', onClick);
    } else if (price) {
      const onClick = neededSteps.paymentForm
        ? handleSubmit(formProps => this.onPaymentFormSubmit(formProps))
        : () => this.processThenClose();

      addActionButton(confirmText || `Get for $${price}`, onClick);
    } else {
      addActionButton(confirmText || 'Get for free', () => this.processThenClose());
    }

    if (neededSteps.both) {
      return {
        preview: [
          closeButton,
          <Button key="0" primary onClick={this.showPaymentForm}>
            Proceed to payment
          </Button>
        ],
        paymentForm: actionButtons
      };
    } else if (neededSteps.paymentForm) {
      return {
        paymentForm: actionButtons
      };
    } else {
      return {
        preview: actionButtons
      };
    }
  };

  getFormattedSubType = sub => {
    if (sub.type === 'annual') {
      return `${capitalize(sub.type)}ly`;
    }
    return capitalize(sub.type);
  };

  getNeededSteps = () => {
    const {
      price,
      hasSeparatePreview,
      isHandledBySupport,
      isContactInfoNeeded,
      paymentMethod
    } = this.props;

    const isFree = !price;
    const isReadyToPurchase = !isFree && !isHandledBySupport && !isContactInfoNeeded;
    const paymentForm = isReadyToPurchase && !paymentMethod;
    const preview = !paymentForm || hasSeparatePreview;
    const both = preview && paymentForm;

    return {preview, paymentForm, both};
  };

  renderSubscriptions() {
    const {subscriptions, price} = this.props;
    if (!price || !subscriptions) return null;
    const {type} = this.state;
    return (
      <div className="ui form">
        <div className="equal width fields">
          {subscriptions.sort(s => !s.primary).map((sub, i) => {
            const checked = type === sub.type;
            const df = sub.discountFactor;
            const amount = (price * (1 - df)).toFixed(2);
            const title = sub.title || (
              <span>
                <strong>
                  Pay {this.getFormattedSubType(sub)}{' '}
                  {df > 0 && <span className="text primary">({df * 100}% discount)</span>}
                </strong>
                <br />
                ${amount} per month
              </span>
            );

            return (
              <div className="field" key={i}>
                <div
                  className={cx('ui radio checkbox', classNames.subCheckbox, {
                    [classNames.checked]: checked
                  })}
                  onClick={() => this.setState({type: sub.type})}>
                  <input
                    type="radio"
                    name="type"
                    value={sub.type}
                    checked={checked}
                    onChange={() => {}}
                  />
                  <label>{title}</label>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }

  renderSubSummary() {
    const {subscriptions, price} = this.props;
    if (!price || !subscriptions) return null;
    const sub = subscriptions.find(s => s.type === this.state.type) || {};
    const amount = (price * sub.quantity * (1 - sub.discountFactor)).toFixed(2);
    return (
      <table className="ui basic striped table" key={amount}>
        <tbody>
          <tr>
            <td>
              <h4>Amount being charged today:</h4>
            </td>
            <td className="right aligned">
              <span className={classNames.amount}>${amount}</span>
            </td>
          </tr>
          <tr>
            <td colSpan={2}>
              You'll be billed immediately, but we offer a <em>30-day Refund Policy</em> if you're
              not happy.{' '}
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="http://support.moonmail.io/faq/pricing/moonmail-pricing">
                Click here
              </a>{' '}
              to read our full Pricing Policy. The next charge on your {sub.type} billing cycle will
              be for ${amount}.
            </td>
          </tr>
        </tbody>
      </table>
    );
  }

  render() {
    const {isOpen, headerText, children, contentClass = 'content'} = this.props;
    const {step} = this.state;
    const neededSteps = this.getNeededSteps();
    const buttons = this.buildButtons();
    const isPreviewOpen = neededSteps.both ? step === STEP_PREVIEW : neededSteps.preview || false;
    const isPaymentFormOpen = neededSteps.both
      ? step === STEP_PAYMENT_FORM
      : neededSteps.paymentForm;
    return (
      <Aux>
        <Modal isOpen={isOpen && isPreviewOpen} size="small">
          <div className="header">{headerText}</div>
          <div className={contentClass}>
            {this.renderSubscriptions()}
            {children}
            {this.renderSubSummary()}
          </div>
          <div className="actions">{buttons.preview}</div>
        </Modal>
        <Modal isOpen={isOpen && isPaymentFormOpen} size="small">
          <div className="header">{headerText}</div>
          <div className="content">
            {this.renderSubscriptions()}
            {!neededSteps.both && children}
            <BillingForm inline {...this.props} />
            {this.renderSubSummary()}
          </div>
          <div className="actions">{buttons.paymentForm}</div>
          <div className={confirmClassNames.branding}>
            <div>
              powered by{' '}
              <a href="https://monei.net" target="_blank" rel="noopener noreferrer">
                MONEI
              </a>
            </div>
          </div>
        </Modal>
      </Aux>
    );
  }
}

export default Payment;
