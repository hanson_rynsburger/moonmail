import React, {Component} from 'react';
import {connect} from 'react-redux';
import WelcomeModal from './WelcomeModal';
import {withRouter} from 'react-router';
import {getIsWelcomePopUpVisible} from 'modules/app/selectors';

class WelcomeModalContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({isOpen: true});
    }, 1000);
  }

  handleCancel = () => {
    this.setState({isOpen: false});
  };

  handleConfirm = () => {
    this.setState({isOpen: false});
    this.props.router.push('/profile/plan');
  };

  render() {
    if (!this.props.isActive) return null;
    return (
      <WelcomeModal
        isOpen={this.state.isOpen}
        onCancel={this.handleCancel}
        onConfirm={this.handleConfirm}
      />
    );
  }
}

const mapStateToProps = state => ({
  isActive: getIsWelcomePopUpVisible(state)
});

export default connect(mapStateToProps)(withRouter(WelcomeModalContainer));
