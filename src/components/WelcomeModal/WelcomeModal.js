import React from 'react';
import Modal from 'components/Modal';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import welcome from 'static/welcome.svg';

const UpgradeModal = ({isOpen, onCancel, onConfirm}) => (
  <Modal isOpen={isOpen} size="tiny">
    <div className="content text center">
      <p>
        <img width={150} height={150} src={welcome} alt="" />
      </p>
      <p>
        Welcome to MoonMail! You just joined more than half a million businesses that choose
        MoonMail to help them reach success.
      </p>
      <p>
        Come on in and see everything MoonMail can do for you! <br />
        Or <b>upgrade today and get a 20% discount!</b>
      </p>
    </div>
    <div className="actions">
      <Button onClick={onCancel}>Use for free</Button>
      <Button onClick={onConfirm} primary>
        Upgrade and save 20%
      </Button>
    </div>
  </Modal>
);

UpgradeModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired
};

export default UpgradeModal;
