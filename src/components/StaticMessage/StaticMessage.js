import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const TYPE_ICONS = {
  info: 'info',
  warning: 'announcement',
  error: 'ban'
};

class StaticMessage extends Component {
  static defaultProps = {
    icon: true,
    dismissible: true
  };
  static propTypes = {
    className: PropTypes.string,
    icon: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    type: PropTypes.string,
    header: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    children: PropTypes.any,
    dismissible: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {
      isDismissed: false
    };
  }

  onClose = () => {
    this.setState({isDismissed: true});
  };

  render() {
    const {className, icon, type, header, children, dismissible} = this.props;
    if (this.state.isDismissed) return null;
    const iconClass = typeof icon === 'boolean' ? TYPE_ICONS[type] : icon;
    return (
      <div className={cx('ui message', className, type, {icon})}>
        {dismissible && type !== 'error' && <i className="close icon" onClick={this.onClose} />}
        {icon && <i className={cx('icon', iconClass)} />}
        <div className="content">
          <div className="header">{header}</div>
          {React.Children.count(children) > 1 ? children : <p>{children}</p>}
        </div>
      </div>
    );
  }
}

export default StaticMessage;
