import ResourceList from './ResourceList';
import _PaginatedList from './PaginatedList';

export const PaginatedList = _PaginatedList;
export default ResourceList;
