import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from './ResourceList.scss';
import cx from 'classnames';
import Loader from 'components/Loader';
import space from 'static/space.svg';

export default class ResourceList extends Component {
  static propTypes = {
    isFetching: PropTypes.bool.isRequired,
    resources: PropTypes.array.isRequired,
    children: PropTypes.func.isRequired,
    resourceName: PropTypes.string.isRequired,
    notFoundText: PropTypes.any,
    className: PropTypes.string,
    from: PropTypes.number,
    total: PropTypes.number,
    size: PropTypes.number,
    changePage: PropTypes.func.isRequired,
    disableLoader: PropTypes.bool,
    richPlaceholder: PropTypes.bool,
    richPlaceholderCTA: PropTypes.element
  };

  static defaultProps = {
    className: 'ui big very relaxed divided list',
    from: 0,
    size: 10
  };

  renderPlaceholder() {
    const {richPlaceholder, notFoundText, resourceName, richPlaceholderCTA} = this.props;
    if (richPlaceholder) {
      return (
        <div className={classNames.richPlaceholder}>
          <img width={150} height={150} src={space} />
          <h3>
            There is nothing here yet. <br />Except black matter.
          </h3>
          <p className="text grey">(Cause Black Matter's everywhere)</p>
          {richPlaceholderCTA}
        </div>
      );
    }
    return <h3 className="ui header">{notFoundText || `No ${resourceName} were found.`}</h3>;
  }

  render() {
    const {
      isFetching,
      resources,
      children,
      className,
      from,
      total,
      size,
      changePage,
      disableLoader
    } = this.props;
    const nextFrom = from + size < total ? from + size : null;
    const prevFrom = from - size >= 0 ? from - size : null;

    return (
      <div className={classNames.wrapper}>
        <Loader
          active={isFetching && !disableLoader}
          inline={false}
          dimmerClassName={classNames.dimmer}
        />
        {resources.length === 0 && (
          <div className={classNames.placeholder}>{!isFetching && this.renderPlaceholder()}</div>
        )}
        {resources.length > 0 && <div className={className}>{resources.map(children)}</div>}
        {total > size && (
          <div className="ui right floated pagination menu">
            <a
              className={cx('icon item', {disabled: prevFrom === null || isFetching})}
              onClick={() => changePage({from: prevFrom, size})}>
              <i className="left chevron icon" />
            </a>
            <a
              className={cx('icon item', {disabled: nextFrom === null || isFetching})}
              onClick={() => changePage({from: nextFrom, size})}>
              <i className="right chevron icon" />
            </a>
          </div>
        )}
      </div>
    );
  }
}
