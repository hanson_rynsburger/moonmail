import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from './ResourceList.scss';
import cx from 'classnames';
import Loader from 'components/Loader';
import space from 'static/space.svg';

export default class ResourceList extends Component {
  static propTypes = {
    isFetching: PropTypes.bool.isRequired,
    resources: PropTypes.array.isRequired,
    children: PropTypes.func.isRequired,
    resourceName: PropTypes.string,
    notFoundText: PropTypes.any,
    className: PropTypes.string,
    dimmerClass: PropTypes.string,
    pageSize: PropTypes.number,
    richPlaceholder: PropTypes.bool,
    richPlaceholderCTA: PropTypes.element
  };

  static defaultProps = {
    pageSize: 10,
    className: 'ui big very relaxed divided list',
    placeholderClass: 'item center aligned'
  };

  constructor(props) {
    super(props);
    this.state = {
      page: 1
    };
  }

  changePage(page) {
    this.setState({page});
  }

  renderPlaceholder() {
    const {richPlaceholder, notFoundText, resourceName, richPlaceholderCTA} = this.props;
    if (richPlaceholder) {
      return (
        <div className={classNames.richPlaceholder}>
          <img width={150} height={150} src={space} />
          <h3>
            There is nothing here yet. <br />Except black matter.
          </h3>
          <p className="text grey">(Cause Black Matter's everywhere)</p>
          {richPlaceholderCTA}
        </div>
      );
    }
    return <h3 className="ui header">{notFoundText || `No ${resourceName} were found.`}</h3>;
  }

  render() {
    const {isFetching, resources, children, pageSize, className, dimmerClass} = this.props;
    const currentPage = this.state.page;
    const lastIndex = currentPage * pageSize;
    const firstIndex = lastIndex - pageSize;
    const pages = Math.ceil(resources.length / pageSize);
    const visibleResources = resources.slice(firstIndex, lastIndex);
    return (
      <div className={classNames.wrapper}>
        <Loader
          active={isFetching}
          inline={false}
          dimmerClassName={cx(classNames.dimmer, dimmerClass)}
        />
        {resources.length === 0 && (
          <div className={classNames.placeholder}>{!isFetching && this.renderPlaceholder()}</div>
        )}
        {resources.length > 0 && <div className={className}>{visibleResources.map(children)}</div>}
        {pages > 1 && (
          <div className="ui right floated pagination menu">
            <a
              className={cx('icon item', {disabled: currentPage <= 1})}
              onClick={() => this.changePage(currentPage - 1)}>
              <i className="left chevron icon" />
            </a>
            <a
              className={cx('icon item', {disabled: currentPage >= pages})}
              onClick={() => this.changePage(currentPage + 1)}>
              <i className="right chevron icon" />
            </a>
          </div>
        )}
      </div>
    );
  }
}
