import React, {Component} from 'react';

class CodeMirror extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ReactCodeMirror: null,
      isReady: false
    };
  }

  async loadLib() {
    const [{Controlled}] = await Promise.all([
      import(/* webpackChunkName: "codemirror" */ 'react-codemirror2'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/lib/codemirror.css'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/theme/material.css'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/mode/htmlmixed/htmlmixed'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/addon/edit/matchbrackets'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/addon/edit/closebrackets'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/addon/edit/closetag'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/addon/dialog/dialog'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/addon/dialog/dialog.css'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/addon/search/searchcursor'),
      import(/* webpackChunkName: "codemirror" */ 'codemirror/addon/search/search')
    ]);
    this.setState({ReactCodeMirror: Controlled, isReady: true});
  }

  componentDidMount() {
    this.loadLib();
  }

  render() {
    const {ReactCodeMirror, isReady} = this.state;
    if (!isReady) return <div className={this.props.className} />;
    return <ReactCodeMirror {...this.props} options={{theme: 'material', ...this.props.options}} />;
  }
}

export default CodeMirror;
