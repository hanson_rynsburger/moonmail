import NoLocalize from './NoLocalize';
import {NO_LOCALIZE as NL} from 'lib/constants';

export const NO_LOCALIZE = NL;
export default NoLocalize;
