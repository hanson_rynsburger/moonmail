import React from 'react';
import PropTypes from 'prop-types';
import {NO_LOCALIZE} from 'lib/constants';
import cx from 'classnames';

const NoLocalize = ({children, el = 'span', className, ...props}) =>
  React.createElement(el, {className: cx(NO_LOCALIZE, className), ...props}, children);

NoLocalize.propTypes = {
  children: PropTypes.any,
  el: PropTypes.string,
  className: PropTypes.string
};

export default NoLocalize;
