import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import classNames from './PopUp.scss';

class PopUp extends Component {
  static propTypes = {
    variation: PropTypes.string,
    on: PropTypes.string,
    showDelay: PropTypes.number,
    closable: PropTypes.bool,
    position: PropTypes.string,
    children: PropTypes.any.isRequired,
    className: PropTypes.string,
    lastResort: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    content: PropTypes.any.isRequired,
    flowing: PropTypes.bool
  };

  componentDidMount() {
    const {variation, on, showDelay, closable, lastResort, position = 'right center'} = this.props;
    this.element.popup({
      popup: this.popup,
      on,
      variation,
      position,
      closable,
      lastResort
    });
    if (showDelay) {
      setTimeout(() => {
        this.element.popup('show');
      }, showDelay);
    }
  }

  componentWillUnmount() {
    this.element.popup('destroy');
  }

  render() {
    const {children, className, content, flowing} = this.props;
    return (
      <span className={cx(classNames.wrapper, className)} ref={n => (this.element = $(n))}>
        {children}
        <span
          className={cx('ui popup transition hidden', {flowing})}
          ref={n => (this.popup = $(n))}>
          {content}
        </span>
      </span>
    );
  }
}

export default PopUp;
