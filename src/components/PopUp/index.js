import React from 'react';
import PopUp from './PopUp';

export default props => {
  if (props.disabled) return props.children;
  return <PopUp {...props} />;
};
