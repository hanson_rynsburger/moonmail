import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import moon from 'static/moon.svg';
import rocket from 'static/rocket.svg';
import classNames from './Loader.scss';

const Loader = ({
  inline = true,
  active,
  className,
  indeterminate,
  dimmerClassName,
  text,
  custom
}) => {
  const message = text || 'Loading';
  if (custom) {
    return (
      <div className={cx('ui inverted dimmer', dimmerClassName, {active})} style={{zIndex: 99}}>
        <div className={classNames.customLoader}>
          <img src={moon} alt="moon" className={classNames.moon} />
          <img src={rocket} alt="rocket" className={classNames.rocket} />
        </div>
        {message}
      </div>
    );
  }
  if (inline) {
    return (
      <div className={cx('ui centered loader inline text', className, {active, indeterminate})}>
        {message}
      </div>
    );
  }
  return (
    <div className={cx('ui inverted dimmer', dimmerClassName, {active})} style={{zIndex: 99}}>
      <div className={cx(className, 'ui loader text', {indeterminate})}>{message}</div>
    </div>
  );
};

Loader.propTypes = {
  active: PropTypes.bool,
  inline: PropTypes.bool,
  indeterminate: PropTypes.bool,
  text: PropTypes.string,
  className: PropTypes.string,
  dimmerClassName: PropTypes.string,
  custom: PropTypes.bool
};

export default Loader;
