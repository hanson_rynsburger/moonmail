import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const StaticMessages = ({children, className, hideAlerts = false}) => {
  const error = React.Children.toArray(children).find(
    msg => msg.props.active && msg.props.type === 'error'
  );
  if (error) {
    return <div className={className}>{React.cloneElement(error)}</div>;
  }
  return (
    <div className={cx(className, {hidden: hideAlerts})}>
      {React.Children.map(children, msg => {
        if (!msg.props.active) return;
        return React.cloneElement(msg);
      })}
    </div>
  );
};

StaticMessages.propTypes = {
  children: PropTypes.any,
  className: PropTypes.string,
  hideAlerts: PropTypes.bool
};

export default StaticMessages;
