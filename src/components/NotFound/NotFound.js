import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/Loader';
import {capitalize} from 'lib/utils';

const NotFound = ({isFetching, resource = 'Resource', className = 'ui vertical segment'}) => (
  <section className={className}>
    {isFetching ? (
      <Loader active inline />
    ) : (
      <h1 className="ui center aligned icon header">
        <i className="frown red icon" />
        <div className="content">Oops! {capitalize(resource)} not found.</div>
      </h1>
    )}
  </section>
);

NotFound.propTypes = {
  isFetching: PropTypes.bool,
  resource: PropTypes.string,
  className: PropTypes.string
};

export default NotFound;
