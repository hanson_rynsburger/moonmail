import PropTypes from 'prop-types';
import React, {Component} from 'react';
import cx from 'classnames';
import {NO_LOCALIZE} from 'lib/constants';

class DropDownMenu extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    className: PropTypes.string,
    action: PropTypes.string,
    children: PropTypes.any,
    isControlled: PropTypes.bool,
    localize: PropTypes.bool,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.array])
  };

  static defaultProps = {
    resetIndex: 0,
    activeClass: 'primary'
  };

  onChange = (value, text, $selectedItem) => {
    const {onChange, action} = this.props;
    if (['hide', 'nothing'].include(action)) {
      onChange && onChange(value, text);
      return;
    }

    if (onChange) {
      onChange(value, text, $selectedItem);
    }
  };

  componentDidMount() {
    this.element.dropdown({
      onChange: this.onChange
    });
    this.props.isControlled && this.selectDefaultValue();
  }

  selectDefaultValue() {
    if (this.props.value) {
      return setTimeout(() => {
        this.element.dropdown('set selected', this.props.value);
      });
    }
    this.element.dropdown('clear');
  }

  componentDidUpdate() {
    this.props.isControlled && this.selectDefaultValue();
  }

  render() {
    const {className, children, localize = true} = this.props;
    return (
      <div
        className={cx('ui dropdown', className, {[NO_LOCALIZE]: !localize})}
        ref={n => {
          this.element = $(n);
        }}>
        {children}
      </div>
    );
  }
}

export default DropDownMenu;
