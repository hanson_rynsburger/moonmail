import React, {Component} from 'react';
import cx from 'classnames';
import {humanize, omitProps} from 'lib/utils';
import 'emoji-mart/css/emoji-mart.css';
import classNames from './EmojiInput.scss';
import ClickOutside from 'react-click-outside';
import {NO_LOCALIZE} from 'components/NoLocalize';

class EmojiInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Picker: null,
      isOpen: false,
      isReady: false,
      wasOpen: false
    };
  }

  async loadLib() {
    const {Picker} = await import(/* webpackChunkName: "emoji-mart" */ 'emoji-mart');
    this.setState({Picker, isReady: true});
  }

  componentDidMount() {
    this.loadLib();
  }

  handleToggle = () => {
    this.setState({isOpen: !this.state.isOpen, wasOpen: true});
  };

  handleClose = () => {
    this.setState({isOpen: false});
  };

  handleInsert = emoji => {
    this.handleClose();
    const {onChange, value = ''} = this.props;
    const start = this.input.selectionStart;
    const end = this.input.selectionEnd;
    onChange(value.substring(0, start) + emoji.native + value.substring(end));
  };

  render() {
    const {invalid, error, touched, fieldClass, label, hint, ...rest} = this.props;
    const labelText = label || humanize(rest.name);
    const errorText = Array.isArray(error) ? error[0] : error;
    const inputProps = omitProps(
      rest,
      'initialValue',
      'autofill',
      'onUpdate',
      'valid',
      'dirty',
      'pristine',
      'active',
      'visited',
      'autofilled'
    );
    const {Picker, isReady, isOpen, wasOpen} = this.state;
    const iconClass = cx('smile outline large link icon', classNames.icon, {
      [classNames.disabled]: inputProps.disabled || !isReady
    });
    return (
      <ClickOutside
        onClickOutside={this.handleClose}
        className={cx('field', classNames.input, fieldClass, {error: touched && invalid})}>
        {label !== false && <label htmlFor={rest.name}>{labelText}</label>}
        <div className="ui icon input">
          <input {...inputProps} id={rest.name} ref={n => (this.input = n)} />
          <i className={iconClass} onClick={this.handleToggle} />
          {isReady &&
            wasOpen && (
              <div
                className={cx(NO_LOCALIZE, classNames.picker, {
                  [classNames.pickerVisible]: isOpen
                })}>
                <Picker
                  onClick={this.handleInsert}
                  emojiSize={22}
                  native
                  color="#404b6d"
                  title="Pick your emoji..."
                  emoji="point_up"
                  autoFocus
                />
              </div>
            )}
        </div>
        {hint && !(touched && invalid) && <div className="hint">{hint}</div>}
        {touched &&
          invalid && <div className="ui basic red pointing prompt label">{errorText}</div>}
      </ClickOutside>
    );
  }
}

export default EmojiInput;
