import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class Search extends Component {
  static defaultProps = {
    searchDelay: 1000
  };

  static propTypes = {
    onSearch: PropTypes.func.isRequired,
    searchDelay: PropTypes.number,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    className: PropTypes.string,
    inputClass: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.value || ''
    };
  }

  debouncedChange(value) {
    const {searchDelay} = this.props;
    return new Promise(resolve => {
      if (this.timerId) clearTimeout(this.timerId);
      this.timerId = setTimeout(() => resolve(value), searchDelay);
    });
  }

  handleKeydown = e => {
    if (e.key === 'Enter') {
      this.props.onSearch(e.target.value);
    }
    if (e.key === 'Escape') {
      this.clearInput(e);
    }
  };

  handleChange = async e => {
    const value = e.target.value;
    this.setState({value});
    await this.debouncedChange(value);
    this.props.onSearch(value);
  };

  clearInput = e => {
    e.preventDefault();
    this.props.onSearch('');
    this.setState({value: ''});
  };

  componentWillReceiveProps({value}) {
    if (!value) {
      this.setState({value: ''});
    }
  }

  render() {
    const {placeholder, className, inputClass} = this.props;
    return (
      <div className={cx('ui search', className)}>
        <div className={cx('ui icon input', inputClass)}>
          <input
            type="text"
            placeholder={placeholder || 'Search keyword...'}
            value={this.state.value}
            onKeyDown={this.handleKeydown}
            onChange={this.handleChange}
          />
          {this.state.value ? (
            <i className="link remove icon" onClick={this.clearInput} />
          ) : (
            <i className="search icon" />
          )}
        </div>
      </div>
    );
  }
}

export default Search;
