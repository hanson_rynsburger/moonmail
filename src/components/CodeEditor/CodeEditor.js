/* eslint-disable no-unused-vars */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CodeMirror from 'components/CodeMirror';
import cx from 'classnames';
import {humanize} from 'lib/utils';
import classNames from './CodeEditor.scss';
import {NO_LOCALIZE} from 'lib/constants';

class CodeEditor extends Component {
  static defaultProps = {
    value: '',
    options: {
      mode: 'text/html',
      lineNumbers: true,
      allowDropFileTypes: ['text/html', 'text/plain'],
      autoCloseTags: true,
      autoCloseBrackets: true,
      matchBrackets: true,
      matchTags: true
    }
  };

  static propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element,
      PropTypes.func,
      PropTypes.bool
    ]),
    error: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
    options: PropTypes.object,
    touched: PropTypes.bool,
    invalid: PropTypes.bool,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    onDrop: PropTypes.func,
    onFocus: PropTypes.func,
    onUpdate: PropTypes.func,
    value: PropTypes.string,
    className: PropTypes.string,
    editorClass: PropTypes.string
  };

  render() {
    const {
      touched,
      invalid,
      label,
      name,
      error,
      onBlur,
      onChange,
      onDrop,
      onFocus,
      onUpdate,
      value,
      className,
      editorClass,
      ...rest
    } = this.props;

    const errorText = Array.isArray(error) ? error[0] : error;
    return (
      <div className={cx('field', className, {error: touched && invalid})}>
        {label !== false && <label>{label || humanize(name)}</label>}
        <CodeMirror
          {...rest}
          onBeforeChange={(editor, data, value) => onChange && onChange(value)}
          onBlur={() => onBlur && onBlur()}
          value={value}
          className={cx(classNames.editor, editorClass, NO_LOCALIZE)}
        />
        {touched &&
          invalid && <div className="ui basic red pointing prompt label">{errorText}</div>}
      </div>
    );
  }
}

export default CodeEditor;
