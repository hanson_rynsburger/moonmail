import React from 'react';
import PropTypes from 'prop-types';
import Modal from './Modal';
import Button from 'components/Button';
import classNames from './Confirm.scss';

const Confirm = ({
  isOpen,
  children,
  isLoading,
  isDisabled,
  onCancel,
  onConfirm,
  confirmText,
  headerText,
  confirmClass,
  size = 'tiny',
  branding
}) => {
  return (
    <Modal isOpen={isOpen} size={size}>
      <div className="header">{headerText || 'Are you sure?'}</div>
      <div className="content">{children}</div>
      <div className="actions">
        <Button onClick={() => onCancel()}>Cancel</Button>
        <Button
          className={confirmClass}
          loading={isLoading}
          onClick={() => onConfirm()}
          disabled={isDisabled || isLoading}>
          {confirmText}
        </Button>
      </div>
      {branding && <div className={classNames.branding}>{branding}</div>}
    </Modal>
  );
};

Confirm.propTypes = {
  isOpen: PropTypes.bool,
  children: PropTypes.any,
  branding: PropTypes.any,
  isLoading: PropTypes.bool,
  isDisabled: PropTypes.bool,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  confirmText: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  confirmClass: PropTypes.string,
  headerText: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  size: PropTypes.oneOf([undefined, 'tiny', 'small', 'large', 'fullscreen'])
};

export default Confirm;
