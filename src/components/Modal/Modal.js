import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import cx from 'classnames';
import getScrollSize from 'scrollbar-size';

let hasActiveModal = false;

class Modal extends Component {
  static defaultProps = {
    style: 'standard'
  };

  static propTypes = {
    isOpen: PropTypes.bool,
    children: PropTypes.any,
    style: PropTypes.oneOf(['standard', 'basic']),
    size: PropTypes.oneOf(['tiny', 'small', 'large', 'fullscreen'])
  };

  constructor(props) {
    super(props);
    this.state = {
      isFrozen: false
    };
    this.container = document.createElement('div');
    document.body.appendChild(this.container);
  }

  componentDidMount() {
    this.scrollSize = getScrollSize() + 5;
    const body = $('body');
    const header = $('#main-header');
    this.fixBody = isFixed => {
      if (isFixed && body[0].scrollHeight > body.innerHeight()) {
        body.css({overflow: 'hidden', paddingRight: this.scrollSize});
        header.css({paddingRight: this.scrollSize});
      } else {
        body.removeAttr('style');
        header.removeAttr('style');
      }
    };
  }

  componentWillUnmount() {
    this.container.parentNode.removeChild(this.container);
    this.fixBody(false);
  }

  componentWillReceiveProps({isOpen}) {
    if (Boolean(isOpen) === Boolean(this.props.isOpen)) return;
    if (isOpen) {
      hasActiveModal = true;
      this.dimmer.transition('fade', '500ms');
      this.modal.transition('scale', '500ms');
      this.fixBody(true);
    } else {
      this.setState({isFrozen: true});
      hasActiveModal = false;
      setTimeout(() => {
        if (!hasActiveModal) this.fixBody(false);
      }, 250);
      this.dimmer.transition('fade', '500ms', () => {
        this.setState({isFrozen: false});
      });
      this.modal.transition('scale', '500ms');
    }
  }

  shouldComponentUpdate(nextProps, {isFrozen}) {
    return !isFrozen;
  }

  render() {
    const {isOpen, children, size, style} = this.props;
    return ReactDOM.createPortal(
      <div
        className="ui dimmer modals page transition active hidden"
        ref={n => {
          this.dimmer = $(n);
        }}>
        <div
          className={cx('ui modal transition active hidden', size, style)}
          ref={n => {
            this.modal = $(n);
          }}>
          {isOpen && children}
        </div>
      </div>,
      this.container
    );
  }
}

export default Modal;
