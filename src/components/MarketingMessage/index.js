import MarketingMessage from './MarketingMessage';
import {getLoginsCount} from 'modules/profile/selectors';
import {connect} from 'react-redux';

const mapStateToProps = state => ({
  isActive: getLoginsCount(state) <= 100
});

export default connect(mapStateToProps)(MarketingMessage);
