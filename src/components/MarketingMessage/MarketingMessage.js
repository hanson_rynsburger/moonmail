import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {getRandomInt} from 'lib/utils';
import * as api from 'lib/api';
import {Link} from 'components/Links';

class MarketingMessage extends Component {
  static propTypes = {
    isActive: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {
      isDismissed: false
    };
  }

  componentDidMount() {
    this.fetchMessages();
  }

  fetchMessages = async () => {
    const {items: messages} = await api.fetchMarketingMessages();
    let index = messages.findIndex(m => m.pinned);
    if (index > 0) {
      this.setState({message: messages[index]});
    } else if (this.props.isActive) {
      index = getRandomInt(0, messages.length);
      this.setState({message: messages[index]});
    }
  };

  onClose = () => {
    this.setState({isDismissed: true});
  };

  render() {
    const {message, isDismissed} = this.state;
    if (!message || isDismissed) return null;
    return (
      <div className="ui info message">
        <i className="close icon" onClick={this.onClose} />
        <div className="content">
          {message.content}{' '}
          {message.url.startsWith('/') ? (
            <Link to={message.url} onClick={this.onClose}>
              Click here!
            </Link>
          ) : (
            <a target="_blank" rel="noopener noreferrer" href={message.url}>
              Click here!
            </a>
          )}
        </div>
      </div>
    );
  }
}

export default MarketingMessage;
