import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Table = ({header, children, footer, className, selectable = false}) => (
  <table className={cx('ui table', className, {selectable})}>
    <thead>{header}</thead>
    <tbody>{children}</tbody>
    <tfoot>{footer}</tfoot>
  </table>
);

Table.propTypes = {
  header: PropTypes.object,
  children: PropTypes.node,
  footer: PropTypes.object,
  className: PropTypes.string,
  selectable: PropTypes.bool
};

export default Table;
