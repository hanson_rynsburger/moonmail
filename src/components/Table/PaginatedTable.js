import React from 'react';
import PropTypes from 'prop-types';
import ResourceTable from './ResourceTable';
import cx from 'classnames';
import classNames from './Table.scss';

const PaginatedTable = ({
  numColumns = 100,
  isFirstPage,
  isLastPage,
  onNextPage,
  onPrevPage,
  totalCount,
  isFetching,
  ...rest
}) => (
  <ResourceTable
    {...{isFetching, numColumns}}
    {...rest}
    footer={
      <tr>
        <th colSpan={numColumns}>
          <div className={classNames.footer}>
            <div className={classNames.totalCount}>
              {totalCount !== undefined && <span>Total: {totalCount}</span>}
            </div>
            <div className="ui pagination menu">
              <a
                className={cx('icon item', {disabled: isFirstPage || isFetching})}
                onClick={onPrevPage}>
                <i className="left chevron icon" />
              </a>
              <a
                className={cx('icon item', {disabled: isLastPage || isFetching})}
                onClick={onNextPage}>
                <i className="right chevron icon" />
              </a>
            </div>
          </div>
        </th>
      </tr>
    }
  />
);

PaginatedTable.propTypes = {
  numColumns: PropTypes.number,
  totalCount: PropTypes.number,
  isFirstPage: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  isLastPage: PropTypes.bool.isRequired,
  onNextPage: PropTypes.func.isRequired,
  onPrevPage: PropTypes.func.isRequired
};

export default PaginatedTable;
