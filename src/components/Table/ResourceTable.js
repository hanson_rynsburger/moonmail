import React from 'react';
import PropTypes from 'prop-types';
import Table from './Table';
import Loader from 'components/Loader';
import classNames from './Table.scss';
import cx from 'classnames';

const ResourceTable = ({
  numColumns = 100,
  children,
  isFetching,
  onRefresh,
  footer,
  resourceName = 'resources',
  notFoundText,
  ...rest
}) => (
  <div className={classNames.tableWrapper}>
    <Table {...rest} footer={footer}>
      {children.length > 0 ? (
        children
      ) : (
        <tr>
          <td
            colSpan={numColumns}
            className={classNames.spacer}
            onClick={() => onRefresh && onRefresh()}>
            {!isFetching && <h3>{notFoundText || `No ${resourceName} were found.`}</h3>}
          </td>
        </tr>
      )}
    </Table>
    <Loader
      active={isFetching}
      inline={false}
      dimmerClassName={cx(classNames.dimmer, {[classNames.noFooter]: !footer})}
    />
  </div>
);

ResourceTable.propTypes = {
  numColumns: PropTypes.number,
  isFetching: PropTypes.bool,
  onRefresh: PropTypes.func,
  children: PropTypes.array,
  footer: PropTypes.object,
  resourceName: PropTypes.string,
  notFoundText: PropTypes.string
};

export default ResourceTable;
