import Table from './Table';
import ResourceTable from './ResourceTable';
import PaginatedTable from './PaginatedTable';

export {Table, PaginatedTable, ResourceTable};
