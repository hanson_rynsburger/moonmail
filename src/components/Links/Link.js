import React from 'react';
import BaseLink from 'react-router/lib/Link';
import IndexLink from 'react-router/lib/IndexLink';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Link = ({
  className,
  disabled,
  activeClassName = 'active',
  index,
  baseUrl = '',
  to = '',
  ...props
}) => {
  const Component = index ? IndexLink : BaseLink;
  return (
    <Component
      className={cx(className, {disabled})}
      to={to.startsWith('/') ? to : baseUrl + to}
      activeClassName={activeClassName}
      {...props}
    />
  );
};

Link.propTypes = {
  className: PropTypes.string,
  activeClassName: PropTypes.string,
  baseUrl: PropTypes.string,
  to: PropTypes.string,
  disabled: PropTypes.bool,
  index: PropTypes.bool
};

export default Link;
