import React from 'react';
import _ItemLink from './ItemLink';
import _Link from './Link';

export const ItemLink = _ItemLink;
export const Link = _Link;

export const getRelativeItemLink = baseUrl => props => <ItemLink baseUrl={baseUrl} {...props} />;

export const getRelativeLink = baseUrl => props => <Link baseUrl={baseUrl} {...props} />;
