import React from 'react';
import Link from './Link';
import PropTypes from 'prop-types';
import cx from 'classnames';

const ItemLink = ({className, ...props}) => <Link className={cx('item', className)} {...props} />;

ItemLink.propTypes = {
  className: PropTypes.string
};

export default ItemLink;
