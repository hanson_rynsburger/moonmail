import _BillingForm from './BillingForm';
import {reduxForm} from 'redux-form';
import validateCardNumber from 'card-validator/src/card-number';
import validateCvc from 'card-validator/src/cvv';
import Validator from 'lib/validator';

const rules = {
  name: 'required',
  ccNumber: 'required',
  cvc: 'required',
  ccMonth: 'required',
  ccYear: 'required'
};

const validate = values => {
  const validator = new Validator(values, rules);
  validator.passes();
  const errors = validator.errors.all();
  const ccNumber = validateCardNumber(values.ccNumber);
  const cvcSize = ccNumber.card ? ccNumber.card.code.size : 3;
  if (!ccNumber.isValid) {
    errors.ccNumber = ['Please enter a valid card number'];
  }
  const cvc = validateCvc(values.cvc, cvcSize);
  if (!cvc.isValid) {
    errors.cvc = ['Please enter a valid code'];
  }
  return errors;
};

export const BillingForm = _BillingForm;

export const BillingFormContainer = Component =>
  reduxForm({
    form: 'billingForm',
    fields: ['name', 'ccNumber', 'ccMonth', 'ccYear', 'cvc'],
    validate
  })(Component);

export default BillingFormContainer(BillingForm);
