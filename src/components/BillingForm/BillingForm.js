import React from 'react';
import PropTypes from 'prop-types';
import {range, padDigits} from 'lib/utils';
import moment from 'moment/src/moment';
import Input from 'components/Input';
import NumberInput from 'components/CardNumberInput';
import Select, {SelectItem} from 'components/Select';
import Button from 'components/Button';
import cx from 'classnames';

const BillingForm = ({
  onFormSubmit,
  invalid,
  isLoading,
  buttonLabel,
  handleSubmit,
  resetForm,
  inline,
  fields: {name, ccNumber, ccMonth, ccYear, cvc}
}) => {
  const currentYear = new Date().getFullYear();
  const years = range(currentYear, currentYear + 20);
  const months = moment.monthsShort();
  const submit = formProps => {
    if (onFormSubmit) {
      onFormSubmit(formProps).then(() => resetForm());
    }
  };
  const formClass = cx('ui form ', {
    'attached fluid segment': !inline
  });
  return (
    <section>
      {!inline && (
        <div className="ui attached message">
          <div className="header">Enter you credit card information</div>
          <p>We will save it for further billing</p>
        </div>
      )}
      <form className={formClass} onSubmit={handleSubmit(submit)}>
        <Input label="Your Name" {...name} />
        <Input label="Card Number" component={NumberInput} {...ccNumber} />
        <div className="fields">
          <div className="eleven wide field">
            <label>Expiration</label>
            <div className="two fields">
              <Select fluid label={false} placeholder="month" {...ccMonth}>
                {months.map((month, i) => (
                  <SelectItem key={i} value={padDigits(i + 1, 2)}>
                    {i + 1} ({month})
                  </SelectItem>
                ))}
              </Select>
              <Select fluid label={false} placeholder="year" {...ccYear}>
                {years.map(year => (
                  <SelectItem key={year} value={year - 2000}>
                    {year}
                  </SelectItem>
                ))}
              </Select>
            </div>
          </div>
          <Input
            label="CVC"
            mask="9999"
            maskChar=""
            fieldClass="five wide input"
            autoComplete="off"
            type="password"
            {...cvc}
          />
        </div>
        {!inline && (
          <Button primary loading={isLoading} type="submit" disabled={invalid || isLoading}>
            {buttonLabel}
          </Button>
        )}
      </form>
      {!inline && (
        <div className="ui bottom attached message">
          <div className="header">Cards accepted</div>
          <div className="ui horizontal big list" style={{marginTop: 5}}>
            <div className="item">
              <div data-position="bottom center" data-tooltip="Visa">
                <i className="icon big visa teal" />
              </div>
            </div>
            <div className="item">
              <div data-position="bottom center" data-tooltip="Master Card">
                <i className="icon big mastercard red" />
              </div>
            </div>
            <div className="item">
              <div data-position="bottom center" data-tooltip="American Express">
                <i className="icon big american express teal" />
              </div>
            </div>
          </div>
        </div>
      )}
      {!inline && (
        <div className="text grey">
          powered by{' '}
          <a href="https://monei.net" target="_blank" rel="noopener noreferrer">
            MONEI
          </a>
        </div>
      )}
    </section>
  );
};

BillingForm.propTypes = {
  fields: PropTypes.shape({
    name: PropTypes.object,
    number: PropTypes.object,
    expMonth: PropTypes.object,
    expYear: PropTypes.object,
    cvc: PropTypes.object
  }).isRequired,
  buttonLabel: PropTypes.string,
  onFormSubmit: PropTypes.func,
  resetForm: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  invalid: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool,
  inline: PropTypes.bool
};

export default BillingForm;
