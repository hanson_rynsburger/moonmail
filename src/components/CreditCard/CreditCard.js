import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const CreditCard = ({brand, last4}) => {
  const iconClass = cx('icon big', {
    'payment grey': !brand,
    'visa teal': brand === 'Visa',
    'mastercard red': brand === 'MasterCard',
    'discover orange': brand === 'Discover',
    'american express teal': brand === 'American Express',
    'jcb teal': brand === 'JCB'
  });
  return (
    <div className="ui icon message">
      <i className={iconClass} />
      <div className="content">
        <div className="header">Current payment method</div>
        <p style={{letterSpacing: 2}}>•••• •••• •••• {last4}</p>
      </div>
    </div>
  );
};

CreditCard.propTypes = {
  brand: PropTypes.string.isRequired,
  last4: PropTypes.string.isRequired
};

export default CreditCard;
