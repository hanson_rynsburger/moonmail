import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import {humanize, omitProps} from 'lib/utils';
import DateTime from 'react-datetime';
import {DATE_FORMAT_SIMPLE} from 'lib/constants';
import './DateTimeInput.scss';

class DateTimeInput extends Component {
  static defaultProps = {
    dateFormat: DATE_FORMAT_SIMPLE,
    timeFormat: 'H:mm',
    closeOnSelect: false,
    showError: true
  };

  static propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.oneOfType([PropTypes.string, PropTypes.bool, PropTypes.element]),
    fieldClass: PropTypes.string,
    inputClass: PropTypes.string,
    error: PropTypes.oneOfType([PropTypes.array, PropTypes.string]),
    touched: PropTypes.bool,
    invalid: PropTypes.bool,
    showError: PropTypes.bool,
    fluid: PropTypes.bool,
    closeOnSelect: PropTypes.bool,
    dateFormat: PropTypes.string,
    timeFormat: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    mask: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func,
    hint: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  };

  onChange = _value => {
    const {closeOnSelect, onChange} = this.props;
    if (closeOnSelect) this.datetime.closeCalendar();
    let value = _value;
    if (typeof _value === 'object') {
      value = _value / 1000;
    }
    setTimeout(() => onChange(value));
  };

  formatValue = value => {
    if (typeof value === 'number') {
      return value * 1000;
    }
    return value;
  };

  render() {
    const {
      invalid,
      error,
      touched,
      showError,
      fieldClass,
      inputClass,
      fluid,
      label,
      hint,
      onBlur,
      value,
      ...rest
    } = this.props;
    const labelText = label || humanize(rest.name);
    const errorText = Array.isArray(error) ? error[0] : error;
    const props = omitProps(
      rest,
      'initialValue',
      'closeOnSelect',
      'autofill',
      'onUpdate',
      'valid',
      'dirty',
      'pristine',
      'active',
      'visited',
      'autofilled',
      'fluid',
      'onChange',
      'inputProps'
    );
    return (
      <div className={cx('field', fieldClass, {error: touched && invalid})}>
        {label !== false && <label>{labelText}</label>}
        <div className={cx('ui icon input', inputClass, {fluid})}>
          <DateTime
            ref={c => (this.datetime = c)}
            value={this.formatValue(value)}
            inputProps={{...props.inputProps}}
            onChange={this.onChange}
            onBlur={() => onBlur()}
            {...props}
          />
          <i className="calendar alternate outline icon" />
        </div>
        {hint && !(touched && invalid) && <div className="hint">{hint}</div>}
        {showError &&
          touched &&
          invalid && <div className="ui basic red pointing prompt label">{errorText}</div>}
      </div>
    );
  }
}

export default DateTimeInput;
