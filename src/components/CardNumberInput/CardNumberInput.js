import React from 'react';
import PropTypes from 'prop-types';
import InputElement from 'react-input-mask';
import validateCardNumber from 'card-validator/src/card-number';
import cx from 'classnames';

const NumberInput = props => {
  const number = validateCardNumber(props.value);
  const cardType = number.card && number.card.type;
  const iconClass = cx('icon big', {
    'payment grey': !cardType,
    'visa teal': cardType === 'visa',
    'mastercard red': cardType === 'master-card',
    'discover orange': cardType === 'discover',
    'american express teal': cardType === 'american-express',
    'jcb teal': cardType === 'jcb'
  });
  return (
    <div className="ui right labeled input">
      <input type="text" name={props.name} style={{display: 'none'}} />
      <InputElement type="text" mask="9999 9999 9999 9999" maskChar="" {...props} />
      <div className="ui basic label" style={{padding: '3px 8px'}}>
        <i className={iconClass} style={{margin: 0, fontSize: '2.2em'}} />
      </div>
    </div>
  );
};

NumberInput.propTypes = {
  value: PropTypes.string,
  name: PropTypes.string
};

export default NumberInput;
