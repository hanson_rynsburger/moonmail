import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Link from 'react-router/lib/Link';

const Button = ({
  disabled,
  active,
  loading,
  primary,
  fluid,
  positive,
  negative,
  basic,
  icon,
  className,
  children,
  onClick,
  type = 'button',
  to,
  style
}) => {
  if (to) {
    return (
      <Link
        to={to}
        style={style}
        className={cx('ui button', className, {
          loading,
          active,
          primary,
          positive,
          negative,
          basic,
          icon,
          fluid,
          disabled: disabled || loading
        })}>
        {children}
      </Link>
    );
  }
  return (
    <button
      style={style}
      className={cx('ui button', className, {
        loading,
        active,
        primary,
        positive,
        negative,
        basic,
        fluid,
        icon
      })}
      type={type}
      disabled={disabled || loading}
      onClick={onClick}>
      {children}
    </button>
  );
};

Button.propTypes = {
  style: PropTypes.object,
  loading: PropTypes.bool,
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  primary: PropTypes.bool,
  positive: PropTypes.bool,
  negative: PropTypes.bool,
  basic: PropTypes.bool,
  fluid: PropTypes.bool,
  icon: PropTypes.bool,
  className: PropTypes.string,
  type: PropTypes.string,
  children: PropTypes.any,
  onClick: PropTypes.func,
  to: PropTypes.string
};

export default Button;
