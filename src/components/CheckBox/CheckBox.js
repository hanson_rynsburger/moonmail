import React from 'react';
import PropTypes from 'prop-types';
import {humanize} from 'lib/utils';
import cx from 'classnames';

const CheckBox = ({
  className,
  onChange,
  checked = false,
  name = '',
  value = false,
  label,
  disabled,
  fitted = false,
  toggle = false
}) => {
  const labelText = label || humanize(name);
  const _value = value === '' ? false : value;
  return (
    <div className={cx('ui checkbox', className, {fitted, toggle, disabled})}>
      <input
        type="checkbox"
        checked={checked}
        onChange={onChange}
        disabled={disabled}
        name={name}
        value={_value}
        id={name}
      />
      <label htmlFor={name}>{labelText}</label>
    </div>
  );
};

CheckBox.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  checked: PropTypes.bool,
  name: PropTypes.string,
  value: PropTypes.bool,
  fitted: PropTypes.bool,
  toggle: PropTypes.bool,
  disabled: PropTypes.bool,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element, PropTypes.func, PropTypes.bool])
};

CheckBox.defaultProps = {
  value: false
};

export default CheckBox;
