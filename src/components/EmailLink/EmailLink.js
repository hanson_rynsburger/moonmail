import React from 'react';
import PropTypes from 'prop-types';

const EmailLink = ({to, ...rest}) => (
  <a href={`mailto:${to}`} target="_top" {...rest}>
    {to}
  </a>
);

EmailLink.propTypes = {
  to: PropTypes.string.isRequired
};

export default EmailLink;
