import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router/lib/Link';

const SenderHint = ({
  resource,
  isFetchingSenders,
  sendersCount,
  hasSenders,
  isUnverifiedSesUser
}) => {
  if (isFetchingSenders) return null;
  const className = 'ui info small message';
  switch (true) {
    case !hasSenders:
      return (
        <div className={className}>
          To select a custom sender please <Link to="/profile/plan">upgrade your account</Link>
        </div>
      );
    case isUnverifiedSesUser:
      return (
        <div className={className}>
          To create {resource} you need a sender. To create it please{' '}
          <Link to="getting-started-aws-ses">add your AWS credentials</Link>
        </div>
      );
    case sendersCount === 0:
      return (
        <div className={className}>
          To create {resource} you need a sender.{' '}
          <Link to="/profile/senders/new">Create sender</Link>
        </div>
      );
    default:
      return null;
  }
};

SenderHint.propTypes = {
  hasSenders: PropTypes.bool.isRequired,
  isUnverifiedSesUser: PropTypes.bool.isRequired,
  resource: PropTypes.string.isRequired,
  sendersCount: PropTypes.number.isRequired,
  isFetchingSenders: PropTypes.bool.isRequired
};

export default SenderHint;
