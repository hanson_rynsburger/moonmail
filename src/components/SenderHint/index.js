import {connect} from 'react-redux';
import sendersSelectors from 'modules/senders/selectors';
import * as profileSelectors from 'modules/profile/selectors';
import SenderHint from './SenderHint';

const mapStateToProps = state => ({
  isFetchingSenders: sendersSelectors.getIsFetchingAll(state),
  sendersCount: sendersSelectors.getIds(state).length,
  hasSenders: profileSelectors.getHasSenders(state),
  isFreeMessUser: profileSelectors.getIsFreeMessUser(state),
  isUnverifiedSesUser: profileSelectors.getIsUnverifiedSesUser(state)
});

export default connect(mapStateToProps)(SenderHint);
