import React from 'react';
import NL from 'components/NoLocalize';
import Link from 'react-router/lib/Link';
import {formatStat} from 'lib/utils';
import {contactSupport} from 'lib/remoteUtils';
import StaticMessage from 'components/StaticMessage';

const UploadLimitHint = ({uploadLimit, isEnterpriseUser, isFreeUser}) => {
  if (uploadLimit === Infinity) return null;
  const getLink = () => {
    if (isEnterpriseUser) {
      return (
        <a
          href="#"
          onClick={e => contactSupport(e, 'Hello. I would like to increase recipients limit.')}>
          contact us
        </a>
      );
    }
    return <Link to="/profile/plan">upgrade your account</Link>;
  };
  if (uploadLimit > 0) {
    return (
      <StaticMessage type="info" icon={false}>
        You can upload up to <NL>{formatStat(uploadLimit)}</NL> subscribers. If you want to upload
        more {isFreeUser && 'and select custom fields'} please {getLink()}.
      </StaticMessage>
    );
  }
  return (
    <StaticMessage type="error" icon={false}>
      You can not upload any more subscribers. Please {getLink()}.
    </StaticMessage>
  );
};

export default UploadLimitHint;
