import {connect} from 'react-redux';
import listSelectors from 'modules/lists/selectors';
import {getIsEnterpriseUser} from 'modules/profile/selectors';
import UploadLimitHint from './UploadLimitHint';

const mapStateToProps = state => ({
  uploadLimit: listSelectors.getRemainingRecipients(state),
  isEnterpriseUser: getIsEnterpriseUser(state)
});

export default connect(mapStateToProps)(UploadLimitHint);
