import React, {Component} from 'react';
import PropTypes from 'prop-types';
import IndexLink from 'react-router/lib/IndexLink';
import {ItemLink as Link} from 'components/Links';
import DropDownMenu from 'components/DropDownMenu';
import Button from 'components/Button';
import NoLocalize from 'components/NoLocalize';
import cx from 'classnames';
import classNames from './Header.scss';
import {Desktop, Mobile} from 'components/MediaQueries';
import {Aux} from 'lib/utils';
import shopify from 'static/shopify.svg';

const userPic = 'https://static.moonmail.io/user.png';

class Header extends Component {
  static propTypes = {
    profile: PropTypes.object.isRequired,
    signIn: PropTypes.func.isRequired,
    signOut: PropTypes.func.isRequired,
    impersonateUser: PropTypes.func.isRequired,
    isDemoUser: PropTypes.bool.isRequired,
    isFreeUser: PropTypes.bool.isRequired,
    isShopifyUser: PropTypes.bool.isRequired,
    isFetching: PropTypes.bool.isRequired,
    impersonations: PropTypes.array.isRequired
  };

  getStarted = () => {
    this.props.signIn({hint: 'To start using MoonMail please log in.', source: 'demo'});
  };

  renderProfileMenu = (isNameVisible = true) => {
    const {profile, signOut, impersonateUser, impersonations} = this.props;
    const hasImpersonations = impersonations.length > 0;
    return (
      <DropDownMenu className="right item">
        <img
          className="ui avatar image"
          src={profile.picture}
          onError={e => (e.target.src = userPic)}
        />
        {isNameVisible && <NoLocalize>{profile.name}</NoLocalize>}
        <i className="dropdown icon" />
        <div className="left menu transition hidden">
          {hasImpersonations &&
            impersonations.map((user, i) => (
              <a className="item truncate" key={i} onClick={() => impersonateUser(user.userId)}>
                <img
                  className="ui avatar image"
                  src={user.picture || userPic}
                  onError={e => (e.target.src = userPic)}
                />
                <NoLocalize>{user.name}</NoLocalize>
              </a>
            ))}
          {hasImpersonations && <div className="divider" />}
          <Link to="/profile">
            <i className="setting icon" />
            Profile
          </Link>
          <Link to="/integrations">
            <i className="plug icon" />
            Integrations
          </Link>
          <Link to="/extensions">
            <i className="cubes icon" />
            Extensions
          </Link>
          <a className="item" onClick={() => signOut(true)}>
            <i className="sign out icon" />
            Sign Out
          </a>
        </div>
      </DropDownMenu>
    );
  };

  renderMainLinks = () => {
    return (
      <Aux>
        <Link to="/campaigns">Campaigns</Link>
        <Link to="/automations">Automations</Link>
        <Link to="/lists">Lists</Link>
        <Link to="/templates">Templates</Link>
      </Aux>
    );
  };

  renderCTA = () => {
    const {isDemoUser, isFreeUser, isFetching} = this.props;
    if (isFetching) return null;
    if (isDemoUser) {
      return (
        <div className="item borderless">
          <Button className="green" onClick={this.getStarted}>
            Get started for free
          </Button>
        </div>
      );
    }
    if (isFreeUser) {
      return (
        <div className="item borderless">
          <Button className="green" to="/profile/plan">
            Upgrade your account
          </Button>
        </div>
      );
    }
  };

  render() {
    return (
      <header id="main-header" className="ui inverted black fixed menu large">
        <div className="ui container">
          <IndexLink to="/" className={cx('ui medium header item', classNames.logo)}>
            <Desktop>
              <img
                src="https://static.moonmail.io/moonmail-logo-white.svg"
                alt="MoonMail"
                className={classNames.mm}
              />
              {this.props.isShopifyUser && (
                <img
                  src={shopify}
                  alt="Shopify"
                  width={30}
                  height={34}
                  className={classNames.shopify}
                  title="Your account is connected to shopify store"
                />
              )}
            </Desktop>
            <Mobile>
              <img
                src="https://static.moonmail.io/moon.svg"
                alt="MoonMail"
                className={classNames.moon}
              />
            </Mobile>
          </IndexLink>
          <Desktop>
            {this.renderMainLinks()}
            {this.renderCTA()}
            {this.renderProfileMenu()}
          </Desktop>
          <Mobile>
            <div className="right menu">
              <DropDownMenu className="item borderless">
                Menu
                <i className="dropdown icon" />
                <div className="menu">
                  {this.renderCTA()}
                  {this.renderMainLinks()}
                </div>
              </DropDownMenu>
              {this.renderProfileMenu(false)}
            </div>
          </Mobile>
        </div>
      </header>
    );
  }
}

export default Header;
