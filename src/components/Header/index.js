import {connect} from 'react-redux';
import Header from './Header';
import * as actions from 'modules/app/actions';
import * as selectors from 'modules/profile/selectors';

const mapStateToProps = state => ({
  profile: selectors.getProfile(state),
  isFetching: selectors.getIsFetching(state),
  isDemoUser: selectors.getIsDemoUser(state),
  isFreeUser: selectors.getIsFreeUser(state),
  impersonations: selectors.getImpersonations(state),
  isShopifyUser: selectors.getIsShopifyUser(state)
});

export default connect(mapStateToProps, actions)(Header);
