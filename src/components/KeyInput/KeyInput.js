import React, {Component} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class KeyInput extends Component {
  static propTypes = {
    value: PropTypes.string,
    onCopy: PropTypes.func.isRequired,
    secret: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state = {
      isSecret: props.secret
    };
  }

  render() {
    const {onCopy, secret, ...rest} = this.props;
    const {isSecret} = this.state;
    return (
      <div className="ui action input">
        <input
          type={isSecret ? 'password' : 'text'}
          readOnly
          id="token"
          {...rest}
          onClick={e => e.target.select()}
        />
        {secret && (
          <button
            onClick={() => this.setState({isSecret: !isSecret})}
            data-tooltip="Reveal value"
            data-position="bottom center"
            data-inverted=""
            className="ui right basic icon button">
            <i className={cx({hide: !isSecret, eye: isSecret}, 'icon')} />
          </button>
        )}
        <button
          onClick={e => {
            e.preventDefault();
            onCopy();
          }}
          className="ui right basic icon button"
          data-tooltip="Copy to clipboard"
          data-position="bottom center"
          data-inverted="">
          <i className="copy outline icon" />
        </button>
      </div>
    );
  }
}

export default KeyInput;
