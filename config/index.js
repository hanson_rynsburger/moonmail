/* eslint key-spacing:0 spaced-comment:0 */
import path from 'path';
import {argv} from 'yargs';
import ip from 'ip';
import dotenv from 'dotenv';

const pkg = require('../package.json');
const localip = ip.address();

if (!process.env.BITBUCKET_REPO_SLUG) {
  dotenv.config();
}

// ========================================================
// Default Configuration
// ========================================================

const config = {
  env: process.env.NODE_ENV || 'development',
  stage: process.env.STAGE || 'development',

  // ----------------------------------
  // Project Structure
  // ----------------------------------
  path_base: path.resolve(__dirname, '..'),
  dir_client: 'src',
  dir_dist: 'dist',
  dir_server: 'server',
  dir_test: 'tests',

  // ----------------------------------
  // Server Configuration
  // ----------------------------------
  server_host: 'localhost', // use string 'localhost' to prevent exposure on local network
  server_port: process.env.PORT || 3000,

  // ----------------------------------
  // Compiler Configuration
  // ----------------------------------
  compiler_devtool: 'cheap-module-eval-source-map',
  compiler_devtool_filename_tempalte: '/[resource-path]',
  compiler_hash_type: 'hash',
  compiler_fail_on_warning: false,
  compiler_public_path: '/',

  // ----------------------------------
  // Test Configuration
  // ----------------------------------
  coverage_reporters: [{type: 'text-summary'}, {type: 'lcov', dir: 'coverage'}],
  coverage_exclude: []
};

// ------------------------------------
// Environment
// ------------------------------------
// N.B.: globals added here must _also_ be added to .eslintrc

config.globals = {
  'process.env': {
    NODE_ENV: config.env,
    BROWSER: true
  },
  NODE_ENV: config.env,
  __STAGE__: config.stage,
  __DEV__: config.env === 'development',
  __PROD__: config.env === 'production',
  __TEST__: config.env === 'test',
  __DEBUG__: config.env === 'development' && !argv.no_debug,
  __COVERAGE__: !argv.watch && config.env === 'test',
  __BASENAME__: process.env.BASENAME || '',
  __VERSION__: pkg.version,
  APP_CONFIG: {
    auth0: {
      clientID: 'yxaHRVmAlZvPS9mwSbwzmy9Y24h8mKcV',
      clientDomain: 'moonmail.auth0.com',
      apiBaseURL: 'https://moonmail.auth0.com/api/v2/'
    },
    miniJsonDbUrl: 'https://json.moonmail.io/',
    sessionStackKeys: ['ee9ffaade62f4e59821560e9eac9d615', '3fbc57b3957a4ff8b69ac3ccf35af149'],
    zapierClientID: 'pvgoPT0JCqTkB1RgFrHGDvCPxcvLVqXhQBvQpr4E'
  }
};

// ------------------------------------
// Utilities
// ------------------------------------
const resolve = path.resolve;
const base = (...args) => Reflect.apply(resolve, null, [config.path_base, ...args]);

config.utils_paths = {
  base,
  client: base.bind(null, config.dir_client),
  dist: base.bind(null, config.dir_dist)
};

// ========================================================
// Environment Configuration
// ========================================================
const environments = require('./environments').default;
const envOverrides = environments[config.env];
if (envOverrides) {
  Object.assign(config, envOverrides(config));
}

// ========================================================
// Stages Configuration
// ========================================================
const stages = require('./stages').default;
const stageOverrides = stages[config.stage];
if (stageOverrides) {
  Object.assign(config, stageOverrides(config));
}

console.log();
export default config;
