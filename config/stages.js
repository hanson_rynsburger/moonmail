// Here is where you can define configuration overrides based on the execution stage.
// Supply a key to the default export matching the STAGE that you wish to target, and
// the base configuration will apply your overrides before exporting itself.
export default {
  // ======================================================
  // Overrides when STAGE === 'development'
  // ======================================================
  development: config => {
    const region = 'us-east-1';
    Object.assign(config.globals.APP_CONFIG, {
      apiBaseURL: 'https://mkre901l1e.execute-api.us-east-1.amazonaws.com/dev',
      csvImportBucket: 'next-recipients.moonmail.dev.us-east-1',
      expertLogosBucket: 'expert-logos-dev',
      userBucket: 'attachments.moonmail.dev.us-east-1',
      templateImagesBucket: 'screenshots.moonmail.dev.us-east-1',
      staticCdnURL: 'https://s3.amazonaws.com/static.moonmail.dev.us-east-1',
      intercomID: 'd84d8u48',
      stripeKey: 'pk_test_qIcVcuBBKwL8D5dWRQnhRnRN',
      iotEndpoint: 'a1qrexblodk5ps.iot.us-east-1.amazonaws.com',
      region
    });

    config.S3 = {
      region,
      accessKeyId: process.env.DEV_AWS_KEY,
      secretAccessKey: process.env.DEV_AWS_SECRET,
      params: {
        Bucket: process.env.DEV_S3_BUCKET_NAME
      }
    };

    return config;
  },

  // ======================================================
  // Overrides when STAGE === 'release'
  // ======================================================
  release: config => {
    const region = 'eu-west-1';
    Object.assign(config.globals.APP_CONFIG, {
      apiBaseURL: 'https://api.moonmail.io',
      csvImportBucket: 'next-recipients.moonmail.prod.eu-west-1',
      expertLogosBucket: 'expert-logos',
      userBucket: 'attachments.moonmail.prod.eu-west-1',
      templateImagesBucket: 'screenshots.moonmail.prod.eu-west-1',
      staticCdnURL: 'https://static.moonmail.io',
      intercomID: 'za787poa',
      stripeKey: 'pk_live_5Ue2kF4wy8PpyLEyUj2Q5z6k',
      iotEndpoint: 'a1bvr492uo85h.iot.eu-west-1.amazonaws.com',
      region
    });

    config.S3 = {
      region,
      accessKeyId: process.env.AWS_KEY,
      secretAccessKey: process.env.AWS_SECRET,
      params: {
        Bucket: process.env.S3_BUCKET_NAME
      }
    };
    config.cloudfront = {
      credentials: {
        accessKeyId: process.env.AWS_KEY,
        secretAccessKey: process.env.AWS_SECRET
      },
      region,
      distributionId: process.env.CF_DISTRIBUTION_ID
    };

    return config;
  }
};
