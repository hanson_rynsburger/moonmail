import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import ExtendedDefinePlugin from 'extended-define-webpack-plugin';
import {BundleAnalyzerPlugin} from 'webpack-bundle-analyzer';
import SWPrecacheWebpackPlugin from 'sw-precache-webpack-plugin';
import ManifestPlugin from 'webpack-manifest-plugin';
import InlineManifestWebpackPlugin from 'inline-manifest-webpack-plugin';
import config from '../config';
const paths = config.utils_paths;
const {__DEV__, __PROD__} = config.globals;

const webpackConfig = {
  target: 'web',
  devtool: config.compiler_devtool,
  resolve: {
    modules: [paths.client(), 'node_modules', 'semantic'],
    extensions: ['.js', '.jsx', '.json']
  },
  module: {
    strictExportPresence: true
  },
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty'
  }
};

// ------------------------------------
// Bundle Output
// ------------------------------------

const fileName = `[name].[${config.compiler_hash_type}].js`;

webpackConfig.output = {
  filename: fileName,
  chunkFilename: fileName,
  path: paths.dist(),
  publicPath: config.compiler_public_path,
  devtoolModuleFilenameTemplate: config.compiler_devtool_filename_tempalte
};

// ------------------------------------
// Plugins
// ------------------------------------

webpackConfig.plugins = [
  new ExtendedDefinePlugin(config.globals),
  new webpack.LoaderOptionsPlugin({
    options: {
      sassLoader: {
        includePaths: paths.client('styles')
      },
      postcss: [
        require('postcss-cssnext')({
          browsers: ['last 2 versions', '> 5%']
        }),
        require('postcss-reporter')()
      ]
    }
  }),
  new webpack.ProvidePlugin({
    $: 'jquery-slim',
    jQuery: 'jquery-slim'
  }),
  // moment.js locales fix see https://github.com/moment/moment/issues/2979
  new webpack.ContextReplacementPlugin(/\.\/locale$/, 'empty-module', false, /js$/)
];

if (__DEV__) {
  // Turn off performance hints during development because we don't do any
  // splitting or minification in interest of speed. These warnings become
  // cumbersome.
  webpackConfig.performance = {
    hints: false
  };

  webpackConfig.entry = {
    app: [
      `webpack-hot-middleware/client?path=${config.compiler_public_path}__webpack_hmr&reload=true`,
      'babel-polyfill',
      paths.client('main.js')
    ]
  };

  webpackConfig.plugins.push(
    // Add module names to factory functions so they appear in browser profiler.
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: paths.client('index.ejs'),
      hash: false,
      favicon: paths.client('static/favicon.png'),
      filename: 'index.html',
      inject: 'body'
    })
  );
} else if (__PROD__) {
  // Don't attempt to continue if there are any errors.
  webpackConfig.bail = true;

  webpackConfig.entry = {
    app: ['babel-polyfill', paths.client('main.js')]
  };

  webpackConfig.plugins.push(
    new HtmlWebpackPlugin({
      template: paths.client('index.ejs'),
      hash: false,
      favicon: paths.client('static/favicon.png'),
      filename: 'index.html',
      inject: 'body',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        // Disabled because of an issue with Uglify breaking seemingly valid code:
        // https://github.com/facebookincubator/create-react-app/issues/2376
        // Pending further investigation:
        // https://github.com/mishoo/UglifyJS2/issues/2011
        comparisons: false
      },
      mangle: {
        safari10: true
      },
      output: {
        comments: false,
        // Turned on because emoji and regex is not minified properly using default
        // https://github.com/facebookincubator/create-react-app/issues/2488
        ascii_only: true
      },
      sourceMap: true
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: m => /semantic|node_modules/.test(m.context)
    }),
    new webpack.optimize.CommonsChunkPlugin({name: 'manifest'}),
    // Generate a manifest file which contains a mapping of all asset filenames
    // to their corresponding output file so that tools can pick it up without
    // having to parse `index.html`.
    new ManifestPlugin({
      fileName: 'asset-manifest.json'
    }),
    new InlineManifestWebpackPlugin({name: 'webpackManifest'}),
    // Generate a service worker script that will precache, and keep up to date,
    // the HTML & assets that are part of the Webpack build.
    new SWPrecacheWebpackPlugin({
      // By default, a cache-busting query parameter is appended to requests
      // used to populate the caches, to ensure the responses are fresh.
      // If a URL is already hashed by Webpack, then there is no concern
      // about it being stale, and the cache-busting can be skipped.
      dontCacheBustUrlsMatching: /\.\w{8}\./,
      filename: 'service-worker.js',
      minify: true,
      // For unknown URLs, fallback to the index page
      navigateFallback: '/',
      // Ignores URLs starting from /__ (useful for Firebase):
      // https://github.com/facebookincubator/create-react-app/issues/2237#issuecomment-302693219
      navigateFallbackWhitelist: [/^(?!\/__).*/],
      // Don't precache sourcemaps (they're large) and build asset manifest:
      staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/]
    })
  );
  // analyze build only on local machine
  if (!process.env.BITBUCKET_REPO_SLUG) {
    webpackConfig.plugins.push(
      new BundleAnalyzerPlugin({
        analyzerMode: 'static'
      })
    );
  }
}

webpackConfig.plugins.push(
  new ExtractTextPlugin({
    filename: `[name].[${config.compiler_hash_type}].css`,
    allChunks: true,
    disable: __DEV__
  })
);

// ------------------------------------
// Rules
// ------------------------------------

// JavaScript / JSON
webpackConfig.module.rules = [
  {
    test: /\.(js|jsx)$/,
    include: paths.client(),
    loader: 'babel-loader',
    options: {
      cacheDirectory: true,
      plugins: [
        [
          'transform-runtime',
          {
            polyfill: false
          }
        ],
        'transform-class-properties',
        'transform-object-rest-spread',
        'syntax-dynamic-import'
      ],
      presets: [
        [
          'env',
          {
            targets: {
              browsers: ['last 2 versions']
            },
            modules: false,
            loose: true
          }
        ],
        'react'
      ],
      env: {
        development: {
          plugins: ['transform-react-jsx-source']
        },
        production: {
          presets: ['react-optimize'],
          compact: true
        },
        test: {
          plugins: ['rewire']
        }
      }
    }
  }
];

// ------------------------------------
// Style Rules
// ------------------------------------

// Add any packge names here whose styles need to be treated as CSS modules.
// These paths will be combined into a single regex.
const PATHS_TO_TREAT_AS_CSS_MODULES = [
  paths.client().replace(/[\^\$\.\*\+\-\?\=\!\:\|\\\/\(\)\[\]\{\}\,]/g, '\\$&') // eslint-disable-line
];

const isUsingCSSModules = !!PATHS_TO_TREAT_AS_CSS_MODULES.length;
const cssModulesRegex = new RegExp(`(${PATHS_TO_TREAT_AS_CSS_MODULES.join('|')})`);

// We use postcss loader, so we tell
// css-loader not to duplicate minimization.
const cssModulesLoader = {
  loader: 'css-loader',
  options: {
    modules: isUsingCSSModules,
    importLoaders: 1,
    localIdentName: '[name]__[local]___[hash:base64:5]'
  }
};

webpackConfig.module.rules.push(
  {
    test: /\.css$/,
    include: cssModulesRegex,
    exclude: /semantic/,
    loader: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [cssModulesLoader, 'postcss-loader']
    })
  },
  {
    test: /\.scss$/,
    include: cssModulesRegex,
    loader: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [cssModulesLoader, 'postcss-loader', 'sass-loader']
    })
  },
  {
    test: /\.css$/,
    exclude: cssModulesRegex,
    loader: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: ['css-loader', 'postcss-loader']
    })
  }
);

// File loaders
webpackConfig.module.rules.push(
  {
    test: /\.svg(\?.*)?$/,
    loader: 'url-loader',
    options: {
      limit: 65000,
      mimetype: 'image/svg+xml'
    }
  },
  {
    test: /\.(png|jpg)$/,
    loader: 'url-loader',
    options: {
      limit: 65000
    }
  },
  {
    test: /\.woff(\?.*)?$/,
    loader: 'url-loader',
    options: {
      limit: 65000,
      mimetype: 'application/font-woff'
    }
  },
  {
    test: /\.woff2(\?.*)?$/,
    loader: 'url-loader',
    options: {
      limit: 65000,
      mimetype: 'application/font-woff2'
    }
  },
  {
    test: /\.otf(\?.*)?$/,
    loader: 'url-loader',
    options: {
      limit: 65000,
      mimetype: 'font/opentype'
    }
  },
  {
    test: /\.ttf(\?.*)?$/,
    loader: 'url-loader',
    options: {
      limit: 65000,
      mimetype: 'application/application/octet-stream'
    }
  },
  {
    test: /\.eot(\?.*)?$/,
    loader: 'url-loader',
    options: {
      limit: 65000,
      mimetype: 'application/vnd.ms-fontobject'
    }
  }
);

export default webpackConfig;
